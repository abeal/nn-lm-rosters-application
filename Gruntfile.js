/**
 * Generic form of Gruntfile - moves task and configuration
 * definition to the grunt_tasks and grunt_tasks/options
 * folders, respectively for organization.
 */
module.exports = function(grunt) {
	"use strict";

	var path = require("path");
	var timestamp = require("monotonic-timestamp");
	var pkg = grunt.file.readJSON("package.json");
	//@see http://firstandthird.github.io/load-grunt-config/#custom-config
	var config = {
		debugging: true,
		projectDescription: pkg.description,
		versionNumber: pkg.version,
		versionTimestamp: timestamp()
	};
	config.javascriptFilename = "script-" + config.version_number + ".js";
	require("load-grunt-config")(grunt, {
		configPath: path.join(process.cwd(), "tasks"), //path to task.js files, defaults to grunt dir
		init: true, //auto grunt.initConfig
		data: config, //data passed into config.  Can use with <%= test %>
		loadGruntTasks: { //can optionally pass options to load-grunt-tasks.  If you set to false, it will disable auto loading tasks.
			pattern: "grunt-*",
			config: require("./package.json"),
			scope: "devDependencies"
		}
	});
};
