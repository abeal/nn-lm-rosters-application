<?php
require_once('env.php');
require_once('MySQLi.php.inc');     
require_once('scripts/debug.class.php');
require_once("Exceptions.php");
require_once('scripts/rosters_stored_procedures.class.php');   
//$_ENV['debugging'] = 3;    
function print_aron($msg){
	if($_SERVER['PHP_AUTH_USER'] == 'abeal'){
		print $msg;
	}
}

/** 
* @class DataTypes
* storage class for data types.  Overkill?  Maybe, but it made code look cleaner.
*/                                       
class DataTypes {    
  const isUnknown = -1;
	const isInteger = 0;
	const isString = 1;
}   


/** 
* class TemplateQueries
* generates queries for populating template definitions, using passed $options parameters.
*/
class TemplateQueries{      
	const QUERY_PEOPLE = 1;
	const QUERY_LIST = 2;
	const rosters_account_types = 
		"SELECT
	        p.PeopleID,
			at.AccountTypeID,
	        at.AccountTypeDescription,
	        at.AccountImageIcon
	    FROM rosters.account_types at
	    INNER JOIN rosters.accounts a
	    ON (at.AccountTypeID = a.AccountTypeID)
		WHERE {IDCLAUSE} 
		AND a.AccountVisibility {VISIBILITYCLAUSE}";
	const rosters_accounts = 
		"SELECT
	        p.PeopleID,
			a.AccountID,
	        at.AccountTypeDescription,
	        at.AccountImageIcon,    
			a.AccountVisibility,
	        a.AccountValue,
			a.AccountComments
	    FROM rosters.accounts a
	    INNER JOIN rosters.account_types at
	    ON (at.AccountTypeID = a.AccountTypeID)
		INNER JOIN rosters.people p
		ON (a.PeopleID = p.PeopleID)   
		WHERE {IDCLAUSE}
		AND a.AccountVisibility {VISIBILITYCLAUSE}
		ORDER BY a.AccountVisibility, at.AccountTypeDescription"; 
	const rosters_list = 
		'SELECT ld.* FROM rosters.list_descriptions ld 
		WHERE {IDCLAUSE}'; 
	const rosters_list_match = '';//account for visibility due to dates                                                              
	const rosters_people = 'SELECT p.*  
		FROM rosters.people p 
		WHERE {IDCLAUSE}';                                           
	const rosters_people_primary =
	 	"SELECT                                   
		 	p.*,
			po.PositionID,
			po.Title,
			po.PositionRanking,       
			po.Region, 
			ca.*
		FROM rosters.people p 
		LEFT JOIN rosters.positions po 
			ON (p.PrimaryPosition = po.PositionID)   
		LEFT JOIN rosters.currentAddress ca
			ON (ca.PositionID = p.PrimaryPosition)
		WHERE {IDCLAUSE}    
	    AND po.StartDate <= CURDATE()       
	 	AND po.PositionVisibility {VISIBILITYCLAUSE}
	 	AND (po.EndDate IS NULL OR po.EndDate > CURDATE())";     
	const rosters_query_roles = 
			"SELECT DISTINCT
				p.PeopleID					
			FROM rosters.roles r
			INNER JOIN rosters.positions po ON (r.PositionID = po.PositionID)
			INNER JOIN rosters.people p ON (p.PrimaryPosition = po.PositionID)
			WHERE r.RoleType = '{IDCLAUSE}'   
			AND po.StartDate <= CURDATE()       
			AND po.PositionVisibility {VISIBILITYCLAUSE}
			AND (po.EndDate IS NULL OR po.EndDate > CURDATE())";
	const rosters_terse = self::rosters_people;     	
	const rosters_short = self::rosters_people;
	const rosters_positions = 
		"(SELECT 
			'rosters' as RecordSource,
			p.PeopleID, 
			po.PositionID, 
			po.PositionVisibility, 
			po.PositionRanking, 
			po.PeopleID, 
			po.Title, 
			po.Email, 
			po.StartDate, 
			po.EndDate, 
			po.LIBID, 
			po.Region, 
			GROUP_CONCAT(r.RoleType SEPARATOR ', ') as Roles, 
			po.INST_INST, 
			po.DEPT_INST, 
			po.STREET_INST, 
			po.STREET2_INST, 
			po.CITY_INST, 
			po.STATE_ETC_CODE_INST, 
			po.ZIP_MAIL_CODE_INST, 
			IF(p.PrimaryPosition = po.PositionID, '(Primary Position)', '') as PrimaryPosition, 
			IF(p.PrimaryPosition = po.PositionID, 'primary_position', 'position') as PositionClass 
		FROM rosters.positions po 
		INNER JOIN rosters.people p ON (po.PeopleID = p.PeopleID) 
		LEFT JOIN rosters.roles r ON (r.PositionID = po.PositionID AND r.PositionID IS NOT NULL) 
		WHERE {IDCLAUSE}   
		AND po.INST_INST IS NOT NULL
		AND po.PositionVisibility {VISIBILITYCLAUSE} 
		AND po.StartDate <= CURDATE() 
		AND (po.EndDate IS NULL OR po.EndDate > CURDATE()) GROUP BY po.PositionID)
		UNION
		(SELECT 
			'docline' as RecordSource,
			p.PeopleID, 
			po.PositionID, 
			po.PositionVisibility, 
			po.PositionRanking, 
			po.PeopleID, 
			po.Title, 
			po.Email, 
			po.StartDate, 
			po.EndDate, 
			po.LIBID, 
			po.Region, 
			GROUP_CONCAT(r.RoleType SEPARATOR ', ') as Roles, 
			d.INST_INST, 
			d.DEPT_INST, 
			d.STREET_INST, 
			d.STREET2_INST, 
			d.CITY_INST, 
			d.STATE_ETC_CODE_INST, 
			d.ZIP_MAIL_CODE_INST, 
			IF(p.PrimaryPosition = po.PositionID, '(Primary Position)', '') as PrimaryPosition, 
			IF(p.PrimaryPosition = po.PositionID, 'primary_position', 'position') as PositionClass 
		FROM rosters.positions po 
		INNER JOIN rosters.people p ON (po.PeopleID = p.PeopleID) 
		LEFT JOIN rosters.roles r ON (r.PositionID = po.PositionID AND r.PositionID IS NOT NULL) 
		LEFT JOIN docline.PUBLIC_LIBRARY_DOCUSERS d ON (d.LIBID = po.LIBID) 
		WHERE {IDCLAUSE} 
		AND po.INST_INST IS NULL
		AND po.PositionVisibility {VISIBILITYCLAUSE} 
		AND po.StartDate <= CURDATE() 
		AND (po.EndDate IS NULL OR po.EndDate > CURDATE()) GROUP BY po.PositionID )
		ORDER BY PrimaryPosition DESC"; 
	const rosters_positions_primary = 
			"(SELECT 
				'rosters' as RecordSource,
				p.PeopleID, 
				po.PositionID, 
				po.PositionVisibility, 
				po.PositionRanking, 
				po.PeopleID, 
				po.Title, 
				po.Email, 
				po.StartDate, 
				po.EndDate, 
				po.LIBID, 
				po.Region, 
				GROUP_CONCAT(r.RoleType SEPARATOR ', ') as Roles, 
				po.INST_INST, 
				po.DEPT_INST, 
				po.STREET_INST, 
				po.STREET2_INST, 
				po.CITY_INST, 
				po.STATE_ETC_CODE_INST, 
				po.ZIP_MAIL_CODE_INST, 
				IF(p.PrimaryPosition = po.PositionID, '(Primary Position)', '') as PrimaryPosition, 
				IF(p.PrimaryPosition = po.PositionID, 'primary_position', 'position') as PositionClass 
			FROM rosters.positions po 
			INNER JOIN rosters.people p ON (po.PositionID = p.PrimaryPosition) 
			LEFT JOIN rosters.roles r ON (r.PositionID = po.PositionID AND r.PositionID IS NOT NULL) 
			WHERE {IDCLAUSE}            
			AND po.INST_INST IS NOT NULL
			AND po.PositionVisibility {VISIBILITYCLAUSE} 
			AND po.StartDate <= CURDATE() 
			AND (po.EndDate IS NULL OR po.EndDate > CURDATE()) GROUP BY po.PositionID)
			UNION
			(SELECT 
				'docline' as RecordSource,
				p.PeopleID, 
				po.PositionID, 
				po.PositionVisibility, 
				po.PositionRanking, 
				po.PeopleID, 
				po.Title, 
				po.Email, 
				po.StartDate, 
				po.EndDate, 
				po.LIBID, 
				po.Region, 
				GROUP_CONCAT(r.RoleType SEPARATOR ', ') as Roles, 
				d.INST_INST, 
				d.DEPT_INST, 
				d.STREET_INST, 
				d.STREET2_INST, 
				d.CITY_INST, 
				d.STATE_ETC_CODE_INST, 
				d.ZIP_MAIL_CODE_INST, 
				IF(p.PrimaryPosition = po.PositionID, '(Primary Position)', '') as PrimaryPosition, 
				IF(p.PrimaryPosition = po.PositionID, 'primary_position', 'position') as PositionClass 
			FROM rosters.positions po 
			INNER JOIN rosters.people p ON (po.PositionID = p.PrimaryPosition) 
			LEFT JOIN rosters.roles r ON (r.PositionID = po.PositionID AND r.PositionID IS NOT NULL) 
			LEFT JOIN docline.PUBLIC_LIBRARY_DOCUSERS d ON (d.LIBID = po.LIBID) 
			WHERE {IDCLAUSE} 
			AND po.INST_INST IS NULL
			AND po.PositionVisibility {VISIBILITYCLAUSE} 
			AND po.StartDate <= CURDATE() 
			AND (po.EndDate IS NULL OR po.EndDate > CURDATE()) GROUP BY po.PositionID )
			ORDER BY PrimaryPosition DESC";    
	const rosters_regions = 
		"SELECT DISTINCT r.* FROM general.regions r";                 
	const rosters_roles = 
		"SELECT DISTINCT
            po.PeopleID,
			r.RoleID,
            rt.RoleDescription,
            r.RoleType,
            po.Title,
            po.LIBID,
            r.Scope,
            IF(p.PrimaryPosition = po.PositionID, 'primary_position_role', 'position_role') as RoleClass
        FROM rosters.roles r
        INNER JOIN rosters.role_types rt
        	ON (r.RoleType = rt.RoleType)
        INNER JOIN rosters.positions po
        	ON (po.PositionID = r.PositionID)
		INNER JOIN rosters.people p
			ON (po.PeopleID = p.PeopleID) 
		WHERE {IDCLAUSE}           
	 		AND po.PositionVisibility {VISIBILITYCLAUSE}
	    	AND po.StartDate <= CURDATE()   
	 		AND (po.EndDate IS NULL OR po.EndDate > CURDATE())"; 
	public static function createQuery($query_name, $IDs=array(),$options=array('visibility'=>'public')){      
		$query_type = self::QUERY_PEOPLE;               
		$pkey = 'p.PeopleID';
		$result = @constant('TemplateQueries::'. 'rosters_'.$query_name);    
		if($result == null){
			throw new Exception("Query $query_name does not exist.");
		}                                                          
		if(strpos($query_name, 'list') !== false){ 
			$pkey = 'ld.ListID';
		} 	                      
		$result = str_replace("{VISIBILITYCLAUSE}", (($options['visibility'] == 'public') ? '= "public"' : '<> "private"'), $result);  
		//print "<pre>";
		//print_r($IDs);
		//print "</pre>";
		if(is_string($IDs)){
			$result = str_replace('{IDCLAUSE}', "$IDs", $result);
		}
		else if($IDs == null || count($IDs) == 0){
		    $result = str_replace('{IDCLAUSE}', " 1", $result);
		}      
		else if(count($IDs) == 1){     
			$result = str_replace('{IDCLAUSE}', " $pkey = ".$IDs[0], $result);       
		}
		else{                      
			$result = str_replace('{IDCLAUSE}', " $pkey IN (".implode(',', $IDs).')', $result);
		}                                                                                        
		return $result;                                             
	}
}
/**
*	@class TemplateDefinitions
*	Storage class for template definition strings.  Defined here for ease of access by all interested parties. 
*/
class TemplateDefinitions{      
	private static $templates = array(     
		/* The empty template is used when content does not exist in a certain venue.  For instance, if some content is not visible
		in terse display, or not available on public view, this template should be returned instead.  Templates where this is the case
		should reference the empty template in their definition. */                     
		'EMPTY_TEMPLATE' => '<tpl></tpl>', 
		'ERROR_TEMPLATE'=>'<tpl>{ErrorMessage}</tpl>',             
		/* The null template is returned when a template is requested that does not exist. */
		'NULL_TEMPLATE' => '<tpl><p style="color: red;">Error: template {TemplateName} does not exist.</p></tpl>',
		/* The accounts template is the base template for accounts display.   It is synonymous with the accounts_intranet_full template */
		'accounts' => '
			<tpl>     
				<p class="account">
					<img class="account_image" src="{AccountImageIcon}" alt="{AccountTypeDescription}" title="{AccountTypeDescription}" /> 
					<span class="{AccountVisibility}"><span class="account_description" > {AccountTypeDescription}: </span> 
					<span class="accountvalue" >{AccountValue}</span> </span>
					<tpl ifb="{AccountComments}"><span class="account_comments" > -- {AccountComments}</span></tpl>
				</p> 
			</tpl>',                                                                         
		'accounts_public_terse' => '<tpl>, <span class="account terse"><span class="accountvalue  {AccountVisibility}" style="white-space:nowrap"><img class="account_image" src="{AccountImageIcon}" alt="{AccountTypeDescription}" title="{AccountTypeDescription}" />{AccountValue}</span>
			</span></tpl>',                    
		'accounts_intranet_terse' => array('accounts_public_terse'),     	
		'accounts_public_short' => array('accounts_public_terse'),   
		'accounts_intranet_short' => array('accounts_public_short'),
		'accounts_intranet_full' => array('accounts'),
		/*'listentry' => '      
			<tpl>            
				<p class="{ListMemberTable}">{ListDescription}
					<tpl if="\'{ListMemberTable}\' == \'list_descriptions\'"><a href="/test/rosters/list.html?ListID={ListMemberTableId}">{DescriptiveText}</a></tpl>
					<tpl if="\'{ListMemberTable}\' == \'people\'"><a href="/test/rosters/index.html?PeopleID={ListMemberTableId}">{DescriptiveText}</a></tpl>
					<tpl ifb="{PositionEmail}">, <a href="mailto:{PositionEmail}">{PositionEmail}</a></tpl>
					<tpl ifb="{PositionTitle}">
					<br /><span class="metadata">{PositionTitle}, {INST_INST}, {DEPT_INST}</span></tpl>
					<tpl ifb="{Notes}"><br /><span class="metadata">{Notes}</span></tpl>
				</p>   
			</tpl>',  */
	    'list'=> '<tpl>                                                            
				<!--<tpl ifb="{Notes}">                                                                    
				<span class="notes">{Notes}</span></tpl>-->
				<tpl ifb="{ListTitle}">
				<p class="list_title">{ListTitle}
					<tpl if="{ListEndDate}"> <span class="metadata">(Active from {ListStartDate} until {ListEndDate})</span></tpl>
				</p></tpl>
				<tpl ifb="{ListDescription}"><div class="list_description">{ListDescription}</div></tpl>   
			</tpl>',             
		'people' => '             
			<tpl>            
			<tpl ifb="{Picture}">
				<img class="person_photo" src="{Picture}" alt="photo" />
			</tpl>
				<p class="metadata">Last updated: {LastUpdt}</p>
				<p class="person_name">{Prefix} {FirstName} {MiddleName} {LastName} {Suffix}</p>
				
				<tpl ifb="{Notes}">
					<br />
					<span class="notes"> {Notes}</span>
				</tpl>
			</tpl>  
	    ',    
		'people_intranet' => '             
			<tpl>         
			<tpl ifb="{Picture}">
				<img class="person_photo" src="{Picture}" alt="photo"/>
			</tpl>
			<tpl ifb="!{Picture}">
				<img class="person_photo" src="/images/noimage.gif" alt="No image available for this person" title="No image available for this person" style="max-width: 100px"/>
			</tpl>
				<p class="metadata">Last updated: {LastUpdt}</p>
				<p class="person_name">{Prefix} {FirstName} {MiddleName} {LastName} {Suffix} <em style="padding-left: 5px;">(intranet visibility)</em></p>       
				<tpl ifb="{Notes}">
					<br />
					<span class="notes"> {Notes}</span>
				</tpl>
			</tpl>  
	  	',                                                 
		'people_public_terse' => '       
			<tpl>
				<span class="public terse" >{FirstName} {LastName}</span>
			</tpl>      
		',       
		'people_intranet_terse'=>array('people_public_terse'),   
		'people_public_short'=>array('people_public_terse'),
		'people_intranet_short'=>array("people_intranet_terse"),
		'positions'=>'         
		<tpl>
			<div class="position {PositionClass} {PositionVisibility}">    
			    <tpl ifb="{Title}">
				<span class="position_title">{Title}
					<tpl ifb="{LIBID}"> ({LIBID})</tpl>
					<tpl ifb="!{LIBID}"><span class="metadata"> [No DOCLINE Affiliation.]</span></tpl> 
					{PrimaryPosition}</span>
				</tpl>
				<tpl ifb="{EndDate}">
					<p class="position_date">Date range: {StartDate}-{EndDate}</p>
				</tpl>
				<tpl ifb="!{EndDate}">
					<p class="position_date">Start date: {StartDate}</p>
				</tpl>        
				<tpl ifb="{INST_INST}">   
					<p class="position_address">{INST_INST}<br/>{DEPT_INST}<br/>{STREET_INST} {STREET2_INST}<br/>
			{CITY_INST} {STATE_ETC_CODE_INST}, {ZIP_MAIL_CODE_INST}</p>
				</tpl>   
				<tpl ifb="{Roles}">
					<p><strong>Position Roles:</strong> {Roles}</p> 
				</tpl>
			</div>
		</tpl>
		',    
		'positions_public' => '              
		<tpl if="\'{PositionVisibility}\' == \'public\'">
			<div class="position {PositionClass} public">       
				<tpl ifb="{Title}"><span class="position_title">{Title}</span></tpl>
				<tpl ifb="{EndDate}">
					<p class="position_date">Date range: {StartDate}-{EndDate}</p>
				</tpl>
				<tpl ifb="!{EndDate}">
					<p class="position_date">Start date: {StartDate}</p>
				</tpl>
				<tpl ifb="{INST_INST}">
					<p class="position_address">{INST_INST}<br/>{DEPT_INST}                            
					<tpl ifb="{STREET_INST}"><br/>{STREET_INST}<tpl ifb="{STREET2_INST}"> {STREET2_INST}</tpl></tpl>
					<tpl ifb="{CITY_INST}"><br />{CITY_INST} {STATE_ETC_CODE_INST}, {ZIP_MAIL_CODE_INST}</tpl>    
					</p>
				</tpl>   
				<tpl ifb="{Roles}">
					<p class="metadata"><strong>Roles:</strong> {Roles}</p> 
				</tpl>
			</div>
		</tpl>
		',                                                                              
		'positions_intranet' => array('positions'),      
		'positions_public_terse' => '<tpl if="\'{PositionVisibility}\' == \'public\'"><span class="{PositionVisibility} terse"><tpl ifb="{Title}">, {Title}</tpl></span>
		</tpl> 
		',     
	    'positions_intranet_terse'=>array('positions_public_terse'),
		'positions_intranet_short'=>'              
		<tpl ifb="{PrimaryPosition}">      
				<span class="position_title terse">{Title} {PrimaryPosition}</span>     
				<tpl ifb="{INST_INST}">
					<br /><span class="position_address terse">{INST_INST}, {DEPT_INST}                                
					</span>
				</tpl> 
		</tpl>
		',
		'positions_public_short'=>' 
		<tpl ifb="{PrimaryPosition}"><tpl if="\'{PositionVisibility}\' == \'public\'">   
				<span class="position_title terse">{Title}</span>   
				<tpl ifb="{INST_INST}">
					<span class="position_address terse">{INST_INST}, {DEPT_INST}                                
					</span>
				</tpl>     
		</tpl></tpl>
		',
		'pseudolist' => '<tpl>PseudoList</tpl>',        
		'region' => '              
		<tpl>
			<hr/>
			<p class="regionlabel">
				<a href="http://nnlm.gov{url}">{ShortName}</a>
			</p>
			<div class="regiongraphic">
				<img src="http://nnlm.gov/images/region_maps/region_{PositionRegion}_sm.png" alt="region graphic" class="regiongraphicimg"/>
			</div>
		</tpl>  
		',                                 
		'roles' => '           
		<tpl>
			<p class="role {RoleClass}">
				<span class="roletype">{RoleType}</span>
				<span class="metadata">(Assigned to position "{Title}")</span>
				<tpl ifb="{RoleDescription}">
					<br/>
					<span class="roledescription">{RoleDescription} ({RoleID})<span class="scope">{Scope}</span></span>
				</tpl>
			</p>
		</tpl>   
		',                                
	    'roles_public' => '<tpl></tpl>'
	);        
	public static function getTemplate($template){    
		$tName = $template;      
		//Failover mechanism: if the template exists in full form, return that.  Otherwise, check for:
		//1) The template name minus the display term. e.g. accounts_public_terse becomes accounts_public.
		//2) The template name minus the display AND the visibility terms. e.g. accounts_public_terse becomes accounts.
		//If a template does not exist in extended form, it is assumed that not enough differences exist to define it.
		//All templates should exist in base form.
		while(!isset(self::$templates[$tName])){   
			$tName = substr($tName, 0, strrpos($tName, '_'));      
			//print "$template not found, looking for $tName<br />";
			if($tName == ''){return str_replace("{TemplateName}", "$template", self::$templates['NULL_TEMPLATE']);}
		}               
		//print "Requested Template: $template, Actual template used: $tName<br />";   
		$result = self::$templates[$tName];                
		return array($tName, $result);     
	}
   	public static function optimize(){                  
		foreach(self::$templates as $k=>$v){
			if(is_array($v)){     
				//the array will contain the name of the other static property whose value it should adopt.
				$v = self::$templates[$v[0]];
			}
			//$v = preg_replace("/\s+/", ' ', $v);    //eliminate whitespace     
		    //$xml = simplexml_load_string($v, 'SimpleXMLElement', LIBXML_NOCDATA | LIBXML_NOBLANKS); //eliminate whitespace         
			//$v = substr($xml->asXML(), 22);           //remove xml prolog
			self::$templates[$k] = $v; 
		}                                                                            
		//debugging                                 
		//print "<pre>";
		//foreach(self::$templates as $k=>$v){                      
		 //   print "$k: " . htmlspecialchars(self::$templates[$k])."\n";
		//}
		//print '</pre>'; 
	}
}  
TemplateDefinitions::optimize();             
/**
*	@class Template
*	This class is responsible for generating output on demand.  It uses the html argument passed
*	to the constructor as the basis for the generated template, parses the argument, and instantiates
*	it with variables where indicated.  The template system has been designed to be similar in nature
* 	to that used by the ExtJS javascript library for their XTemplate system, used elsewhere in this project.
*   This class was added as existing string replacement was becoming too cumbersome for more powerful  
* 	requirements made of the system, such as the ability to customize output based on location.  
*/
class Template{
	private $html = null; //html string to be used as the template
	private $parser = null; //for storage of a parser object
	private $rowdata = null; //passed row data.  Needs global storage as I can't add hooks to the parser callback functions
	private $output = ''; //the final output.  Needs global storage as I can't add hooks to the parser callback functions
	private $outputBlocking = 0; //halts output when a template tag 'if' statement evaluates to false.
	private $tName = null;
	/**  
	* @class Template
	* The template class is designed to provide xml parser functionality to template strings.  
	* The primary function is 'compile', which takes row data and returns a populated html string suitable for 
	* direct output.
	* @param $html {string} the template string to use for all subsequent compile calls.
	*/
	public function Template($tName){                                                              
		list($this->tName, $this->html) = TemplateDefinitions::getTemplate($tName);  
		//print "Template used: ".$this->tName.'<br />';
	}
	/** compile
	* takes an associative array of row data, and returns an output html string.
	* @param $rowdata {assoc. array} the row data retrieved from a mysql query, used 
	* to populate the variables within the template string stored internally.
	* @returns {string} the html string populated with the row data.  Suitable for direct output.
	**/
	public function compile($rowdata, $options=false){     
		libxml_use_internal_errors(true);   
		$this->rowdata = (is_array($rowdata)) ? $rowdata : array(); 
		                                                       
		//BEGIN PARSING  
		
		/*print "<h2>".$this->tName.'</h2>';      
		print 'data<br />';
		print '<pre>';
		print_r($rowdata);
		print '</pre>'; 
		print '<h3>original</h3>';
		 
		highlight_string($this->html); */
		
		/*$d = DOMImplementation::createDocument(null, 'html',
		    DOMImplementation::createDocumentType("html",
		        "-//W3C//DTD XHTML 1.0 Transitional//EN",
		        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"));   
		*/
		$d = new DOMDocument();       
		$d->preserveWhiteSpace = false;
		$d->formatOutput = true;
		$d_out = $d->cloneNode();           
		try{
			$d->loadXML($this->html);    
		}catch(DOMException $e){print $e->getMessage();}     
		if(!$d->hasChildNodes()){       
			throw new Exception('Error in template structure for template '.$this->tName.', template structure: '.$this->html);
		}
	   	self::processNode($d);        
		$result = substr($d->saveXML(), 22);       
		//print '<h3>mid</h3>';
		//highlight_string($result);
		self::processOut($d_out, $d, $d_out);
		libxml_use_internal_errors(false);             
		$result = substr($d_out->saveXML(), 22);  
		$result = trim(preg_replace("/[\n]+/", "\n", $result));
		//print '<h3>final</h3>';
		//highlight_string($result);    
		//print "<hr />";
		return $result;  
		
		/*$this->parser = xml_parser_create('UTF-8');
		xml_parser_set_option($this->parser, XML_OPTION_CASE_FOLDING, 0);
		xml_parser_set_option($this->parser, XML_OPTION_SKIP_WHITE, 1);
		xml_set_object($this->parser, $this);
		xml_set_element_handler($this->parser, "startElement", "endElement");
		xml_set_character_data_handler($this->parser, "charData");
		$this->rowdata = (is_array($rowdata)) ? $rowdata : array();
		$this->output = '';
		$this->haltOutput = 0;
		ob_start();
		xml_parse($this->parser, $this->html);
		//ob_end_flush();
		ob_end_clean();
		xml_parser_free($this->parser);    */
		//return $this->output;
	} 
	private function processOut(&$d_out, &$sourceNode, &$targetNode){       
	 	//print "<p>Node: ".$sourceNode->nodeName."</p>";
		switch($sourceNode->nodeName){   
		case '#document':    
			if($sourceNode->hasChildNodes()){                    
				self::processOut($d_out, $sourceNode->firstChild, $targetNode);
			}
			if($sourceNode->nextSibling){            
				self::processOut($d_out, $sourceNode->nextSibling, $targetNode);
			}      
		   break;
		case 'tpl':      
		  	if($sourceNode->hasChildNodes() && !$sourceNode->getAttribute('prune')){       
				self::processOut($d_out, $sourceNode->firstChild, $targetNode);      
			} 
			else if($sourceNode->hasChildNodes()){
				//print "Pruning node " .$sourceNode->nodeName.', value: <pre>'. htmlspecialchars($sourceNode->nodeValue, true).'</pre><br />';
			}              
			if($sourceNode->nextSibling){            
				self::processOut($d_out, $sourceNode->nextSibling, $targetNode);
			}                
			break;    
		default:            
			$new = $d_out->importNode($sourceNode, false);
			$targetNode->appendChild($new);
			if($sourceNode->hasChildNodes()){                    
				self::processOut($d_out, $sourceNode->firstChild, $new);
			}
			if($sourceNode->nextSibling){            
				self::processOut($d_out, $sourceNode->nextSibling, $targetNode);
			}                                                	 
		}        
	}   
	private function compileString($text, $asBoolean=false, $specialChars=false){ 
		//$text = $text);  
		if(trim($text) == ''){return '';}  
		$doprint = false;
	   	while(preg_match("/\{([^}]+)\}/", $text, $matches)){  
			if(!isset($this->rowdata[$matches[1]]) || $this->rowdata[$matches[1]] == ''){     
			    if($asBoolean){  
					$text = str_replace($matches[0], 'false', $text);
				}     
				else{                          
					$text = str_replace($matches[0], '', $text); 
				}                                                                     
			}  
			else{
			    if($asBoolean){  
					$text = str_replace($matches[0], 'true', $text);
				}       
				else{                                                                         
					$text = str_replace($matches[0], $this->rowdata[$matches[1]], $text);
				}
			}                                                                       
		}     
		return ($specialChars) ? htmlspecialchars($text) : $text;
	}   
	private function compileTextNode($DOMTextNode){   
		$text = self::compileString($DOMTextNode->wholeText); 
		return new DOMText($text);  
	}
	private function replaceAttributes($node){      
		$matches = array();                
		if($node->hasAttributes()){        
			//for each attribute value...
			for($i = 0; $i < $node->attributes->length; $i++){      
				$n = $node->attributes->item($i);
				switch($n->nodeName){
				case 'ifb':           
					$n->nodeValue = 'return '.self::compileString($n->nodeValue, true, false).';';
					break;
				 case 'if':   
					$n->nodeValue = 'return '.self::compileString($n->nodeValue, false, false).';'; 
					break;
				default: 	
					$n->nodeValue = self::compileString($n->nodeValue, false, true);
				}                                                     
			}                                       
		}  
	}    
	private function processNode(&$node, $recurse=true){        
		//print "<p>";
		switch($node->nodeName){     
		case '#document':              
					//print '<p><strong>Document.  Child count: ' . $node->hasChildNodes().'</strong></p>';
			if($node->hasChildNodes()){                    
				self::processNode($node->firstChild);
			}    
			return;    
		case 'tpl':         
				//print '<p><strong>'.$this->tName.':'.$node->nodeName . '&nbsp;'.htmlspecialchars($node->nodeValue).'&nbsp;/'.$node->nodeName.'</strong></p>';   
			self::replaceAttributes($node);  
			$keep = true;
			if($node->hasAttributes()){        
				foreach(array('if', 'ifb') as $a){              
					$e = $node->getAttribute($a);  
					if($e === ''){continue;}    
				   	//print "<br />attrib: $a, condition: $e eval result: ".eval($e).'<br />';
					ob_start(); 
					$keep = $keep && eval($e);       
					$eval_output = ob_get_clean();
					if(strstr($eval_output,'Parse error')){
						throw new Exception("Parse error found in evaluated expression.  Cannot continue.  Template: ".$this->tName.", Code: \"$e\", Error: $eval_output");
					}        
				}          
				                            
			}           
			if(!$keep || !$node->hasChildNodes()){  
				//print "<p>Setting node to be pruned.</p>";
				$node->setAttribute('prune', true);  
			}         			           
		    break;
		case 'html':
		case 'body':   
			//print '<p><strong>'.$this->tName.':'.$node->nodeName .': Child count: ' . $node->hasChildNodes().'</strong></p>';
			break;       
		case '#text':                                                             
			//print '<p><strong>'.$node->nodeName . '&nbsp;'.htmlspecialchars($node->wholeText).'&nbsp;/'.$node->nodeName.'</strong></p>'; 
			$value = $node->wholeText;       	
			//print "<p>Value before: \"".$value.'"</p>';  
			if($value == ''){break;}
			$parent = $node->parentNode;     
			$replacement = self::compileTextNode($node);
			$parent->replaceChild($replacement, $node);     
			$node = $replacement;
			//print "<p>Value after: ".$node->wholeText.'</p>';          
		default:  
			//print '<p><strong>'.$this->tName.':'.$node->nodeName . '&nbsp;'.htmlspecialchars($node->nodeValue).'&nbsp;/'.$node->nodeName.'</strong></p>';  								
			self::replaceAttributes($node); 
		}  
		
		if($node->hasChildNodes() && $recurse){                    
			self::processNode($node->firstChild, $recurse);
		}
		if($node->nextSibling && $recurse){
			self::processNode($node->nextSibling, $recurse);
		}               
	}
}
/**
* @class RList
* a class to handle list printing and management.  Allows for merging, sorting on multiple levels,
* and varying print options.  
* Basic use: create a list, and initialize it with your options object.  Passing a ListID or list metadata
* array and calling 'init' will create a 'real' list, based on actual list data in the db.  Passing nothing, 
* and calling 'pseudoinit' will create a 'fake' list, which you can then fill with people to use.
* Note: the 'init' or 'pseudoinit' functions must be invoked immediately
* after instantiation, depending on what you're using the RList for.  The 'init' function is for creating a list
* with a genuine ListID; it auto-instantiates the list from the db when invoked.  'pseudoinit' assumes
* you're only interested in the list sorting and grouping functionality, and the list itself does not
* actually exist. See method documentation for more details.
*/
abstract class RList{
	public $ListID = null;   //Easy reference to this list's id.  null if pseudolist.
	public $metadata = null;  //metadata for the list.  Title, description, start and end date
	public $parent = null; //a pointer to the list parent.
	public $sublists = array(); //holds information for all sublists.
	public $people = array(); //holds information for all people contained by this list (not sublists)   
	protected $locked = false; //boolean flag for halting print output based on conditions.        
	protected $list_filters = array(
		'required'=>array(),
		'disallowed'=>array(),
		'default'=>array()
	); //filtered based on 'Notes' field for individual list entries.
	protected $people_filters = array(
		'required'=>array(),
		'disallowed'=>array(),
		'default'=>array()
	);  //filtered based on 'Notes' field for individual list entries.  
	protected static $a = null; //template for printing account entries.
	protected static $accounts = array();
	protected static $compiled = false; //whether the list has been initialized appropriately.
    protected static $curUserLID = null; //MyListID for logged in user, if on dev or staff.
    protected static $curUserPID = null; //PeopleID for logged in user, if on dev or staff.
	protected static $init = false;
	protected static $l = null; //template for printing list entries.
	protected static $options = null; // the options array passed to the RList.  Contains display options.
	protected static $p = null; //template for printing people entries. 
	protected static $po = null; //template for printing primary position entry.
	protected static $positions = array();   
	protected static $ranking_map = array(
		'Director'=>0,
		'Assistant / Associate Director'=>1,
		'Professional'=>2,
		'Other'=>3,
		'Support'=>4,
		'Student / Temp'=>5
	); 
	protected static $region_data = array(); //metadata for all the regions.  Used for regional grouping.
	/****************** BEGIN PRIVATE FUNCTIONS *****************/
	protected function addListFilter($filter){ 
		if(!self::$init){ 
			throw new Exception("Error: You must initialize a list before calling any functions on it.");
		}      
		if(!isset($filter['required'])){
			$filter['required'] = array();
		}      
		if(!isset($filter['disallowed'])){
			$filter['disallowed'] = array();
		}     
		if(!isset($filter['default'])){
			$filter['default'] = array();
		}
		if(isset($filter['val'])){                                                                   
			switch($filter['val'][0]){
			case '+':
				$filter['required'][]=ltrim($term, $term[0]); 
				break;
			case '-':                                                          
					$filter['disallowed'][]=ltrim($term, $term[0]);
				break;
			default:                                          
				$filter['default'][]=$filter['val'];     
			}   
			unset($filter['val']);
		}     
		if(isset($filter['fid'])){   
			if(!empty($filter['fid'])){    
				$filter['affected'] = $filter['fid']; 
			}                                     
			unset($filter['fid']);
		}                                           
		if(!isset($filter['affected']) || $this->ListID == $filter['affected']){                                                                         
			$this->list_filters['default'] = array_merge($this->list_filters['default'], $filter['default']);          
			$this->list_filters['disallowed'] = array_merge($this->list_filters['disallowed'], $filter['disallowed']);
			$this->list_filters['required'] = array_merge($this->list_filters['required'], $filter['required']);  
		}                                         
		$filter['depth']--;       
		//print '<p>depth: '.$filter['depth'].'</p>';
		if($filter['depth'] >= 0){   
			foreach($this->sublists as $slist){        
				$slist->addListFilter($filter);
			}   
		}   
	}    
	protected function addPeopleFilter($filter){  
		if(!self::$init){ 
			throw new Exception("Error: You must initialize a list before calling any functions on it.");
		}      
		if(!isset($filter['required'])){
			$filter['required'] = array();
		}      
		if(!isset($filter['disallowed'])){
			$filter['disallowed'] = array();
		}     
		if(!isset($filter['default'])){
			$filter['default'] = array();
		}     
		if(isset($filter['val'])){                                                                   
			switch($filter['val'][0]){
			case '+':
				$filter['required'][]=ltrim($term, $term[0]); 
				break;
			case '-':                                                          
					$filter['disallowed'][]=ltrim($term, $term[0]);
				break;
			default:                                          
				$filter['default'][]=$filter['val'];     
			}   
			unset($filter['val']);
		}   
		if(isset($filter['fid'])){
			$filter['affected'] = $filter['fid'];  
			unset($filter['fid']);
		}
		if(!isset($filter['affected']) || empty($filter['affected']) || $this->ListID == $filter['affected']){    
			$this->people_filters['default'] = array_merge($this->people_filters['default'], $filter['default']);          
			$this->people_filters['disallowed'] = array_merge($this->people_filters['disallowed'], $filter['disallowed']);
			$this->people_filters['required'] = array_merge($this->people_filters['required'], $filter['required']);  
		}     
		$filter['depth']--; 
		if($filter['depth'] >= 0){    
			foreach($this->sublists as $slist){        
				$slist->addPeopleFilter($filter);
			}   
		}
	}
	/** 
	* clearListFilters
	* removes all filters on this list. These filters prevent display of sublists based on the Notes
	* field that is present with their entry in this list.
	* @param $recursive {boolean} true (default) if sub-lists should be cleared as well.
	**/
	protected function clearListFilters($recursive=true){ 
		if(!self::$init){ 
			throw new Exception("Error: You must initialize a list before calling any functions on it.");
		}               
		$this->list_filters = array();  
		if($recursive){    
			foreach($this->sublists as $slist){        
				$slist->clearListFilters($recursive);
			}   
		}
	}         
	/** 
	* clearPeopleFilters
	* removes all people filters.  These filters prevent display of people based on the Notes field that
	* is present with their entry in a list.  
	* @param $recursive {boolean} true (default) if sub-lists should be cleared as well.
	**/
	protected function clearPeopleFilters($recursive=true){       
		if(!self::$init){ 
			throw new Exception("Error: You must initialize a list before calling any functions on it.");
		}                               
	   	$this->people_filters = array();   
		if($recursive){    
			foreach($this->sublists as $slist){        
				$slist->clearPeopleFilters($recursive);
			}   
		}
	}           
    /**
 	* compile
	* responsible for retrieving all the information this list needs to display itself, its
	* nested people, and its nested lists.  This function should be implemented by extending classes further down.
	*/
	abstract protected function compile($mixed);
	/**
	* compare
	* helper function for the mergesort implementation
	* @param $o1 {integer|string} the first object to compare.
	* @param $o2 {integer|string} the second object to compare.
	* @param $sort_key {string} the parameter by which to compare them.
	**/
	protected static function compare($o1, $o2, $sort_key){
		/*static $sort_type = null; //remember, static.  Only sets on first invocation.          
		if($sort_type == null){ 
			//instantiate the sort type for ALL entries based on the first entry.
			switch(gettype($o1[$sort_key])){  //assumed o2 is the same type...
			case 'integer':
				$sort_type = DataTypes::isInteger;
				break;        
			case 'string':
				$sort_type = DataTypes::isString;
				break;      
			default:
				$sort_type = DataTypes::isUnknown;   
			}
		}                                       
		switch($sort_type){
	   	case DataTypes::isInteger:           
	   	case DataTypes::isString:     
			if($o1[$sort_key] < $o2[$sort_key]){return -1;}
			if($o1[$sort_key] > $o2[$sort_key]){return 1;} 
			return 0;
		default:
			throw new Exception("Unknown comparison type.");
		}  */    
	   //eliminating complex logic for now.  It doesn't work in the face of multiple sort keys.
		if(!(isset($o1[$sort_key]) && isset($o2[$sort_key]))){
			return 0;
		}
		if($o1[$sort_key] < $o2[$sort_key]){return -1;}
		if($o1[$sort_key] > $o2[$sort_key]){return 1;} 
		return 0;             
	}
	/** 
	* doSort
	* helper function for the mergesort implementation.
	* @param $arr {array} the array to be sorted.
	* @param $sort_keys {array} the names of the keys to sort by, in the order they should be sorted.
	* @param $sort_dir {int} the direction to sort.  1 means 'sort ascending', 0 means 'sort descending' 
	*/
	protected static function doSort($arr, $sort_keys, $sort_dir=1){      
		static $depth = null;
		if($depth == null){$depth = 0;}else{$depth++;}      
		if(count($arr) <= 1){return $arr;}  
		$middle = floor(count($arr)/2);
		$left = self::doSort(array_slice($arr, 0, $middle), $sort_keys, $sort_dir);
		$right = self::doSort(array_slice($arr, $middle), $sort_keys, $sort_dir);
		$result = self::merge($left, $right, $sort_keys, $sort_dir);
		return $result;
	}     
	public function isEmpty(){
		$result = true;
		if(count($this->people) > 0){$result = false;}
		if(count($this->sublists) > 0){
			foreach($this->sublists as $L){
				$result = $result && $L->isEmpty();
			}
		}
		return $result;
	}
	/**
	* mergehelper
	* helper class for the mergesort function  
	* @param $o1 {integer|string} the first object to compare.
	* @param $o2 {integer|string} the second object to compare.            
	* @param $sort_keys {array} the names of the keys to sort by, in the order they should be sorted.
	* @return {int} -1 if $o1 is smaller, 1 if $o2 is smaller, or 0 if they are equal 
	*/       
	protected static function mergehelper($o1, $o2, $sort_keys){   
		//print "<p>";
		foreach($sort_keys as $k){
		   $c = self::compare($o1, $o2, $k);    
			//print "<br />testing key $k against ($o1[LastName])$o1[$k], ($o2[LastName])$o2[$k]";
			if ($c == -1){
				//print '<br />*smaller</p>';
	       		return -1; 
			}
            else if($c == 1){  
				//print '<br />*bigger</p>';                             
	       		return 1;
			} 
			//otherwise equal; test next key in set.
		} 
		//if here, equal in all ways that we care about.  return 0;
		//print '<br />*completely equal</p>';
		return 0;
	}
	/**
	* merge
	* implementation of mergesort algorithm.  Do not call directly; use 'sort' function instead.  
	* @param $left {array} the left half of an array to merge
	* @param $right {array} the right half of an array to merge                              
	* @param $sort_keys {array} the names of the keys to sort by, in the order they should be sorted.
	* @param $sort_dir {int} the direction to sort.  1 means 'sort ascending', 0 means 'sort descending'	     
	* @return {array} the merged array
	*/
	protected static function merge($left, $right, $sort_keys, $sort_dir=1){
		$result = array();         
		$i = 0;       
	    while (count($left) > 0 or count($right) > 0){
	        if (count($left) > 0 and count($right) > 0){     
				$c = self::mergehelper($left[0], $right[0], $sort_keys);
				if(($c == -1)){array_push($result, array_shift($left));}
				else if(($c == 1)){array_push($result, array_shift($right));}
				else{array_push($result, array_shift($left));}   //arbitrary choice.
	        }else if (count($left) > 0){       	
		       	array_push($result, array_shift($left));
	        }else if(count($right) > 0){    
				array_push($result, array_shift($right));
			}
	    }
	    return $result;
	}
	/** 
	* mergeData
	* collapses the list tree into a single list.  Duplicates are eliminated.  List
	* Titles are concatenated by comma and space.
	* @return {void}
	*/
	protected function mergeData(){
		if(!self::$init){
			throw new Exception("Error: You cannot merge a list that has not been initialized.");
		}
		if($this->ListID === -1){return;} //pseudo list.
		if(count($this->sublists) !== 0){
			foreach($this->sublists as $list){
				$list->mergeData($options);
			}
		}
		if($this->parent == null){
			$this->sublists = array(); //no more child lists; 1 way operation
			return;
		}
		$this->parent->metadata['ListVisibility'] = ($this->parent->metadata['ListVisibility'] == 'intranet') ? 'intranet' : $this->Visibility;
		$this->parent->metadata['ListTitle'] .= ', ' . $this->metadata['ListTitle'];
		foreach($this->$people as $id->$person){
			$this->parent->addPerson($person);
		}
	}                                                  
	protected function printListStructure(){  
		print "\n<pre style=\"margin: 10px\">".$this->metadata['ListTitle'];
		foreach($this->people as $person){
			print "\n\t$person[FirstName] | $person[LastName] | $person[PositionRankingSortValue] | $person[RegionName]";
		}
		foreach($this->sublists as $list){
			$list->printListStructure();
		}                               
		print "\n</pre>";
	}                           
	/****************** END PRIVATE FUNCTIONS *****************/ 
           
    /** 
	* addFilters
	* convenience function for multiple filters.  See addFilter for details
	* $mixed {array} a list of string filters to be applied. */
	public function addFilters($mixed){               
		foreach($mixed as $filter){                                                           
			$this->addFilter($filter);      
		}   
	}       
	/** 
	* addFilter
	* adds a filter to the output of this list.  
	* @param $filter {array} the filter to apply.  Filters can be of type 'people' or 'lists' (or 'libraries', soon?)
	* filter format:    
	* array(                         
	* 	terms=> array(Term1[, Term2])   //use + for required terms, - for disallowed terms
	* 	type=>(people|lists) //optional, default is both
	* 	affected=> array(ListID[, ListID]) //which lists this filter affects.  Optional; default is all   
	* 	depth=>(number) //how deep this filter applies.  Optional; default is current and all sub-lists.
	* )          
	*/
	public function addFilter($filter){   
		if(is_string($filter)){                                                        
			$filter = array('terms'=>array($filter));
		}         
		if(!isset($filter['val'])){
			throw new Exception("<p>List filter is missing one or more required terms.  Each filter should look like so:</p>
			<ol><li><strong>terms</strong>an array of terms to filter by.  Use + for required terms, - for disallowed terms </li>
			<li><strong>type</strong>The term 'people', or the term 'lists', determining what the filter applies to. Use * for both</li>  
			<li><strong>affected</strong>an array of people or lists that the filter applies to.</li>  
			<li><strong>depth</strong>How many sublists the filter applies to.  A value of 0 will filter only the current list. (This is the default)</li></ol>
			");       
		}                            
		$filter['type'] = (isset($filter['type'])) ? $filter['type'] : 'g';
		$filter['depth'] = (isset($filter['depth'])) ? $filter['depth'] : 20;                              
		switch($filter['type']){       
		case 'g':                           
			$this->addPeopleFilter($filter);                     
			$this->addListFilter($filter);
			break;                         
		case 'p':                
			//TODO: add 'affected' term to people filters?     
			$this->addPeopleFilter($filter);                             
			break;     
		case 'l':                                            
			$this->addListFilter($filter);                                         
			break;      
		default: 
			throw new Exception("Incorrect type passed to addFilter function");
		}        
	}
	public function addMetadata($key, $value){
		if(!self::init){              
			throw new Exception("List has not been initialized yet");
		}  
		$this->metadata[$key] = $value;
	}
	private static function compilePerson($person){
		$output = '';
		$classes = (self::$options['visibility'] == 'public') ? "rosters_public_record" : "rosters_intranet_record";
		$classes .= (self::$options['display'] == 'terse') ? " hang" : '';
        $output .= "<p class='$classes hang'>";
	 	$output .= self::$p->compile($person);
		if(count(@self::$positions['_'.$person['PeopleID']]) > 0){
			foreach(@self::$positions['_'.$person['PeopleID']] as $po_data){
			   	$output .= self::$po->compile($po_data);
			}	
		}
		if(count(@self::$accounts['_'.$person['PeopleID']])){
			foreach(@self::$accounts['_'.$person['PeopleID']] as $a_data){
			   	$output .= self::$a->compile($a_data);
			}
		}                                                                                      
        $output .= "</p>";
		return $output;
	}
	/**
	* generateListOutput
	* takes the List with its established filters and merge states, and generates string output suitable for 
	* browser display.
	* @param $options {assoc. array} The array of print options.  See the printList function for details.
	* @return {string} The html output.
	*/
	public function generateListOutput($level=0){
		if(!self::$init){
			throw new Exception("Error: You cannot print a list that has not been initialized.");
		}    
		//Establishing defaults.                                                                      
		$merge = false;         
		$delimiter = false;   
		$hasOutput = false;     
		$level++;                                                   
		if(isset(self::$options['sort']) && is_string(self::$options['sort'])){
			self::$options['sort'] = explode(',', self::$options['sort']);			
		}              
		
		//Begin output assembly here.    
		$output = '';
		$wrapper = false;  
		if($level > 1){         
			$output .= "<hr />";
			if(self::$options['showsublisttitles'] == false){
				unset($this->metadata['ListTitle']);
			}
			else{$wrapper = true;}
			if(self::$options['showsublistdescriptions'] == false){
				unset($this->metadata['ListDescription']);
			}
			else{$wrapper=true;}
		}
		else{
			if(self::$options['showlisttitle'] == false){
				unset($this->metadata['ListTitle']);
			}
			else{$wrapper=true;}
			if(self::$options['showlistdescription'] == false){
				unset($this->metadata['ListDescription']);
			}
			else{$wrapper=true;}
		} 
		if($this->locked == true){        
		   	return "<p class='metadata'>[ Not available.]</p>";
		}  
		//confirmed to print at this point.  
		//see if there is any output at all... 
		$output .= "<div class='rosters_".$this->metadata['ListVisibility']."_list  L$level";
		$output .= ($wrapper) ? ' showborder' : '';
 		$output .= "'>";         
		if($_ENV['dev'] && $this->ListID){     
			$output .= "<div style='float: right; font-size: x-small;background-color: white; color: gray; border: 1px solid gray; padding: 5px; border-radius: 3px;'>ListID: " . $this->ListID .'<br />';                                             
			$output .= "People count: " . count($this->people) .'<br />';   
			$output .= "Sublist count: " . count($this->sublists) .'</div>'; 
			
		}
		$output .= self::$l->compile($this->metadata);                                    
		//NOTE: filters only apply to people and sublists. This may change after user feedback.     
		$count = 0;
		$tmpoutput = '';                   
		foreach($this->people as $person){      
			if(isset(self::$options['shownotes']) && self::$options['shownotes'] == false){
				unset($person['Notes']);
			}
			if(isset(self::$options['group'])){  
				$key = null;
				switch(self::$options['group']){
				case 'r':	
					$key = 'RegionName';
					break;
				case 'p':
					$key = 'PositionRanking';
					break;
				default:
				}
				if(!isset($person[$key])){
					throw new Exception ("sorting key $key not set for $person[FirstName] $person[LastName]");
					//continue;
				}
				if($delimiter != $person[$key]){
					$delimiter = $person[$key];
					$tmpoutput .= "<p class='delimiter'>".$delimiter."</p>\n";
				}           
			}                         
			//default value - allow anything if there are no filters, disallow everything if there are.
			   
			$allow = (count($this->people_filters['required']) == 0 &&  
					count($this->people_filters['disallowed'])  == 0 && 
					count($this->people_filters['default']) == 0 
					);  
			foreach($this->people_filters['required'] as $v){ 	   			
				if(isset($person['Notes']) && stripos($person['Notes'], $v) !== false){       
					$allow = true;                                                       
					break;    //required term not found.  Filter OUT.
				}
			}	
			foreach($this->people_filters['disallowed'] as $v){              				
				if(isset($person['Notes']) && stripos($person['Notes'], $v) !== false){    
					//print "<p class='metadata'>Filtering OUT based on disallowed term $v found</p>";                                           
					//filtering OUT entry $person based on disallowed term $v found';
					break;      //disallowed term found.  Filter OUT.          
				}    
			}
			foreach($this->people_filters['default'] as $v){         
				//print "person notes for $person[FirstName] $person[LastName]: $person[Notes]<br />";
				if(empty($person['Notes'])){   
					//Empty entries are allowed even in the presence of filters.          
					$allow = true;     
				}
				else if(stripos($person['Notes'], $v) !== false){   
					//one of many allowed terms were found.
					//tentatively allowed, but check the rest of the filters first.            
					$allow = true;     
				}    
			}             
			if($allow){    
				//print "<p class='metadata'>Filtering IN $person[FirstName] $person[LastName] based on one or more filter terms found</p>";                                    
		   		$tmpoutput .= $this->compilePerson($person);   
				$count++;   
			}     
			else{   
				//print "<p class='metadata'>Filtering OUT $person[FirstName] $person[LastName] based on no filter terms found</p>";
		    }
		}   
		if($count > 0){$output .= $tmpoutput;}
		foreach($this->sublists as $list){   
			//print "<p>sublist $list->ListID</p>";
			if(count($this->list_filters) <= 0){          
				$output .= $list->generateListOutput($level);    
				continue;
			}                                            	
			$allow = (count($this->people_filters['required']) == 0 &&       
				count($this->people_filters['disallowed'])  == 0 && 
				count($this->people_filters['default']) == 0         
			);                                   
			foreach($this->list_filters['required'] as $v){ 				
				if(isset($list->metadata['Notes']) && stripos($list->metadata['Notes'], $v) !== false){       
					$allow = true;
					//filtering OUT entry $person based on required term $v not found';
					break;    //required term not found.  Filter OUT.
				} 
			}	
			foreach($this->list_filters['disallowed'] as $v){              				
				if(isset($list->metadata['Notes']) && stripos($list->metadata['Notes'], $v) !== false){     
				   // print "<p class='metadata'>List: Filtering OUT based on disallowed term $v found</p>";                                             
					//filtering OUT entry $person based on disallowed term $v found';
					break;     //disallowed term found.  Filter OUT.          
				}    
			}
			foreach($this->list_filters['default'] as $v){    
				if(empty($list->metadata['Notes'])){   
					//Empty entries are allowed even in the presence of filters.    
					//print "Allow 1<br />";      
					$allow = true;     
				}
				else if(stripos($list->metadata['Notes'], $v) !== false){   
					//one of many allowed terms were found.
					//tentatively allowed, but check the rest of the filters first.   
					//print "Allow 2<br />";         
					$allow = true;     
				} 
				else{
					//print "Disallow<br />";
				}   
			}             
			if($allow){                   
				//print "<p class='metadata'>List: Filtering IN list '".$list->metadata['ListTitle']."' based on one or more filter terms found</p>";                     
		   		$output .= $list->generateListOutput($level);   
				$count++;   
			}       
			    
			else{   
				//print "<p class='metadata'>List:Filtering OUT list '".$list->metadata['ListTitle']."' based on no filter terms found</p>";
		    }             
		} 
        $output .= "</div>\n";
		//print "Output: " .highlight_string($output);
		return $output;
	}
    /** initialize
	* invoked on the class itself prior to instantiation, passing the $options variable that defines the
	* environment it will be working within.
	* $options {array} the options array passed to the constructor.
	* $force {boolean} whether to force reinitalization. Default is false.
	*/
	public static function initialize($options, $force=false){  
		if(self::$init && !$force){return;}          
		self::$options = $options;          
		self::$curUserPID = Rosters_Stored_Procedures::getCurrentUserID();
		self::$curUserLID = Rosters_Stored_Procedures::getCurrentUserLID();            
		$query_string = TemplateQueries::createQuery('regions', null, self::$options);      
		foreach(Rosters::execQuery($query_string, true) as $region){ 
			self::$region_data['_'.$region['Region_Code']] = $region['ShortName'];
		}        
		$default_visibility = ($_ENV['staff']) ? 'intranet' : 'public';
		self::$region_data['None'] = '<span class="metadata">[No region]</span>';
		//establish templates for people and list output.
		self::$p = (isset($options['listentry_people_template'])) ? new Template($options['listentry_people_template']) : new Template("people_".$default_visibility."_terse");   
		self::$po = (isset($options['listentry_positions_template'])) ? new Template($options['listentry_positions_template']) : new Template("positions_".$default_visibility."_terse");
		self::$a = (isset($options['listentry_accounts_template'])) ? new Template($options['listentry_accounts_template']) : new Template("accounts_".$default_visibility."_terse");                         
		self::$l = (isset($options['listentry_list_template'])) ? new Template($options['listentry_list_template']) : new Template('list');
		self::$init = true;
	}
	/**
	* printList
	* convenience function.  See 'generatelistoutput' for meat 'n potatoes. 
	*/ 
	public function printList(){
		print self::generateListOutput();
	}
 
	/* OBSOLETE.  This is now set by the options array passed during construction. Keeping for reference.
	public function setPeopleDisplay($display='full'){   
		if(!self::$init){
			throw new Exception("Error: You cannot set display levels on a list that has not been initialized.");
		}
		switch($display){                                          
		case 'terse':
			break;
		case 'short':
			break;
		case 'full':
			break;
		default:
			throw new Exception("undefined display type $display");
		}                 
        $peeps = self::execQuery($queries['people'], true);
		//$this->p = TemplateDefinitions::get($display);
	}          
	public function setListDisplay($display='full'){   
		if(!self::$init){
			throw new Exception("Error: You cannot set display levels on a list that has not been initialized.");
		}
		switch($display){                                          
		case 'terse':                                  
			break;
		case 'short':                                  
			break;
		case 'full':                                  
			break;
		default:
			throw new Exception("undefined display type $display");
		}                                                    
		$leeds = self::execQuery($queries['list_descriptions'], true);
		$r_count = 0;
		while(count($leeds) > 0){  
			$data = array_pop($leeds);
			if(!$root->hangList($data)){
				array_push($leeds, $data);
			}             
			$r_count++;
			if($r_count > 100){
				throw new Exception("Could not assemble list!");
			}
		}   
		//$this->l = TemplateDefinitions::get($display);
	} 
	*/    
	public function setPersonTemplate($tName){
	   	self::$pName = $tName;// = new Template($tName);      		
	}   
	public function setListTemplate($tName){
	   	self::$lName = $tName; // = new Template($tName);      
	}           
	public function sort($sort_keys){
		/*
		'r': sort by region.
		'a': sort alphabetically.  Alias for 'f'.
		'f': sort alphabetically, by first name
		'l': sort alphabetically, by last name
		'p': sort by position rank.  Lists are placed last in this sorting.
		'q': sort by position rank.  Lists are placed first in this sorting.
		*/              
		if(!self::$init){
			throw new Exception("Error: You cannot sort a list that has not been initialized.");
		}	
		$sort_dir = 1;
		if(is_string($sort_keys)){
			$sort_keys = explode(',', $sort_keys);	
			$tmp = array();
			foreach($sort_keys as $v){   
				switch($v){
				case 'r':            
					$tmp []= 'RegionName'; 
					self::$options['group'] = 'r'; //must group by region when sorting by region.
					$sort_dir = -1;        
					break;
				case 'a': 
				case 'l':       
					$tmp []= 'LastName';  
				    $tmp []= 'FirstName';
					break;
				case 'f':                                                           
					$tmp []= 'FirstName';
					$tmp []= 'LastName';
					break;
				case 'p':		                                                          
					$tmp []= 'PositionRankingSortValue';
					self::$options['group'] = 'p'; //must group by region when sorting by region.
					break;   
				default:   
				}                
			}           
			$sort_keys = $tmp;	
		}                  
		if(count($sort_keys) == 0){return;}   
		 
	//	print "ID: $this->ListID, Sort keys: " . print_r($sort_keys, true) . '<br />';
		//print '<pre>Presort: ';
		//print_r($this->people);
		//print '</pre>'; 
		$this->people = self::doSort($this->people, $sort_keys, $sort_dir);  
		//print '<pre>PostSort: ';
		//print_r($this->people);
		//print '</pre>';  
		//print 'here';   
		foreach($this->sublists as $list){
			$list->sort($sort_keys);
		}         
	}
	
}   
class AutoList extends RList{
	public function AutoList($mixed, $parent=null){   
		if(!self::$init){
			throw new Exception("Please initialize the " . __CLASS__ . " class before creating an instance.");
		} 
		if(is_numeric($mixed)){
			//print "ID: $mixed<br />";
			$this->ListID = $mixed;  
			$this->metadata = array_pop(Rosters::execQuery(  
				TemplateQueries::createQuery('list', array($mixed)), true));
		}   
		else if(is_array($mixed)){
			$this->metadata = $mixed;   
			$this->ListID = $this->metadata['ListID'];
		}                                                                                     
		// some metadata correction.     
		if(!is_array($this->metadata)){
			throw new Exception("List $this->ListID could not be instantiated.");
		}   
		if($this->metadata['ListStartDate'] == '0000-00-00'){unset($this->metadata['ListStartDate']);}
		if(@$this->metadata['ListEndDate'] == '0000-00-00'){unset($this->metadata['ListEndDate']);}     
		if($this->metadata['ListVisibility'] == 'intranet' && !$_ENV['staff']){
			$this->locked = true;
		}    
		if($this->metadata['ListVisibility'] == 'private' && self::$curUserLID != $this->metadata['ListID']){   
			$this->locked = true;
		}     
		if(isset(self::$options['filters'])){  
			foreach(self::$options['filters'] as $filter){
				$this->addFilter($filter);
			}                                                   
		}                  
		$this->parent = $parent;
		$this->compile();
	}  
	/* The compile function is responsible for building the list from the db, and should be called from the topmost list only.
	*/
	protected function compile($mixed=null){   
		if($this->parent !== null){return;} //only run for root node.     
		$listdata_l = array(); //temp array for holding the portion of list entries that are lists.
		$listdata_p = array();  //temp array for holding the portion of list entries that are people.
		$ldata = array(); //stores full metadata for all lists in this list.  Transferred to static global.
		$pdata = array();  //stores full metadata for all people in this list.  Transferred to static global.   
		$podata = array();  //stores full metadata for all people in this list.  Transferred to static global.
		$adata = array();  //stores full metadata for all people in this list.  Transferred to static global.       
		$lists = array(); //stores an array of list objects until they can be hung properly.  
		$people = array();   //stores an array of people objects until they can be hung properly. 
		/* Populate data for this list.  Requires ID. */   
		$listdata = Rosters_Stored_Procedures::getFullList($this->ListID, self::$options['visibility']); 
		//1. Slice the results into lists and people.  Note, we know this list comes pre-sorted, with sublists first.
		for($i = 0; $i < count($listdata); $i++){          
			if($listdata[$i]['ListMemberTable'] === 'people'){        
				//print ("<p>ListID: $this->ListID, Actual people count: ".(count($listdata)-$i).', Actual list count: '.$i.'</p>');  
				$listdata_l = array_slice($listdata, 0, $i);  
				$listdata_p = array_slice($listdata, $i);    
				//print "<p>People: <pre>" . print_r($listdata_p, true) . "</pre></p>"; 
				//print "<p>Lists: <pre>" . print_r($listdata_l, true) . "</pre></p>";                                
				break;       
			}                                                        
		} 
		//2. Get a set of unique ids for determining metadata for each person and list. 
		$lambda = function($row){
			return $row['ListMemberTableID'];
		};       
		$lids = array_unique(array_map($lambda, $listdata_l));     
		$pids = array_unique(array_map($lambda, $listdata_p));            
		//3. Retrieve requested metadata.   
		if(count($pids) > 0){     
			//Debug::prettyPrintSQL($query_string);                       
			$pdata = Rosters::execQuery(TemplateQueries::createQuery('people', $pids, self::$options), true);
			$podata = Rosters::execQuery(TemplateQueries::createQuery('positions', $pids, self::$options), true);
			$adata = Rosters::execQuery(TemplateQueries::createQuery('accounts', $pids, self::$options), true);
		}                          
		if(count($lids) > 0){                                                      
			$ldata = Rosters::execQuery(TemplateQueries::createQuery('list', $lids, self::$options), true);
		}                            
		//4. create id indices into this data for quick reference, and create lists 
		//for the global static list metadata.  All nested lists reference this same store.    
		//use underscores so that index is treated as a string value.
		//Index all people related data by peopleid.
		foreach($pdata as $k=>$person){   
			$person['RegionName'] = 'None'; //default; override after positions are established.
			$person['PositionRanking'] = 'Other'; //default; override after positions are established.
			$person['PositionRankingSortValue'] = self::$ranking_map[$person['PositionRanking']];
			$pdata['_'.$person['PeopleID']] = $person;
			if(is_numeric($k)){$pdata[$k] = false;}
		}
		$pdata = array_filter($pdata);
		//print "<pre>".print_r($pdata,true)."</pre>";
		foreach($podata as $k=>$position){   
			if(!isset($podata['_'.$position['PeopleID']])){
				$podata['_'.$position['PeopleID']] = array();
			}
			//print "<pre>".print_r($position,true)."</pre>";
			$position['RegionName'] = (!empty($position['Region'])) ?  self::$region_data['_'.$position['Region']] : self::$region_data['None']; 

			$podata['_'.$position['PeopleID']][]= $position;
			if($position['PrimaryPosition'] == '(Primary Position)'){
				$pdata['_'.$position['PeopleID']]['RegionName'] = $position['RegionName'];
				$pdata['_'.$position['PeopleID']]['PositionRanking'] = $position['PositionRanking'];
				$pdata['_'.$position['PeopleID']]['PositionRankingSortValue'] = self::$ranking_map[$position['PositionRanking']];
			}
			if(is_numeric($k)){$podata[$k] = false;}
			//establish position in people record for ease of reference.  Assumed all positions have same region
			
		}
		self::$positions = array_filter($podata);
		foreach($adata as $k=>$account){   
			if(!isset($adata['_'.$account['PeopleID']])){
				$adata['_'.$account['PeopleID']] = array();
			}
			$adata['_'.$account['PeopleID']][]= $account;
			if(is_numeric($k)){$adata[$k] = false;}
		}   
		self::$accounts = array_filter($adata);   
		foreach($ldata as $k=>$list){                        
			$ldata['_'.$list['ListID']] = $list;  
			$ldata[$k] = false;                     
		}    
		$ldata = array_filter($ldata);
		//5. create and organize this and all sublist structures. 
		$parents = array($this->ListID);       
		$lists['_'.$this->ListID]=$this;  
		/*print '<pre>';
		print_r($listdata_l);
		print '</pre>';*/     
		//small pseudo-recursive loop that will reconstruct the list from the server.
		while(count($parents) !== 0){ 
			$tmp = array();
			for($j = 0; $j < count($parents); $j++){
				//print 'Processing entry _'.$parents[$j].'<br />';
				$P = $lists['_'.$parents[$j]];
				//print "looking for children of " . $P->ListID ."<br />";
				for($i = 0; $i < count($listdata_l); $i++){
					$ParentID = $listdata_l[$i]['ListID']; 
					$ListID = $listdata_l[$i]['ListMemberTableID']; //id of the new list to add.	
					if($ParentID == $P->ListID){ 
						$data = $ldata['_'.$ListID];
						$data['Notes'] = $listdata_l[$i]['Notes'];
						$data['ListEntryStartDate'] = $listdata_l[$i]['ListEntryStartDate'];
						$data['ListEntryEndDate'] = $listdata_l[$i]['ListEntryEndDate'];
						$L = new AutoList($data, $P); 
						$ID = $L->ListID;
						//print "Adding new parent: $ID<br />";
						$tmp []= $ID;  
						$P->sublists []= $L; 
						$lists['_'.$ID]= $L;
						//print "establishing new list with id $L->ListID<br />";
						/*print '<p>Lists: ';
						foreach($lists as $list){
							print "<p>".$list->ListID.'</p>';
						}
						print '</p>';*/
						//$listdata_l[$i] = false;
					}
				}	
				$parents[$j] = false;
			}   
			
			$parents = array_filter($parents);//removes all entries that are 'false';
			$parents = array_merge($parents, $tmp);	
			/*print '<pre>New parents: ';
			print_r($parents);
			print '</pre>'; */
			$listdata_l = array_filter($listdata_l);   
		}             
			
		//7. Hang the people.             
		foreach($listdata_p as $data){
			//Establish region name for display.
			$ID = '_'.$data['ListMemberTableID'];
			//print "ID: $ID, Person:";
			//note that in the following, $person is a copy, and modifications do not affect original.  Any
			//modifications should be applicable to the list entry in particular, and not to the person in general.
			$person = $pdata[$ID]; 
			$person['Notes'] = $data['Notes']; //port this info from the list entry.
			$person['ListEntryStartDate'] = $data['ListEntryStartDate'];
			$person['ListEntryEndDate'] = $data['ListEntryEndDate'];
			//print"$person[FirstName] $person[LastName]<br />";
			$L =& $lists['_'.$data['ListID']];
			//print "Hanging person from list ".$L->ListID."<br />";
			$L->people []= $person;
		}  
		if(isset(self::$options['sort'])){
			$this->sort(self::$options['sort']);
		}
	}
}  
/**
* @class PseudoList
* Extension of the RList class that creates a list not based on any real entry in the 
* rosters DB, and is solely to give access to list functionality.  For creating lists 
* on the fly.     
* @param $mixed {array} an array of peopleids                          
* @param $options{assoc. array} the options to use with this array.      
*/ 
class PseudoList extends RList{     
	public function PseudoList($mixed, $parent=null){
		if(!self::$init){
			throw new Exception("Please initialize the " . __CLASS__ . " class before creating an instance.");
		}
		if(!is_array($mixed)){
			throw new Exception('Bad argument to Psuedolist constructor');
		}    	
		$this->metadata = array(
			'ListTitle' => '[Title not available]', //is 'Title' the right var to pass?
			'ListDescription'=> '', //is 'Title' the right var to pass?   
			'ListVisibility'=> isset(self::$options['visibility']) ? self::$options['visibility'] : 'public',
			'ListStartDate'=> '2001-01-1' //arbitrary
		);
		if(!empty($mixed['ListTitle'])){$this->metadata['ListTitle'] = $mixed['ListTitle'];}    
		if(!empty(self::$options['ListTitle'])){$this->metadata['ListTitle'] = self::$options['ListTitle'];}
		if(!empty($mixed['ListDescription'])){$this->metadata['ListDescription'] = $mixed['ListDescription'];}
		if(!empty(self::$options['ListDescription'])){$this->metadata['ListDescription'] = self::$options['ListDescription'];}      
		    
		
		//print '<pre>Metadata:';
		//print_r($this->metadata);
		//print '</pre>';     
		$p = (isset($mixed['People'])) ? $mixed['People'] : ((isset($mixed['PeopleIDs'])) ? $mixed['PeopleIDs'] : array());  
		$l = (isset($mixed['Lists'])) ? $mixed['Lists'] : ((isset($mixed['ListIDs'])) ? $mixed['ListIDs'] : array());
 
		$this->parent = $parent;
		$this->compile($p, $l);
	}  
	/* The compile function is responsible for building the list from the db, and should be called from the topmost list only.
	*/
	protected function compile($listdata_p=array(), $listdata_l=array()){
		if($this->parent !== null){return;} //only run for root node.
		$ldata = array(); //stores full metadata for all lists in this list.  Transferred to static global.
		$pdata = array();  //stores full metadata for all people in this list.  Transferred to static global.   
		$podata = array();  //stores full metadata for all people in this list.  Transferred to static global.
		$adata = array();  //stores full metadata for all people in this list.  Transferred to static global.       
		$lists = array(); //stores an array of list objects until they can be hung properly.  
		$people = array();   //stores an array of people objects until they can be hung properly. 
		$lids = array();
		$pids = array();
		/* Populate data for this list.  Requires ID. */   
		//1. Slice the results into lists and people.  Note, we know this list comes pre-sorted, with sublists first.
		//2. Get a set of unique ids for determining metadata for each person and list. 
		if(count($listdata_p) > 0){
			if(is_numeric($listdata_p[0])){
				//it's an array of ids.
				$pids = $listdata_p;
			}
			else{
				$pdata =& $listdata_p;
			}
		}    
		if(count($listdata_l) > 0){
			if(is_numeric($listdata_l[0])){
				//it's an array of ids.
				$lids = $listdata_l;
			}
			else{
				$ldata =& $listdata_l;
			}
		}    
		//3. Retrieve requested metadata.   
		if(count($pids) > 0){     
			//Debug::prettyPrintSQL($query_string);                             
			$pdata = Rosters::execQuery(TemplateQueries::createQuery('people', $pids, self::$options), true);
			$podata = Rosters::execQuery(TemplateQueries::createQuery('positions', $pids, self::$options), true);
			$adata = Rosters::execQuery(TemplateQueries::createQuery('accounts', $pids, self::$options), true);
		}   
		if(count($lids) > 0){
			$ldata = Rosters::execQuery(TemplateQueries::createQuery('list', $lids, self::$options), true);
		}                       
		//4. create id indices into this data for quick reference, and create lists 
		//for the global static list metadata.  All nested lists reference this same store.    
		//use underscores so that index is treated as a string value.
		//Index all people related data by peopleid.
		foreach($pdata as $k=>$person){   
			$person['RegionName'] = 'None'; //default; override after positions are established.
			$person['PositionRanking'] = 'Other'; //default; override after positions are established.
			$person['PositionRankingSortValue'] = self::$ranking_map[$person['PositionRanking']];
			$pdata['_'.$person['PeopleID']] = $person;
			if(is_numeric($k)){$pdata[$k] = false;}
		}
		$pdata = array_filter($pdata);
		foreach($podata as $k=>$position){   
			//print "<pre>".print_r($position,true)."</pre>";
			$position['RegionName'] = (!empty($position['Region'])) ?  self::$region_data['_'.$position['Region']] : self::$region_data['None']; 
			if(!isset($podata['_'.$position['PeopleID']])){
				$podata['_'.$position['PeopleID']] = array();
			}
			$podata['_'.$position['PeopleID']][]= $position;
			if($position['PrimaryPosition'] == '(Primary Position)'){
				$pdata['_'.$position['PeopleID']]['RegionName'] = $position['RegionName'];
				$pdata['_'.$position['PeopleID']]['PositionRanking'] = $position['PositionRanking'];
				$pdata['_'.$position['PeopleID']]['PositionRankingSortValue'] = self::$ranking_map[$position['PositionRanking']];
			}
			if(is_numeric($k)){$podata[$k] = false;}
		}
		self::$positions = array_filter($podata);
		if(count($adata) > 0){
			foreach($adata as $k=>$account){   
				if(!isset($adata['_'.$account['PeopleID']])){
					$adata['_'.$account['PeopleID']] = array();
				}
				$adata['_'.$account['PeopleID']][]= $account;
				if(is_numeric($k)){$podata[$k] = false;}
			}   
			self::$accounts = array_filter($adata);
		}
		if(count($ldata) > 0){
			foreach($ldata as $k=>$list){                        
				$ldata['_'.$list['ListID']] = $list;  
				if(is_numeric($k)){$ldata[$k] = false;}			                    
			}  
			$ldata = array_filter($ldata);
		}  
		

		//5. create and organize this and all sublist structures. 
		$lists['_'.$this->ListID]=$this;            
    
		$lists['_'.$this->ListID]=$this;  
		//print "List count: " . count($ldata);
		foreach($ldata as $k=>$data){	
			$L = new PseudoList($data, $this);  
			$lists['_'.$L->ListID]=& $L; 
			$this->sublists []= $L;
		}	  
		
		//7. Hang the entries         
		foreach($pdata as $k=>$person){ 
			if(is_integer($k)){continue;}
			//print "K: $k<br />";
			$this->people []= $person;
		}  
		if(isset(self::$options['sort'])){
			$this->sort(self::$options['sort']);
		}
	}
}
class Rosters{
    private static $init = false;
    private static $idVerified = false;
    private static $visibility = 'public';
		private static $linkActions = array(
			'people'=>'index.html?PeopleID=',
			'list_descriptions'=>'list.html?ListID='
		);
    public static $messages = array(
        'NO_RES'=>"No user was found with the requested id.  Please double check
        that you are logged in, or if you have no login option, please contact
        the page author and tell them about this message.
        ",
        'COMPILATION_FAILURE'=>'The requested template was not compiled.',
		'USAGE_INFO'=>'<div style="padding: 10px; font-size: 10pt; color: #733;">
		<h1>BAD INPUT</h1>
		<p>Hi, this is the system software at nnlm.gov.  I\'m afraid I cannot determine who you\'re looking for, based on the information given me.</p>
		<p>I suggest hitting up the <a href="http://nnlm.gov/about/staff/">staff page</a>
			and choosing the person you\'re interested in.</p>
		</div>',
		'DEV_USAGE_INFO'=>'
			<div style=" width: 30%;border: 1px solid black; background: pink; padding: 5px; position: absolute; bottom: 0; right: 0;"><p style="font-weight: bold">Dear webpage developer:</p><p>Having trouble?  For usage information, see <a href="https://staff.nnlm.gov/wiki/Rosters_external_software_libraries" target="_blank">here</a>.</p></div>'
    );
		public static $urltargets = array();
	    private static $output = array(
	        'people'=>array(),
	        'positions'=>array(),
	        'accounts'=>array(),
	        'roles'=>array(),
	        'lists'=>array()
	    );
		public static $not_found = array(
			'people'=>'<h2>Not found</h2><p>The requested staff member was not found</p>',
			'short'=>'',
		    'positions'=>'<div class="metadata"><p>No positions were found for this individual.  This either can mean the person does not have any positions assigned, or that all assigned positions have passed their end date</p></div>',
		    'accounts'=>'',
		    'roles'=>'',
		    'lists'=>''
		);
    public static $default_print_values = array(
		'PHONE_INST'=>'<span class="metadata">[No phone number found]</span>',
        'Picture'=>'/images/noimage.gif',
        'EndDate'=>'<em>[No position end date]</em>',
        'ListEndDate'=>'<em>[No list end date]</em>',
        'LIBID'=>'<em>[No position LIBID assigned]</em>',
    );
    private static $default_print_options = array();
    private static function is_assoc($arr){
        return (is_array($array) && (0 !== count(array_diff_key($array, array_keys(array_keys($array)))) || count($array)==0));
    }

	/** compileList
     * workhorse function.  Takes the templates, runs a series of queries against them, and
     * produces output in self::$output.  This output can be called repeatedly
     * by the target page to display some bit of information or another within
     * one page invocation.  The function is set to return immediately if it has
     * already been executed once, to insure it gets called before output while
     * avoiding execution overhead.
     * @param ListID {int} The ListID of the list to print. 
     * @param $options {array} (optional) an array of parameters to specify
     * more granular behavior from the function.  See function 'printPublicList'
     * for available options.
     * @return {void}
     *
     */
    private static function compileList($options){
		if(!isset($options['ListID'])){
			Debug::err("ListID option not set in compileList function.  Cannot continue.");
			return;
		}                               
		$lid = '_'.$options['ListID'];                  
        /* establish function baseline. */                                                                  
		                 
		if(!isset(self::$output['list'])){
            self::$output['list'] = array();
		}   
        if(!isset(self::$output['list'][$lid])){
            self::$output['list'][$lid] = array();
        }           
		AutoList::initialize($options);
		$root = new AutoList($options['ListID']);                                                                           
		self::$output['list'][$lid][]= $root->generateListOutput();    
    }
	/** expandRequestShorthand
     * Helper function.  Designed to take variables and values
     * passed in a URL query string, and convert them into
     * consistent, full english equivalents.  This is to
     * allow for shorter URL strings.  Modifies the $_REQUEST
     * server variable directly.
     * @return {void}
     */
	public static function expandRequestShorthand(&$output_array){   	
		//print "<pre>INPUT: <br />".print_r($output_array, true)."</pre>";
		foreach($output_array as $k=>$v){
			//print '<p>k: ' . $k . ', v: ' . $v . '</p>';
			switch($k){
			case 'f': //format
				$output_array['format'] = $v;    
				unset($output_array[$k]);
				break;
			case 'v': //visibility	             
				switch($v){
				case 'p':
					$output_array['visibility'] = 'public';
					break;
				case 'i':
					$output_array['visibility'] = 'intranet';
					break;
				default:
					$output_array['visibility'] = $v;
				}   
				unset($output_array[$k]);
				break;
			case 'd': //display	                      
				switch($v){
				case 't':
					$output_array['display'] = 'terse';          
					break;
				case 's':
					$output_array['display'] = 'short';
					break;
				case 'f':
					$output_array['display'] = 'full';
					break;
				default:
					$output_array['display'] = $v;
				}  
				unset($output_array[$k]);
				break;
			case 's': //sort by	     
				$output_array['sort'] = $v;    
				unset($output_array[$k]);
				break;
			case 'q': //query string           
				$output_array['query'] = $v;    
				unset($output_array[$k]);  
				break;    
			case 'x':   
				$output_array['filters'] = $v;   
				unset($output_array[$k]); 
			    break;
			default:                                     
			}                         
		} 
		//print "<pre>RESULT: <br />".print_r($output_array, true)."</pre>";                        
	}
    /** compilePerson
     * workhorse function.  Takes the templates, runs a series of queries against them, and
     * produces output in self::$output.  This output can be called repeatedly
     * by the target page to display some bit of information or another within
     * one page invocation.  The function is set to return immediately if it has
     * already been executed once, to insure it gets called before output while
     * avoiding execution overhead.
     * @param PeopleID {int} (optional) The PeopleID of the person to print.  Leaving
     * blank results in the user currently logged into the site, if exists,
     * or an error message if not.
     * @param $options {array} (optional) an array of parameters to specify
     * more granular behavior from the function.  See function 'printRecord'
     * for available options.
     * @return {void}
     *
     */
    private static function compilePerson($options){
        if(self::$init == true && !isset($options['force'])){
            return; //templates already populated.
        }
        $PeopleIDs = (isset($options['PeopleIDs'])) ? $options['PeopleIDs'] : array($options['PeopleID']);
        if(!is_array($PeopleIDs)){
            throw new Exception("Invalid argument passed to printRecords function: array expected, ".gettype($PeopleIDs)." found.");
        }
        $PeopleID = $PeopleIDs[0];
        $PIDstrings = array();
        foreach($PeopleIDs as $pid){
            $PIDstrings []= 'p.PeopleID = '.$pid;
        }
        /* process templates */
        $templateList = (isset($options['template'])) ? array($options['template']) : $options['templates'];
        foreach($templateList as $template){  
			//print "<p>Template $template</p>";
			//Lists uses the RList special class, and must be handled distinctly.
			if($template == 'lists'){
				$ListsPersonIsPartOf = Rosters::execQuery(Rosters_Stored_Procedures::getListAssignations($options['PeopleID'], $options['visibility']), true);
				//die('QS: '.$query_string);                 
				//print '<pre>';
				//print_r($ListsPersonIsPartOf);
				//print '</pre>';
				if(count($ListsPersonIsPartOf) > 0){
					if(!isset(self::$output['list'])){self::$output['list'] = array();}
					PseudoList::initialize(array_merge($options, array(
						'showlisttitle'=>false,
						'showlistdescription'=>false,
						'showsublisttitles'=>true,
						'showsublistdescriptions'=>false
					)));
					$root = new PseudoList(array('Lists'=>$ListsPersonIsPartOf));   
					self::$output[$template][]= $options['header'];
					self::$output[$template][]= $root->generateListOutput();
				}
				return;
			}
			$actual_template_name = $template.'_'.$options['visibility'].'_'.$options['display'];   
	        $query_string = TemplateQueries::createQuery($template, $PeopleIDs, $options);     
            $a = self::execQuery($query_string, true);

            //Debug::prettyPrintSQL($query_string, true);
			//print "<p>Result count: ".count($a)."</p>";                
           	self::$output[$template] = array();     
			try{       
	            //print "Creating template  '$actual_template_name'<br />";
				$t = new Template($actual_template_name); 
			}    
			catch(Exception $e){print $e->getMessage(); return;}   
			if(count($a) == 0){
				print (isset($options['notfound'])) ? $options['notfound'] : Rosters::$not_found[$template];
			}
            foreach($a as $row){     
	            $row['listlinktarget'] = $options['listlinktarget'];
		        $row['peoplelinktarget'] = $options['peoplelinktarget'];
			   	$output = $t->compile($row, $options);
				if(($output) === ''){continue;}
				self::$output[$template][]=$output;
            }
        }       
	   	//print_aron ("Templates: " . '<pre>'.print_r(self::$output, true).'</pre>');
	   // print "<h2>Compilation complete</h2>";
        self::$init = true;
    }
	public static function generateLinkTarget($page){
		$url = $_ENV['prefix'];
		$url .= ($_ENV['staff']) ? '/rosters/output_demo/' : '/docs/examples/rosters/';
		return $url.$page;
	}
    private static function getPidsFromNames($nameArr){
        foreach($nameArr as $i=>$name){
            $nameArr[$i] = "f.fullName = '$name'";
        }
        $query_string = "SELECT p.PeopleID
        FROM rosters.people p
        INNER JOIN rosters.fullName f
        ON (f.PeopleID = p.PeopleID)
        WHERE  " . implode(" OR ", $nameArr);
        Debug::prettyPrintSQL($query_string);
        $r = self::execQuery($query_string, false);
        $result = array();
        while(list(, $pid) = each($r)){
            $result []= $pid[0];
        }
        return $result;
    }

    public static function execSingleQuery($query_string, $assoc=false){
		try{
			return MySQLiC::singleRowQuery($query_string, $assoc);
		} catch(Exception $e){
			err(SQL_ERR, $e->getMessage());
		}
    }
    /** Convenience function to execute a query.  Puts the entire result 
	in an array.   Nothing the rosters does is expected to
	run into memory issues from large results. */
	public static function execQuery($query_string, $assoc=false){
        $mysqli = MySQLiC::getDBConnection('inserter');
        if(!$m_result = $mysqli->query($query_string)){   
		   print "Query string: " . Debug::prettyPrintSQL($query_string, false);
           throw new Exception("SQL Error:<br />".$mysqli->error);
        }
        $result = array();
        $method = ($assoc) ? MYSQLI_ASSOC : MYSQLI_NUM;
        while($row = $m_result->fetch_array($method)){
            $result[]= $row;
        }
        $m_result->close();
        $mysqli->close();
        return $result;
    }
    private static function applyDefaults(&$options, $defaults, $override=false){
       // print "<pre>B4:".print_r($options, true);
        foreach($defaults as $k=>$v){
            if(!isset($options[$k]) || $override){
                $options[$k] = $v;
            }
        }
       // print "FTR:".print_r($options, true)."</pre>";
        if(isset($options['names'])){
            $options['PeopleIDs'] = self::getPidsFromNames($options['names']);
            $options['PeopleID'] = $options['PeopleIDs'][0];
        }
       // if(!isset($options['PeopleID']) || $options['PeopleID'] == NULL){
        //    $options['PeopleID'] = null;//self::getCurUserPID();
        //}
        //if(!isset($options['PeopleIDs']) || $options['PeopleIDs'] == NULL){
         //   $options['PeopleIDs'] = array($options['PeopleID']);
        //}
        if(!isset($options['wrapper'])){
            $options['wrapper'] = true;
        }
        if(!isset($options['header'])){
            $options['header'] = false;
        }
        if(!isset($options['nolabels'])){
            $options['nolabels'] = false;
        }
        if(!isset($options['templates'])){
            if(!isset($options['template'])){
                return;
            }
            $options['templates'] = array($options['template']);
        }
    }
	/** 
	* printList
	* primary function for compiling and printing list data.  Requires either a ListID or a query (ListTitle or ListDescription)
	* parameter to retrieve a list
	* @param $options {assoc. array} The options to use when printing the array.  See the allowed parameters variable for more.
	*/
	public static function printList($options=array()){
		$required_parameters = array(
			'ListID'=>'/NUMERIC/'
		);
		$allowed_parameters = array(
			'display'=>'/(terse|short)/', 
			'peoplelinktarget'=>'/URL/',
			'listlinktarget'=>'/URL/',
			'Notes'=>'/TEXT/',
			'query'=>'/TEXT/',
			'showlistdescription'=>'/BOOLEAN/',
			'showlisttitle'=>'/BOOLEAN/',
			'shownotes'=>'/BOOLEAN/',
			'showsublisttitles'=>'/BOOLEAN/',
			'showsublistdescriptions'=>'/BOOLEAN/',
			'sort'=>'/^[raflpq]{1}(,[raflpq])*$/',
			'visibility'=>"/^(public|intranet)$/",
			'filters'=>array(
				'type'=>'/^(p|l|g)$/',
				'val'=>'/TEXT/',
				'fid'=>'/NUMERIC/',
				'target'=>'/^(Notes)$/'
			)
		); 
		$default_parameters = array(
			'display'=>'terse', 
			'peoplelinktarget'=>self::generateLinkTarget('index.html'),
			'listlinktarget'=>self::generateLinkTarget('list.html'),
			'showlistdescription'=>true,
			'showlisttitle'=>true,
			'shownotes'=>true,
			'showsublisttitles'=>true,
			'showsublistdescriptions'=>true,
			'sort'=>'r,l',
            'templates'=>array(
                'list',
				'listentry'
            ),
			'filters'=>array(),
			'visibility'=>(($_ENV['staff']) ? 'intranet' : 'public')
		);               
		if(!is_array($options)){   
            throw new Exception("Invalid argument passed to printRecords function: array expected, ".
				gettype($arr)." found.");
        }                
		//hard lock on public display                    
		if(isset($options['query'])){
			$options['ListID'] = MySQLiC::simpleQuery("
				SELECT
		            l.ListID,
		            l.ListTitle,
		            l.ListDescription,
		            l.ListVisibility,
		            l.ListStartDate,
		            l.ListEndDate
			        FROM rosters.list_descriptions l
		        WHERE (ListVisibility = '$options[visibility]') 
				AND MATCH(l.ListTitle, l.ListDescription)
		        AGAINST ('$options[query]') LIMIT 1");
			//print ("<p class='metadata'>ListID: $options[ListID]</p>");
			unset($options['query']);
		} 	
		if(!isset($options['ListID'])){
			print "<p class='metadata'>[No list found.]</p>";
			return;
		}       
		try {
        	Rosters_Stored_Procedures::verifyLID($options['ListID']);
		} catch(Exception $e) {
			print $e->getMessage();
			print self::$messages['USAGE_INFO'];
			if($_ENV['dev'] || $_ENV['staff']){
				print self::$messages['DEV_USAGE_INFO'];
			}
			return;
		}    
		self::expandRequestShorthand($options);
		if(!$_ENV['staff']){
			$options['visibility'] = 'public';
		}   	
		//print '<pre>Pre:';     
		//print_r($options);
		//print '</pre>';                         
		self::applyDefaults($options, $default_parameters);
		self::sanity_check($options, $required_parameters, true);
		self::sanity_check($options, $allowed_parameters);   
		//print '<pre>Post:';
		//print_r($options);
		//print '</pre>';  
		//print ('returning for debugging purposes');   
		//return;
		                          
		//print "<p onclick='expand_debug()'>&rarr; Show/hide debugging output</p><div id='debugging_output' style='display:none'>";
	    self::compileList($options);   
		//print "</div>";
		$id = '_'.$options['ListID'];
        /*print ($options['visibility'] == 'public') ? "<div class='rosters_public_list'>" : "<div class='rosters_intranet_list'>";*/
		foreach(self::$output['list'][$id] as $entry){
			print "$entry";
		}
        /*print "</div>";*/
	}
    /** printRecords
     * Convenience function that prints records for all PeopleID's in the
     * passed array
     * @param arr {int array|int} a PeopleID or an array of them to print.
     * @return {void}
     */
    public static function printRecords($options=array()){
		$required_parameters = array(
			'PeopleID'=>'/NUMERIC/'
		);		
		$allowed_parameters = array(
			'display'=>'/(terse|short|full)/',
			'visibility'=>"/(public|intranet)/",
			'PeopleIDs'=>'/TEXT/'
		);
		if(is_integer($options)){
    		$options = array('PeopleID'=>$options);
    	}
		if(isset($options['PeopleID'])){			
			if(is_numeric($options['PeopleID'])){
				return self::printRecord($options);
			}
			else{
				//assume array of pids, attempt as such
				$options['PeopleIDs'] = $options['PeopleID'];
				unset($options['PeopleID']);
				return self::printRecord($options);
			}
		}
		else if (isset($options['PeopleIDs'])){
			//assume singular of pids, attempt as such
			if(is_numeric($options['PeopleIDs'])){
				$options['PeopleID'] = $options['PeopleIDs'];
				unset($options['PeopleIDs']);
				return self::printRecord($options);
			}
			else{	
				if(!is_array($options['PeopleIDs'])){
					$options['PeopleIDs'] = explode(',', trim($options['PeopleIDs'], '[]'));	
				}
				try {
		        	Rosters_Stored_Procedures::verifyPID($options['PeopleIDs']);
				} catch(Exception $e) {
					print self::$messages['USAGE_INFO'];
					if($_ENV['dev']){
						print self::$messages['DEV_USAGE_INFO'];
					}
					return;
				}
				$options['force'] = true; //forces compiler to reset after each output.
				$options_clone = array_slice($options, 0);
				unset($options_clone['PeopleIDs']);
				if(!array_key_exists('display', $options)){
		            $options['display'] = 'terse';
		        }
				foreach($options['PeopleIDs'] as $pid){
					$options['PeopleID'] = $pid;
					self::printRecord($options);
				}
			}
		}
		else if(isset($_REQUEST['PeopleID']) && is_numeric($_REQUEST['PeopleID'])){
			$options['PeopleID'] = $_REQUEST['PeopleID'];
			return self::printRecord($options);
		}
		else if(isset($_SERVER['PHP_AUTH_USER'])){
			$options['PeopleID'] = Rosters_Stored_Procedures::getCurrentUserID();
			return self::printRecord($options);
		}
		else{
			print self::$messages['USAGE_INFO'];
			if($_ENV['dev'] || $_ENV['staff']){
				print self::$messages['DEV_USAGE_INFO'];
			}
			
		}
    }
    
	/** printRecord
     * prints the full record that is visible from the public side.  Optional
     * 'options' array allows specifying subsections of the record to print,
     * rather than the full.
     * @param PeopleID {int} (optional) The PeopleID of the person to print.  Leaving
     * blank results in the user currently logged into the site, if exists,
     * or an error message if not.
     * @param $options {array} (optional) an array of parameters to specify
     * more granular behavior from the function.  Available options are:
     *  'visibility'=>(public|intranet)
     *      show public or intranet visible information.
     *      Defaults to 'public'.
     * @returns nothing.
     */
    public static function printRecord($options=array()){
    	if(is_integer($options)){
    		$options = array('PeopleID'=>$options);
    	}
		$required_parameters = array(
			'PeopleID'=>'/NUMERIC/'
		);      
		$allowed_parameters = array(
			'AccountType'=>'/TEXT/',
			'display'=>'/(terse|short|full)/',
			'emptymsg'=>'/TEXT/',    
			'peoplelinktarget'=>'/URL/',
			'listlinktarget'=>'/URL/',
			'visibility'=>"/(public|intranet)/",
		); 
		$defaults = array(
			'display'=>'full',
            'emptymsg'=>'The requested staff member was not found.',
			'peoplelinktarget'=>self::generateLinkTarget('index.html'),
			'listlinktarget'=>self::generateLinkTarget('list.html'),
            'templates'=>array(
                'people',
				'positions',
				'accounts'
            ),
			'visibility'=>(($_ENV['staff']) ? 'intranet' : 'public'),
		);                                                   
		self::expandRequestShorthand($options); 
		self::applyDefaults($options, $defaults); 
		//UGLY.
		if($options['display'] == 'full' && $options['visibility'] == 'intranet'){
			$options['templates'][]= 'lists';
		}
		if(!isset($options['PeopleID'])){   
			if($_ENV['staff'] || $_ENV['dev']){
				$options['PeopleID'] = Rosters_Stored_Procedures::getCurrentUserID();
			}     
			else{                               
				return false;     
			}         
		}                        
		//defaults established, validate all inputs.                                                     
		self::sanity_check($options, $required_parameters, true);
		self::sanity_check($options, $allowed_parameters);     
		//hard lock on public display
		if(!$_ENV['staff']){
			$options['visibility'] = 'public';
		}
		try {
        	Rosters_Stored_Procedures::verifyPID($options['PeopleID']);
		} catch(Exception $e) {
		    //echo $e->getMessage();
			print self::$messages['USAGE_INFO'];
			if($_ENV['dev']){
				print self::$messages['DEV_USAGE_INFO'];
			}
			return;
		}  
		self::compilePerson($options);  
		//print '<pre>';
		//highlight_string(print_r(self::$output, true));
		//print '</pre>';        
		print ($options['visibility'] == 'public') ? '<div class="rosters_public_record">' : '<div class="rosters_intranet_record">';
		print ($options['display'] == 'terse') ? '<p class="hang">' : '';
		// ($_ENV['dev']) ? "<p>Display: $options[display], Visibility: $options[visibility]</p>" : '';
		foreach($options['templates'] as $tName){   
			//print "Printing template $tName<br />";
			/*print '<pre>';
			print_r(self::$output[$tName]);
			print '</pre>';*/
			self::printFoo($options, $tName);
		}               
		print ($options['display'] == 'terse') ? '</p>' : '';  
		print '</div>';
		return true;
    }
	/** printIntranetRecord 
		see documentation for printRecord.
	**/
	public static function printIntranetRecord($options=array()){
		if(is_numeric($options)){
            $options = array('PeopleID'=>$options);
        }
		$options['visibility'] = 'intranet';
		self::printRecord($options);
	}
    /**
     * printFoo
     * Workhorse function.  takes the options parameter, and prints a single
     * template $tName based on the values contained within it.
     * @param $options {keyed array} an array dictating what portions of the
     * template are to be printed, and how.
     * @param $tName {string} the name of the template to print.
     */
    private static function printFoo($options, $template){    	
		if(!isset(self::$output[$template])){
			throw new Exception(self::$messages['COMPILATION_FAILURE']. ": Template $template was not compiled.");
		}                   	
        $output = self::$output[$template];
        if(isset($options['PeopleIDs']) && is_array($options['PeopleIDs'])){ 
   			if(count($output) == 0){print "No output found.<br />"; return;}      
			else{
				print "Id count: " .count($output)."<br />";
			}                                  
        }      
		if($output == null){return;}
		//BEGIN OUTPUT
		ob_start();    
		switch($options['display']){
		case 'terse':      
			break;
		default:  
			print "<div class='$template'>";//<em></em>"<h2>Template $template</h2>";   
		}         
		if($options['header'] !== false && $options['header'] != ''){ 
			print "<h2>$options[header]</h2>";
		}                      
		foreach(self::$output[$template] as $o){
			print "$o";
		}
        switch($options['display']){
		case 'terse':      
			break;
		default:  
			print "</div>";   
		}                                             
		ob_end_flush();                                                                                      
    }   
    public static function printPerson($options=array()){
        if(is_numeric($options)){
            $options = array('PeopleID'=>$options);
        }
		//note: text is added to the name for the intranet version via css.
        self::applyDefaults($options, array(
            'visibility'=>'public',
            'templates'=>array('people')
        ), true);
		self::compilePerson($options);
        self::printFoo($options, 'people');
    }
    public static function printPositions($options=array()){
        if(is_numeric($options)){
            $options = array('PeopleID'=>$options);
        }
        self::applyDefaults($options, array(
            'visibility'=>'public',
            'header'=>'Positions',
            'highlightPrimary'=>true,
            'emptymsg'=>'This member has no assigned positions.',
            'templates'=>array('positions')
        ), true);
		self::compilePerson($options);
        self::printFoo($options, 'positions');
    }
    public static function printAccounts($options=array()){
        if(is_numeric($options)){
            $options = array('PeopleID'=>$options);
        }
        self::applyDefaults($options, array(
            'visibility'=>'public',
            'header'=>($options['visibility'] == 'intranet') ? 'Accounts' : 'Contact information',
            'emptymsg'=>'This member does not have any accounts.',
            'templates'=>array('accounts')
        ), true);
		self::compilePerson($options);
        self::printFoo($options, 'accounts');
    }
    public static function printRoles($options=array()){
        if(is_numeric($options)){
            $options = array('PeopleID'=>$options);
        }
        self::applyDefaults($options, array(
            'visibility'=>'public',
            'header'=>'Roles',
            'emptymsg'=>'This member does not have any roles assigned to any of their positions.',
            'templates'=>array('roles')
        ), true);
		if($options['visibility'] == 'public'){
			return; //DO NOT PRINT FOR THE PUBLIC.
		}
		self::compilePerson($options);
        self::printFoo($options, 'roles');
    }
    /*public static function printLists($options=array()){
        if(is_numeric($options)){
            $options = array('PeopleID'=>$options);
        }
        self::applyDefaults($options, array(
            'visibility'=>'public',
            'header'=>($options['visibility'] == 'public') ? 'Member of the following lists' : 'Lists',
            'emptymsg'=>'This member does not belong to any lists.',
            'templates'=>array('lists')
        ), true);
		self::compilePerson($options);
        self::printFoo($options, 'lists');
    }*/         
	/** UNDOCUMENTED, 2.1 feature? **/
	public static function query($options=array()){
		$field_of_interest = null;
		$tablename = null;
		$allowed_parameters = array(
			'AccountType'=>'/TEXT/',
			'display'=>'/(terse|short|full)/',
			'emptymsg'=>'/TEXT/', 
            'header'=>'/TEXT/',
			'peoplelinktarget'=>'/URL/',
			'listlinktarget'=>'/URL/',
			'RoleType'=>'/TEXT/',
			'showlistdescription'=>'/BOOLEAN/',
			'showlisttitle'=>'/BOOLEAN/',
			'shownotes'=>'/BOOLEAN/',
			'showsublisttitles'=>'/BOOLEAN/',
			'showsublistdescriptions'=>'/BOOLEAN/',
			'visibility'=>"/(public|intranet)/",
		);
		$defaults = array(
			'display'=>'terse',
            'emptymsg'=>'No results found.',  
            'header'=>"Staff members with the role $options[RoleType]",
			'peoplelinktarget'=>self::generateLinkTarget('index.html'),
			'listlinktarget'=>self::generateLinkTarget('list.html'),
			'showlistdescription'=>true,
			'showlisttitle'=>true,
			'shownotes'=>true,
			'showsublisttitles'=>true,
			'showsublistdescriptions'=>true,
			'sort'=>'r,l',
            'templates'=>array(
                'list',
				'listentry'
            ),
			'visibility'=>(($_ENV['staff']) ? 'intranet' : 'public')
		);
		self::expandRequestShorthand($options);
		self::applyDefaults($options, $defaults);
		//print '<pre>';
		//var_dump($options);
		//print '</pre>';
		//hard lock on public display
		if(!$_ENV['staff']){
			$options['visibility'] = 'public';
		}
		self::sanity_check($options, $allowed_parameters);
		if(!(
			isset($options['RoleType']) ||
			isset($options['AccountType'])
		)){throw new Exception("No type supplied.");}	
		//bounds checking complete; start processing here.
		$output = array();
		if(isset($options['RoleType'])){
			$options['ListTitle'] = "Assigned Role: ".$options['RoleType'];
			$mysqli = MySQLiC::getDBConnection('inserter');
		    $query_string = TemplateQueries::createQuery('query_roles', $options['RoleType'], $options, true);
			$result = $mysqli->query($query_string);
		    if(!$result){
				$err = $mysqli->error;
			    $mysqli->close();
			    print  'Query: '. Debug::prettyPrintSQL($query_string, false);
		        throw new SQL_Error($err);
		    }
		    if($result->num_rows == 0){
				print self::$messages['USAGE_INFO'];
				if($_ENV['dev']){
					print self::$messages['DEV_USAGE_INFO'];
				}
				return;
		    }
			PseudoList::initialize($options);
			$results = array('PeopleIDs'=>array());
			
		    while(list($pid) = $result->fetch_row()){
		    	$results ['PeopleIDs'][] = $pid;
		    }
			$root = new PseudoList($results);   
			$output []= $root->generateListOutput();
			/*
		    $options['PeopleIDs'] = $pids;
			self::compilePerson($options);		
			self::printFoo($options, $options['templates'][0]);
			*/
		}
		else if(isset($options['AccountType'])){
			//TODO: apply logic for automatic account type generation			
		}
		foreach($output as $person){
			print $person;
		}
	}
	/** sanity_check
	 *  takes a data array, and purges it of key-value pairs that are not in the list of valid keys
	 *  for the array.
	 *  @param $request_data_array {array} the data array to be purged.
	 *  @param $mixed {array|string} The valid keys.  If an array is passed, the keys
	 *  will be valid value keys, and the values will be regular expressions to be used
	 *  to validate the data present in the passed request_data_arr.
	 *  If a string is passed instead, the string is assumed to be a tablename in
	 *  the rosters database, and the valid keys are determined from fields in that
	 *  table.
	 *  @returns {special} return value is void if there is no issue with the data.
	 *  Otherwise, the function will trigger an error message to standard out and
	 *  halt execution.
	 */
	
	//foldingStartMarker = '(/\*|\{\s*$|<<<HTML)';
	//foldingStopMarker = '(\*/|^\s*\}|^HTML;)';
	private static function sanity_check(&$request_data_arr, $mixed, $all_required=false){
	    $valid_field_keys = false;
	    if(!is_array($request_data_arr)){
	        simple_response(true);
			exit;
	    }
	    if(is_array($mixed)){
	        $valid_field_keys = $mixed;
	    }
	    else{ //is string, otherwise
	        $valid_field_keys = getValidFieldNames($mixed);
	    }
	    $anySaneRequest = true;
        $mysqli = MySQLiC::getDBConnection('inserter');
		try{
	    	foreach($request_data_arr as $k=>$v){
				$msg = "Checking key $k: ";
		        //if the key exists in request_data_arr, it must match the pattern
		        //in valid_field_keys
		        if(array_key_exists($k, $valid_field_keys)){
		            if($valid_field_keys[$k] == null){
		                //no sanity expression defined.  Use a general mysqli escape.
		                //print ("Escaping.  Prevalue: $request_data_arr[$k]");
		                $request_data_arr[$k] = $mysqli->real_escape_string($request_data_arr[$k]);
						$msg .= "No sanity expression defined for key $k";
						print ($msg);
		            } 
					else if(is_array($valid_field_keys[$k])){
						foreach($v as $arr){
							$anySaneRequest = $anySaneRequest && self::sanity_check($v, $valid_field_keys[$k], $all_required);
						}
					}
		            else if(substr($valid_field_keys[$k], 0, 1) == '/'){
		                switch($valid_field_keys[$k]){
		                case '/BOOLEAN/':
		                    //special case: peopleid on client is not known.
							$msg .= " (Boolean):";
							if(!is_bool($request_data_arr[$k])){
								if($request_data_arr[$k] === 'true' || $request_data_arr[$k] === 'false'){
									$request_data_arr[$k] = ($request_data_arr[$k] === 'true') ? TRUE : FALSE;
								}								
							}
		                    $anySaneRequest = $anySaneRequest && is_bool($request_data_arr[$k]);
		                    if(!$anySaneRequest){
		                        throw new Exception("The supplied value ".
		                        "'$request_data_arr[$k]' for the key $k was not a valid boolean value.");
		                    }
		                    break;
		                case '/NUMERIC/':	
							$msg .= " (Numeric):";
		                    //special case: peopleid on client is not known.
		                    $anySaneRequest = $anySaneRequest && is_numeric($request_data_arr[$k]);
		                    if(!$anySaneRequest){
		                        throw new Exception("The supplied value ".
		                        "'$request_data_arr[$k]' for the key $k was not a valid numeric value.");
		                    }
		                    break;
		                case '/DATE/':	
							$msg .= " (Date):";

							if($request_data_arr[$k] == ''){
								$msg .= " WARNING: empty string - unsetting value";
								$request_data_arr[$k] = 'NULL';
							}
							else{
								$request_data_arr[$k] = preg_replace("/T.*$/", "", $request_data_arr[$k]);
								$converted = date('Y-m-d H:i:s', strtotime($request_data_arr[$k]));
		                    	$anySaneRequest = $anySaneRequest && ($converted !== false);
								$msg .= " NOTE: converted value $request_data_arr[$k] to date $converted";
						        $request_data_arr[$k] = $converted;

							}
							if(!$anySaneRequest){
		                        throw new Exception ("The supplied value ".
		                        "'$request_data_arr[$k]' for the key $k was not a valid date value.");
		                    }
		                    break;
		                case '/TEXT/':	
							$msg .= " (Text):";
		                    $anySaneRequest = $anySaneRequest && preg_match("/^[-@A-z0-9._, \(\)+]*$/", $request_data_arr[$k]);
		                    if(!$anySaneRequest){
		                        throw new Exception("The supplied value ".
		                        "'$request_data_arr[$k]' for the key $k was not a ".
		                        "valid text value.");
		                    }
		                    break;
		                case '/COMPLEXTEXT/':	
							$msg .= " (Complex Text):";
		                    $mysqli = MySQLiC::getDBConnection('inserter');
		                    $request_data_arr[$k] = $mysqli->real_escape_string($request_data_arr[$k]);
		                    $anySaneRequest = $anySaneRequest && true;
		                    $mysqli->close();
		                    break;
						case '/URL/':
								if (!preg_match("/(((http|ftp|https):\/{2})+(([0-9a-z_-]+\.)+(aero|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|ax|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cu|cv|cx|cy|cz|cz|de|dj|dk|dm|do|dz|ec|ee|eg|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|me|mg|mh|mk|ml|mn|mn|mo|mp|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|nom|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ra|rs|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sj|sk|sl|sm|sn|so|sr|st|su|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tl|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw|arpa)(:[0-9]+)?((\/([~0-9a-zA-Z\#\+\%@\.\/_-]+))?(\?[0-9a-zA-Z\+\%@\/&\[\];=_-]+)?)?))\b/imuS", $request_data_arr[$k])) {					
									throw new UserDataException("The supplied value ".
			                        "'$request_data_arr[$k]' for the key $k was not a ".
			                        "valid URL value.");
								}
								break;
		                default:	
							$msg .= " (Custom):";
		                    //validation match is normal regular expression
		                    if(!preg_match($valid_field_keys[$k], $request_data_arr[$k])){
		                        throw new Exception("The value for the key '$k' is not valid.");
		                        $anySaneRequest = false;
		                    }
		                    else{
		                        $anySaneRequest = $anySaneRequest && true;
		                    }
		                }

						$msg .= " OK";
						//print ($msg);

		            }
		            else{
		                //value to be matched is a literal value.  Do direct comparison.

						$msg .= "Checking key $k (Literal)";
						//print ($msg);
		                if($valid_field_keys[$k] != $request_data_arr[$k]){
		                    throw new Exception("The value for the key '$k' is not valid.");
		                    $anySaneRequest = false;
		                }
		                else{
		                    $anySaneRequest = $anySaneRequest && true;
		                }
		            }
		        }
				else{
					//throw new Exception("The key $k does not exist in the valid field keys array, skipping");
				}
		    }
		    $mysqli->close();
		    unset($valid_field_keys['extid']); //not required.
		    if($all_required){
		        //The key in valid_field_keys *must* be present in the input array.
		        foreach($valid_field_keys as $k=>$v){
		            if(!array_key_exists($k, $request_data_arr)){
		                $errors []= "The required key '$k' is missing from this request.";
		                $anySaneRequest = false;
		            }
		        }
		    }
		    if(!$anySaneRequest){
		        throw new Exception("Errors in page: " .implode(" | ", $errors));
		    }
		} catch(Exception $e){
			print '<div style="color: #900; font-weight: bold; border: 1px dotted red; padding: 10px; margin: 10px;">';
			if($_ENV['dev'] || $_ENV['staff']){
				print $e->getMessage(). '  See <a href="https://staff.nnlm.gov/wiki/Rosters_external_software_libraries#Displaying_Lists">the user documentation</a>.';
			}
			else{
				//TODO: Add link to term 'RML'.
				print "An error has occurred with this page.  Please contact your RML.";
			}
			print '</div>';
			die('Stopping.');
		}
		//print ("Sanity check passed.  Keys tested: " . implode(", ", array_keys($valid_field_keys)));
	    return true;
	}
}    
                 
?>
