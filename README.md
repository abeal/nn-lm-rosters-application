NN/LM Rosters Project
---------------------
This repository contains code for the NN/LM Rosters Project, primarily hosted on the NN/LM [staff intranet](https://staff.nnlm.gov/rosters).  Its primary purpose is to provide NN/LM staff members a directory of people working for NN/LM, and to track the positions, roles, and groups these staff members are assigned to.

This repo also contains code for the rosters output libraries and output libraries module.  These are relatively independent projects whose purpose is to take the contents of the rosters database, and provide a means to display that content in a variety of environments in as simple a fashion as possible.

This is an NN/LM Phing Project.  Please see the [relevant repository](https://bitbucket.org/webstoc/phing) if you are intending to build this code.