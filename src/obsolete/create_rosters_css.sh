#!/usr/bin/env bash
#all stylesheets
##NONE, other than national
#print stylesheets
##NONE, other than national
compress=false
function process_stylesheet()
{
	if [ "$#" -eq 0 ]; then
		echo "No argument!";
		return 1;
	fi;
	if [[ ! -e $1 ]]; then
		echo "CSS file $1 does not exist.";
		return 1;
	fi;
	fil=$1;
	size=$(stat -c %s $fil); # skip empty files.
    if [[ $size == 0 ]]; then
		echo "skipping $fil - no content";
		return 0;
	fi;
	echo "Adding file $fil to master stylesheet /i3/rosters/rosters.screen.css"
	cat $fil >> /i3/rosters/tmp.css
	if [[ $compress == true ]]; then
		compress.py /i3/rosters/tmp.css
	fi
	name=`basename $fil`
	echo -e "\n/* BEGIN FILE $name*/" >> /i3/rosters/rosters.screen.css
	cat /i3/rosters/tmp.css >> /i3/rosters/rosters.screen.css
	echo -e "\n/* END FILE $name */" >> /i3/rosters/rosters.screen.css
	rm /i3/rosters/tmp.css
}
#screen stylesheets
if [ "$#" -eq 1 ]; then
	compress=$1
fi;
rm /i3/rosters/rosters.screen.css
process_stylesheet /i3/styles/nnlm.screen.css
process_stylesheet /i3/styles/nnlm_small.screen.css
#process_stylesheet /i3/scripts/ext/resources/css/ext-all.css
#process_stylesheet /i3/scripts/ext/resources/css/xtheme-gray.css
process_stylesheet /i3/rosters/styles/rosters.screen.css
process_stylesheet /i3/styles/rosters.screen.css
process_stylesheet /i3/styles/rosters.css
process_stylesheet /i3/rosters/styles/rosters.views.screen.css
process_stylesheet /i3/rosters/styles/icons.css
process_stylesheet /i3/rosters/styles/Find.css
process_stylesheet /i3/rosters/styles/Edit.css
process_stylesheet /i3/rosters/styles/List.css 
process_stylesheet /i3/styles/src/rosters.css
#process_stylesheet /i3/styles/src/rosters_list.css
echo "rosters.screen.css master style file established in rosters root."
