#!/usr/bin/env bash
compress=false
if [ "$#" -eq 1 ]; then
	compress=$1
fi;
/i3/rosters/create_rosters_js.sh $compress
status=$?
if [[ $status != 0 ]]; then
    echo "Could not build js from cache."
    #cp /i3/rosters/index_dev.html /i3/rosters/index.html
    exit 1;
fi;
/i3/rosters/create_rosters_css.sh $compress
status=$?
if [[ $status != 0 ]]; then
    echo "Could not build css."
    #cp /i3/rosters/index_dev.html /i3/rosters/index.html
    exit 1;
fi;
echo "Build successful."
#cp /i3/rosters/index_live.html /i3/rosters/index.html
