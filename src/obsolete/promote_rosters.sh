#!/bin/bash
#make sure we are running the most current build, and compress all relevant files that need it.
/i3/rosters/create_rosters.sh true
cp /i3/styles/src/rosters.css /i3/styles/dist/
compress.py /i3/styles/dist/rosters.css

#promote to India production
echo "******************Promoting to India Production ******************"
echo "Promoting graphical dependencies..."

##Graphics
rsync -aqv /i3/rosters/images/ /staff.nnlm.gov/rosters/images
rsync -aqv /i3/images/noimage.gif /staff.nnlm.gov/images/noimage.gif
#rm -f /staff.nnlm.gov/rosters/images/social_networking;
#ln -s /staff.nnlm.gov/images/social_networking /staff.nnlm.gov/rosters/images/
#rsync -av /i3/images/social_networking/ /staff.nnlm.gov/images/social_networking

##Library Code
echo "Promoting rosters library files..."
rsync -aqv /i3/scripts/class.rosters.php /staff.nnlm.gov/scripts/
rsync -aqv /i3/scripts/rosters_stored_procedures.class.php /staff.nnlm.gov/scripts/
rsync -aqv /i3/scripts/Exceptions.php /staff.nnlm.gov/scripts/Exceptions.php
#update symlink to point to production version.
rm -f /staff.nnlm.gov/rosters/classfiles/server/Exceptions.php
ln -s /staff.nnlm.gov/scripts/Exceptions.php /staff.nnlm.gov/rosters/classfiles/server/

##CSS
echo "Promoting cascading style sheets..."
rsync -aqv /i3/styles/dist/rosters.css /staff.nnlm.gov/styles/rosters.css
rsync -aqv /i3/rosters/rosters.screen.css /staff.nnlm.gov/rosters/rosters.screen.css

##Ext Code
echo "Promoting Ext libraries..."
rsync -aqv /i3/scripts/ext-3.4.0/ /staff.nnlm.gov/scripts/ext
mkdir -p /staff.nnlm.gov/scripts/ext/extensions
rsync -aqv /i3/scripts/ext_extensions/ /staff.nnlm.gov/scripts/ext/extensions

##Output demo pages  
echo "Promoting output demo pages..."
rsync -aqv --delete /i3/rosters/output_demo/index.html /staff.nnlm.gov/rosters/output_demo/
rsync -aqv --delete /i3/rosters/output_demo/query.html /staff.nnlm.gov/rosters/output_demo/
rsync -aqv --delete /i3/rosters/output_demo/list.html /staff.nnlm.gov/rosters/output_demo/

##App Code
echo "Promotic application code..."
mkdir -p /staff.nnlm.gov/rosters/classfiles/server
rsync -av /i3/rosters/classfiles/server/ /staff.nnlm.gov/rosters/classfiles/server
rsync -av /i3/rosters/views/ /staff.nnlm.gov/rosters/views
rsync -av /i3/rosters/rosters.js /staff.nnlm.gov/rosters/rosters.js
rsync -av /i3/rosters/index_live.html /staff.nnlm.gov/rosters/index.html
echo "India complete."
echo "******************Promoting libraries to dev server (Hotel) ******************"
echo "Promoting graphical dependencies..."
##Graphics
#is the following line necessary for Hotel?  Not sure...
#rsync -av /i3/rosters/images/ Hotel:/w3/images
rsync -av /i3/images/noimage.gif Hotel:/w3/images/
rsync -av /i3/images/social_networking/ Hotel:/w3/images/social_networking

##Library Code
echo "Promoting rosters library files..."
rsync -av /i3/scripts/class.rosters.php Hotel:/w3/scripts/
rsync -av /i3/scripts/rosters_stored_procedures.class.php Hotel:/w3/scripts/
rsync -av /i3/scripts/Exceptions.php Hotel:/w3/scripts/

##CSS
echo "Promoting cascading style sheets..."
rsync -av /i3/styles/src/rosters.css Hotel:/w3/styles/src/ #copy the dev version too, for debugging.
rsync -av /i3/styles/dist/rosters.css Hotel:/w3/styles/dist/

##Output demo pages  (edit: omitted for now)
#echo "Promoting output demo pages... (note the target dir change.)"
#rsync -av /i3/rosters/output_demo/ Hotel:/w3/docs/examples/rosters

echo "Hotel complete."

echo "Promotion almost complete.  Run the final commands from a hotel shell finish:"
echo "rsync -av /w3/images/noimage.gif /nnlm.gov/images/";
echo "rsync -av /w3/images/social_networking/ /nnlm.gov/images/social_networking";

##Library Code
echo "rsync -av /w3/scripts/class.rosters.php /nnlm.gov/scripts/"
echo "rsync -av /w3/scripts/rosters_stored_procedures.class.php /nnlm.gov/scripts/"
echo "rsync -av /w3/scripts/Exceptions.php /nnlm.gov/scripts/"

##CSS
echo "rsync -av /w3/styles/dist/rosters.css /nnlm.gov/styles/dist/"

