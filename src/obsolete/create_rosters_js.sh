#!/usr/bin/env bash
compress=false
function process_script()
{
	if [ "$#" -eq 0 ]; then
		echo "No argument!";
		return 1;
	fi;
	if [[ ! -e $1 ]]; then
		echo "JS file $1 does not exist!";
		exit 1;
	fi;
	fil=$1;
	size=$(stat -c %s $fil); # skip empty files.
    if [[ $size == 0 ]]; then
		echo "skipping $fil - no content";
		return 0;
	fi;
	echo "Adding file $fil"
	cat $fil >> /i3/rosters/tmp.js
	if [[ $compress == true ]]; then
		compress.py /i3/rosters/tmp.js
	fi
	cat /i3/rosters/tmp.js >> /i3/rosters/rosters.js
	rm /i3/rosters/tmp.js
}
#screen stylesheets
if [ "$#" -eq 1 ]; then
	compress=$1
fi;
rm /i3/rosters/rosters.js
echo 'Establishing master script file /i3/rosters/rosters.js'
process_script /i3/scripts/ext/adapter/ext/ext-base.js
process_script /i3/scripts/ext/ext-all.js
process_script /i3/rosters/classfiles/client/environment.js
process_script /i3/scripts/ext_extensions/CheckColumn.js
process_script /i3/scripts/ext_extensions/TabCloseMenu.js
#process_script /i3/scripts/ext/examples/ux/fileuploadfield/FileUploadField.js
#process_script /i3/scripts/ext/examples/ux/treegrid/TreeGridSorter.js
#process_script /i3/scripts/ext/examples/ux/treegrid/TreeGridColumnResizer.js
#process_script /i3/scripts/ext/examples/ux/treegrid/TreeGridNodeUI.js
#process_script /i3/scripts/ext/examples/ux/treegrid/TreeGridLoader.js
#process_script /i3/scripts/ext/examples/ux/treegrid/TreeGridColumns.js
#process_script /i3/scripts/ext/examples/ux/treegrid/TreeGrid.js
process_script /i3/rosters/classfiles/client/Records.js
process_script /i3/rosters/classfiles/client/Components.js
process_script /i3/rosters/classfiles/client/DataManager.js
process_script /i3/rosters/classfiles/client/DisplayManager.js
process_script /i3/rosters/classfiles/client/List.js
process_script /i3/rosters/classfiles/client/Find.js
process_script /i3/rosters/classfiles/client/Create.js
process_script /i3/rosters/classfiles/client/Edit.js
echo "NNLM.Rosters.init();" >> /i3/rosters/rosters.js
echo "rosters.js master script file established in rosters root."