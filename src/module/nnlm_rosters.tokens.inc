<?php
use NNLM\Rosters\Utilities as U; define("ROSTERS_TEMP_WARNING", "&nbsp;WARNING: This token is only intended to be
  temporary, and should not be used for anything other than testing purposes.
// Time is in seconds
  It may be removed without warning."); define("ROSTERS_NOT_YET_IMPLEMENTED", "&nbsp;This token is not yet implemented.");
define("NNLM_ROSTERS_CACHE_LIFETIME", 60 * 60 * 24);
define("NNLM_ROSTERS_TEST_STRING", "person:id=4003");

/**
 * Implements hook_token_info
 *
 * @return array An associatve array of the tokens created by this hook.
 *  The root level should be 'tokens.', and the terminal entry is the token
 *  name and value.  Beyond that, it depends on whether we are referencing an
 *  existing token or creating our own.  ['tokens']['node'] would reference
 *  the existing 'node' group, for example.
 */
function nnlm_rosters_token_info() {
  // Add any new tokens.
  $info['types']['rosters'] = array(
    'name' => t('Rosters'),
    'description' => t('These tokens are used to request output from the NN/LM Rosters system.  Data
      for these tokens is supplied by the NN/LM Staff Roster Application
        (https://staff.nnlm.gov/rosters).  A rosters token invocation typically requires a type and an id at a minimum.'),
    'needs-data' => 'rosters',
  );
  $template_types = array(
    'people' => array(
      'name' => t("Collection type: People"),
      'description' => t("A collection type token to display an NN/LM staff member.  This token must be used in conjunction with an id, separated by a forward slash, with that id being a valid PeopleID in the Rosters system.  Example: '[rosters:people/4003]' is a valid use of this token, while '[rosters:people]' alone is not."),
    ),
    'lists' => array(
      'name' => t("Collection type: Lists"),
      'description' => t("A collection type token to display a list that has been defined in the Rosters system.  This token must be used in conjunction with an id, separated by a forward slash, with that id being a valid ListID in the Rosters system.  Example usage: '[rosters:lists/8691]' is a valid use of this token, while '[rosters:lists]' alone is not."),
    ),
  );
  $template_options = array(
    'display' => array(
      'name' => t("Display"),
      'description' => t("An option token.  Controls how the output is formatted.  This token must come after the collection token, and must be used with a value assigned to it via the = sign. Valid values to assign are 'block' (block display for positions and accounts), 'table' (css table display), and 'inline' (default). Example usage: [rosters:people/4003:display=block].<br />Valid values are:
        <ul>
        <li><strong>'inline'</strong>: Attempts to render everything inline.  This is the default. Note that the picture cannot be displayed when the display variable is set to inline.</li>
        <li><strong>'block'</strong>: Attempts to display everything in block display.</li>
        <li><strong>'table'</strong>: Attempts to display everything in table display.  This or 'block' is usually the best choice for rendering large amounts of information.</li></ul>"),
    ),
    'show_positions' => array(
      'name' => t("Show Positions"),
      'description' => t("An option token.  Determines whether to display all active positions, or simply the primary. This token must come after the collection token, and must be used with a value assigned to it via the = sign. Example usage: [rosters:people/4003:show_positions=all]. <br />
        Valid values are:<ul><li><strong>'primary'</strong>: Displays just the primary position.  This is the default.</li>
        <li><strong>'all'</strong>: Shows all positions that have a start date in the past, and either no end date, or an end date that has not expired.</li></ul>"),
    ),
    'fields' => array(
      'name' => t("Fields"),
      'description' => t("An option token.  Determines how much information to display from the position record via a predefined shorthand.  This token must come after the collection token, and must be used with a value assigned to it via the = sign. See the documentation for 'show' below for the full list of available fields.
        Possible values are:
        <ul><li><strong>'terse'</strong>: Displays the person's name, position title (if set in the rosters system), and public accounts.</li>
        <li><strong>'short'</strong>: Same as above, but adds the position institution and department information.</li>
        <li><strong>'all'</strong>: Same as above, but adds the position street address.</li></ul>")
    ),
    'group' => array(
      'name' => t("Group by"),
      'description' => t("An option token.  Allows grouping of individual lists.  This token must come after the collection token, and must be used with a value assigned to it via the = sign. Possible values are:
        <ul><li><strong>'region'</strong>: Groups by region.  Uses the data in the general.regions database for regional group names.  Subsequent sorting is done by last name, and cannot currently be overridden.</li></ul>")
    ),
    'show' => array(
      'name' => t("Show"),
      'description' => t("An option token.  Allows displaying of individual field output that is hidden by default. This token must come after the collection token, and must be used with a value assigned to it via the = sign.  Use commas to enter multiple values, e.g. 'show=list_title,list_description'.  Note that it is often easier to use the 'fields' command to establish a preset, and then modify from there.<br /> Possible values are:
        <ul><li><strong>'address'</strong>: Display the street address of a position.   Default is to hide this field.</li>
        <li><strong>'institution'</strong>: Display the institution name of a position.   Default is to hide this field.</li>
        <li><strong>'department'</strong>: Display the department name of an institution.   Default is to hide this field.</li>
        <li><strong>'picture'</strong>: Display the person's rosters picture, if present.  If not, a 'no picture found' block is displayed.   Default is to hide this field.</li>
        <li><strong>'positions'</strong>: Display the position information.  Note that subsections of positions like 'address' and 'institution' depend on this to be true in order to display.  Default is to hide this field.</li>
        <li><strong>'accounts'</strong>: Display the user's publicly visible accounts.  Default is to show this field.</li>
        <li><strong>'list_title'</strong>: Show the title of a list when rendering.   Default is to show this field.</li>
        <li><strong>'list_description'</strong>: Show the description of a list when rendering.  Default is to hide this field.</li>
        <li><strong>'sublist_title'</strong>: Show the title of a sub-list when rendering.   Default is to hide this field.  (NOTE: sublists are not yet implemented - this is a setting for a future version.)</li>
        <li><strong>'sublist_description'</strong>: Show the description of a sub-list when rendering.   Default is to hide this field.  (NOTE: sublists are not yet implemented - this is a setting for a future version.)</li>.")
    ),
    'hide' => array(
      'name' => t("Hide"),
      'description' => t("An option token.  Allows hiding of individual field output. This token must come after the collection token, and must be used with a value assigned to it via the = sign. See 'show' for possible values.  Defaults are the opposite of those established in 'show'.")
    )
  );
  foreach (array($template_types, $template_options) as $arr) {
    foreach ($arr as $token => $definition) {
      $info['tokens']['rosters'][$token] = $definition;
    }
  }
  ksort($info['tokens']['rosters']);
  return $info;
}

/**
 * Implements hook_tokens
 * Responsible for replacing rosters token occurrences in node body fields with appropriate
 * rosters content.
 *
 * @param  string $type    existing token, or our custom token type
 * @param  a. array $tokens  an associative array of the tokens.  Split this by name=>original
 * @param  array  $data    ?
 * @param  array  $options ?
 *
 * @return a. array
 */
function nnlm_rosters_tokens($token_type, $tokens, array$data = array(), array$options = array()) {
  if (!preg_match("/^rosters*/", $token_type)) {
    return;
  }
  //timer_start('rosters');
  $replacements = array();
  $templateGenerator = new NNLM\Rosters\TemplateGenerator();
  foreach ($tokens as $token_string => $token) {
    //U::dump($token_string, "Processing token string");
    //Requests are satisfied via the construction of REST-like uris. Template
    //generator returns a render array.
    try {
      $replacement = $templateGenerator->render($token_string);
      //keep these steps separate so we can allow for api hooks in the future.
      $replacements[$token] = drupal_render($replacement);
    }
    catch(\Exception$e) {
      $replacements[$token] = '<p>Error: ' . $e->getMessage() . '</p>';
    }
  }
  //U::dump('rosters cache execution time:' . (timer_read('rosters')) . ' milliseconds');
  timer_stop('rosters');
  return $replacements;
}

