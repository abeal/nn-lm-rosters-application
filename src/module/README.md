NN/LM Rosters Drupal Module
===========================

This module is designed to provide access to data in the rosters database in an easy-to-access form.  It does so utilizing Drupal [Tokens](https://drupal.org/project/token), a concise syntax used to represent specific queries to be executed against the rosters database, and specific templates to be used for formatting the output.

This module uses the third party library [Twig](http://twig.sensiolabs.org/doc/intro.html) to provide output.  This is in part due to the desire to have a safe mechanism for rendering templated user data, and also due to the fact that Twig is expected to be integral in Drupal 8.

Module layout
-------------

Files directly used by drupal are located at the site root. Classes responsible for providing output when passed a token are located in the 'includes' folder.  Within includes, files are set up to use an SPL autoloader, and are namespaced according to the [PSR-4](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-4-autoloader.md) standard. (Read [here](https://drupal.org/comment/8273119) to see why)