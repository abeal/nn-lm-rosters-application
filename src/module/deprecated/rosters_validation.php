<?php

/**
 * @deprecated Functionality absorbed by other classes
 * class for performing input sanity checks and ensuring requested fields
 * are legitimate.  Also manages translating template aliases to table
 * and field names.
 */
class RostersValidation {
 /*protected static $rosters_table_aliases = array(
    "OARF_DUMP" => 'OD',
    "account_types" => 'at',
    "accounts" => 'a',
    //VIEW
    "currentAddress" => 'addr',
    //VIEW
    "currentInfo" => 'info',
    //VIEW
    "currentPhone" => 'phone',
    //VIEW
    "descriptiveList" => 'dl',
    "drupal_migration_map" => 'dmm',
    "drupal_migration_map_tag_assignment" => 'dmm_ta',
    "fullName" => 'fn',
    //VIEW
    "listView" => 'lv',
    "list_descriptions" => 'ld',
    "lists" => 'l',
    "people" => 'pe',
    "positions" => 'po',
    "role_types" => 'rt',
    "roles" => 'r',
    "transaction_log" => 'tl',
    "users_with_content" =>'uwc'
  );
  //actual table and field names in rosters db.
  private static $rosters_data_fields = array(
    "OARF_DUMP" => array(
      "People_ID" => FILTER_SANITIZE_NUMBER_INT,
      "Oldid" => FILTER_SANITIZE_NUMBER_INT,
      "FirstName" => FILTER_SANITIZE_STRING,
      "LastName" => FILTER_SANITIZE_STRING,
      "Phone_Area_Code" => FILTER_SANITIZE_NUMBER_INT,
      "Phone" => FILTER_SANITIZE_STRING,
      "Email" => FILTER_SANITIZE_STRING,
      "Institution" => FILTER_SANITIZE_STRING,
      "Street" => FILTER_SANITIZE_STRING,
      "City" => FILTER_SANITIZE_STRING,
      "State" => FILTER_SANITIZE_STRING,
      "Zip" => FILTER_SANITIZE_STRING,
      "Fax" => FILTER_SANITIZE_NUMBER_INT,
      "Phone_Ext" => FILTER_SANITIZE_NUMBER_INT,
      "Fax_Ext" => FILTER_SANITIZE_NUMBER_INT,
      "Phone_Country_Code" => FILTER_SANITIZE_NUMBER_INT,
      "Fax_Country_Code" => FILTER_SANITIZE_NUMBER_INT,
      "Fax_Area_Code" => FILTER_SANITIZE_STRING,
      "Department" => FILTER_SANITIZE_STRING,
      "Picture" => FILTER_SANITIZE_STRING,
      "WM_Alias" => FILTER_SANITIZE_STRING,
      "nnlm_user" => FILTER_SANITIZE_STRING,
      "Personal_URL" => FILTER_SANITIZE_STRING,
      "Updated" => FILTER_SANITIZE_STRING,
    ),
    "account_types" => array(
      "AccountTypeID" => FILTER_SANITIZE_STRING,
      "AccountType" => FILTER_SANITIZE_STRING,
      "AccountTypeDescription" => FILTER_SANITIZE_STRING,
      "AccountImageIcon" => FILTER_SANITIZE_STRING,
      "Singleton" => FILTER_SANITIZE_STRING,
    ),
    "accounts" => array(
      "AccountID" => FILTER_SANITIZE_STRING,
      "PeopleID" => FILTER_SANITIZE_STRING,
      "AccountTypeID" => FILTER_SANITIZE_STRING,
      "AccountVisibility" => FILTER_SANITIZE_STRING,
      "AccountValue" => FILTER_SANITIZE_STRING,
      "AccountComments" => FILTER_SANITIZE_STRING,
    ),
    //VIEW
    "currentAddress" => array(
      "RecordSource" => FILTER_SANITIZE_STRING,
      "PositionID" => FILTER_SANITIZE_STRING,
      "LIBID" => FILTER_SANITIZE_STRING,
      "INST_INST" => FILTER_SANITIZE_STRING,
      "DEPT_INST" => FILTER_SANITIZE_STRING,
      "STREET_INST" => FILTER_SANITIZE_STRING,
      "STREET2_INST" => FILTER_SANITIZE_STRING,
      "CITY_INST" => FILTER_SANITIZE_STRING,
      "STATE_ETC_CODE_INST" => FILTER_SANITIZE_STRING,
      "ZIP_MAIL_CODE_INST" => FILTER_SANITIZE_STRING,
      "COUNTY_INST" => FILTER_SANITIZE_STRING,
      "PROVINCE_INST" => FILTER_SANITIZE_STRING,
    ),
    //VIEW
    "currentInfo" => array(
      "RecordSource" => FILTER_SANITIZE_STRING,
      "PositionID" => FILTER_SANITIZE_STRING,
      "LIBID" => FILTER_SANITIZE_STRING,
      "INST_INST" => FILTER_SANITIZE_STRING,
      "DEPT_INST" => FILTER_SANITIZE_STRING,
      "STREET_INST" => FILTER_SANITIZE_STRING,
      "STREET2_INST" => FILTER_SANITIZE_STRING,
      "CITY_INST" => FILTER_SANITIZE_STRING,
      "STATE_ETC_CODE_INST" => FILTER_SANITIZE_STRING,
      "ZIP_MAIL_CODE_INST" => FILTER_SANITIZE_STRING,
      "COUNTY_INST" => FILTER_SANITIZE_STRING,
      "PROVINCE_INST" => FILTER_SANITIZE_STRING,
      "PHONE_COUNTRY_CODE_INST" => FILTER_SANITIZE_STRING,
      "PHONE_AREA_CODE_INST" => FILTER_SANITIZE_STRING,
      "PHONE_INST" => FILTER_SANITIZE_STRING,
      "PHONE_EXT_INST" => FILTER_SANITIZE_STRING,
    ),
    //VIEW
    "currentPhone" => array(
      "PositionID" => FILTER_SANITIZE_STRING,
      "RecordSource" => FILTER_SANITIZE_STRING,
      "PHONE_COUNTRY_CODE_INST" => FILTER_SANITIZE_STRING,
      "PHONE_AREA_CODE_INST" => FILTER_SANITIZE_STRING,
      "PHONE_INST" => FILTER_SANITIZE_STRING,
      "PHONE_EXT_INST" => FILTER_SANITIZE_STRING,
    ),
    //VIEW
    "descriptiveList" => array(
      "ListEntryID" => FILTER_SANITIZE_STRING,
      "ListID" => FILTER_SANITIZE_STRING,
      "ListMemberTable" => FILTER_SANITIZE_STRING,
      "ListMemberTableID" => FILTER_SANITIZE_STRING,
      "ListEntryStartDate" => FILTER_SANITIZE_STRING,
      "ListEntryEndDate" => FILTER_SANITIZE_STRING,
      "Notes" => FILTER_SANITIZE_STRING,
      "ListTitle" => FILTER_SANITIZE_STRING,
    ),
    "drupal_migration_map" => array(
      "loc" => FILTER_SANITIZE_STRING,
      "title" => FILTER_SANITIZE_STRING,
      "lastmod" => FILTER_SANITIZE_STRING,
      "content_type" => FILTER_SANITIZE_STRING,
      "notes" => FILTER_SANITIZE_STRING,
      "migrate" => FILTER_SANITIZE_STRING,
    ),
    "drupal_migration_map_tag_assignment" => array(
      "loc" => FILTER_SANITIZE_STRING,
      "tag_id" => FILTER_SANITIZE_STRING,
    ),
    "fullName" => array(
      "PeopleID" => FILTER_SANITIZE_STRING,
      "fullName" => FILTER_SANITIZE_STRING,
    ),
    //VIEW
    "listView" => array(
      "ListEntryID" => FILTER_SANITIZE_STRING,
      "ListID" => FILTER_SANITIZE_STRING,
      "ListMemberTable" => FILTER_SANITIZE_STRING,
      "ListMemberTableID" => FILTER_SANITIZE_STRING,
      "ListEntryStartDate" => FILTER_SANITIZE_STRING,
      "ListEntryEndDate" => FILTER_SANITIZE_STRING,
      "Notes" => FILTER_SANITIZE_STRING,
      "DescriptiveText" => FILTER_SANITIZE_STRING,
      "AltDescriptiveText" => FILTER_SANITIZE_STRING,
      "Title" => FILTER_SANITIZE_STRING,
      "Email" => FILTER_SANITIZE_STRING,
      "Region" => FILTER_SANITIZE_STRING,
      "PositionRanking" => FILTER_SANITIZE_STRING,
      "PositionVisibility" => FILTER_SANITIZE_STRING,
      "PHONE_COUNTRY_CODE_INST" => FILTER_SANITIZE_STRING,
      "PHONE_AREA_CODE_INST" => FILTER_SANITIZE_STRING,
      "PHONE_INST" => FILTER_SANITIZE_STRING,
      "PHONE_EXT_INST" => FILTER_SANITIZE_STRING,
      "INST_INST" => FILTER_SANITIZE_STRING,
      "DEPT_INST" => FILTER_SANITIZE_STRING,
    ),
    "list_descriptions" => array(
      "ListID" => FILTER_SANITIZE_STRING,
      "ListTitle" => FILTER_SANITIZE_STRING,
      "ListDescription" => FILTER_SANITIZE_STRING,
      "ListVisibility" => FILTER_SANITIZE_STRING,
      "ListStartDate" => FILTER_SANITIZE_STRING,
      "ListEndDate" => FILTER_SANITIZE_STRING,
    ),
    "lists" => array(
      "ListEntryID" => FILTER_SANITIZE_STRING,
      "ListID" => FILTER_SANITIZE_STRING,
      "ListMemberTable" => FILTER_SANITIZE_STRING,
      "ListMemberTableID" => FILTER_SANITIZE_STRING,
      "ListEntryStartDate" => FILTER_SANITIZE_STRING,
      "ListEntryEndDate" => FILTER_SANITIZE_STRING,
      "Notes" => FILTER_SANITIZE_STRING,
    ),
    "people" => array(
      "PeopleID" => FILTER_SANITIZE_STRING,
      "Prefix" => FILTER_SANITIZE_STRING,
      "FirstName" => FILTER_SANITIZE_STRING,
      "MiddleName" => FILTER_SANITIZE_STRING,
      "LastName" => FILTER_SANITIZE_STRING,
      "Suffix" => FILTER_SANITIZE_STRING,
      "PrimaryPosition" => FILTER_SANITIZE_STRING,
      "Picture" => FILTER_SANITIZE_STRING,
      "LastUpdt" => FILTER_SANITIZE_STRING,
      "MyListID" => FILTER_SANITIZE_STRING,
    ),
    "positions" => array(
      "PositionID" => FILTER_SANITIZE_STRING,
      "PositionVisibility" => FILTER_SANITIZE_STRING,
      "PositionRanking" => FILTER_SANITIZE_STRING,
      "PeopleID" => FILTER_SANITIZE_STRING,
      "Title" => FILTER_SANITIZE_STRING,
      "Email" => FILTER_SANITIZE_STRING,
      "StartDate" => FILTER_SANITIZE_STRING,
      "EndDate" => FILTER_SANITIZE_STRING,
      "Region" => FILTER_SANITIZE_STRING,
      "LIBID" => FILTER_SANITIZE_STRING,
      "INST_INST" => FILTER_SANITIZE_STRING,
      "DEPT_INST" => FILTER_SANITIZE_STRING,
      "STREET_INST" => FILTER_SANITIZE_STRING,
      "STREET2_INST" => FILTER_SANITIZE_STRING,
      "CITY_INST" => FILTER_SANITIZE_STRING,
      "STATE_ETC_CODE_INST" => FILTER_SANITIZE_STRING,
      "ZIP_MAIL_CODE_INST" => FILTER_SANITIZE_STRING,
      "COUNTY_INST" => FILTER_SANITIZE_STRING,
      "PROVINCE_INST" => FILTER_SANITIZE_STRING,
      "PHONE_COUNTRY_CODE_INST" => FILTER_SANITIZE_STRING,
      "PHONE_AREA_CODE_INST" => FILTER_SANITIZE_STRING,
      "PHONE_INST" => FILTER_SANITIZE_STRING,
      "PHONE_EXT_INST" => FILTER_SANITIZE_STRING,
      "LastUpdt" => FILTER_SANITIZE_STRING,
    ),
    "role_types" => array(
      "RoleType" => FILTER_SANITIZE_STRING,
      "RoleDescription" => FILTER_SANITIZE_STRING,
    ),
    "roles" => array(
      "RoleID" => FILTER_SANITIZE_STRING,
      "RoleType" => FILTER_SANITIZE_STRING,
      "PositionID" => FILTER_SANITIZE_STRING,
      "Scope" => FILTER_SANITIZE_STRING,
    ),
    "transaction_log" => array(
      "ModifiedDate" => FILTER_SANITIZE_STRING,
      "ModifierID" => FILTER_SANITIZE_STRING,
      "TableModified" => FILTER_SANITIZE_STRING,
      "ModifiedID" => FILTER_SANITIZE_STRING,
      "ActionTaken" => FILTER_SANITIZE_STRING,
      "Comments" => FILTER_SANITIZE_STRING,
    ),
    //VIEW
    "users_with_content" => array(
      "peopleid" => FILTER_SANITIZE_STRING,
      "name" => FILTER_SANITIZE_STRING,
      "mail" => FILTER_SANITIZE_STRING,
      "region_code" => FILTER_SANITIZE_STRING,
      "region_name" => FILTER_SANITIZE_STRING,
      "status" => FILTER_SANITIZE_STRING,
    ),
  );*/

  /**
   * @deprecated Functionality moved
   * the table alias for a given table.  used in building
   * queries.
   * @param  [type] $db_table_name [description]
   * @return [type]                [description]
   */
  /*protected static function getTableAlias($db_table_name){
    if(empty($db_table_name)) ||
      !isset(self::$rosters_table_aliases[$db_table_name]){
        throw new Exception("Invalid table name passed to getTableAlias: $db_table_name");
    }
    return self::$rosters_table_aliases[$db_table_name];
  }*/
  /**
   * @deprecated Functionality moved
   * Retrieves the correct filter type to be used with filter_var for the
   * passed key value.  Key will be tested for validity.
   *
   * @param  string $template_type The type or table name of template to show.
   * @param  string $key the field name or alias to populate
   *
   * @return int A php filter_var const that can be used in conjunction with
   * the function of the same name to perform sanity checks on passed data.
   * @throws  Exception If $key is not a valid rosters field or alias.
   */
  /*public static function getFilterType($template_type, $token) {
    BOOKMARK - FIX THIS.
    $db_fields = self::resolveAlias($template_type, $token);
    if ($db_fields === FALSE) {
      throw new Exception("'$token' is not an allowed token for the
        template $template_type.");
    }
    list($db_table_name, $db_field_name) = $db_fields;
    return self::$rosters_data_fields[$db_table_name][$db_field_name];
  }*/

  /**
   * @deprecated Functionality moved
   * performs safety filtering on passed content
   *
   * @param  string $template_type the type of template being used to perform
   * the filtering.
   * @param  string $key           the alias used to describe requested content.
   *                               Must be one of the ones predefined in this
   *                               class.
   * @param  string $value         data retrived from the db from the table
   *                               and field that the passed alias corresponds
   *                               to.
   *
   * @return string                the sanitized data value.  This may be an
   *                               empty string if the data is not valid.
   * @throws Exception If $key is not a valid rosters value.  see getFilterType
   */
  /*public static function sanitize($template_type, $key, $value) {
    $filter_type = self::getFilterType($template_type, $key);
    return filter_var($value, $filter_type);
  }*/
}

