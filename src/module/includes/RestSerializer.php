<?php
namespace NNLM\Rosters;

class RestSerializer {
  private static $host = '//localhost';
  private static $base_resource_uri = 'api/rosters';
  private static $plural_type_map = array(
    'person' => 'people',
    'list' => 'lists',
    'account' => 'accounts',
    'position' => 'positions',
  );
  private static $valid_paths = array(
    // all accounts (constrained to public visibility)
    ///'accounts',
    // a specific account id
    //'accounts/\d+',
    // all publicly visible lists
    //'lists',
    // specific list
    'lists/\d+',
    // all people (constrained to public visibility)
    'people',
    // specific person
    'people/\d+',
    // specific position
    //'positions',
    // a specific account
    //'people/accounts/\d+',
    // all positions for a given person
    'people/\d+/accounts',
    // a specific account
    //'people/\d+/accounts/\d+',
    // all positions for a given person
    'people/\d+/positions',
    //'people/\d+/positions/\d+'
  );
  public static function to_resource($collection, $id, $subcollection = NULL, $subcollection_id = NULL) {

    //see also: https://restful-api-design.readthedocs.org/en/latest/urls.html
    $resource_url = array_values(array(
      $collection,
      $id,
      $subcollection,
      $subcollection_id
    ));
    $result = implode('/', $resource_url);
    foreach (self::$valid_paths as $regex) {
      if (preg_match('|' . $regex . '|', $result)) {
        return implode('/', array(
            self::$host,
            self::$base_resource_uri,
            $result,
          ));
      }
    }
    throw new \Exception("token data could not be parsed into a url");
  }
  public static function from_resource($uri) {}
}

