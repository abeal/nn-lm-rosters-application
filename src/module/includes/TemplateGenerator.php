<?php
namespace NNLM\Rosters;
use NNLM\Rosters\Twig\Extension as RostersTwigExtension;
use NNLM\Rosters\Exception\System as RostersSystemException;
use NNLM\Rosters\Exception\Token as RostersTokenException;
use NNLM\Rosters\Twig\Environment as RostersTwigEnvironment;
use NNLM\Rosters\RestSerializer as RestSerializer;
use NNLM\Rosters\Utilities as U;

/**
 *  @class Template
 *  This class is responsible for generating output on demand.  It uses the
 *  token_data argument generated by rosters_tokenizer as parameters to
 *  choose the correct template. It then utilizes the Twig library (with
 *  a few custom plugins) to retrieve and display rosters data.
 *
 *  Twig was chosen in order to be as fully
 *  compatible as possible for the move to drupal 8.  Any rules allowed in
 *  Twig are allowed in the templates for this module.
 *
 */
class TemplateGenerator {
  // passed row data.  Needs global storage as I can't add hooks to the parser callback functions
  private $rowdata = NULL;
  // the final output.  Needs global storage as I can't add hooks to the parser callback functions
  private $output = '';
  //file path to template files.  Set in constructor
  private $template_path = NULL;
  //populated once the template is loaded.  See the constructor for what this
  //object will look like.
  private $chosen_template = NULL;
  //populated with actual template data once a template is chosen

  //a representation of all templates in the filesystem.  Populated in constructor.
  private $defined_templates = array();

  public static $fa_iconmap = array(
    "drupal" => "fa fa-drupal",
    "nnlm_user" => "fa fa-user-secret",
    "shell" => "fa fa-terminal",
    "email" => "fa fa-envelope",
    "phone" => "fa fa-phone-square",
    "fax" => "fa fa-fax",
    "nnlm_mysql" => "fa fa-user-secret",
    "facebook" => "fa fa-facebook-official",
    "skype" => "fa fa-skype",
    "twitter" => "fa fa-twitter",
    "wordpress" => "fa fa-wordpress",
    "linkedin" => "fa fa-linkedin-square",
    "staffwiki" => "fa fa-user-secret",
    "moodle" => "fa fa-mortar-board",
    "googleplus" => "fa fa-google-plus-square",
    "ntrpuser" => "fa fa-user-secret",
  );
  public static $vcard_map = array(
    'phone' => 'tel',
    'email' => 'email',
  );

  /**
   * Creates a new template object
   *
   * @param string $template_name The name of the template file to load
   */
  public function __construct() {
    //U::dump("construct", __CLASS__);
    $this->template_path = join(DIRECTORY_SEPARATOR, array(
        drupal_get_path('module', 'nnlm_rosters'),
        'templates',
      ));
    if (!is_dir($this->template_path)) {
      throw new RostersSystemException("Rosters templates folder not found.");
    }
    /*
    Open the directory containing the templates, and parse the names of the templates.     */



    if ($dh = opendir($this->template_path)) {
      while (($file_name = readdir($dh)) !== FALSE) {
        $file_path = join(DIRECTORY_SEPARATOR, array($this->template_path, $file_name));
        if (filetype($file_path) === 'dir') {
          continue;
        }
        $template_name = preg_replace("/\..+$/", '', $file_name);
        //set default values
        $this->defined_templates[$template_name] = new \stdClass();
        $this->defined_templates[$template_name]->file_name = $file_name;
        $this->defined_templates[$template_name]->file_path = $file_path;
      }
      closedir($dh);
    }
  }

  /**
   * Loads the requested template file. Expects the file to live in
   * a subfolder called "templates" relative to the current file.  Throws
   * an exception if any problems with loading occur - call in a try/catch
   * block.  Template is loaded into $this->template, Name into $this->template_name
   *
   * @param  array $token_data Defines the template to be used.  Must contain
   *                           an entry for TemplateGenerator::TEMPLATE_TYPE at the
   *                           very least
   *
   * @return array A drupal render array.
   * @throws Exception If Any problems with file loading occur.
   */
  public function render($token_string) {
    // a map to classnames used by fontawesome
    $tokenizer = new Tokenizer();
    $token_data = $tokenizer->tokenize($token_string);
    U::dump($token_data, "Token string: $token_string, Token data");
    if (!isset($this->defined_templates[$token_data->template])) {
      throw new RostersTokenException("No template type available for token type " . $token_data->template);
    }
    $template = $this->defined_templates[$token_data->template];
    $twig_loader = new \Twig_Loader_Filesystem($this->template_path);
    $twig = new \Twig_Environment($twig_loader, array(
        'debug' => TRUE,
        // ...
      ));
    $twig->addExtension(new \Twig_Extension_Debug());
    $twig->getExtension('core')->setDateFormat('Y-m-d', '%d days');
    $markup = &drupal_static(__FUNCTION__);
    if (!$markup) {
      if ($markup = cache_get('nnlm_rosters_token_cache')) {
        $markup = $cache->data;
      }
      else {
        $resave_cache = TRUE;
        $markup = array();
      }
    }
    $cache_key = md5(serialize($token_data));
    if (!isset($markup[$cache_key])) {
      try {
        //NOTE: data is json-encoded.  This is not currently necessary, but
        //has been added in anticipation of moving rosters data to a
        //consumable REST api.
        $data = DataProvider::getData($token_data->path, $token_data->options);
        $data = json_decode($data, TRUE);
        //U::dump($data, "Data");
        $data['options'] = $token_data->options;
        $twig_template = $twig->loadTemplate($template->file_name);
        $markup[$cache_key] = $twig_template->render($data);
        $resave_cache = TRUE;
      }
      catch(\Exception$e) {
        return array("#markup" => "<div style='color: #600; background-color: #fee'><h2>Exception</h2><p>" . $e->getMessage() . '</p></div>');
      }
    }
    else {
      //U::dump("Token cache hit");
    }
    if ($resave_cache) {
      cache_set('nnlm_rosters_token_cache', $markup, 'cache_token', NNLM_ROSTERS_CACHE_LIFETIME);
    }
    return array(
      '#markup' => $markup[$cache_key],
    );
  }
}

