<?php
namespace NNLM\Rosters\Exception;
/** @class User
 * user exception for errors that arise when there is a problem
 * with the rosters code that is related to user action.
 */

class User extends Exception {

}
