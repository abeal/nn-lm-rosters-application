<?php
namespace NNLM\Rosters\Exception;
/**
 * @file Exceptions custom to the nnlm_rosters module
 */

/**
 * @class Exception
 * Generic exception for errors that arise from the nnlm_rosters module
 */
class Exception extends \Exception {
  // Redefine the exception so message isn't optional
  public function __construct($message, $code = 0, \Exception $previous = NULL) {
    // some code

    // make sure everything is assigned properly
    parent::__construct($message, $code, $previous);
  }

  // custom string representation of object
  public function __toString() {
    return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
  }
}
