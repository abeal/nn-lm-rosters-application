<?php
namespace NNLM\Rosters;
use NNLM\Rosters\Exception\Exception as RostersException;
use NNLM\Rosters\Exception\Token as RostersTokenException;
use NNLM\Rosters\Exception\System as RostersSystemException;
use NNLM\Rosters\Utilities as U;

/**
 * @class
 * Performs tokenizing of the input string, sorting tokens into
 * the appropriate token type (often depends on other tokens
 * present), and assembling in a fashion to determine the
 * appropriate TemplateGenerator to create (getTemplate method)
 *
 * Note: there are two separate tokenizers in this project.  The latter
 * is a Twig extension class, responsible for tokenizing certain tags
 * within a Twig template.  This, by contrast, is responsible only for
 * tokenizing input strings from Drupal fields.
 */
class Tokenizer {
  const TOKEN_UNDEFINED = -1;
  // a collection token.
  const TOKEN_COLLECTION = 1;
  // the default value for a token if none is supplied
  const TOKEN_DEFAULT_VALUE = 2;
  // an option token
  const TOKEN_OPTION = 4;
  // an option only for people displays
  const TOKEN_PEOPLE_OPTION = 8;
  // an option only for lists
  const TOKEN_LISTS_OPTION = 16;
  // not necessary?
  //const TOKEN_OPTION_SINGLE = 32;
  // allows multiple values to be assigned
  const TOKEN_OPTION_MULTIPLE = 64;
  const TOKEN_VALUE = 128;

  private $defined_collections = array();
  //allowed variables for the query string portion.  These variables modify
  //both the data retrieved from the collection type, and how that data
  //is displayed.  The first variable in each of the provided options
  //will be the default, if not otherwise set.
  private $defined_options = array();
  private $defined_fields = array(
    'address' => FALSE,
    'institution' => FALSE,
    'department' => FALSE,
    'name' => FALSE,
    'accounts' => FALSE,
    'picture' => FALSE,
    'positions' => FALSE,
    'list_title' => FALSE,
    'list_description' => FALSE,
    'sublist_title' => FALSE,
    'sublist_description' => FALSE,
  );
  //'fields' is token shorthand for show/hide options.  set this up here
  private $field_presets = array();
  public function __construct() {
    $this->defined_tokens = array(
      //Types
      'lists' => self::TOKEN_COLLECTION,
      'people' => self::TOKEN_COLLECTION,
      'regions' => self::TOKEN_COLLECTION,
      'roles' => self::TOKEN_COLLECTION,
      'display' => self::TOKEN_OPTION,
      'show_positions' => self::TOKEN_OPTION,
      'fields' => self::TOKEN_OPTION,
      'sort' => self::TOKEN_OPTION,
      'group' => self::TOKEN_OPTION,
      'show' => self::TOKEN_OPTION | self::TOKEN_LISTS_OPTION | self::TOKEN_OPTION_MULTIPLE,
      'hide' => self::TOKEN_OPTION | self::TOKEN_LISTS_OPTION | self::TOKEN_OPTION_MULTIPLE,
      //populated by rosters_validation
    );
    $this->defined_options = array(
      //changes flow of elements only
      'display' => array(
        'inline' => self::TOKEN_VALUE | self::TOKEN_DEFAULT_VALUE,
        'block' => self::TOKEN_VALUE,
        'table' => self::TOKEN_VALUE,
      ),
      //some people require display of the non-primary position, but that should be a special circumstance.
      'show_positions' => array(
        'primary' => self::TOKEN_VALUE | self::TOKEN_DEFAULT_VALUE,
        'all' => self::TOKEN_VALUE,
      ),
      //shorthand - modifies 'show' and 'hide' variables
      'fields' => array(
        'terse' => self::TOKEN_VALUE,
        'short' => self::TOKEN_VALUE,
        'all' => self::TOKEN_VALUE,
      ),
      //currently cannot set both 'sort' and 'group'
      'sort' => array(
        'lastname' => self::TOKEN_VALUE | self::TOKEN_DEFAULT_VALUE,
        'firstname' => self::TOKEN_VALUE,
      ),
      //currently cannot set both 'sort' and 'group'
      'group' => array(
        'none' => self::TOKEN_VALUE | self::TOKEN_DEFAULT_VALUE,
        'region' => self::TOKEN_VALUE,
      )
    );
    //define 'hide' and 'show' options for all field definitions
    foreach ($this->defined_fields as $k => $v) {
      $this->defined_options['show'][$k] = self::TOKEN_VALUE;
      $this->defined_options['hide'][$k] = self::TOKEN_VALUE;
    }
    //preset field arrangements for easy declaration of field visibility in bulk
    $this->field_presets['terse'] = array(
        'name' => TRUE,
        'accounts' => TRUE,
        'list_title' => TRUE,
      ) + $this->defined_fields;
    $this->field_presets['short'] = array(
        'positions' => TRUE,
        'institution' => TRUE,
        'department' => TRUE,
      ) + $this->field_presets['terse'];
    $this->field_presets['all'] = array(
        'picture' => TRUE,
        'address' => TRUE,
      ) + $this->field_presets['short'];
  }
  private static function verify($value, $token_type) {
    return (($value & $token_type) === $token_type);
  }

  /**
   * Tokenizes a string of data provided by rosters.tokens.inc hook_tokens
   *
   * @param string $token_string The string of tokens.  Separated by ':'.
   * @throws Exception If an invalid token is passed.
   */
  public function tokenize($token_string = '') {
    $token_data = new \stdClass();
    // stores original
    $token_data->token_string = $token_string;
    //set defaults
    $token_data->path = self::TOKEN_UNDEFINED;
    $token_data->template = self::TOKEN_UNDEFINED;
    $token_data->options = array();
    //field_list will keep a running tally of which fields will end up being displayed.
    $field_list = $this->field_presets['terse'];

    /* A rosters token will be one of the following:
    - a REST formatted request url, consisting of a request for a collection, an id for the collection, and optionally a subcollection and subcollection id.  Example: 'people/4003' would be a request for the staff account with the PeopleID of 4003.  The id parameter will correspond to the primary key for whatever table is the base table for the type being served.  See the query classes for more information, as well as https://restful-api-design.readthedocs.org/en/latest/urls.html
    - 'options' - a set of options, modifying how the output is presented.  These will be in key=value format in the query string portion of the url. */
    $tokens = explode(':', $token_string);
    if (empty($tokens)) {
      return array();
    }
    // the first token passed must always be a collection.
    $collections_token = array_shift($tokens);
    if ((stripos($token, '/') !== FALSE)) {
      throw new \Exception("The first token must be a collections token.  If it is, it is malformed.");
    }
    foreach (explode('/', $collections_token) as $part) {
      if (empty($part)) {
        continue;
      }
      if (preg_match("/^\d+$/", $part)) {
        //a collection identifier - skip
        continue;
      }
      if (!isset($this->defined_tokens[$part])) {
        throw new \Exception("'$part' is not a valid collection or collection id.");
      }
      if (!self::verify($this->defined_tokens[$part], self::TOKEN_COLLECTION)) {
        throw new \Exception("The passed collection type token '$part' is not valid.");
      }
      if ($token_data->template === self::TOKEN_UNDEFINED) {
        //the check is necessary because the first encountered collections token defines the template.  There may be subcollection tokens here.
        $token_data->template = $part;
      }
      continue;
    }
    //if at this point, the collections token has passed muster.
    $token_data->path = $collections_token;


    /* the remainder are options tokens that modify what and how information is presented.  The
    tokenizer actually passes a list of fields to display (from the above $this->defined_fields variable) - many of the passed tokens are converted here to change the visibility of said fields. */
    //parse all option tokens and store.
    foreach ($tokens as $token) {
      //this is an option token
      list($key, $values) = explode('=', $token);
      $values = explode(',', $values);
      if (!array_key_exists($key, $this->defined_tokens)) {
        throw new RostersTokenException("Invalid token '$key'.");
      }
      if (count($values) > 1) {
        if (!self::verify($this->defined_tokens[$key], self::TOKEN_OPTION_MULTIPLE)) {
          throw new RostersTokenException("Only one value is allowed for the options token  '$key'.");
        }
      }
      foreach ($values as $v) {
        if (!array_key_exists($v, $this->defined_options[$key])) {
          throw new RostersTokenException("Invalid options token value '$v'.");
        }
        if (!self::verify($this->defined_options[$key][$v], self::TOKEN_VALUE)) {
          throw new RostersTokenException("Invalid options token value '$v'.");
        }
        $token_data->options[$key][] = $v;
      }
    }

    //use the 'field' option to modify field default display.
    if (isset($token_data->options['fields'])) {
      $field_preset_name = array_pop($token_data->options['fields']);
      unset($token_data->options['fields']);
      if (!isset($this->field_presets[$field_preset_name])) {
        throw new \Exception("'$field_preset_name' is not a valid field preset");
      }
      //use the field list to set default options
      foreach ($this->field_presets[$field_preset_name] as $key => $bool) {
        $field_list[$key] = $bool;
      }
    }
    //use 'show' and 'hide' to further change field display
    if (isset($token_data->options['show'])) {
      foreach ($token_data->options['show'] as $key) {
        $field_list[$key] = TRUE;
      }
    }
    //TODO: add checks for conflicts between show and hide?
    if (isset($token_data->options['hide'])) {
      foreach ($token_data->options['hide'] as $key) {
        $field_list[$key] = FALSE;
      }
    }
    //add the final field list to options
    //note that the 'fields' entry is fundamentally changing the nature of what is stored during this step.  Prior to this, it was a string that denoted a preset arrangement of field visibilities.  Subsequent to this, it will be an array of those field values individually, with boolean flags indicating whether or not they are to be displayed.
    foreach ($field_list as $name=>$bool) {
      if($bool){
        $token_data->options['fields'][]= $name;
      }
    }
    //set default options (first one in each collection is the default)
    //note that we're passing default show/hide and field options here, but they
    //won't be evaluated by anything further on.  only
    foreach ($this->defined_options as $key => $arr) {
      foreach ($arr as $value => $bitmask) {
        if (self::verify($bitmask, self::TOKEN_DEFAULT_VALUE)) {
          if (!isset($token_data->options[$key]) || empty($token_data->options[$key])) {
            $token_data->options[$key][] = $value;
          }
        }
      }
    }
    //final checks
    if ($token_data->path === self::TOKEN_UNDEFINED) {
      throw new RostersTokenException("Required path attribute was not found");
    }
    if ($token_data->template === self::TOKEN_UNDEFINED) {
      throw new RostersTokenException("No valid template could be determined for this token string");
    }

    return $token_data;
  }
}

