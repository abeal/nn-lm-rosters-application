<?php
namespace NNLM\Rosters;
use NNLM\Rosters\Exception\System as RostersSystemException;
/**
 * Helper class for functions used across this module.
 */
class Utilities {
  private static $has_firephp = FALSE;
  public static function msg($msg, $label, $level = NULL) {
    if (self::$has_firephp) {
      switch ($level) {
        case 'info':
          $level = \FirePHP::INFO;
          break;

        case 'warning':
          $level = \FirePHP::WARN;
          break;

        case 'error':
          $level = \FirePHP::ERROR;
          break;

        case 'notice':
        default:
          $level = \FirePHP::LOG;
          break;
      }
      dfb($msg, $label, \FirePHP::LOG);
      return;
    }
    nnlm_core_dump($msg, $label, $level);
  }
  public static function dump($msg = '', $label = '') {
    self::msg($msg, $label, 'notice');
  }
  public static function error($msg = '', $label = '') {
    self::msg($msg, $label, 'error');
  }
  public static function warn($msg = '', $label = '') {
    self::msg($msg, $label, 'warning');
  }
  public static function info($msg = '', $label = '') {
    self::msg($msg, $label, 'message');
  }
}

