<?php
namespace NNLM\Rosters;
use NNLM\Rosters\Exception\Exception as RostersException;
use NNLM\Rosters\Twig\Extension as RostersTwigExtension;
use NNLM\Rosters\Exception\System as RostersSystemException;
use NNLM\Rosters\Exception\Token as RostersTokenException;
use NNLM\Rosters\Utilities as U;

/*
 * @class AccountQuery
 * Shows all accounts for a given id (PeopleID).
*/
class DataProvider {
  private static $allowedCollections = array(
    'people',
    'positions',
    'roles',
    'accounts',
    'lists',
  );
  private static function prettyPrint($json) {
    $result = '';
    $level = 0;
    $in_quotes = FALSE;
    $in_escape = FALSE;
    $ends_line_level = NULL;
    $json_length = strlen($json);

    for ($i = 0; $i < $json_length; $i++) {
      $char = $json[$i];
      $new_line_level = NULL;
      $post = "";
      if ($ends_line_level !== NULL) {
        $new_line_level = $ends_line_level;
        $ends_line_level = NULL;
      }
      if ($in_escape) {
        $in_escape = FALSE;
      }
      elseif ($char === '"') {
        $in_quotes = !$in_quotes;
      }
      elseif (!$in_quotes) {
        switch ($char) {
          case '}':
          case ']':
            $level--;
            $ends_line_level = NULL;
            $new_line_level = $level;
            break;

          case '{':
          case '[':
            $level++;
          case ',':
            $ends_line_level = $level;
            break;

          case ':':
            $post = " ";
            break;

          case " ":
          case "\t":
          case "\n":
          case "\r":
            $char = "";
            $ends_line_level = $new_line_level;
            $new_line_level = NULL;
            break;
        }
      }
      elseif ($char === '\\') {
        $in_escape = TRUE;
      }
      if ($new_line_level !== NULL) {
        $result .= "\n" . str_repeat("\t", $new_line_level);
      }
      $result .= $char . $post;
    }

    return $result;
  }
  private static function sortByLastName(&$collection){

  }
  private static function sortByRegion(&$collection){
    U::dump("Sorting by region");
    foreach ($collection as $k => & $v) {
      if (is_object($v)) {
        self::sortByRegion($v, $sort_value);
      }
      elseif (is_array($v)) {
        if($k === 'people'){
          usort($v, function($a, $b){
            if(!isset($a->PrimaryRegion) || !isset($b->PrimaryRegion)){
              return 0;
            }
            if($a->PrimaryRegion == $b->PrimaryRegion){
              return ($a->LastName < $b->LastName) ? -1 : 1;
            }
            return ($a->PrimaryRegion < $b->PrimaryRegion) ? -1 : 1;
          });
        }
        /*foreach ($v as $o) {
          self::sortByRegion($o, $sort_value);
        }*/
      }
    }
  }
  private static function sort(&$collection, $sort_value) {
    static $sorting_functions = array(
      'lastname'=>'sortByLastName',
      'region'=>'sortByRegion'
    );
    if(is_array($sort_value)){
      //only one sort value currently allowed
      $sort_value = $sort_value[0];
    }
    if($sort_value === 'none'){
      return;
    }
    if (!array_key_exists($sort_value, $sorting_functions)) {
      U::dump("Cannot sort by $sort_value - invalid value");
      return;
    }
    //U::dump($collection, "Sorting collection by $sort_value");
    call_user_func('self::'.$sorting_functions[$sort_value], $collection);
  }
  private static function group(&$collection, $group_value) {
    //sort by group value, and then apply wrappers
    if(is_array($group_value)){
      //only one group value currently allowed
      $group_value = $group_value[0];
    }
    if($group_value === 'none'){
      return;
    }
    self::sort($collection, $group_value);
  }
  /**
   * returns data from a simulated REST api.
   *
   * @param  string $token_string A uri conforming to a REST request, with the following values:
   *                              collection: one of the allowed values defined in $allowedCollections above
   *                              collection_id: a numeric value corresponding to the primary id of the
   *                              item in the collection
   *                              subcollection: one of the allowed values defined in $allowedCollections above
   *                              subcollection_id: a numeric value corresponding to the primary id of the
   *                              item in the subcollection
   *
   * @return array                A drupal render array
   */
  public static function getData($token_string, $options = array()) {
    //note - token string sanity is performed by the tokenizer
    list($collection, $collection_id, $subcollection, $subcollection_id) = explode("/", $token_string);

    $rosters_query_class = 'NNLM\\Rosters\\Query\\' . ucfirst($collection) . 'Query';
    $rosters_query = new $rosters_query_class();
    $output = new \stdClass();
    $result = $rosters_query->getData($collection_id);
    if($collection == 'lists' && isset($options['group'])){
      self::group($result, $options['group']);
    }
    //U::dump($result, "Result");
    $output->{$collection}[] = $result;
    return self::prettyPrint(json_encode($output));
  }
}

