<?php
namespace NNLM\Rosters\Query;
use NNLM\Rosters\Utilities as U;
use NNLM\Rosters\DataProvider as DataProvider;

/**
 * @class ListsQuery
 */
class ListsQuery {
  private $sorting_functions = array();

  public function __construct(){
  }
  public function getData($id, $options = array()) {
    $output = &drupal_static(__FUNCTION__);
    $resave_cache = FALSE;
    if (!$output) {
      if ($cache = cache_get('nnlm_rosters_query_results')) {
        $output = $cache->data;
      }
      else {
        $resave_cache = TRUE;
        $output = new \stdClass();
      }
    }
    if (!$output->lists) {
      $resave_cache = TRUE;
      $output->lists = new \stdClass();
    }
    if (!isset($output->{$id})) {
      $resave_cache = TRUE;
      //U::dump($id, "Retrieving list id");
      $output->lists->{$id} = new \stdClass();
      $current_record = &$output->lists->{$id};

      $list_queue = array($id);
      $connection = \Database::getConnection('default', 'rosters');
      while (!empty($list_queue)) {
        $id = array_shift($list_queue);

        $query = $connection->select('list_descriptions', 'ld')->fields('ld', array('ListID', 'ListTitle', 'ListDescription', 'ListStartDate', 'ListEndDate'));
        $query->condition('ld.ListID', $id, '=');
        $query->condition('ld.ListVisibility', 'public', '=');
        $query->condition('ld.ListStartDate', 'CURDATE()', '<');
        $query->where("((ld.ListEndDate IS NULL) OR (ld.ListEndDate > CURDATE()))");
        $db_result = $query->execute();
        if ($db_result->rowCount() === 0) {
          continue;
        }
        $row = $db_result->fetchAssoc();
        //list data
        foreach ($row as $k => $v) {
          // format like render array does, with pound sign for properties
          $current_record->{$k} = $v;
        }
        unset($db_result);
        unset($row);
        $query = $connection->select('lists', 'l')->fields('l');
        $query->innerJoin('people', 'p', 'p.PeopleID = l.ListMemberTableID');
        $query->condition('l.ListID', $id, '=');
        $query->condition('l.ListMemberTable', 'people', '=');
        $query->where("((l.ListEntryStartDate IS NULL) OR (l.ListEntryStartDate < CURDATE()))");
        $query->where("((l.ListEntryEndDate IS NULL) OR (l.ListEntryEndDate > CURDATE()))");
        $query->orderBy('p.LastName');
        $db_result = $query->execute();
        if ($db_result->rowCount() === 0) {
          continue;
        }
        $current_record->people = array();
        while ($row = $db_result->fetchAssoc()) {
          $data = json_decode(DataProvider::getData('people/' . $row['ListMemberTableID'], $options));
          $current_record->people[] = $data->people[0];
        }
        //list entry data
      }
    }
    else {
      nnlm_core_dump("Cache hit", __FUNCTION__);
    }
    if ($resave_cache) {
      cache_set('nnlm_rosters_query_results', $output, 'cache', NNLM_ROSTERS_CACHE_LIFETIME);
    }
    return $output->lists->{$id};
  }
}

