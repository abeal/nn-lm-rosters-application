<?php
namespace NNLM\Rosters\Query;
use NNLM\Rosters\Exception\Exception as RostersException;
use NNLM\Rosters\Twig\Extension as RostersTwigExtension;
use NNLM\Rosters\Exception\System as RostersSystemException;
use NNLM\Rosters\Exception\Token as RostersTokenException;
use NNLM\Rosters\TemplateGenerator as TemplateGenerator;
use NNLM\Rosters\Utilities as U;

/**
 * @class PersonQuery
 * The base class contains all the data for a person, but we need to enhance
 * that to a degree.  The decision to display a person or not in a given
 * situation can depend on a variety of factors - most notably, whether or
 * not that person has any active positions.  We do not need to search all
 * positions, as if the person has at least one active position, then they *should*
 * have an active primary position as well (which is likely the same one).
 * We do a tie to the positions table by the primary position
 * id, and use the template to make the decision when and how to display
 * inactive staff.
 */
class PeopleQuery {
  private $sorting_functions = array();
  public function __construct() {
    $sorting_functions['lastname'] =
    function ($a, $b) {
      if (!isset($a['LastName']) || !isset($b['LastName'])) {
        return 0;
      }
      return ($a['LastName'] < $b['LastName']) ? -1 : 1;
    };
    $sorting_functions['firstname'] =
    function ($a, $b) {
      if (!isset($a['FirstName']) || !isset($b['FirstName'])) {
        return 0;
      }
      return ($a['FirstName'] < $b['FirstName']) ? -1 : 1;
    };
  }
  public function getData($id, $options = array()) {

    $region_names = &drupal_static(__FUNCTION__ . '_region_names');
    if (!$region_names) {
      if ($cache = cache_get('nnlm_rosters_region_names')) {
        $region_names = $cache->data;
      }
      else {
        $region_names = array();
        $query = \Database::getConnection('default', 'general')->select('regions', 'r')->fields('r', array('Region_Code', "ShortName"));
        $db_result = $query->execute();
        if ($db_result->rowCount() === 0) {
          throw new \Exception("Could not retrieve regional info");
        }
        $region_names = $db_result->fetchAllKeyed();
        cache_set('nnlm_rosters_region_names', $region_names, 'cache', NNLM_ROSTERS_CACHE_LIFETIME);
      }
    }
    //U::dump($region_names, "Region names");
    $output = &drupal_static(__FUNCTION__);
    $resave_cache = FALSE;
    if (!$output) {
      if ($cache = cache_get('nnlm_rosters_query_results')) {
        $output = $cache->data;
      }
      else {
        $output = new \stdClass();
        $resave_cache = TRUE;
      }
    }
    if (!$output->people) {
      $output->people = new \stdClass();
      $resave_cache = TRUE;
    }
    if (!isset($output->people->{$id})) {
      $output->people->{$id} = new \stdClass();
      $current_record = &$output->people->{$id};
      $query = \Database::getConnection('default', 'rosters')->select('people', 'p')->fields('p');
      $query->condition('p.PeopleID', $id, '=');
      $query->join('positions', 'po', 'p.PeopleID = po.PeopleID');
      $query->condition('po.StartDate', 'CURDATE()', '<');
      $query->where("((po.EndDate IS NULL) OR (po.EndDate > CURDATE()))");
      foreach ($options['sort'] as $sortvalue) {
        switch ($sortvalue) {
          case 'firstname':
            $query->orderBy('FirstName', 'ASC');
            break;

          case 'lastname':
          default:
            $query->orderBy('LastName', 'ASC');
            break;
        }
      }
      $db_result = $query->execute();
      if ($db_result->rowCount() === 0) {
        return $current_record;
      }
      while ($row = $db_result->fetchAssoc()) {
        foreach ($row as $k => $v) {
          // format like render array does, with pound sign for properties
          $current_record->{$k} = $v;
        }
      }
      //add positions as sub-object
      $current_record->positions = array();
      $query = \Database::getConnection('default', 'rosters')->select('positions', 'po')->fields('po', array(
          'PositionID',
          'PositionRanking',
          'PeopleID',
          'Title',
          'Email',
          'StartDate',
          'EndDate',
          'Region',
        ));
      $query->join('people', 'p', 'p.PeopleID = po.PeopleID');
      $query->join('currentAddress', 'ca', 'po.PositionID = ca.PositionID');
      $query->fields('ca');
      $query->condition('po.PeopleID', $id, '=');
      $query->condition('po.StartDate', 'CURDATE()', '<');
      $query->where("((po.EndDate IS NULL) OR (po.EndDate > CURDATE()))");
      $query->orderBy('p.LastName');
      foreach ($options['sort'] as $sortvalue) {
        switch ($sortvalue) {
          default:
            $query->orderBy('PositionRanking', 'ASC');
            break;
        }
      }
      $db_result = $query->execute();
      while ($row = $db_result->fetchAssoc()) {
        if ($row['PositionID'] == $current_record->PrimaryPosition) {
          $row['PrimaryPosition'] = TRUE;
          $current_record->PrimaryRegion = $region_names[$row['Region']];
        }
        else {
          $row['PrimaryPosition'] = FALSE;
        }
        $current_record->positions[] = json_decode(json_encode($row));
      }
      //add accounts as sub-object
      $current_record->accounts = array();
      $query = \Database::getConnection('default', 'rosters')->select('accounts', 'a');
      $query->innerJoin('people', 'p', 'p.PeopleID = a.PeopleID');
      $query->innerJoin('account_types', 'at', 'at.AccountTypeID = a.AccountTypeID');

      $query->fields('at', array('AccountType', 'AccountTypeDescription'));
      $query->fields('a', array('AccountID', 'AccountValue'));
      $query->condition('a.AccountVisibility', 'public', '=');
      $query->condition('a.PeopleID', $id, '=');
      $query->orderBy('at.AccountType', 'ASC');
      $db_result = $query->execute();
      while ($row = $db_result->fetchAssoc()) {
        $row['AccountImageIcon'] = TemplateGenerator::$fa_iconmap[$row['AccountType']];
        $row['vcard_class'] = TemplateGenerator::$vcard_map[$row['AccountType']];
        $current_record->accounts[] = json_decode(json_encode($row));
      }
      $resave_cache = TRUE;
    }
    else {
      //nnlm_core_dump("Cache hit", __FUNCTION__);
    }
    if ($resave_cache) {
      cache_set('nnlm_rosters_query_results', $output, 'cache', NNLM_ROSTERS_CACHE_LIFETIME);
    }
    return $output->people->{$id};
    //add default conditions applicable to all person searches
  }
}

