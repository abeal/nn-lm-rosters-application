<?php
namespace NNLM\Rosters;

/**
 * Autoloads Rosters classes.
 * Adheres to the PSR-4 standard
 * @author Aron Beal <abeal@uw.edu>
 */
class Autoloader {

  /**
   * Registers Twig_Autoloader as an SPL autoloader.
   *
   * @param Boolean $prepend Whether to prepend the autoloader or not.
   */
  public static function register() {
    spl_autoload_register(array(new self, 'autoload'), TRUE, TRUE);
  }

  /**
   * Handles autoloading of classes.
   *
   * @param string $class A class name.
   */
  public static function autoload($class) {
    //print "Namespace: ".__NAMESPACE__."\n";
    $file_path = NULL;
    if (strpos($class, 'NNLM\\Rosters') === FALSE) {
      return;
    }
    $parts = explode("\\", $class.".php"); //Note: this is only a single backslash, escaped.
    //remove the NNLM\Rosters portion of the namespace - maps to
    //the includes folder
    array_shift($parts);
    array_shift($parts);
    $filename = implode(DIRECTORY_SEPARATOR, $parts);
    //nnlm_core_dump("Loading Rosters class: $class ($filename)");
    //nnlm_core_dump("Filename: $filename");
    $file_path = join(DIRECTORY_SEPARATOR, array(drupal_get_path('module', 'nnlm_rosters'),
     'includes', $filename));
    if (file_exists($file_path) && is_readable($file_path)) {
      include_once ($file_path);
    }
    else {
      throw new \Exception("File $file_path was not found, or not readable.");
    }
  }
}

