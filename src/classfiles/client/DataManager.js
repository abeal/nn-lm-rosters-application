/**
 *   @class NNLM.Rosters.DataManager
 *     <p>The DataManager is responsible for maintaining a series of stores, and  connecting
 *   to the servlets that supply them with data.</p>
 *   The data in the NNLM Rosters application is present in two places - on the server, in the
 *   rosters database, and on the client, in the form of Ext Store objects.  In order to coordinate
 *   store behavior, all stores and server communication happens within this component.  Any major
 *   user record that is interacted with by multiple components resides in one or more of the
 *   stores in this component, especially to include all data that is allowed to undergo
 *   editing.
 *   Stores may exist outside of the DataManager, but
 *   they will be simple in nature, not allowed to exist outside of whatever component
 *   created them, and read-only.</p>
 *
 *   @author     Aron Beal
 *   @copyright (c) 2008, by the University of Washington
 *   @date       May 6 2008
 *   @license All files in this web application are  licensed under the
 *   terms of the Open Source LGPL 3.0 license.
 *   License details: http://www.gnu.org/licenses/lgpl.html
 */
/*
All data stores are currently placed in the global scope. This is likely to change
once I figure out a good tradeoff between reuse of objects, and the necessity for
unique instances.
The configs are defined separately, as they may be reused in these unique instances.
*/
Ext.ns('NNLM.Rosters');
NNLM.Rosters.DataManager = function() {
    /* a series of listeners that dictate behavior.  Each listener is
    added to every store under the datamanager's control */
    var lastID = 0;
    var LISTENER_BUFFER_TIME = 100;
    var CURRENT_USER = null;
    var Listeners = {

        /* Listeners defined here are common for all stores. */
        /**
         * @event datachanged
         * Fires when the data cache has changed in a bulk manner (e.g., it has been sorted, filtered, etc.) and a
         * widget that is using this Store as a Record cache should refresh its view.
         * @param {Store} this
         */
        'datachanged': function(store) {
            _dump('datachanged event occurred', 'event');
        },

        doremove: function() {
            this.myremoved++;
        },
        dosave: function() {
            this.myremoved = 0;
        },
        doreset: function() {
            this.myremoved = 0;
        },
        /**
         * @event metachange
         * Fires when this store's reader provides new metadata (fields). This is currently only supported for JsonReaders.
         * @param {Store} this
         * @param {Object} meta The JSON metadata
         */
        'metachange': function(store, meta) {
            _dump('metachange event occurred', 'event');
        },

        /**
         * @event add
         * Fires when Records have been {@link #add}ed to the Store
         * @param {Store} this
         * @param {Ext.data.Record[]} records The array of Records added
         * @param {Number} index The index at which the record(s) were added
         */
        'add': function(store, record, addedIndex) {
            _dump('add event occurred.  store: ' + store.storeName + ', index: ' + addedIndex + ', dirty: ' + store.isDirty(), 3);
            if (store.isDirty()) {
                NNLM.Rosters.DisplayManager.notify("datachanged", true);
            }
        },

        /**
         * @event remove
         * Fires when a Record has been {@link #remove}d from the Store
         * @param {Store} this
         * @param {Ext.data.Record} record The Record that was removed
         * @param {Number} index The index at which the record was removed
         */
        'remove': function(store, record, removedIndex) {
            _dump('remove event occurred.    store: ' + store.storeName, 'event');
            store.fireEvent('doremove');
            if (store.isDirty()) {
                NNLM.Rosters.DisplayManager.notify("datachanged", true);
            }
        },

        /**
         * @event update
         * Fires when a Record has been updated
         * @param {Store} this
         * @param {Ext.data.Record} record The Record that was updated
         * @param {String} operation The update operation being performed.  Value may be one of:
         *
         *   Ext.data.Record.EDIT
         *   Ext.data.Record.REJECT
         *   Ext.data.Record.COMMIT
         *
         */
        'update': function(store, record, op) {
            _dump('update', 'event');
            _dump('Record updated: ' +
                record.get(NNLM.Rosters.DataManager.getStoreKey(store.storeName)) + ',  Store: ' + store.storeName + ", op: " + op, 2);
            if (op === Ext.data.Record.EDIT && store.isDirty()) {
                NNLM.Rosters.DisplayManager.notify("datachanged", true);
            }
            /*else if(op === Ext.data.Record.REJECT){
				NNLM.Rosters.DisplayManager.notify("afterreset");
			}
			else{
				NNLM.Rosters.DisplayManager.notify("aftersave");
            }*/
            //return true;
        },

        /**
         * @event clear
         * Fires when the data cache has been cleared.
         * @param {Store} this
         */
        'clear': function(store) {
            _dump('clear event occurred', 'event');
        },

        /**
         * @event beforeload
         * Fires before a request is made for a new data object.  If the beforeload handler returns
         * false the {@link #load} action will be canceled.
         * @param {Store} this
         * @param {Object} options The loading options that were specified (see {@link #load} for details)
         */
        'beforeload': function(store, loadOptions) {
            _dump('beforeload event occurred', 'event');
        },

        /**
         * @event load
         * Fires after a new set of Records has been loaded.
         * @param {Store} this
         * @param {Ext.data.Record[]} records The Records that were loaded
         * @param {Object} options The loading options that were specified (see {@link #load} for details)
         */
        'load': function(store, records, options) {
            _dump('Load event. Store: ' + store.storeName + ', extid: ' + options.extid, 'event');
            if (isset(Callbacks[options.extid])) {
                var c = Callbacks[options.extid];
                c.tlc--;
                _dump("TLC for store " + store.storeName + ', count: ' + c.tlc +
                    ', extid: ' + c.extid, 3);
                c.success = c.success && true;
                if (c.tlc === 0) {
                    _dump("TLC: all stores loaded for id " + c.extid + ".  Final callback", 3);
                    if (!c.success) {
                        _dump("Callback FAILED!", 'warn');
                    }
                    c.callback(c.success, records);
                    delete c.callback;
                    delete Callbacks[c.extid];
                }
            }
            NNLM.Rosters.DisplayManager.notify('afterload');
        },
        /**
         * @event loadexception
         * Fires if an exception occurs in the Proxy during loading.
         * Called with the signature of the Proxy's "loadexception" event.
         */
        'exception': function(proxy, type, action, options, response, arg) {
            //
            var error = '';
            console.log(response, 'info');
            //if(response.error !== undefined){error = response.error;}
            if (response.responseText !== undefined) {
                error = JSON.parse(response.responseText).error;
            } else if (response.raw !== undefined) {
                //if(response.raw.error !== undefined){error = response.raw.error;}
                //else if(response.raw.results !== undefined && response.raw.results.error !== undefined){error = response.raw.results.error;}
            }
            //else if(response.statusText !== undefined){error = response.statusText;}   
            else {
                error = "Unknown error";
            }
            alert(error);
            var servlet = proxy.api[action].url.substring(proxy.api[action].url.lastIndexOf('/') + 1);
            NNLM.Rosters.DisplayManager.err(error + "<br /><!--<span class='metadata'>Servlet: " + servlet + "</span>-->");
            if (isset(options.params.extid) && isset(Callbacks[options.params.extid])) {
                var c = Callbacks[options.params.extid];
                if (c !== undefined) {
                    c.tlc--;
                    _dump("TLC for store " + c.storeName + ', count: ' + c.tlc +
                        ', extid: ' + c.extid, 3);
                    c.success = false;
                    if (c.tlc === 0) {
                        _dump("TLC: all stores loaded for id " + c.extid + ", with exception.  Final callback", 1);
                        c.callback(false, null);
                        delete c.callback;
                        delete Callbacks[c.extid];
                    }
                }
                if (NNLM.Rosters.DataManager.STORES[Callbacks[options.params.extid].storeName].isDirty()) {
                    NNLM.Rosters.DisplayManager.notify("datachanged", true);
                }
            }
        },
        'save': function(store, batch, data) {
            _dump('save.   store: ' + store.storeName, 'event');
            store.fireEvent('dosave');
            NNLM.Rosters.DisplayManager.notify("datachanged", false);
            NNLM.Rosters.DisplayManager.notify('aftersave');
        },
        'write': function(store, action, dataResult, res, rs) {
            _dump('store ' + store.storeName + ' write', 'event');
            NNLM.Rosters.DisplayManager.notify("datachanged", false);
            NNLM.Rosters.DisplayManager.notify('aftersave');
            if (res.success) {
                _dump('store ' + store.storeName + ' committing changes!', 2);
                store.commitChanges();
            } else {
                _e("DataManager Write Error!");
                NNLM.Rosters.DataManager.reset(store.storeName);
            }
            if (isset(res.raw.extid) && isset(Callbacks[res.raw.extid])) {
                var c = Callbacks[res.raw.extid];
                c.tlc--;
                _dump("TLC for store " + c.storeName + ', count: ' + c.tlc +
                    ', extid: ' + c.extid, 3);
                c.success = c.success && true;
                if (c.tlc === 0) {
                    _dump("TLC: all stores written to for id " + c.extid + ".  Final callback", 3);
                    _dump("callback: " + c.callback, 3);
                    c.callback(c.success, rs);
                    _dump("aftercallback: " + c.callback, 3);
                    delete c.callback;
                    delete Callbacks[c.extid];
                }
            }
        },
        /**
         * @event saveexception
         * Fires when a network save exception occurs.
         * @param {DirectProxy} proxy
         * @param {Object} dataResult
         * @param {Ext.Direct.ExceptionEvent}
         */
        'saveexception': function(proxy, dataResult, exception) {
            _dump('saveexception event occurred', 'event');
        },

        /**
         * @event beforedestroy
         * Fires before a record will be destroyed
         * @param {Store} this
         * @param {Record/Record[]} rs, record(s) to be destroyed
         */
        'beforedestroy': function(store, rs) {
            _dump('beforedestroy', 'event');
        },

        /**
         * @event destroy
         * Fires after a record has been destroyed
         * @param {Store} this
         * @param {Object} dataResult
         * @param {Ext.Direct.Event} response
         */
        'destroy': function(store, dataResult, response) {
            _dump("destroy.", 'event');
        },

        /**
         * @event destroyexception
         * Fires when a destroy exception occurred
         * @param {Store} this
         * @param {Ext.data.Record[]} rs
         * @param {Ext.Direct.Event} response
         */
        'destroyexception': function(store, rs, response) {
            _dump('destroyexception event occurred: ' + NNLM.printObject(response), 'event');
        },

        /**
         * @event beforecreate
         * Fires before a network create request occurs
         * @param {Store} this
         * @param {Record/Record[]}
         */
        'beforecreate': function(store, record) {
            _dump(' beforecreate', 'event');
        },

        /**
         * @event create
         * Fires after network create request occurs
         * @param {Store} this
         * @param {Object} dataResult
         * @param {Ext.Direct.ExceptionEvent} response
         */
        'create': function(store, dataResult, response) {
            _dump("Event: create. committing changes", 'event');
            store.commitChanges();
            NNLM.Rosters.DisplayManager.notify('aftersave');
        },

        /**
         * @event createexception
         * Fires after network create exception occurs
         * @param {DirectProxy} this
         * @param {Record} record
         * @param {Ext.Direct.ExceptionEvent}
         */
        'createexception': function(proxy, record, exception) {
            _dump('createexception event occurred: ' + exception, 'event');
        }
    };
    /* a series of listeners that dictate behavior.  Each listener is
    added to every store proxy under the datamanager's control */
    var ProxyListeners = {
        /**
         * @event beforeload
         * Fires before a request to retrieve a data object.
         * @param {DataProxy} this The proxy for the request
         * @param {Object} params The params object passed to the {@link #request} function
         */
        'beforeload': function(proxy, params) {
            //_dump('proxy beforeload called', 3);
        },

        /**
         * @event load
         * Fires before the load method's callback is called.
         * @param {DataProxy} this The proxy for the request
         * @param {Object} o The request transaction object
         * @param {Object} options The callback's options property as passed to the {@link #request} function
         */
        'load': function(proxy, o, options) {
            var servlet = proxy.api.read.url.substring(proxy.api.read.url.lastIndexOf('/') + 1);
            if (servlet === 'LoadList.servlet.php') {
                //
            }
            //_dump('proxy load called. proxy: ' + servlet, 'focus');
        },
        /**
         * @event exception
         * Fires if an exception occurs in the Proxy during a remote request.
         * This event is relayed through a corresponding
         * {@link Ext.data.Store}.{@link Ext.data.Store#exception exception},
         * so any Store instance may observe this event.
         * See also:
         * http://www.extjs.com/deploy/dev/docs/?class=Ext.data.DataProxy
         */
        /*'exception': function(proxy, type, action, options, response, arg){	
				//handled in the store listener.
        },*/
        /**
         * @event beforewrite
         * Fires before a request is generated for one of the actions Ext.data.Api.actions.create|update|destroy
         * @param {DataProxy} this The proxy for the request
         * @param {String} action [Ext.data.Api.actions.create|update|destroy]
         * @param {Record/Array[Record]} rs The Record(s) to create|update|destroy.
         * @param {Object} params The request params object.  Edit params to add parameters to the request.
         */
        'beforewrite': function(proxy, action, r, params) {
            _dump(' proxy beforewrite', 'event');
        },

        /**
         * @event write
         * Fires before the request-callback is called
         * @param {DataProxy} this The proxy that sent the request
         * @param {String} action [Ext.data.Api.actions.create|upate|destroy]
         * @param {Object} data The data object extracted from the server-response
         * @param {Object} response The decoded response from server
         * @param {Record/Record{}} rs The records from Store
         * @param {Object} options The callback's options property as passed to the {@link #request} function
         */
        'write': function(proxy, action, data, response, r, options) {
            /*for(var k in data){
                if(isset(r.data[k])){
                    r.set(k, data[k]); //override any local values with updates from the server.
                }
            }*/
            //
        }
    };
    /* Configurations define what store records look like, and mirror
    database fields. While in cases it would have been cleaner to name
    entries in the configurations with different names or different
    casing, the entries here echo the database side fields so that the
    mapping will be clear when revisiting this code later. */
    var Configurations = {
        /* matches the table rosters.people */
        people: [{
            fieldLabel: false,
            name: 'PeopleID',
            type: 'string'
        }, {
            name: 'Prefix',
            type: 'string'
        }, {
            fieldLabel: 'First Name',
            name: 'FirstName',
            type: 'string'
        }, {
            fieldLabel: 'Middle Name',
            name: 'MiddleName',
            type: 'string'
        }, {
            fieldLabel: 'Last Name',
            name: 'LastName',
            type: 'string'
        }, {
            name: 'Suffix',
            type: 'string'
        }, {
            fieldLabel: 'Primary Position',
            name: 'PrimaryPosition',
            type: 'string'
        }, {
            fieldLabel: 'Picture URL',
            name: 'Picture',
            type: 'string'
        }, {
            fieldLabel: 'Date Last Updated',
            name: 'LastUpdt',
            type: 'string',
            dateFormat: 'Y-m-d'
        }, {
            fieldLabel: "User List",
            name: 'MyListID',
            type: 'string'
        }],
        /* matches the table rosters.positions */
        positions: [{
            fieldLabel: false,
            name: "PositionID",
            type: "string"
        }, {
            fieldLabel: 'Position Visibility',
            name: "PositionVisibility",
            type: "string"
        }, {
            fieldLabel: 'Position Type',
            name: "PositionRanking",
            type: "string"
        }, {
            fieldLabel: false,
            name: "PeopleID",
            type: "string"
        }, {
            name: "Title",
            type: "string"
        }, {
            name: "Email",
            type: "string"
        }, {
            fieldLabel: 'Start Date',
            name: "StartDate",
            type: "date",
            dateFormat: 'Y-m-d'
        }, {
            fieldLabel: 'End Date',
            name: "EndDate",
            type: "date",
            dateFormat: 'Y-m-d'
        }, {
            name: "Region",
            type: "string"
        }, {
            name: "LIBID",
            type: "string"
        }, {
            fieldLabel: 'Institution',
            name: "INST_INST",
            type: "string"
        }, {
            fieldLabel: 'Department',
            name: "DEPT_INST",
            type: "string"
        }, {
            fieldLabel: 'Street',
            name: "STREET_INST",
            type: "string"
        }, {
            fieldLabel: 'Street 2',
            name: "STREET2_INST",
            type: "string"
        }, {
            fieldLabel: 'City',
            name: "CITY_INST",
            type: "string"
        }, {
            fieldLabel: 'State',
            name: "STATE_ETC_CODE_INST",
            type: "string"
        }, {
            fieldLabel: 'Zip',
            name: "ZIP_MAIL_CODE_INST",
            type: "string"
        }, {
            fieldLabel: 'County',
            name: "COUNTY_INST",
            type: "string"
        }, {
            fieldLabel: 'Province',
            name: "PROVINCE_INST",
            type: "string"
        }, {
            fieldLabel: 'Telephone Country Code',
            name: "PHONE_COUNTRY_CODE_INST",
            type: "string"
        }, {
            fieldLabel: 'Telephone Area Code',
            name: "PHONE_AREA_CODE_INST",
            type: "string"
        }, {
            fieldLabel: 'Telephone',
            name: "PHONE_INST",
            type: "string"
        }, {
            fieldLabel: 'Telephone Extension',
            name: "PHONE_EXT_INST",
            type: "string"
        }, {
            fieldLabel: 'Date this record was last updated',
            name: "LastUpdt",
            type: "date",
            dateFormat: 'Y-m-d'
        }],
        role_types: [{
            fieldLabel: 'Role Type',
            name: 'RoleType',
            type: 'string'
        }, {
            fieldLabel: 'Role Description',
            name: 'RoleDescription',
            type: 'string'
        }, {
            name: 'obsolete',
            type: 'boolean'
        }],
        roles: [{
            name: 'RoleID',
            type: 'string'
        }, {
            name: 'RoleType',
            type: 'string'
        }, {
            name: 'RoleDescription',
            type: 'string'
        }, {
            name: 'PositionID',
            type: 'string'
        }, {
            name: 'Scope',
            type: 'string'
        }],
        accounts: [{
            name: 'AccountID',
            type: 'string'
        }, {
            name: 'PeopleID',
            type: 'string'
        }, {
            name: 'AccountTypeID',
            type: 'string'
        }, {
            fieldLabel: 'Account Visibility',
            name: 'AccountVisibility',
            type: 'string'
        }, {
            fieldLabel: 'Account Value',
            name: 'AccountValue',
            type: 'string'
        }, {
            fieldLabel: 'Account Comments',
            name: 'AccountComments',
            type: 'string'
        }, {
            fieldLabel: 'Account Verified',
            name: 'AccountVerified',
            type: 'int'
        }],
        /* matches part of the table docline.PUBLIC_LIBRARY_DOCUSERS. */
        docline: [{
            name: "PeopleID",
            type: "string"
        }, {
            name: "LIBID",
            type: "string"
        }, {
            fieldLabel: 'Institution',
            name: "INST_INST",
            type: "string"
        }, {
            fieldLabel: 'Department',
            name: "DEPT_INST",
            type: "string"
        }, {
            fieldLabel: 'Street',
            name: "STREET_INST",
            type: "string"
        }, {
            fieldLabel: 'Street 2',
            name: "STREET2_INST",
            type: "string"
        }, {
            fieldLabel: 'City',
            name: "CITY_INST",
            type: "string"
        }, {
            fieldLabel: 'State',
            name: "STATE_ETC_CODE_INST",
            type: "string"
        }, {
            fieldLabel: 'Zip',
            name: "ZIP_MAIL_CODE_INST",
            type: "string"
        }, {
            fieldLabel: 'County',
            name: "COUNTY_INST",
            type: "string"
        }, {
            fieldLabel: 'Province',
            name: "PROVINCE_INST",
            type: "string"
        }, {
            fieldLabel: 'Telephone Country Code',
            name: "PHONE_COUNTRY_CODE_INST",
            type: "string"
        }, {
            fieldLabel: 'Telephone Area Code',
            name: "PHONE_AREA_CODE_INST",
            type: "string"
        }, {
            fieldLabel: 'Telephone',
            name: "PHONE_INST",
            type: "string"
        }, {
            fieldLabel: 'Telephone Extension',
            name: "PHONE_EXT_INST",
            type: "string"
        }],
        list_descriptions: [{
            name: 'ListID',
            type: "string"
        }, {
            name: 'ListTitle',
            type: 'string'
        }, {
            name: 'ListDescription',
            type: 'string'
        }, {
            name: 'ListVisibility',
            type: 'string'
        }, {
            name: 'ListStartDate',
            type: 'date',
            dateFormat: 'Y-m-d'
        }, {
            name: 'ListEndDate',
            type: 'date',
            dateFormat: 'Y-m-d'
        }, {
            name: 'Locker',
            type: "boolean"
        }],
        lists: [{
            name: 'ListEntryID',
            type: 'string'
        }, {
            name: 'ListID',
            allowBlank: false,
            type: 'string'
        }, {
            name: 'ListMemberTable',
            allowBlank: false,
            type: 'string'
        }, {
            name: 'ListMemberTableID',
            allowBlank: false,
            type: 'string'
        }, {
            name: 'ListEntryStartDate',
            type: 'date',
            dateFormat: 'Y-m-d'
        }, {
            name: 'ListEntryEndDate',
            type: 'date',
            dateFormat: 'Y-m-d'
        }, {
            name: 'Notes',
            type: 'string'
        }, {
            name: 'DescriptiveText',
            type: 'string'
        }]
    };
    /* Constructors define individual stores under the DataManager's
    control. There should be a store constructor for every database
    field that the DataManager communicates with.  Any outside objects
    that wish to independently communicate with the database should do
    so by first cloning a constructor in the DataManager using the
    cloneStore method.*/

    var Constructors = {
        docline: {
            name: 'docline',
            proxy: {
                api: {
                    read: NNLM.get('SERVLET_BASE_URL') + 'GetDocline.servlet.php'
                }
            },
            reader: {
                idProperty: 'LIBID'
            }
        },
        people: {
            name: 'people',
            proxy: {
                api: {
                    read: NNLM.get('SERVLET_BASE_URL') + 'people/LoadPerson.servlet.php',
                    create: NNLM.get('SERVLET_BASE_URL') + 'people/CreatePerson.servlet.php',
                    update: NNLM.get('SERVLET_BASE_URL') + 'people/SavePerson.servlet.php',
                    destroy: NNLM.get('SERVLET_BASE_URL') + 'people/DestroyPerson.servlet.php'
                }
            },
            reader: {
                idProperty: 'PeopleID'
            }
        },
        positions: {
            name: 'positions',
            proxy: {
                api: {
                    read: NNLM.get('SERVLET_BASE_URL') + 'positions/LoadPosition.servlet.php',
                    create: NNLM.get('SERVLET_BASE_URL') + 'positions/CreatePosition.servlet.php',
                    update: NNLM.get('SERVLET_BASE_URL') + 'positions/SavePosition.servlet.php',
                    destroy: NNLM.get('SERVLET_BASE_URL') + 'positions/DestroyPosition.servlet.php'
                }
            },
            reader: {
                idProperty: 'PositionID'
            }
        },
        role_types: {
            name: 'role_types',
            proxy: {
                api: {
                    read: NNLM.get('SERVLET_BASE_URL') + 'roles/GetRoleTypes.servlet.php'
                }
            },
            reader: {
                idProperty: 'RoleType'
            },
            sortInfo: {
                field: 'RoleType',
                direction: 'ASC'
            }
        },
        roles: {
            name: 'roles',
            proxy: {
                api: {
                    read: NNLM.get('SERVLET_BASE_URL') + 'roles/LoadRoles.servlet.php',
                    create: NNLM.get('SERVLET_BASE_URL') + 'roles/CreateRole.servlet.php',
                    update: NNLM.get('SERVLET_BASE_URL') + 'roles/SaveRole.servlet.php',
                    destroy: NNLM.get('SERVLET_BASE_URL') + 'roles/DestroyRole.servlet.php'
                }
            },
            reader: {
                idProperty: 'RoleID'
            }
        },
        accounts: {
            name: 'accounts',
            proxy: {
                api: {
                    read: NNLM.get('SERVLET_BASE_URL') + 'accounts/LoadAccount.servlet.php',
                    create: NNLM.get('SERVLET_BASE_URL') + 'accounts/CreateAccount.servlet.php',
                    update: NNLM.get('SERVLET_BASE_URL') + 'accounts/SaveAccount.servlet.php',
                    destroy: NNLM.get('SERVLET_BASE_URL') + 'accounts/DestroyAccount.servlet.php'
                }
            },
            reader: {
                idProperty: 'AccountID'
            }
        },
        list_descriptions: {
            name: 'list_descriptions',
            proxy: {
                api: {
                    read: NNLM.get('SERVLET_BASE_URL') + 'lists/LoadListDescription.servlet.php',
                    create: NNLM.get('SERVLET_BASE_URL') + 'lists/CreateListDescription.servlet.php',
                    update: NNLM.get('SERVLET_BASE_URL') + 'lists/SaveListDescription.servlet.php',
                    destroy: NNLM.get('SERVLET_BASE_URL') + 'lists/DestroyListDescription.servlet.php'
                }
            },
            reader: {
                idProperty: 'ListID'
            }
        },
        lists: {
            name: 'lists',
            proxy: {
                api: {
                    read: NNLM.get('SERVLET_BASE_URL') + 'lists/LoadList.servlet.php',
                    create: NNLM.get('SERVLET_BASE_URL') + 'lists/CreateList.servlet.php',
                    update: NNLM.get('SERVLET_BASE_URL') + 'lists/SaveList.servlet.php',
                    destroy: NNLM.get('SERVLET_BASE_URL') + 'lists/DestroyList.servlet.php'
                }
            },
            reader: {
                idProperty: 'ListEntryID'
            },
            sortInfo: {
                field: 'DescriptiveText',
                direction: 'ASC'
            }
        }
    };
    /* callbacks store the callback functions for collective store loads.  Any
     *   'load' request will have a callback of some sort, even if it's only an
     *   empty function.  This callback will be triggered only when all stores have returned.
     *   This is an object hash, with a unique id as a key, and an object hash as value.
     *   That value hash, in turn, contains 'count' and 'fn', representing the number of store
     *   requests still outstanding to complete the load, and the function to process as a callback.
     *   This integer is incremented whenever a store load for that callback is established, and
     *   decremented whenever said store load completes.
     */
    var Callbacks = {};
    var SaveMap = {}; //possible solution to saves having no callback parameter
    var _a = function(msg) {
        NNLM.Rosters.DisplayManager.notify('statusmsg', {
            text: msg
        });
    };
    var _bindCallback = function(storeName, e, fnid) {
        var callback = function(store, r, i) {
            Callbacks[fnid](store, r, i);
            clearListeners();
        };
        var exception = function(proxy, type, action, options, response, arg) {
            _dump("Exception for store " + storeName + ': ' + str);
            clearListeners();
            fn(NNLM.Rosters.DataManager.STORES[storeName], null, -1);
        };
        var clearListeners = function() {
            _dump("clearing " + e + " listener id " + fnid + " for store " + storeName, 3);
            NNLM.Rosters.DataManager.STORES[storeName].removeListener('exception', exception, NNLM.Rosters.DataManager);
            NNLM.Rosters.DataManager.STORES[storeName].removeListener(e, callback, NNLM.Rosters.DataManager);
        };
        NNLM.Rosters.DataManager.STORES[storeName].addListener(e, callback, this);
        NNLM.Rosters.DataManager.STORES[storeName].addListener('exception', exception, this);
    };
    var _createStore = function(config, extended_config) {
        var dataResult = Ext.extend(Ext.data.GroupingStore, {
            storeId: config.name,
            proxy: new Ext.data.HttpProxy({
                api: {
                    read: config.proxy.api.read,
                    create: config.proxy.api.create,
                    update: config.proxy.api.update,
                    destroy: config.proxy.api.destroy
                },
                timeout: 60000,
                listeners: ProxyListeners
            }),
            reader: new Ext.data.JsonReader({
                idProperty: config.reader.idProperty,
                root: 'results',
                totalProperty: 'count',
                record: 'row',
                successProperty: 'success'
            }, Configurations[config.name]),
            writer: new Ext.data.JsonWriter({
                returnJson: true,
                writeAllFields: false
            }),
            paramsAsHash: true,
            batchSave: false,
            autoSave: false,
            //pruneModifiedRecords: true,
            baseParams: {
                request: ''
            },
            listeners: Listeners,
            sortInfo: (config.sortInfo === undefined) ? {
                field: config.reader.idProperty,
                direction: 'ASC'
            } : config.sortInfo
        });
        if (extended_config === undefined) {
            dataResult = new dataResult();
        } else {
            dataResult = new dataResult(extended_config);
        }
        dataResult.addEvents('doreset', 'dosave', 'doremove');
        return dataResult;
    };
    /** _dump
     *   @private
     *   dumps a string message to the Firefox console, with some text and formatting indicating
     *   the message originated from this component.
     *   @param msg {String} the message to be displayed.
     */
    var _dump = function(msg, type) {
        NNLM.Rosters.dump('\n* DataManager(' + (new Date().format('i:s.u')) + '): ' + msg, type);
    };
    /** _e
     *   @private
     *   displays an error box specific to the DataManager with a user notification message.
     *
     */
    var _e = function(msg) {
        NNLM.Rosters.DisplayManager.err('DataManager Error: ' + msg);
    };
    /* Begin Public Object */
    var result = {

        STORES: {}, //Contains all managed stores
        addRecord: function(storeName, r) {
            _dump('Adding record.', 'info');
            this.STORES[storeName].add(r);
        },
        checkStore: function(storeName) {
            if (!isset(this.STORES[storeName])) {
                throw 'Fatal error: The data store ' + storeName + ' does not exist, and could not be created!';
            }
            var mystore = this.STORES[storeName];
            var dirty = false;
            mystore.each(function() {
                if (this.phantom) {
                    dirty = true;
                }
            });
            return dirty;
        },
        /** clearFilters
         * clears filters on all stores.
         * @returns {void}
         */
        clearFilters: function() {
            _dump("clearFilters called", 3);
            this.STORES.accounts.clearFilter();
            this.STORES.people.clearFilter();
            this.STORES.positions.clearFilter();
            this.STORES.roles.clearFilter();
            this.STORES.lists.clearFilter();
            _dump("clearfilters operation performed" +
                ". Postop store counts: " +
                "\nPeople: " + this.STORES.people.getCount() +
                "\nPositions: " + this.STORES.positions.getCount() +
                "\nRoles: " + this.STORES.roles.getCount() +
                "\nAccounts: " + this.STORES.accounts.getCount(), 1
            );
        },
        /** cloneStore
         *   creates a new store (empty) that is a clone of one of the stores managed by the Data Manager.
         *   This is necessary because some components will access the same servlets as the
         *   data manager stores do, but whose data is entirely transitory; it is only used for
         *   an intermediary step - to  choose records to add to one of the main stores.  These stores
         *   only live within their Ext host component, and are destroyed when that component is.
         *   @param storeName {string} the name of the store to be cloned.  This
         *   must be one of the stores managed by the datamanager.
         *   @param config {object} a key-value hash object containing additional
         *   parameters to assign to the cloned store.
         *   @returns {Ext.data.Store}
         */
        cloneStore: function(storeName, config) {
            config = (config === undefined) ? {} : config;
            config.autoDestroy = true;
            if (!isset(Constructors[storeName])) {
                _e("Fatal error: The data store constructor does not exist, and could not be created!");
                return false;
            }
            return _createStore(Constructors[storeName], config);
        },
        /** cloneRecord
         *  clones a basic record from a named store.  Record is set with default values for all fields.
         *  @param storeName {String} The name of the store whose record should be cloned.
         */
        cloneRecord: function(storeName) {
            if (!isset(this.STORES[storeName])) {
                _e('Fatal error: The data store ' + storeName + ' does not exist, and could not be created!');
                return false;
            }
            var mystore = this.STORES[storeName];
            return new mystore.recordType({});
        },

        /** createQuery
         * turns a javascript object into a request string suitable for passing to
         * a store's servlet.
         * @param o {Object} the parameters to be wrapped for delivery.
         * @returns {String} the JSON string result.
         */
        createQuery: function(o, p) {
            var dataResult = 'request=' + JSON.stringify(o);
            if (p !== undefined) {
                dataResult += '&params=' + JSON.stringify(p);
            }
            return dataResult;
        },
        /** createRecord
         * returns a new record of the type specified by the first parameter.  Accepts
         * as arguments the values in the object in the second parameter.  Uses remote loading
         * servlets to create the record.
         *  @param storeName {String} the name of the store where the record will be added.
         *  @param _params {object} an object containing additional parameters to define
         *  some starting values for the store.
         *  @param fn {function} an optional callback function to execute once the server
         *  returns the definition and the record is created.  This function will be called
         *  with the newly instantiated record as an argument, or false if the record creation
         *  was a failure.
         *  @returns {Ext.data.Record}
         */
        createRecord: function(storeName, _params, fn) {
            if (!isset(NNLM.Rosters.DataManager.STORES[storeName])) {
                _e('Fatal error: The data store ' + storeName + ' does not exist, and could not be created!');
                return false;
            }
            if (_params.preventDuplicates !== undefined && _params.preventDuplicates === true) {
                delete _params.preventDuplicates;
                var exists = false;
                NNLM.Rosters.DataManager.STORES[storeName].findBy(function(record, id) {
                    if (exists) {
                        return;
                    }
                    for (var k in _params) {
                        if (record.get(k) !== _params[k]) {
                            /* _a(r.get('RoleType') + ' is not a duplicate by virtue of key ' + k +
							': ' + r.get(k) +' vs. ' + _params[k]); */
                            return;
                        }
                    }
                    exists = true;
                });
                if (exists) {
                    _a("This record already exists. As duplicates are not allowed here, your entry was not added a second time.");
                    return false;
                }
            }
            if (fn !== undefined) {
                var callback = function(store, record, i) {
                    fn(true);
                    clearListeners();
                };
                var exception = function(proxy, type, action, options, response, arg) {
                    clearListeners();
                    fn(false);
                    _dump("Exception: " + str, 3);
                };
                var clearListeners = function() {
                    NNLM.Rosters.DataManager.STORES[storeName].removeListener('exception', exception, NNLM.Rosters.DataManager);
                    NNLM.Rosters.DataManager.STORES[storeName].removeListener('save', callback, NNLM.Rosters.DataManager);
                };
                this.STORES[storeName].addListener('save', callback, this);
                this.STORES[storeName].addListener('exception', exception, this);
            }
            var r = new this.STORES[storeName].recordType(_params);
            this.STORES[storeName].add(r);
            return r;
        },

        /*   OBSOLETE?  Not found in project...
		createRecordFromForm: function(storeName, bform){
            if(!isset(NNLM.Rosters.DataManager.STORES[storeName])){
               	throw 'Fatal error: The data store ' + storeName + ' does not exist, and could not be created!';
            }
			if(bform.updateRecord === undefined){
				throw "Incorrect second argument passed to createRecordFromForm function";
			}
			var r = new this.STORES[storeName].recordType();
			//persist values to the record on behalf of the form.
			bform.updateRecord(r);
			this.STORES[storeName].add(r);                             
			return r;
        },  */
        /** destroyRecord
         * destroys a record in the named store, and calls a callback function once the server returns with results.
         * @param storeName {string} the name of the store.
         * @param record {Ext.data.Record} the record object to be removed.
         * @param fn {function} any callback function to be executed upon completion.
         * @returns {boolean} true if the request is successfully sent the server, false otherwise.  Note that this
         * does not include the server's success in deleting the record.  The callback function passed should take this
         * into account - it is passed one parameters:
         *      success {boolean} whether or not the operation was a success
         */
        destroyRecord: function(storeName, record, fn) {
            if (!isset(this.STORES[storeName])) {
                _e('Fatal error: The data store ' + storeName + ' does not exist, and could not be created!');
                return false;
            }
            if (record === undefined || record === null) {
                _e("Record with id " + record + " could not be found");
                return false;
            }
            if (record.phantom) {
                _dump('Destroying *phantom* record.', 'info');
                this.STORES[storeName].remove(record);
                this.checkStore(storeName);
                if (fn !== undefined) {
                    fn(true);
                }
                return true;
            }
            this.STORES[storeName].remove(record);
            return true;
        },
        /** destroyRecords
         * destroy more than one record at once.
         */
        destroyRecords: function(storeName, records, fn) {
            if (!Ext.isString(storeName)) {
                throw "Illegal argument to destroyRecords function";
            }
            if (!isset(this.STORES[storeName])) {
                _e('Fatal error: The data store ' + storeName + ' does not exist, and could not be created!');
                return false;
            }
            var coll = new Ext.util.MixedCollection();
            if (records.each !== undefined) { //MixedCollection
                coll = records;
            } else if (Ext.isArray(records)) {
                for (var i = 0; i < records.length; i++) {
                    coll.add(records[i]);
                }
            } else {
                throw "Illegal argument to destroyRecords function";
            }
            if (coll.getCount() === 0) {
                return true;
            }
            var allphantom = true;
            coll.each(function() {
                allphantom = allphantom && this.phantom;
                NNLM.Rosters.DataManager.STORES[storeName].remove(this);
            });
            if (allphantom) {
                this.checkStore(storeName);
                if (fn !== undefined) {
                    fn(true);
                }
            }
            return true;
        },
        /** destroyRecordById
         * convenience function for calling destroyRecord with just a record id.  Executes
         * any passed callback function with the results of the query once it returns from the server.  This function
         * will not be executed if the record id passed is not found.
         * @param storeName {string} the name of the store
         * @param id {int} the integer that refers to the record's primary key
         * @param fn {object} a callback function to be executed once the deletion results
         * are returned from the server.  The function is executed with a single argument,
         * @returns {boolean} true if the request is successfully sent the server, false otherwise.  Note that this
         * does not include the server's success in deleting the record.  The callback function passed should take this
         * into account - it is passed one parameters:
         *      success {boolean} whether or not the operation was a success
         */
        destroyRecordById: function(storeName, id, fn) {
            if (id === '') {
                _e("This record has no defined id.");
                return false;
            }
            var r = this.getRecordById(storeName, id);
            if (!r) {
                _e("record " + id + " not found.");
                return false;
            }
            return this.destroyRecord(storeName, r, fn);
        },
        destroyRecordByIndex: function(storeName, i, fn) {
            _a("destroyRecordByIndex: " + i);
            var r = this.getRecordByIndex(storeName, i);
            if (r === null) {
                return;
            }
        },
        destroyRecordByProperty: function(storeName, k, v, fn) {
            var coll = this.STORES[storeName].query({
                field: k,
                value: v,
                anyMatch: false,
                caseSensitive: false
            });
            return this.destroyRecords(coll);
        },
        /** filterBy is a convenience function that filters all stores by
         * a given PeopleID.  When the PeopleID is not a key in that store, the
         * function does the appropriate cross-referencing to determine the correct
         * key(s) related the to the peopleID, an filters by those instead
         * @param PeopleID {int} the peopleID to filter by.
         * @returns {void}
         */
        filterBy: function(PeopleID) {
            if (isNaN(PeopleID)) {
                throw "Invalid PeopleID type passed to filterBy function: '" + PeopleID;
            }
            PeopleID = parseInt(PeopleID, 10);
            var myfilter = [{
                property: 'PeopleID',
                value: PeopleID,
                exactMatch: true
            }];
            this.STORES.accounts.filter(myfilter);
            this.STORES.people.filter(myfilter);
            this.STORES.positions.filter(myfilter);
            var libids = [];
            var roles = [];
            this.STORES.positions.each(function(record) {
                libids.push(record.get('LIBID'));
                roles.push(record.get('PositionID'));
            });
            //warning - this filter will blow up quick with large local cache.
            //should be a non-issue (small ops only.)
            /*
             this.STORES.docline.filterBy(function(record, id){
                return (libids.indexOf(id) !== -1);
            });
            */
            this.STORES.roles.filterBy(function(record, id) {
                return (roles.indexOf(record.get('PositionID')) !== -1);
            });
            _dump("filterBy operation performed, using PeopleID " + PeopleID +
                ". Postop store counts: " +
                "\nPeople: " + this.STORES.people.getCount() +
                "\nPositions: " + this.STORES.positions.getCount() +
                "\nRoles: " + this.STORES.roles.getCount() +
                "\nAccounts: " + this.STORES.accounts.getCount(), 2
            );
        },
        /** filterStore filters a single store, using the data managers
         * innate knowledge of that store's primary key, and applying
         * all appropriate conversions and checks to incoming data.
         * @param storeName {string} the name of the store.
         * @param params {object} a hash containing one or more of the following:
         * 	key		: The key to filter by.  Determined automatically if not present.
         * 	value	: The value to filter by.  Required.
         */
        filterStore: function(storeName, params) {
            if (!Ext.isArray(params)) {
                params = [params];
            }
            var defaultkey = 'PeopleID';
            switch (storeName) {
                case 'people':
                case 'positions':
                    defaultkey = 'PeopleID';
                    break;
                case 'list_descriptions':
                case 'lists':
                    defaultkey = 'ListID';
                    break;
                case 'roles':
                case 'role_descriptions':
                    defaultkey = 'PositionID';
                    break;
                default:
                    throw "Invalid storeName value in DataManager.filterStore";
            }
            Ext.each(params, function() {
                if (this.property === undefined) {
                    this.property = defaultkey;
                }
                this.exactMatch = true;
            });
            this.STORES[storeName].filter(params);
            NNLM.Rosters.DisplayManager.notify('afterfilter');
        },
        /** getColumnModel(storeName)
         * returns a basic column model using the store's configuration, usable
         * in a gridPanel
         *   @param storeName {string} the name of the store configuration to query
         *   @returns {Ext.grid.ColumnModel}
         */
        getColumnModel: function(storeName) {
            var config = this.getConfig(storeName);
            if (!config) {
                return [];
            }
            var fields = arguments.length > 1 ? arguments[1] : jQuery.keys(config);
            var dataResult = [];
            for (var i in fields) {
                if (config.hasOwnProperty(i)) {
                    var o = {
                        'name': config[i].name
                    };
                    o.header = (isset(config.fieldLabel)) ? config.fieldLabel : config.name;
                    dataResult.push(o);
                }
            }
            return new Ext.grid.ColumnModel(dataResult);
        },
        /** getConfig
         *   returns the configuration array for a given store.
         *   @param storeName {String} the name of the store to be retrieved.  See also:
         *
         *   @return {Array} the store configuration referenced by the passed parameter,
         *   or an empty array if
         *   the referenced store does not exist.
         */
        getConfig: function(storeName) {
            if (!isset(Configurations[storeName])) {
                _e("Fatal error: The data store configuration does not exist, and could not be created!");
                return false;
            }
            return Configurations[storeName];
        },
        getCurrentUser: function() {
            if (CURRENT_USER === null) {
                throw "Current user is not defined!  This could be because there was a timeout in the server response.  Try reloading the page.";
            }
            return CURRENT_USER;
        },
        /** getFieldNames
         *   returns a form configuration object for a given store.
         *   @param storeName {string} the name of the store configuration to query
         *   @returns {array} list of field names, or empty array if something went wrong.
         */
        getFieldConfig: function(storeName) {
            var config = this.getConfig(storeName);
            if (!config) {
                return [];
            }
            var dataResult = [];
            for (var i in config) {
                if (config.hasOwnProperty(i)) {
                    var o = {
                        'name': config[i].name
                    };
                    if (isset(config.fieldLabel)) {
                        if (config.fieldLabel !== false) {
                            o.fieldLabel = config.fieldLabel;
                        }
                    } else {
                        o.fieldLabel = o.name;
                    }
                    dataResult.push(o);
                }
            }
            return dataResult;
        },

        /** getRecord
         *   retrieves a composite record object from the stores.
         *   @param PeopleID {Number} The PeopleID of the record of the person to retrieve.
         *   @return {Object Hash} the results from the store in question, or an empty hash if none.
         *       Entries in the object hash will be Ext.data.MixedCollections of Ext.data.Record objects.
         */
        getRecords: function(PeopleID) {
            var results = {},
                queryByFn = function(r, id) {
                    return (r.data.PeopleID === PeopleID);
                };
            for (var storeName in this.STORES) {
                if (this.STORES.hasOwnProperty(storeName)) {
                    if (this.STORES[storeName] === false) {
                        continue;
                    }
                    results[storeName] = this.STORES[storeName].queryBy(queryByFn);
                }
            }
            return results;
        },
        getRecordById: function(storeName, id) {
            var storeKey = NNLM.Rosters.DataManager.getStoreKey(storeName);
            var r = this.STORES[storeName].getById(id.toString());
            if (!r) {
                //edge case: record created and saved is not visible to getById.  Outstanding bug in ExtJS <= 3.4, methinks.
                var k = this.getStoreKey(storeName);
                this.STORES[storeName].each(function(r2) {
                    if (r) {
                        return;
                    }
                    if (r2.get(k) === id) {
                        r = r2;
                    }
                    //ids.push(r2.get(k) + ' - type: ' + (typeof r2.get(k)));
                });
                if (!r) {
                    //_e("Sorry, but no local copy of the requested record was found.  Try refreshing the Rosters application.");
                    return false;
                }
            }
            return r;
        },
        getRecordByIndex: function(storeName, i) {
            if (!isset(this.STORES[storeName])) {
                throw 'Fatal error: The data store ' + storeName + ' does not exist!';
            }
            if (this.STORES[storeName].getCount() <= i) {
                _e("Record requested is beyond the store length");
                return null;
            }
            return this.STORES[storeName].getAt(i);
        },
        /** getStoreNames
         *   provides the index by which an outside source would query a particular store
         *   using the Ext getById call.
         *   @param storeName {string} the name of the store to provide the key for
         *   @returns {string} the value of the key for that store.
         */
        getStoreKey: function(storeName) {
            return Constructors[storeName].reader.idProperty;
        },
        hasChanges: function() {
            var changesum = 0;
            for (var k in this.STORES) {
                if (this.STORES.hasOwnProperty(k)) {
                    changesum += this.STORES[k].getModifiedRecords().length;
                    changesum += this.STORES[k].getRemovedCount();
                    if (this.STORES[k].getModifiedRecords().length > 0) {
                        _dump("Store " + k + " has " + this.STORES[k].getModifiedRecords().length + " changes.\n");
                    }
                }
            }
            //_dump("Modified record count: " + changesum, 'focus');
            return (changesum > 0);
        },
        /** Constructor
         *   @private
         */
        init: function() {
            /*
            create a new store for each constructor, and bind the datamanager's
            event listeners to the store, so each store calls each listener at the
            appropriate time. */
            var mystore = false;
            for (var indx in Constructors) {
                if (Constructors.hasOwnProperty(indx)) {
                    mystore = _createStore(Constructors[indx], {
                        autoLoad: false
                    });
                    mystore.storeName = indx;
                    for (var indx2 in Listeners) {
                        if (Listeners.hasOwnProperty(indx)) {
                            _dump("Adding " + indx2 + " listener to store " + indx + ".");
                            mystore.addListener(indx2, Listeners[indx2], mystore);
                        }
                    }
                    this.STORES[indx] = mystore;
                    mystore.name = indx;
                }
            }
            var me = this;
            Ext.Ajax.request({
                url: NNLM.get('SERVLET_BASE_URL') + 'people/GetCurrentUser.servlet.php',
                success: function(xhr) {
                    var reader = NNLM.Rosters.DataManager.STORES.people.reader;
                    console.log(xhr);
                    if (xhr.responseText === '') {
                        throw "Cannot determine the current logged in user.";
                    }
                    var o = reader.read(xhr);
                    var response = Ext.util.JSON.decode(xhr.responseText);
                    if (!o.success) {
                        _e(response.results.error);
                        window.clearInterval(window.RostersInit);
                        window.clearInterval(window.RostersErr);
                        return;
                    }
                    NNLM.Rosters.DataManager.load({
                        params: {
                            PeopleID: o.records[0].data.PeopleID
                        },
                        callback: function(success, arr) {
                            if (NNLM.Rosters.DataManager.STORES.people.getCount() === 0) {
                                _e("Could not determine the currently logged in user.");
                            }
                            var r = NNLM.Rosters.DataManager.STORES.people.getAt(0);
                            NNLM.Rosters.DataManager.setCurrentUser(r);
                        }
                    });
                },
                failure: function(xhr) {
                    throw 'initialization exception!';
                }
            });
            _dump("Data Manager initialized.", 3);
            this.init = true;
        },

        isReady: function() {
            return (CURRENT_USER !== null && CURRENT_USER.get !== undefined && this.init === true);
        },
        /* load
         *  This function is the primary means by which external components and
         *   other managers talk to the data manager.  Load takes a single argument - a
         *   set of parameters, that are designed to be used by the Ext Stores contained
         *   within the data manager.
         *
         *   @param obj {Object Hash}:      any arguments to pass to
         *               the store load.  Arguments may be one of the following:
         *
         *               obj.params: the query parameters to be loaded.  Will equate
         *               to a key=value transmission via XMLHttpRequest.  Example:
         *               obj.params = {foo:bar} would send the key value pair "foo=bar".
         *
         *               Note: The parameter 'PeopleID' is required.
         *
         *               obj.callback: a callback function to be executed on load completion.
         *               This will be called immediately following the DataManager callback.
         */
        load: function(obj) {
            if (!isset(obj.params)) {
                throw 'no parameters passed to DataManager load method';
            }
            if (!isset(obj.params.extid)) {
                obj.params.extid = (new Date()).getTime().toString();
            }
            var storesToLoad = (obj.storeName !== undefined) ?
                [obj.storeName.toString()] : jQuery.keys(this.STORES);


            if (obj.callback !== undefined) {
                Callbacks[obj.params.extid] = {
                    success: true,
                    extid: obj.params.extid,
                    tlc: storesToLoad.length,
                    callback: obj.callback
                };
            }
            _dump("Loading stores: " + NNLM.printObject(storesToLoad) +
                ',TLC (start): ' + storesToLoad.length + ', extid: ' + obj.params.extid +
                ', params: ' + NNLM.Rosters.DataManager.createQuery(obj.params), 2);

            for (var i = 0; i < storesToLoad.length; i++) {
                NNLM.Rosters.DataManager.STORES[storesToLoad[i]].load({
                    params: NNLM.Rosters.DataManager.createQuery(obj.params),
                    extid: obj.params.extid,
                    scope: NNLM.Rosters.DataManager,
                    add: true
                });
            }
        },
        recordExists: function(storeName, o) {
            var dataResult = this.STORES[storeName].findBy(function(r) {
                for (var k in o) {
                    if (r.get(k) !== o[k]) {
                        //compares strings to numbers at times.  Yes, I want this;  it's easier than converting manually.
                        return false;
                    }
                }
                return true;
            });
            return (dataResult === -1) ? false : true;
        },
        reset: function(storeName, fn) {
            if (!isset(this.STORES[storeName])) {
                throw 'Fatal error: The data store ' + storeName + ' does not exist, and could not be created!';
            }
            this.STORES[storeName].rejectChanges();
            this.STORES[storeName].purgePhantomRecords();
            _dump('Resetting store ' + storeName + '.', 3);
            NNLM.Rosters.DisplayManager.notify('afterreset');
            this.STORES[storeName].fireEvent('doreset');
            if (fn !== undefined) {
                //
                fn();
            }
            NNLM.Rosters.DisplayManager.notify("datachanged", false);
            NNLM.Rosters.DisplayManager.notify('afterreset');
        },
        reload: function(storeName, options, fn) {
            if (storeName === undefined) {
                throw "storename is undefined!";
            }
            if (!isset(this.STORES[storeName])) {
                _e('Fatal error: The data store ' + storeName + ' does not exist, and could not be created!');
                return;
            }
            for (var k in options) {
                this.STORES[storeName].removeBy(k, options[k]);
            }
            this.load({
                storeName: storeName,
                params: options,
                callback: fn
            });
        },
        save: function(storeName, fn) {
            if (storeName === undefined) {
                throw "storename is undefined!";
            }
            if (!isset(this.STORES[storeName])) {
                _e('Fatal error: The data store ' + storeName + ' does not exist, and could not be created!');
                return;
            }
            var extid = (new Date()).getTime().toString();

            if (fn !== undefined) {
                Callbacks[extid] = {
                    success: true,
                    storeName: storeName,
                    extid: extid,
                    tlc: 1,
                    callback: fn
                };
            }
            this.STORES[storeName].addListener('beforesave', function(store, data) {
                store.setBaseParam('extid', extid);
                return true;
            }, this, {
                single: true
            });
            _dump("Persisting records in store " + storeName, 3);
            var addcount = this.STORES[storeName].getModifiedRecords().length;
            var removecount = this.STORES[storeName].getRemovedCount();
            var msgs = [];
            switch (storeName) {
                case 'roles':
                    if (addcount > 0) {
                        msgs.push('Adding or changing ' + addcount + ' role(s)');
                    }
                    if (removecount > 0) {
                        msgs.push('Removing ' + removecount + ' role(s)');
                    }
                    break;
                case 'accounts':
                    if (addcount > 0) {
                        msgs.push('Adding or changing ' + addcount + ' account(s)');
                    }
                    if (removecount > 0) {
                        msgs.push('Removing ' + removecount + ' account(s)');
                    }
                    break;
                case 'people':
                    if (addcount > 0) {
                        msgs.push('Adding or changing ' + addcount + ' staff member record'); //should always be 1
                    }
                    break;
                case 'list_descriptions':
                    if (addcount > 0) {
                        msgs.push('Adding or changing ' + addcount + ' new list'); //should always be 1
                    }
                    break;
                case 'lists':
                    if (addcount > 0) {
                        msgs.push('Adding or changing ' + addcount + ' list item(s)'); //should always be 1
                    }
                    if (removecount > 0) {
                        msgs.push('Removing ' + removecount + ' list item(s)');
                    }
                    break;
                default:
            }
            if (msgs.length > 0) {
                NNLM.Rosters.DisplayManager.notify('wait', {
                    wait: true,
                    text: msgs.join('<br />')
                });
            } else {
                NNLM.Rosters.DisplayManager.notify('wait', {
                    wait: true
                });
            }
            this.STORES[storeName].save();
        },
        setCurrentUser: function(r) {
            CURRENT_USER = r;
        },
        updateRecord: function(storeName, recordId, bform) {
            if (!isset(NNLM.Rosters.DataManager.STORES[storeName])) {
                _e('Fatal error: The data store ' + storeName + ' does not exist, and could not be created!');
                return false;
            }
            if (bform.updateRecord === undefined) {
                if (bform.getForm !== undefined) {
                    bform = bform.getForm();
                } else {
                    _e("Incorrect third argument passed to updateRecord function: " + bform.updateRecord);
                    return false;
                }
            }
            var r = this.getRecordById(storeName, recordId);
            if (!r) {
                _e("Could not find record for updating");
                return false;
            }
            //persist values to the record on behalf of the form.
            bform.updateRecord(r);
            return true;
        }
    };
    result.init();
    return result;
};
NNLM.Rosters.dump("Data Manager loaded.");
