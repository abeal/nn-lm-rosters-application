/* Records */
/* Ext Objects that define a client side data record populated from the server.
*	In most cases, a record here will be the same as a record on the server, but
*	not always.  It may be desirable to create records on the client that are
*	an amalgam of different database tables on the server.
*/

Ext.ns("NNLM.Rosters.Records");
NNLM.Rosters.Records = {
	/** FindPerson
	*	corresponds to the FindPerson servlet.  Returns the following
	* 	data field:
	*	general.people.PeopleID
	*	general.people.FirstName
	*	general.people.LastName
	*/
	Person: Ext.data.Record.create([
		{name: 'PeopleID'},
		{name: 'FirstName'},
		{name: 'LastName'}
	])
};
