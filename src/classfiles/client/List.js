/**
 *     @class NNLM.Rosters.List
 *     <p>This class is independently downloaded by the Rosters application during runtime, and
 *    houses all the operations performed by the find search panel.  A find panel is created
 *    using the NNLM.Rosters.Find.makeInstance method.</p>
 *
 *   @author     Aron Beal
 *   @copyright (c) 2008, by the University of Washington
 *   @date       May 6 2008
 *   @license All files in this web application are  licensed under the
 *   terms of the Open Source LGPL 3.0 license.
 *   License details: http://www.gnu.org/licenses/lgpl.html
 */
/** extern Ext, NNLM */
Ext.ns('NNLM.Rosters.List');

NNLM.Rosters.List.makeInstance = function(uniqueId) {
	var GLOBALS = {};
	var _dump = function(msg) {
		var time = new Date().toTimeString();
		NNLM.Rosters.dump('\n* Edit (' + time + '): ' + msg);
	};
	var _idump = function(msg) {
		if (window.console === undefined) {
			throw "console messages enabled in a browser that does not support console messages.";
		}
		window.console.info("\nList: " + msg);
	};
	/**   
	 *  _a
	 *  display an alert dialog with this class prefixed.  used for debugging.
	 *  @param msg {string} the message to display
	 **/
	var _a = function(msg) {
		NNLM.Rosters.dump(msg, 'warn');
		NNLM.Rosters.DisplayManager.notify('statusmsg', {
			text: msg
		});
	};
	/**  _e
	 *  display an error dialog with this class prefixed.  used for debugging.
	 * @param msg {string} the message to display
	 **/
	var _e = function(msg) {
		NNLM.Rosters.dump(msg, "error");
		if (arguments.length === 1) {
			NNLM.Rosters.DisplayManager.err(msg);
		} else {
			NNLM.Rosters.DisplayManager.notify('wait', {
				title: "List ERROR: ",
				msg: msg,
				wait: true,
				panel: arguments[1],
				decay: NNLM.get('MESSAGE_DELAY_LENGTH')

			});
		}
	};
	var _listDateRenderer = function(v, p, r) {
		if (v === undefined || v === null) {
			return '<span class="metadata">[List Default]</span>';
		}
		return Ext.util.Format.date(v, 'm/d/Y');
	};
	var viewModeHandler = function(i) {
		MainPanel.get(1).getComponent('listView').getLayout().setActiveItem(i);
	};
	var _setListsPanel = function(number) {
		if (!NNLM.Rosters.DisplayManager.isViewChangeOk()) {
			return false;
		}
		MainPanel.chute('panels').getLayout().setActiveItem(number);
		MainPanel.fireEvent('dofilter');
		return true;
	};
	var _functions = {
		'addtolist': function() {
			var bbar = MainPanel.chute('activeListContents').getBottomToolbar();
			var typeId = bbar.chute('switcher').getValue();
			if (!bbar.chute(typeId).validate()) {
				_a("You must first choose whether to add a list or a person, and then choose the specific list or person you wish to add.");
				return;
			}
			var value = bbar.chute(typeId).getValue();

			var grid = MainPanel.chute('activeListContents');
			if (grid === null) {
				throw "grid is null";
			}
			grid.stopEditing();
			NNLM.Rosters.DataManager.createRecord('lists', {
				ListID: MainPanel.ListID,
				ListMemberTable: typeId,
				ListMemberTableID: value,
				preventDuplicates: true
			});
		},
		'addlisttolist': function() {
			if (!_setListsPanel(1)) {
				return;
			}
			MainPanel.setAddType('list_descriptions');
		},
		'addlisttolocker': function() {
			if (!_setListsPanel(1)) {
				return;
			}
			MainPanel.setAddType('list_descriptions');
		},
		'addpersontolist': function() {
			if (!_setListsPanel(1)) {
				return;
			}
			MainPanel.setAddType('people');
		},
		'addpersontolocker': function() {
			if (!_setListsPanel(1)) {
				return;
			}
			MainPanel.setAddType('people');
		},
		'addlibrarytolist': function() {
			_a("Feature not yet available.");
			return;
		},
		'addlibrarytolocker': function() {
			_a("Feature not yet available.");
			return;
		},
		'deletelistitem': function(ListEntryID) {
			NNLM.Rosters.DataManager.destroyRecordById('lists', ListEntryID);
		},
		'editlist': function() {
			var c = MainPanel.chute('mainList').chute('activeListContents');
			var ListID = c.getSelectionModel().getSelected().get('ListMemberTableID');
			_a('ListID: ' + ListID);
			if (ListID === null) {
				_e("selected list id is null");
				return;
			}
			NNLM.Rosters.DisplayManager.notify('list', {
				ListID: ListID
			});
		},
		'removefromlist': function() {
			var g = MainPanel.chute('panels').chute('activeListContents');
			if (!g.getSelectionModel().hasSelection()) {
				_a("You must first select an entry to remove.");
				return;
			}
			var selections = g.getSelectionModel().getSelections();
			NNLM.Rosters.DataManager.destroyRecords('lists', selections);
		},
		'removefromlocker': function() {
			var r = MainPanel.chute('panels').chute('activeListContents').getSelectionModel().getSelected();
			NNLM.Rosters.DataManager.destroyRecord('lists', r);
		}
	};

	var _actions = {
		'addtolist': {
			text: 'Add to list',
			tooltip: 'This control adds the entry specified in the toolbar adjacent to this control to the above list. You must first choose whether to add a list or a person, and then choose the specific list or person you wish to add, before you use this control.'
		},
		'editlist': {
			text: 'Edit Contents',
			tooltip: 'Edit the contents of this list.'
		},
		'addpersontolist': {
			text: 'Add person',
			tooltip: 'Add a person to this list.'
		},
		'addlisttolocker': {
			text: "Add list",
			tooltip: 'Add a list to your locker.'
		},
		'addlisttolist': {
			text: 'Add list',
			tooltip: 'Add a list to this list.'
		},
		'addpersontolocker': {
			text: 'Add person',
			tooltip: 'Add a person to your locker.'
		},
		'addlibrarytolist': {
			text: 'Add library',
			tooltip: "Add a library to this list (Available in version 2.1)"
		},
		'addlibrarytolocker': {
			text: 'Add library',
			tooltip: "Add a library to this locker (Available in version 2.1)"
		},
		'removefromlist': {
			text: 'Remove',
			tooltip: 'This control removes the currently selected entry from the list.  If nothing is selected, the control does nothing.'
		},
		'removefromlocker': {
			text: 'Remove',
			tooltip: 'Remove this entry from this locker.'
		}
	};
	for (var k in _actions) {
		_actions[k].itemId = k;
		_actions[k].handler = _functions[k].createCallback();
		_actions[k] = new Ext.Action(_actions[k]);
	}
	var _menus = {
		'list': [
			_actions.addpersontolist,
			_actions.addlisttolist,
			_actions.addlibrarytolist
		],
		'locker': [
			_actions.addpersontolocker,
			_actions.addlisttolocker,
			_actions.addlibrarytolocker
		]
	};
	var _templates = {
		listDropTpl: new Ext.XTemplate('<tpl for=".">', '<div class="x-combo-list-item">', '<p class="gridList">({ListID}) {ListTitle}', '</p></div>', '</tpl>', {
			getDate: function(start, end) {
				start = (start === null) ? '[no start date]' : start.format("d/m/Y");
				end = (end === null) ? '[no end date]' : end.format("d/m/Y");
				return "(" + start + ' - ' + end + ')';
			},
			getHTMLDescription: function(description) {
				return Ext.util.Format.nl2br(description);
			}
		}).compile(),
		listPersonTpl: new Ext.XTemplate('<tpl for=".">', '<div class="x-combo-list-item">', '<span class="iconed title person">{LastName}, {FirstName} ({PeopleID})', '</span></div>', '</tpl>').compile(),
		listTpl: new Ext.XTemplate(
			'<tpl for=".">',
			'<div class="x-combo-list-item">',
			'<tpl if="ListMemberTable === \'people\'">',
			'<span class="listcontents iconed user">',
			'</tpl>',
			'<tpl if="ListMemberTable === \'list_descriptions\'">',
			'<span class="listcontents iconed script">',
			'</tpl>',
			'{DescriptiveText}', '</span>', '</tpl>'),
		listEntryDeleteTpl: new Ext.XTemplate(
			'<tpl for="."><div class="delete_link"><a href="#" onclick="NNLM.Rosters.DisplayManager.notify(\'relay\', {target: \'deletelistitem\', args:\'{ListEntryID}\'}); return false">Remove<br />from list</a></div></tpl>'
		).compile(),
		lockerEntryDeleteTpl: new Ext.XTemplate(
			'<tpl for="."><div class="delete_link"><a href="#" onclick="NNLM.Rosters.DisplayManager.notify(\'relay\', {target: \'deletelistitem\', args:\'{ListEntryID}\'}); return false">Remove<br />from locker</a></div></tpl>'
		).compile()
	};
	var _stores = {
		'people': NNLM.Rosters.DataManager.cloneStore('people'),
		'list_descriptions': NNLM.Rosters.DataManager.cloneStore('list_descriptions'),
		'docline': NNLM.Rosters.DataManager.cloneStore('docline'),
		'list_expanded': NNLM.Rosters.DataManager.cloneStore('lists')
	};
	var _editors = {
		'people': {
			xtype: 'combo',
			itemId: 'people',
			title: 'Add a staff member',
			allowBlank: false,
			tpl: '<tpl for="."><p class="x-combo-list-item">{FirstName} {LastName} ({PeopleID})</p></tpl>',
			store: _stores.people,
			valueField: 'PeopleID',
			choice: true,
			hideTrigger: true,
			width: 300,
			forceSelection: true,
			mode: 'remote',
			triggerAction: 'all',
			selectOnFocus: true,
			emptyText: 'First then last name',
			listeners: {
				select: function(combo, r, i) {
					if (NNLM.Rosters.DataManager.createRecord('lists', {
							ListID: MainPanel.ListID,
							ListMemberTable: 'people',
							ListMemberTableID: r.get('PeopleID'),
							DescriptiveText: r.get('FirstName') + ' ' + r.get('LastName'),
							preventDuplicates: true
						}) !== false) {
						_a("Entry added to the bottom of the list.");
					}
				}
			}
		},
		'list_descriptions': {
			xtype: 'combo',
			itemId: 'list_descriptions',
			allowBlank: false,
			choice: true,
			width: 300,
			hidden: true,
			hideTrigger: true,
			title: 'Add a list',
			tpl: '<tpl for="."><p class="x-combo-list-item">{ListTitle} ({ListID})<br />' +
				'<span class="metadata">{ListDescription}</span></p></tpl>',
			store: _stores.list_descriptions,
			valueField: 'ListID',
			emptyText: 'Any words from title or description',
			forceSelection: true,
			mode: 'remote',
			triggerAction: 'all',
			selectOnFocus: true,
			listeners: {
				select: function(combo, r, i) {
					/*if(NNLM.Rosters.DataManager.recordExists('lists',{
		                ListID: MainPanel.ListID,
						ListMemberTable: 'list_descriptions',
						ListMemberTableID: r.get('ListID')
		            })){
						_e('This record already exists, and was not added.');
						return;
					}
					NNLM.Rosters.DataManager.createRecord('lists', {
		                ListID: MainPanel.ListID,
						ListMemberTable: 'list_descriptions',
						ListMemberTableID: r.get('ListID'),
						DescriptiveText: r.get('ListTitle')
		            });
					NNLM.Rosters.DisplayManager.notify('setvalid', true);   
					_a("Entry added to the bottom of the list.");  
					*/

					if (NNLM.Rosters.DataManager.createRecord('lists', {
							ListID: MainPanel.ListID,
							ListMemberTable: 'list_descriptions',
							ListMemberTableID: r.get('ListID'),
							DescriptiveText: r.get('ListTitle')
						})) {
						_a("Entry added to the bottom of the list.");
					}
				}
			}
		},
		'libraries': {
			xtype: 'combo',
			itemId: 'libraries',
			disabled: true,
			choice: true,
			width: 300,
			hidden: true,
			hideTrigger: true,
			title: 'Add a list',
			tpl: '<tpl for="."><p class="x-combo-list-item">{INST_INST} ({LIBID})<br />' +
				'<span class="metadata">{ListDescription}</span></p></tpl>',
			store: _stores.docline,
			valueField: 'LIBID',
			emptyText: '(Feature coming soon in version 2.1)',
			forceSelection: true,
			mode: 'remote',
			triggerAction: 'all',
			selectOnFocus: true,
			listeners: {
				select: function(combo, r, i) {
					_a("Not yet implemented.  This feature is a placeholder only, until the 2.1 release.");
					if (NNLM.Rosters.DataManager.recordExists('lists', {
							ListID: MainPanel.ListID,
							ListMemberTable: 'libraries',
							ListMemberTableID: r.get('LIBID')
						})) {
						_e('This record already exists, and was not added.');
						return;
					}
					NNLM.Rosters.DataManager.createRecord('lists', {
						ListID: MainPanel.ListID,
						ListMemberTable: 'libraries',
						ListMemberTableID: r.get('LIBID'),
						DescriptiveText: r.get('INST_INST') + ' ' + r.get('DEPT_INST')
					});
					NNLM.Rosters.DisplayManager.notify('setvalid', true);
					_a("Entry added to the bottom of the list.");
				}
			}
		}
	};
	var _controls = {
		addList: {
			xtype: 'combo',
			itemId: 'addlist',
			fieldLabel: 'Add a list:',
			tpl: '<tpl for=".">{ListTitle}<br /><span class="metadata">{ListDescription}</span></tpl>',
			store: _stores.list_descriptions
		},
		addPerson: {
			xtype: 'combo',
			itemId: 'addperson',
			tpl: '<tpl for=".">{FirstName} {LastName}</tpl>',
			store: _stores.people
		},
		addLibrary: {
			xtype: 'label',
			text: 'Add Library to be implemented in version 2.1'
		}
		/*,
        pruneList: {
            text: 'Remove entries from this list that are present in sublists. ',
            iconCls: 'listClean',
            handler: function() {
                _a('Prune duplicates not yet implemented.');
            }
        },
        removeListEntry: {
            text: 'Remove the selected entries from this list',
			itemId: 'removeListEntry',
            iconCls: 'delete',
			disabled: true,
            handler: function(btn, ev) {   
            },
            scope: this
        }    
		toggleTreeViewB: {
			text: 'Show tree view',
			itemId: 'toggletreeviewb',
			iconCls: 'iconed chart_organisation',
			handler: _setListsPanel.createCallback(2)
		}
		*/
	};
	var _colModels = {
		'people': new Ext.grid.ColumnModel({
			columns: [{
				dataIndex: 'PeopleID',
				renderer: function(value, metaData, record, rowIndex, colIndex, store) {
					return _templates.listPersonTpl.applyTemplate(record.data);
				}
			}]
		}),
		'list_descriptions': NNLM.Rosters.DataManager.getColumnModel('list_descriptions'),
		'list_custom1': new Ext.grid.ColumnModel({
			columns: [{
				header: 'Sort',
				dataIndex: 'ListID',
				renderer: function(v, p, r) {
					var dateString = "(";
					dateString += (window.isset(r.get('ListStartDate')) ? r.get('ListStartDate').format("d/m/Y") : '[no start date]');
					dateString += (window.isset(r.get('ListEndDate')) ? ' - ' + r.get('ListEndDate').format("d/m/Y") : ' - [no end date]');
					dateString += ')';
					return '<p class="gridList"><span class="title">' + r.get('ListTitle') + ' <span class="metadata">(ListID: ' + r.get('ListID') + ')</span><br /><span class="metadata">' + dateString + '</span></span><span class="description">' + r.get('ListDescription').replace(/[\n\r]/g, "<br />") + '</span></p>';
				}
			}]
		}),
		'list_custom2': new Ext.grid.ColumnModel({
			columns: [{
					header: 'Entry',
					dataIndex: 'DescriptiveText',
					sortable: true,
					width: 100,
					renderer: function(value, metadata, r, rowIndex, colIndex, store) {
						var cssClass = (r.get('ListMemberTable') === 'people') ? 'user' : 'script';
						return '<span class="iconed ' + cssClass + '">' +
							r.get('DescriptiveText') + "</span>";
					}
				}, {
					header: '<span class="editable">Start Date</span>',
					dataIndex: 'ListEntryStartDate',
					width: 50,
					sortable: true,
					renderer: _listDateRenderer,
					editor: {
						xtype: 'datefield'
					}
				}, {
					header: '<span class="editable">End Date</span>',
					width: 50,
					dataIndex: 'ListEntryEndDate',
					sortable: true,
					renderer: _listDateRenderer,
					editor: {
						xtype: 'datefield'
					}
				}, {
					header: '<span class="editable">Notes (255 char max)</span>',
					dataIndex: 'Notes',
					sortable: true,
					editor: {
						xtype: 'textfield'
					}
				}, {
					dataIndex: 'ListEntryID',
					header: 'Controls',
					width: 50,
					editable: false,
					renderer: function(value, metaData, record, rowIndex, colIndex, store) {
						if (MainPanel.isLocker) {
							return _templates.lockerEntryDeleteTpl.applyTemplate(record.data);
						}
						return _templates.listEntryDeleteTpl.applyTemplate(record.data);
					}
				}]
				/*,
						    getCellEditor : function(colIndex, rowIndex) {
								if(colIndex === 0){
									var type = MainPanel.chute('panels').chute('activeListContents').getSelectionModel().getSelected().get('ListMemberTable');
									this.setEditor(0, _editors[type]);
								}
						        return this.config[colIndex].getCellEditor(rowIndex);
						    }*/
		})
	};
	/*end menus */
	/** tree panel in center display - displays the main list for this panel.
	 * one of three 'views' for the main list. */
	var MainPanel = new Ext.Panel({
		layout: 'hbox',
		layoutConfig: {
			align: 'stretch',
			pack: 'start'
		},
		bodyCssClass: 'rostersMenuPanel',
		id: 'MainPanel-' + uniqueId,
		iconCls: 'iconed script',
		isLocker: false,
		closable: true,
		defaults: {
			flex: 1,
			xtype: 'panel',
			frame: false,
			border: false
		},
		items: [{
			xtype: 'panel',
			title: 'Controls:',
			layout: 'vbox',
			itemId: 'controls',
			flex: 2,
			bodyCssClass: 'MenuPanel',
			layoutConfig: {
				align: 'stretch',
				pack: 'start',
				defaultMargins: {
					top: 5,
					right: 0,
					bottom: 5,
					left: 15
				}
			},
			defaults: {
				xtype: 'button',
				/* flex values should add to 10.  Adjust the bracketing menupanels to compensate*/
				flex: 1,
				border: 0
			},
			items: [{
				xtype: 'panel',
				bodyCssClass: 'MenuPanel'
			}, {
				text: 'About',
				itemId: 'metadata',
				iconCls: 'iconed script_edit',
				handler: _setListsPanel.createCallback(0)
			}, {
				text: 'Contents',
				itemId: 'contents',
				iconCls: 'iconed script_gear',
				handler: _setListsPanel.createCallback(1)
			}, {
				text: 'Public View',
				itemId: 'publicview',
				iconCls: 'iconed',
				handler: _setListsPanel.createCallback(2)
			}, {
				xtype: 'panel',
				flex: 5,
				bodyCssClass: 'MenuPanel'
			}],
			listeners: {
				'doload': function(ListID) {
					this.ListID = ListID;
				},
				'dofilter': function() {
					var number = MainPanel.chute('panels').getLayout().getActiveItemNum();
					this.items.each(function(item, i) {
						if (item.xtype !== 'button') {
							return;
						}
						if (i === (number + 1)) {
							item.toggle(true);
						} else {
							item.toggle(false);
						}
					});
				}
			}
		}, {
			itemId: 'panels',
			xtype: 'panel',
			layout: 'card',
			activeItem: 1,
			flex: 10,
			frame: false,
			defaults: {
				viewConfig: {
					forceFit: true
				},
				scroll: 'auto',
				border: false,
				listeners: {
					'valid': {
						fn: function(field) {
							MainPanel.fireEvent('valid', field);
						},
						buffer: 50
					}
				}
			},
			items: [{
					itemId: 'listForm',
					xtype: 'form',
					help: 'list-metadata.html',
					title: 'Metadata',
					cls: 'list_subpanel',
					trackResetOnLoad: true,
					autoPersist: function(field) {
						if (NNLM.Rosters.DataManager.updateRecord('list_descriptions', this.ListID, this.getForm())) {
							NNLM.Rosters.DisplayManager.notify("setvalid");
						}
					},
					frame: false,
					defaults: {
						xtype: 'textfield',
						width: '90%'
					},
					items: [{
							xtype: 'displayfield',
							name: 'ListID',
							cls: 'metadata',
							fieldLabel: 'List ID'
						}, {
							fieldLabel: "Title",
							name: 'ListTitle'
						}, {
							fieldLabel: "Description",
							name: 'ListDescription',
							xtype: 'textarea'
						},
						NNLM.Rosters.createSelect({
							name: 'ListVisibility',
							itemId: 'visibility',
							fieldLabel: 'Visibility'
						}, [{
							name: 'Public',
							value: 'public'
						}, {
							name: 'Intranet',
							value: 'intranet'
						}]), {
							fieldLabel: "Start Date",
							itemId: 'liststartdate',
							name: 'ListStartDate',
							xtype: 'datefield'
						}, {
							fieldLabel: "End Date",
							name: 'ListEndDate',
							xtype: 'datefield'
						}
					],
					listeners: {
						'doload': function(ListID) {
							this.ListID = ListID;
							var r = NNLM.Rosters.DataManager.getRecordById("list_descriptions", this.ListID);
							this.getForm().loadRecordNoEvents(r);
						},
						'dofilter': function() {
							var r = NNLM.Rosters.DataManager.getRecordById("list_descriptions", this.ListID);
							NNLM.Rosters.DataManager.filterStore('lists', {
								value: this.ListID
							});
							this.chute('visibility').setDisabled(MainPanel.isLocker);
							this.chute('liststartdate').setDisabled(MainPanel.isLocker);
						},
						'dosave': function() {
							_dump(' dosave (list form)', 'event');
							NNLM.Rosters.DataManager.save('list_descriptions');
							this.fireEvent('dofilter');
						},
						'doreset': function() {
							this.getForm().reset();
						}
					}
				}, {
					xtype: 'editorgrid',
					cls: 'list_subpanel',
					help: 'list-contents.html',
					itemId: 'activeListContents',
					bbar: [{
							xtype: 'tbtext',
							text: 'Add a new '
						},
						NNLM.Rosters.createSelect({
							width: 30,
							itemId: 'switcher',
							listeners: {
								'collapse': function(combo) {
									MainPanel.setAddType(combo.getValue());
									combo.focus();
								}
							}
						}, [{
							name: 'Person',
							value: 'people'
						}, {
							name: 'List',
							value: 'list_descriptions'
						}, {
							name: 'Library',
							value: 'libraries'
						}]), {
							xtype: 'tbtext',
							text: ':'
						},
						_editors.people,
						_editors.list_descriptions,
						_editors.libraries
					],
					flex: 5,
					selectedListID: null,
					view: new Ext.grid.GroupingView({
						//markDirty: false,
						emptyText: 'No entries.',
						forceFit: true,
						scroll: 'auto'
					}),
					store: NNLM.Rosters.DataManager.STORES.lists,
					sm: new Ext.grid.RowSelectionModel({
						moveEditorOnEnter: false,
						listeners: {
							'rowselect': function(sm, i, r) {
								if (r.get('ListMemberTable') === 'list_descriptions') {
									//TODO
								} else {
									//TODO                             
								}
							},
							'rowdeselect': function(sm, i, r) {
								//TODO
							}
						}
					}),
					cm: _colModels.list_custom2,
					listeners: {
						'dofilter': function() {
							NNLM.Rosters.DisplayManager.addActions([
								_actions.addtolist,
								_actions.removefromlist
							]);
							_actions.addtolist.enable();
							_actions.removefromlist.enable();
						},
						'doload': function(ListID) {
							this.ListID = ListID;
							/*this.store.on('add', function(store, records, i){ 
								MainPanel.chute('activeListContents').getView().focusRow(i);
							});*/
						},
						'dosave': function() {
							_dump(" dosave (MainPanel)", 'event');
							NNLM.Rosters.DataManager.save('lists');
						},
						'doreset': function() {
							NNLM.Rosters.DataManager.reset('lists');
						},
						'celldblclick': function(grid, ri, ci, e) {
							if (ci !== 0) {
								return;
							}
							var r = this.getSelectionModel().getSelected();
							if (r.get('ListMemberTable') === 'people') {
								NNLM.Rosters.DisplayManager.notify("edit", {
									PeopleID: r.get('ListMemberTableID')
								});
							} else if (r.get('ListMemberTable') === 'libraries') {
								//TODO: for 2.1 release.
							} else {
								NNLM.Rosters.DisplayManager.notify("list", {
									ListID: r.get('ListMemberTableID')
								});
							}
						}
					}
				}, {
					itemId: 'details',
					title: 'List Details',
					help: 'list-publicview.html',
					autoScroll: true,
					cls: 'BodyPanel',
					listeners: {
						'doload': function(ListID) {
							this.ListID = ListID;
						},
						'dofilter': function() {
							this.load(NNLM.get('SERVLET_BASE_URL') + 'lists/GetListHTML.servlet.php?ListID=' + this.ListID);
						}
					}
				},
				/*,
				{
			  		itemId: 'details',
			        title: 'List Tree View',
			        help: 'list-treeview.html',
			        xtype: 'treepanel',
					rootVisible: false,
			        autoScroll: true,
			        animate: true,
			        containerScroll: true,
			        loader: new Ext.tree.TreeLoader({
			            dataUrl: NNLM.get("SERVLET_BASE_URL")+'lists/getList.servlet.php',
			            nodeParameter: 'ListID'
			        }),
			        root: {
			            nodeType: 'async',
			            allowChildren: true,
			            cls: 'emptynode',
			            text: "[Search for a list using the toolbar searchbox.]",
			            id: -1,
			            expandable: true,
			            expanded: true
			        },
			        listeners: {
		                'doload': function(ListID) {
		                    this.ListID = ListID;
		                },
						'dofilter': function(){
							var r = NNLM.Rosters.DataManager.getRecordById("list_descriptions", this.ListID);
							var root = new Ext.tree.AsyncTreeNode({
			                    text: r.get('ListTitle'),
			                    draggable:false, // disable root node dragging
			                    id: r.get('ListID'),
					            expandable: true,
					            expanded: true
			                });
							this.setRootNode(root);
						}
			        }
				},*/
				{
					frame: true,
					cls: 'BodyPanel',
					autoScroll: true,
					bodyCssClass: 'overview',
					listeners: {
						'doload': function(ListID) {
							this.ListID = ListID;
						},
						'dofilter': function() {
							this.load(NNLM.get('SERVLET_BASE_URL') + 'lists/GetListHTML.servlet.php?ListID=' + this.ListID);
						}
					}
				}
			],
			listeners: {
				'doload': function(ListID) {
					this.ListID = ListID;
					this.rainEvent('doload', ListID);
				},
				'dofilter': function() {
					//TODO
					NNLM.Rosters.DisplayManager.notify('showhelp', {
						help: this.getLayout().activeItem.help
					});
					this.getLayout().activeItem.fireEvent('dofilter');
				},
				'dosave': function() {
					this.getLayout().activeItem.fireEvent('dosave');
				},
				'doreset': function() {
					this.getLayout().activeItem.fireEvent('doreset');
				},
				'enter': function(args) {
					this.getLayout().activeItem.fireEvent('enter');
				}
			}
		}],
		listeners: {
			// all data change events come in from subpanel event relaying.
			//passing form directly instead of regular valid event arg signature.
			//this allows calling the form
			'tabchange': {
				fn: function(tabPanel, panel) {
					_dump(' tabchange (MainPanel) (buffered)', 'event');
					if (!MainPanel.loaded) {
						return;
					}
					//do viewchange instead of binding directly to tabchange
					//gives me more granular control as to when this message
					//is passed up to the Display Manager.                        
					NNLM.Rosters.DisplayManager.notify('viewchange');
					MainPanel.fireEvent('dofilter');
				},
				buffer: 100
			},
			'enter': function(args) {
				_dump(" enter (MainPanel)", 'event');
				this.chute('panels').fireEvent('enter', args);
			},
			'doload': function(ListID) {
				this.ListID = ListID;
				var r = NNLM.Rosters.DataManager.getRecordById("list_descriptions", this.ListID);
				MainPanel.isLocker = r.get("Locker");
				var title = NNLM.Rosters.DisplayManager.ellipsis(r.get('ListTitle'));
				this.setTitle(title);
				this.rainEvent('doload', ListID);
				this.setAddType('people');
			},
			'dofilter': {
				fn: function() {
					NNLM.Rosters.DisplayManager.clearActions();
					NNLM.Rosters.DisplayManager.notify("wait", false);
					NNLM.Rosters.DisplayManager.notify('beforefilter', this);
					NNLM.Rosters.DataManager.filterStore('lists', {
						value: this.ListID
					});
					NNLM.Rosters.DataManager.filterStore('list_descriptions', {
						value: this.ListID
					});
					this.rainEvent('dofilter');
				},
				buffer: 200
			},
			'dosave': function() {
				this.chute('panels').fireEvent('dosave');
			},
			'doreset': function() {
				this.rainEvent('doreset');
			},
			'relay': function(o) {
				if (_functions[o.target] === undefined) {
					throw 'Relay called a non-existing target';
				}
				_functions[o.target](o.args);
			}
		},
		load: function(args) {
			if (!window.isset(args.ListID)) {
				_e("ListID argument was not passed properly.");
				return false;
			}
			_dump("Executing list load() operation.");
			NNLM.Rosters.DisplayManager.notify('viewchange');
			this.ListID = args.ListID;
			this.fireEvent('doload', this.ListID);
			this.loaded = true;
			//this.fireEvent('dofilter');
			if (!isset(args.Visibility)) {
				args.Visibility = 'intranet';
			}
			NNLM.Rosters.DataManager.load({
				params: args,
				storeName: 'lists',
				scope: this,
				callback: function() {
					MainPanel.fireEvent('dofilter');
				}
			});
			return {
				type: 'list',
				id: args.ListID
			};
		},

		setAddType: function(typeId) {
			var bbar = MainPanel.chute('activeListContents').getBottomToolbar();
			var value = '';
			bbar.chute('switcher').setValue(typeId);
			bbar.items.each(function() {
				if (this.choice === undefined) {
					return;
				}
				if (this.isVisible()) {
					value = this.getValue();
				}
				if (this.itemId === typeId) {
					this.setValue(value);
					this.show();
					return;
				}
				this.hide();
			});
		}
	});
	return MainPanel;
};
NNLM.Rosters.dump("List management system loaded.");
NNLM.Rosters.List.loaded = true;
//NNLM.Rosters.DisplayManager.notify('scriptloaded', {script:'list'});