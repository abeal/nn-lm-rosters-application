/**
 *     @class NNLM.Rosters.DisplayManager
 *     <p>The DisplayManager is responsible for handling the detail
 *    of the visual output.  All components either make requests directly
 *    to it, or notify it once a change in status has occurred.</p>
 *    <p>Any component that is changing
 *   itself or being changed by another component is expected to notify the DisplayManager.</p>
 *
 *   @author     Aron Beal
 *   @copyright (c) 2008, by the University of Washington
 *   @date       May 6 2008
 *   @license All files in this web application are  licensed under the
 *   terms of the Open Source LGPL 3.0 license.
 *   License details: http://www.gnu.org/licenses/lgpl.html
 */
Ext.ns('NNLM.Rosters');
NNLM.Rosters.DisplayManager = function() {
	//CAPS elements correspond to DOM page body sections.
	var MAIN = null;
	var BODY = null;
	var HELP = null;
	var CONSOLE = null;
	var NORTHCONTROLS = null;
	var WESTCONTROLS = null;
	var SOUTHCONTROLS = null;
	var PROGRESS = null;
	var KEYMAP = null;
	var EVENT_DELAY = 500;
	var LANDING_PAGE = null;
	var displaysLocked = [];
	var activePanels = {};
	var maskedPanels = [];
	var msgDialogs = new Ext.util.MixedCollection();
	var statusmessages = [];
	var messageConsole = null;
	var currentUserPID = null;
	var _actions = {
		/** 
		 * Saves all local changes to the server.
		 */
		'save': new Ext.Action({
			text: 'Save&nbsp;Changes',
			iconCls: 'server_database',
			itemId: 'save',
			permanent: true,
			disabled: true,
			tooltip: 'This button ignites when the focus leaves a changed value in a form field.',
			handler: function(b, e) {
				NNLM.Rosters.DisplayManager.notify('dosave');
			}
		}),
		/** 
		 * Resets all local information
		 */
		'reset': new Ext.Action({
			itemId: 'reset',
			iconCls: 'server_cancel',
			text: 'Cancel&nbsp;Changes',
			disabled: true,
			permanent: true,
			handler: function(b, e) {
				NNLM.Rosters.DisplayManager.notify('doreset');
			}
		})
	};
	var eventHandlers = {
		/**
		 * creats a modal dialog with the name and version number
		 **/
		about: function() {
			eventHandlers.createmodal({
				config: [{
					xtype: 'panel',
					html: "<h1>NN/LM Rosters Application</h1><p>Version " + NNLM.get('VERSION_NUMBER') + "</p>"
				}],
				ok_enabled: true,
				ok_handler: function() {}
			});
		},
		/** 
		 * invoked by DataManager after any store filtering operation.
		 */
		afterfilter: {
			fn: function() {
				_d("afterfilter", 'event');
				_getCurrentPanel().getEl().unmask();
			},
			buffer: 100
		},
		/** 
		 * invoked by Datamanager after any store load operation.
		 **/
		afterload: {
			fn: function(args) {
				_d("afterload (buffered)", 'event');
				NNLM.Rosters.DisplayManager.notify("wait", false);
				_getCurrentPanel().fireEvent('afterload');
			},
			buffer: EVENT_DELAY
		},
		/**
		 * Invoked by Datamanager after a successful save event.
		 **/
		aftersave: {
			fn: function(args) {
				_d("aftersave (buffered)", 'event');
				NNLM.Rosters.DisplayManager.notify('wait', false);
				_getCurrentPanel().fireEvent('aftersave');
				//eventHandlers.statusmsg("Changes saved.");
			},
			buffer: EVENT_DELAY
		},
		/**
		 * Invoked by Datamanager after a reset event.
		 **/
		afterreset: {
			fn: function(args) {
				_d("afterreset (buffered)", 'event');
				eventHandlers.statusmsg("Changes canceled.");
				//NNLM.Rosters.DisplayManager.notify("setdirty", false);
				NNLM.Rosters.DisplayManager.notify("wait", false);
				//_getCurrentPanel().fireEvent('afterreset');
			},
			buffer: EVENT_DELAY
		},
		/** 
		 * Invoked by Datamanager after a write event. May be obsolete.
		 **/
		afterwrite: function(args) {
			_d("afterwrite", 'event');
			_toggleProgress(false);
			//NNLM.Rosters.DisplayManager.notify("setdirty", false);
			NNLM.Rosters.DisplayManager.notify("wait", false);
		},
		/** 
		 * not yet operational
		 **/
		changepassword: function(args) {
			if (!NNLM.Rosters.DisplayManager.isViewChangeOk()) {
				return;
			}
			var cp = BODY.getComponent('passwd_panel');
			if (cp) {
				cp.fireEvent('dofilter');
				return;
			}
			cp = BODY.add({
				xtype: "panel",
				layout: 'hbox',
				id: 'passwd_panel',
				help: 'change-password.html',
				itemId: 'passwd_panel',
				title: 'Change your intranet password',
				closable: true,
				frame: true,
				defaults: {
					flex: 1
				},
				layoutConfig: {
					align: 'stretch',
					pack: 'start'
				},
				items: [{
					xtype: 'panel',
					frame: true,
					tpl: new Ext.XTemplate('<tpl><h2>Modifying information for {FirstName} {LastName}</h2>', '</tpl>'),
					listeners: {
						'dofilter': function() {
							NNLM.Rosters.DisplayManager.notify('showhelp', {
								help: this.help
							});
							var r = NNLM.Rosters.DataManager.getCurrentUser();
							//alert(r.get("FirstName"));
							this.tpl.overwrite(this.body, r.data);
						}
					}
				}, {
					xtype: 'form',
					frame: true,
					defaults: {
						allowBlank: false
					},
					items: [{
						xtype: 'textfield',
						fieldLabel: 'Old Password'
					}, {
						xtype: 'textfield',
						fieldLabel: 'New Password'
					}, {
						xtype: 'textfield',
						fieldLabel: 'New Password (again)'
					}]
				}],
				listeners: {
					dofilter: function() {
						this.rainEvent("dofilter");
					}
				}
			});
			BODY.setActiveTab(cp.id);
			//NNLM.Rosters.DisplayManager.notify('showhelp',{help:cp.help});
		},
		/**
		 * invoked by anyone as a request to close a given panel.
		 *
		 **/
		close: function(args) {
			if (args.itemId !== undefined) {
				args.id = BODY.getComponent(args.itemId).id;
			}
			if (args.id !== undefined) {
				var p = BODY.chute(args.id);
				activePanels[p.panelType].pcount--;
				p.destroy();
				return;
			}
			throw "Error in DisplayManager close routine";
		},
		confirm: function(args) {
			eventHandlers.createmodal({
				config: {
					title: args.title,
					xtype: 'label',
					text: args.text,
					ok_enabled: true
				},
				ok_handler: args.callback,
				nok_handler: NNLM.Rosters.DisplayManager.msg.createCallback("Operation cancelled")
			});
		},
		console: function() {
			if (messageConsole === null) {
				messageConsole = new Ext.Window({
					bodyCssClass: 'BodyPanel console',
					autoDestroy: true,
					closable: true,
					height: 350,
					title: 'Message History',
					width: 700,
					modal: true,
					layout: 'border',
					bbar: [{
						xtype: 'button',
						itemId: 'mailbutton',
						text: 'Send log to abeal@uw.edu',
						handler: function() {
							var msgs = [statusmessages.slice(0).reverse()];
							msgs.push("OS: ");
							msgs.push((Ext.isMac) ? 'Mac' : (Ext.isWindows) ? "Windows" : (Ext.isLinux) ? "Linux" : "Unknown");
							msgs.push("Browser:");
							msgs.push((Ext.isIE && Ext.isIE6) ? 'IE6' : (Ext.isIE && Ext.isIE7) ? 'IE7' : (Ext.isIE && Ext.isIE8) ? 'IE8' : (Ext.isIE && Ext.isIE9) ? 'IE9' : (Ext.isIE && Ext.isIE) ? 'IE' : (Ext.isGecko && Ext.isGecko3) ? 'Firefox 3.x+' : (Ext.isGecko) ? 'Firefox' : (Ext.isSafari && Ext.isSafari2) ? 'Safari 2.x' : (Ext.isSafari && Ext.isSafari3) ? 'Safari 3.x' : (Ext.isSafari && Ext.isSafari4) ? 'Safari 4.x' : (Ext.isSafari) ? 'Safari' : (Ext.isOpera) ? 'Opera' : "Unknown Browser");
							dump(msgs);
							msgs = Ext.util.JSON.encode(msgs);
							Ext.Ajax.request({
								url: NNLM.get('SERVLET_BASE_URL') + '/mailer.php',
								params: {
									'request': msgs
								},
								success: function(xhr) {
									var b = messageConsole.getBottomToolbar().getComponent('mailbutton');
									b.setText("Log sent.");
									b.disable();
								}
							});
						}
					}],
					items: [{
						itemId: 'content',
						autoScroll: true,
						region: 'center',
						flex: 3
					}, {
						itemId: 'help',
						region: 'east',
						width: 200,
						text: "Loading...",
						cls: 'HelpPanel',
						autoLoad: NNLM.get('HELP_BASE_URL') + 'message-history.html',
						flex: 1
					}],
					listeners: {
						'show': function() {
							var msgs = statusmessages.slice(0).reverse();
							var content = '<h1>Message History</h1>';
							for (var i = 0; i < msgs.length; i++) {
								content += '<p' + ((i % 2 === 0) ? ' style="background-color:#eee"' : '') + '>' + msgs[i] + '</p>';
							}
							this.chute('content').update(content);
							NNLM.Rosters.DisplayManager.clearDialogs();
						},
						'beforeclose': function() {
							messageConsole = null;
						}
					}
				});
			}
			messageConsole.show();

		},
		/**
		 * adds a new panel of type "create"
		 * @param args {object} the args object
		 */
		create: function(args) {
			_d("create", 'event');
			if (!NNLM.Rosters.DisplayManager.isViewChangeOk()) {
				return;
			}
			var activeInterface = (args.typeId === undefined) ? 'people' : args.typeId;
			NNLM.Rosters.DisplayManager.notify('createpanel', {
				name: 'create',
				uniqueId: _createPanelId(),
				fn: function(newpanel) {
					newpanel.setInterface(activeInterface);
					NNLM.Rosters.DisplayManager.notify('showhelp', {
						help: 'Create.html'
					});
				}
			});
		},
		createperson: function(args) {
			args = (args === undefined) ? {} : args;
			args.typeId = 'people';
			eventHandlers.create(args);
		},
		/**
		 * creates a new panel of type 'list'
		 * @param Object args Can contain the following properties:
		 * 	TODO: complete
		 * @returns void
		 * @type String|Object|Array|Boolean|Number
		 */
		createlist: function(args) {
			args = (args === undefined) ? {} : args;
			args.typeId = 'list_descriptions';
			eventHandlers.create(args);
		},
		/**
		 * creates a new panel
		 * @param args {string} the args object
		 */
		createpanel: function(args) {
			_d(" createpanel", 'event');
			args = {
				name: args.name === undefined ? 'Untitled Panel' : args.name,
				uniqueId: args.uniqueId === undefined ? _createPanelId() : args.uniqueId,
				fn: args.fn === undefined ?
					function() {} : args.fn
			};
			try {
				//script namespaces are stored and referenced in camel case
				//everything else is stored and referenced in lowercase
				var ns = args.name.substr(0, 1).toUpperCase() + args.name.substr(1);
				var nsl = args.name.toLowerCase();
				if (NNLM.Rosters.Configs[nsl] === undefined) {
					_e(nsl + " is not defined as a valid panel configuration.");
				}
				if (activePanels[nsl] === undefined) {
					//establish the object space for the new namespace.
					activePanels[nsl] = {
						pcount: 0
					};
				}
				if (NNLM.Rosters.Configs[nsl].maxCount !== undefined && NNLM.Rosters.Configs[nsl].maxCount <= activePanels[nsl].pcount) {
					_e("Maximum number of tabs of this type has been reached.");
					return;
				}
				NNLM.Rosters.DisplayManager.notify("wait", true);
				//Load any required scripts, and call the init method on the namespace once
				//loaded.
				Ext.ns('NNLM.Rosters.' + ns);
				_d('Firing createpanel callback for ns: ' + ns, 3);
				NNLM.Rosters.DisplayManager.notify("wait", false);
				if (NNLM.Rosters[ns].makeInstance === undefined) {
					_e("Namespace " + ns + " is not defined: ");
					return;
				}
				activePanels[nsl].pcount++;
				var p = NNLM.Rosters[ns].makeInstance(args.uniqueId);
				p.setTitle(NNLM.Rosters.Configs[nsl].title);
				p.panelType = nsl;
				p.addListener('beforeclose', function() {
					if (!NNLM.Rosters.DisplayManager.isViewChangeOk()) {
						return false;
					}
				});
				p.addListener('close', (function(panelKey) {
					p.destroy();
					activePanels[panelKey].pcount--;
				}).createDelegate(p, [nsl]));
				//add context menu for refreshing tab content, in case a refresh handler misfires.
				p.addListener('contextmenu', function(tabpanel, panel, e) {
					e.preventDefault();
				});

				BODY.add(p);
				BODY.setActiveTab(BODY.items.length - 1);
				NNLM.Rosters[ns].loaded = true;
				args.fn(p);
				if (NNLM.Rosters.Configs[nsl].content !== undefined) {
					p.load(NNLM.get('BODY_BASE_URL') + NNLM.Rosters.Configs[nsl].content);
				}
			} catch (e) {
				_e(e.message);
				throw e;
			}
		},
		/** createmodal
		 * creates a modal dialog with ok and cancel buttons
		 * This modal dialog can have one internal component of type
		 * Ext.form.Field or Ext.form.Combobox (or appropriate registered
		 * subclass).
		 * @param args {object}:
		 *      config {object}: an Ext style configuration object for a
		 *      registered Ext form component to be used as the primary display.
		 *      This configuration object may anticipate it is being placed in
		 *      a panel with a form layout.
		 *      ok_handler {function}: the callback to execute if the user
		 *      clicks 'ok'.  This callback will be passed the realized Ext
		 *      component from the configuration object in the first argument
		 *      as a parameter when executed.
		 */
		createmodal: function(args) {
			if (args === undefined || args.config === undefined || args.ok_handler === undefined) {
				_e("createmodal: missing or invalid argument.");
				return;
			}
			if (args.config.listeners === undefined) {
				args.config.listeners = {};
			}
			args.config.validateOnBlur = false;
			args.config.validationDelay = 50;
			args.config.listeners.invalid = function() {
				this.ownerCt.getBottomToolbar().toggleButton('b_ok', false);
			};
			args.config.listeners.valid = function() {
				this.ownerCt.getBottomToolbar().toggleButton('b_ok', true);
			};
			args.ok_enabled = (args.config.ok_enabled === undefined) ? false : args.config.ok_enabled;
			//_lockPanel(BODY.id);
			var win = new Ext.Window({
				title: (args.config.title === undefined) ? '' : args.config.title,
				layout: (args.config.xtype === 'textfield') ? 'form' : 'fit',
				width: 500,
				closable: (args.config.xtype === 'textfield') ? false : true,
				hideBorders: true,
				modal: true,
				autoDestroy: true,
				bodyStyle: "background-color: white; padding: 10px;",
				items: [args.config],
				bbar: [{
					xtype: 'button',
					itemId: 'b_ok',
					text: 'OK',
					disabled: (!args.ok_enabled),
					getModalField: function() {
						var f = win.get(0);
						return (f.xtype === 'label') ? true : f;
					},
					handler: function() {
						var f = this.getModalField();
						args.ok_handler(f);
						args = undefined;
						win.close();
						_unlockPanel(BODY.id);
					}
				}, {
					xtype: 'button',
					text: 'Cancel',
					handler: function() {
						if (args.nok_handler !== undefined) {
							args.nok_handler();
						}
						args = undefined;
						win.close();
						_unlockPanel(BODY.id);
					}
				}],
				keys: [{
					key: Ext.EventObject.ENTER,
					fn: function() {
						var field = win.get(0);
						if (field.getValue !== undefined && field.getValue() === '') {
							return;
						}
						args.ok_handler(field);
						args = undefined;
						win.close();
						_unlockPanel(BODY.id);
					}
				}]
			});
			win.show();
		},
		datachanged: function(bool) {
			_d('data changed: ' + bool, 3);
			if (bool) {
				_d("Igniting save button", 'info');
				_actions.save.enable();
				_actions.save.setIconClass('server_database_over');
				_actions.reset.enable();
			} else {
				_d("Quenching save button", 'warn');
				_actions.save.setIconClass('server_database');
				_actions.save.disable();
				_actions.reset.disable();
			}
		},
		dolayout: function() {
			BODY.doLayout(false, true);
			_getCurrentPanel().fireEvent('dofilter');
		},
		dosave: function(b, e) {
			_d("dosave", 'event');
			_getCurrentPanel().fireEvent('dosave');
			//_toggleProgress(true);
		},
		doreset: function(b, e) {
			_d(" doreset", 'event');
			_getCurrentPanel().fireEvent('doreset');
		},
		edit: function(args) {
			//_d(" edit.", 'event');
			//_expandMenu('edit');
			if (!NNLM.Rosters.DisplayManager.isViewChangeOk()) {
				return false;
			}
			NNLM.Rosters.DisplayManager.notify('showhelp', {
				help: 'Edit.html'
			});
			//if no id is present, grab the relevant one from the current user server login
			//and set up to call back after.
			args = (args === undefined) ? {} : args;
			if (args.ListID !== undefined) {
				return eventHandlers.list(args);
			}
			var r = null;
			if (args.PeopleID === undefined) {
				r = NNLM.Rosters.DataManager.getCurrentUser();
				_d("establishing peopleid as " + r.get('PeopleID'));
				args.PeopleID = r.get('PeopleID');
			} else {
				r = NNLM.Rosters.DataManager.getRecordById("people", args.PeopleID);

			}
			//if user record is not present locally, load it and set up to call back after.
			NNLM.Rosters.DisplayManager.notify('wait', true);
			if (!r) {
				NNLM.Rosters.DataManager.load({
					params: {
						PeopleID: args.PeopleID
					},
					callback: function() {
						NNLM.Rosters.DisplayManager.notify('wait', false);
						NNLM.Rosters.DisplayManager.notify('edit', args);
					}
				});
				return true;
			}
			_d('Edit called.  People id: ' + r.get('PeopleID'));
			var panelId = _checkPanel({
				PeopleID: r.get('PeopleID'),
				type: "edit"
			});

			//If the panel is already open, switch to it and return.
			if (panelId !== false) {
				var p = BODY.chute(panelId);
				_d('Edit panel already open (' + panelId + '); switching views');
				BODY.setActiveTab(panelId);
				return true;
			}
			var _uid = _createPanelId();
			NNLM.Rosters.DisplayManager.notify('createpanel', {
				name: 'edit',
				uniqueId: _uid,
				fn: function(newpanel) {
					_d("loading info into edit panel", 3);
					newpanel.load(args);
				}
			});
			NNLM.Rosters.DisplayManager.notify('wait', false);
			return true;
		},
		enter: {
			fn: function(args) {
				_d(" enter (DisplayManager)", 'event');
				_getCurrentPanel().fireEvent('enter', [args.target]);
			},
			buffer: 100
		},
		error: function(args) {
			NNLM.Rosters.DisplayManager.notify('wait', false);
			if (Ext.isString(args)) {
				args = {
					text: args
				};
			}
			args.cls = 'error';
			eventHandlers.statusmsg(args);
		},
		expandmenu: function(args) {
			if (args.menu === undefined) {
				throw "Incorrect parameters sent to expandmenu event";
			}
			if (args.menu === 'actions') {
				eventHandlers.toggleactions();
			} else {
				NORTHCONTROLS.getTopToolbar().chute(args.menu).showMenu();
			}

		},
		exception: function(args) {
			//NNLM.Rosters.DisplayManager.notify("setdirty", false);
			NNLM.Rosters.DisplayManager.notify("wait", false);
			var errmsg = "An unexpected error has occurred.  Please reload the page to reset the application.";
			if (isset(args.response)) {
				if (args.response !== undefined && args.response.raw !== undefined && args.response.raw.results !== undefined && args.response.raw.results.error !== undefined) {
					errmsg = args.response.raw.results.error;
				} else if (args.response.responseText !== undefined) {
					errmsg = args.response.responseText;
				}
			} else if (args.text !== undefined) {
				errmsg = args.text;
			}
			eventHandlers.statusmsg({
				text: errmsg,
				cls: 'error'
			});
		},
		find: function(args) {
			_d(" find", 'event');
			args = (args === undefined) ? {} : args;
			var typeId = (args.typeId === undefined) ? 'people' : args.typeId;
			var searchby = (args.searchby === undefined) ? 'name' : args.searchby;
			if (!NNLM.Rosters.DisplayManager.isViewChangeOk()) {
				return;
			}
			NNLM.Rosters.DisplayManager.notify('createpanel', {
				name: 'find',
				uniqueId: _createPanelId(),
				fn: function(newpanel) {
					newpanel.setInterface(typeId, searchby);
				}
			});
			NNLM.Rosters.DisplayManager.notify('showhelp', {
				help: 'Find.html'
			});
			return;
		},
		findperson: function(args) {
			args = (args === undefined) ? {} : args;
			args.typeId = 'people';
			args.searchby = 'name';
			eventHandlers.find(args);
		},
		findpersonbylibid: function(args) {
			args = (args === undefined) ? {} : args;
			args.typeId = 'people';
			args.searchby = 'libid';
			eventHandlers.find(args);
		},
		findpersonbyname: function(args) {
			eventHandlers.findperson(args);
		},
		findpersonbypeopleid: function(args) {
			args = (args === undefined) ? {} : args;
			args.typeId = 'people';
			args.searchby = 'peopleid';
			eventHandlers.find(args);
		},
		findpersonbyrole: function(args) {
			args = (args === undefined) ? {} : args;
			args.typeId = 'people';
			args.searchby = 'role';
			eventHandlers.find(args);
		},
		findpersonbyregion: function(args) {
			args = (args === undefined) ? {} : args;
			args.typeId = 'people';
			args.searchby = 'region';
			eventHandlers.find(args);
		},
		findlist: function(args) {
			args = (args === undefined) ? {} : args;
			args.typeId = 'list_descriptions';
			eventHandlers.find(args);
		},
		ignite: function(args) {
			/*var t = NORTHCONTROLS.getTopToolbar();
			if(typeof args === 'string'){
				args = [args];
			}                 
			for(var i = 0; i < args.length; i++){            
		    	t.toggleMenuOption('actions', args[i], true);
			}     */
		},
		/** list
		 opens the list interface.  Creates it if it does not exist.
		 **/
		list: function(args) {
			_d(" list", 'event');
			if (!NNLM.Rosters.DisplayManager.isViewChangeOk()) {
				return;
			}
			args = (args === undefined) ? {} : args;
			args._count = args._count === undefined ? 0 : (args._count + 1);
			if (args._count > 1) {
				NNLM.Rosters.DisplayManager.notify('error', {
					text: "Recursion halt triggered.",
					ttl: 10
				});
				return;
			}
			if (args === undefined || args.ListID === undefined) {
				NNLM.Rosters.DisplayManager.notify('wait', true);
				NNLM.Rosters.DataManager.load({
					params: {
						ListID: 'self'
					},
					storeName: 'list_descriptions',
					callback: function(success, records) {
						NNLM.Rosters.DisplayManager.notify('wait', false);
						NNLM.Rosters.DisplayManager.notify('list', {
							ListID: records[0].get('ListID')
						});
					}
				});
				return;
			}
			//attribute PeopleID is required to edit.
			var lid = args.ListID;
			_d('List called.  List id: ' + lid, 3);

			var r = NNLM.Rosters.DataManager.getRecordById("list_descriptions", lid);
			if (!r) {
				NNLM.Rosters.DisplayManager.notify('wait', true);
				NNLM.Rosters.DataManager.load({
					params: args,
					storeName: 'list_descriptions',
					callback: function(success, records) {
						NNLM.Rosters.DisplayManager.notify('wait', false);
						if (!success || records.length === 0) {
							NNLM.Rosters.DisplayManager.notify('error', {
								text: "The requested record could not be found."
							});
							return;
						}
						NNLM.Rosters.DisplayManager.notify('list', {
							ListID: records[0].get('ListID'),
							_count: args._count
						});
					},
					scope: this
				});
				return;
			}
			//Record already loaded at this point
			//check for list panel existence.
			var panelId = _checkPanel({
				type: 'list',
				ListID: lid
			});

			//If the panel is already open, switch to it and return.
			if (panelId !== false) {
				var p = BODY.getById(panelId);
				_d('List panel already open (' + panelId + '); switching views');
				BODY.setActiveTab(panelId);
				NNLM.Rosters.DisplayManager.notify('showhelp', {
					help: 'List.html'
				});
				return;
			}
			var _uid = _createPanelId();
			NNLM.Rosters.DisplayManager.notify('createpanel', {
				name: 'list',
				uniqueId: _createPanelId(),
				fn: function(newpanel) {
					_d("loading info into list panel", 3);
					newpanel.load({
						ListID: lid
					});
					NNLM.Rosters.DisplayManager.notify('showhelp', {
						help: 'List.html'
					});
				}
			});
		},
		/* msg
          causes an Ext message dialog to be displayed in the center of the
          display.
          @param args{object} should have the following values:
            args.msg {string} the text message to display
        */
		msg: function(args) {
			args.cls = 'notice';
			eventHandlers.statusmsg(args);
		},
		/** prompttext
		 * displays a user dialog with a text box.
		 * @param args {object} expects the following parameters:
		 *      text {string} the text to display next to the dialog
		 *      callback {function} the callback to execute if the user
		 *      clicks 'ok'.
		 *
		 */
		prompt: function(args) {
			args.type = (args.type === undefined) ? 'default' : args.type;
			var html = '<p style="color: red">' + args.text + '</p>';
			switch (args.type) {
				case 'destroy':
					html = '<div style="color: red;font-weight: bold;">' + args.text + '</div>';
					break;
				default:
					break;
			}
			eventHandlers.createmodal({
				config: {
					xtype: 'panel',
					ok_enabled: true,
					html: html,
					bodyCssClass: (args.css === undefined) ? null : args.css,
					bodyStyle: "margin: 5px",
					allowBlank: false
				},
				ok_handler: (args.callback === undefined) ? NNLM.Rosters.emptyFn : args.callback
			});
		},
		/** prompttext
		 * displays a user dialog with a text box.
		 * @param args {object} expects the following parameters:
		 *      text {string} the text to display next to the dialog
		 *      callback {function} the callback to execute if the user
		 *      clicks 'ok'.
		 *
		 */
		prompttext: function(args) {
			eventHandlers.createmodal({
				config: {
					xtype: 'textfield',
					fieldLabel: args.text,
					width: '50%',
					bodyStyle: "margin: 5px",
					allowBlank: false
				},
				ok_handler: (args.callback === undefined) ? NNLM.Rosters.emptyFn : args.callback
			});
		},
		quench: function(args) {
			/*var t = NORTHCONTROLS.getTopToolbar();
			if(typeof args === 'string'){
				args = [args];
			}                 
			for(var i = 0; i < args.length; i++){            
		    	t.toggleMenuOption('actions', args[i], false);
			}    */
		},
		relay: function(o) {
			if (o.target === undefined) {
				throw "Relay message not properly constructed.";
			}
			_getCurrentPanel().fireEvent('relay', o);
		},
		removetransient: function() {
			_removeTransientButtons();
		},
		reset: {
			fn: function() {
				eventHandlers.statusmsg("Data reset.");
			},
			buffer: 100
		},
		/** scriptloaded
		 * lets external scripts notify the display manager when their load
		 * is complete.
		 * @param args {object} valid values are:
		 *  'script': the namespace of the script whose load is complete.
		 */
		scriptloaded: function(args) {
			if (args.script === undefined) {
				_e('missing parameter for scriptloaded function');
			}
			var ns = args.script.substr(0, 1).toUpperCase() + args.script.substr(1).toLowerCase();
			NNLM.Rosters[ns].loaded = true;
			if (NNLM.Rosters[ns].callback !== undefined) {
				NNLM.Rosters[ns].callback();
				delete NNLM.Rosters[ns].callback;
			}
		},
		setcurrentuser: function(args) {
			currentUserPID = args.PeopleID;
		},
		/** showhelp
		 * causes the help panel on the right side to load
		 * the html content indicated by the passed url
		 * @param args.url {string} the url to the html help file content.
		 **/
		showhelp: {
			fn: function(args) {
				//_d(" showhelp ("+args.help+")", 'event');
				if (!isset(args.help)) {
					_d("nohelp! (" + args.help + ")", 'warn');
					return;
				}
				HELP.load({
					url: NNLM.get('HELP_BASE_URL') + args.help,
					cls: 'HelpPanel',
					nocache: false,
					text: "Loading...",
					timeout: 3,
					scripts: false
				});
			},
			buffer: 1500
		},
		/** slidemsg
		 * displays a message in a slide-down window in the display top right
		 * @param msg {string} the message to display.
		 */
		slidemsg: function(msg) {
			if (!this.msgCt) {
				this.msgCt = Ext.DomHelper.insertFirst(document.body, {
						id: 'msg-div'
					},
					true);
			}
			this.msgCt.alignTo(document, 't-t');
			//var s = String.format.apply(String, Array.prototype.slice.call(arguments, 1));
			var m = Ext.DomHelper.append(this.msgCt, {
					html: createBox('Rosters system message:', msg)
				},
				true);
			m.slideIn('t').pause(1).ghost("t", {
				remove: true
			});
		},
		/** statusmsg
		 * displays a message to the user in the status bar
		 * at the bottom of the interface, with effects
		 * param args{object}:
		 *      text {string}: the text message to display
		 *      msg {string}: (optional) alias of text
		 */
		statusmsg: function(args) {
			if (isset(statusmessages.firecountTimeout)) {
				window.clearTimeout(statusmessages.firecountTimeout);
			}
			args = (typeof args === undefined) ? {} : (typeof args === 'string') ? {
				text: args
			} : args;
			if (args.text === undefined) {
				return;
			}
			args.cls = (!isset(args.cls)) ? 'msg' : args.cls;
			NNLM.Rosters.dump("status message: " + args.text, 1);
			statusmessages.push('<span class="' + args.cls + ' help_example">' + args.text + '</span>');
			msgDialogs.add(Ext.example.msg(args));
		},
		tab: function(args) {
			_d(" tab (DisplayManager)", 'event');
			_getCurrentPanel().fireEvent('tab', [args.target]);
		},
		tabchange: {
			fn: function(args) {
				_d(' tabchange', 'event');
				var p = _getCurrentPanel();
				NNLM.Rosters.DataManager.clearFilters();
				NNLM.Rosters.DisplayManager.clearDialogs();
				p.fireEvent('tabchange', p);
			},
			buffer: EVENT_DELAY
		},
		toggleactions: function() {
			var ACTIONS = _getCurrentPanel().getActions();
			if (ACTIONS === null) {
				return;
			}
			if (ACTIONS.hasVisibleMenu()) {
				ACTIONS.hideMenu();
				return;
			}
			ACTIONS.showMenu();

		},
		togglehelp: function() {
			if (HELP.collapsed) {
				HELP.expand();
				return;
			}
			HELP.collapse();
		},
		transactionlog: {
			fn: function() {
				if (!NNLM.Rosters.DisplayManager.isViewChangeOk()) {
					return;
				}
				var tlog = BODY.getComponent('transaction_log');
				if (tlog) {
					tlog.fireEvent('dofilter');
					return;
				}
				var entryTpl = new Ext.XTemplate('<tpl for=".">', '<p>On {ModifiedDate}, {Modifier} ', '{[this.actionText(values.ActionTaken)]} ', '{[this.typeText(values.TableModified)]} ', ' "{Modified}".', '<br /><span class="metadata">Comments: {Comments}</span></p>', '</tpl>', {
					actionText: function(action) {
						switch (action) {
							case 'CREATE':
								return 'created a new ';
							case 'UPDATE':
								return 'updated the ';
							case 'DELETE':
								return 'deleted a(n) ';
							default:
								return ' ... ';
						}
					},
					typeText: function(table) {
						switch (table) {
							case 'accounts':
								return 'account for the staff member named';
							case 'list_descriptions':
								return 'list titled';
							case 'lists':
								return 'list entry for the list titled';
							case 'people':
								return 'staff record for the staff member named';
							case 'positions':
								return 'position for the staff member named';
							case 'roles':
								return 'role for the staff member named';
						}
						return '';
					}
				});
				var tlogStore = new Ext.data.JsonStore({
					autoDestroy: true,
					url: NNLM.get('SERVLET_BASE_URL') + 'transactionlog.servlet.php',
					root: 'results',
					totalProperty: 'count',
					remoteSort: true,
					record: 'row',
					successProperty: 'success',
					fields: [{
						name: 'ModifiedDate',
						type: 'string'
					}, {
						name: 'TableModified',
						type: 'string'
					}, {
						name: 'Modifier',
						type: 'string'
					}, {
						name: 'Modified',
						type: 'string'
					}, {
						name: 'ActionTaken',
						type: 'string'
					}, {
						name: 'Comments',
						type: 'string'
					}],
					paramsAsHash: true,
					listeners: {
						load: function(store, records) {
							_d("Transaction log loaded.", 3);
						},
						exception: function(proxy, type, action, options, response, arg) {
							_e("The transaction log store failed to load!");
						}
					}
				});
				tlogStore.load({
					params: {
						start: 0,
						limit: 50
					}
				});
				tlog = BODY.add({
					xtype: 'grid',
					id: 'transaction_log',
					help: 'transaction-log.html',
					itemId: 'transaction_log',
					title: 'NN/LM Rosters Transaction Log',
					closable: true,
					emptyText: 'No entries found',
					frame: true,
					bbar: [{
						xtype: 'paging',
						pageSize: 50,
						store: tlogStore,
						displayInfo: true,
						displayMsg: 'Displaying log entries {0} - {1} of {2}',
						emptyMsg: "No entries to display"
					}],
					store: tlogStore,
					disableSelection: true,
					loadMask: true,

					// grid columns
					colModel: new Ext.grid.ColumnModel({
						columns: [{
							header: "Entry",
							dataIndex: 'ModificationDate',
							renderer: function(value, metaData, record, rowIndex, colIndex, store) {
								return entryTpl.applyTemplate(record.data);
							}
						}]
					}),

					// customize view config
					viewConfig: {
						forceFit: true
					},
					listeners: {
						'dofilter': function() {
							//alert(this.help);
							NNLM.Rosters.DisplayManager.notify('showhelp', {
								help: this.help
							});
						}
					}
				});
				BODY.setActiveTab(tlog.id);
				NNLM.Rosters.DisplayManager.notify('showhelp', {
					help: tlog.help
				});
			},
			buffer: 200
		},

		/** viewchange
		 * a notification event to the Display Manager that one of its
		 * child components has changed its active view.  Note that tabchange also
		 * triggers this evnt
		 * @param args.p {Ext.Component} The component that has altered
		 * its view.  Usually, this Component is a direct child of one
		 * of the DisplayManager core components; any subchildren relay
		 * their information to these direct children, who handle internal
		 * logic before notification.  So, this will not be the direct
		 * component that fired the event.
		 * This method is not expected to do anything to the component that
		 * originated the event, only to the other interface components of the
		 * manager.
		 */
		viewchange: function(args) {
			args = (args === undefined) ? {} : args;
			_d(" viewchange", 'event');
			//reset all controls to be disabled.  The component will request-enable
			//as needed in subsequent calls.
			if (args.help !== undefined) {
				NNLM.Rosters.DisplayManager.notify('showhelp', args);
			}
			NORTHCONTROLS.fireEvent('afterrender');
			return true;
		},
		/** wait
		 * * displays a modal waiting dialog with the message defined by @param
		 * @param toggle {boolean} true to turn modal dialog on, false otherwise.
		 * @param msg || text {string} [optional] the message to display (second parameter only)
		 * @param panel {Ext.Component}
		 * @param decay {int} [optional] the time when the window should automatically unmask (in seconds)
		 * @returns {void}
		 */
		wait: function(args) {
			if (typeof args === "boolean") {
				args = {
					wait: args
				};
			}
			//_d(" wait.  Args: " + NNLM.printObject(args), 'event');
			args.decay = (args.decay === undefined) ? NNLM.get("MESSAGE_DELAY_LENGTH") : args.decay;
			args.msg = 'Communicating with the server...';

			if (args.wait !== undefined) {
				args.toggle = args.wait;
			}
			args.toggle = (args.toggle === undefined) ? false : args.toggle; //if missing, is error; turn off by default.
			if (args.text !== undefined) {
				eventHandlers.statusmsg(args.text);
			}
			if (args.toggle === true) {
				BODY.getEl().mask(args.msg, 'x-mask');
				//_lockPanel(BODY.id);
				NNLM.Rosters.DisplayManager._masktimeout = setTimeout(function() {
						BODY.getEl().unmask();
					},
					args.decay * 1000);
			} else {
				//_d("Unmasking", 'info', 3);
				_unlockPanel(BODY.id);
				BODY.getEl().unmask();
				if (isset(NNLM.Rosters.DisplayManager._masktimeout)) {
					window.clearTimeout(NNLM.Rosters.DisplayManager._masktimeout);
				}
				return;
			}
		},
		warn: function(args) {
			_w(args);
		},
		warning: function(args) {
			MAIN.fireEvent('warn', args);
		},
		welcome: function(args) {
			_d(" welcome", 'event');
			NNLM.Rosters.DisplayManager.notify('createpanel', {
					name: 'welcome'
				},
				BODY);
		}
	};
	/** bindListener
	 *   an assistance function that binds all callback functions to
	 *   the appropriate elements within a declared component.  It also
	 *   serves to make this manager the intermediary in deciding whether or not
	 *   those functions get to execute.
	 *   @param obj {Ext.Component} The component to perform a descending
	 *   bind operation on.  The component is traversed, and every component
	 *   with a callback: {eventName: fn} property has their event 'eventName'
	 *   bound to sequentially call back this manager, and then execute their
	 *   speficied function fn.
	 *   Note that these components are skeleton objects I've created myself
	 *   that contain the properties necessary to define their resultant objects.  The
	 *   callbacks are defined in this skeleton, and assigned if present.
	 */
	var _a = function(text) {
		_modal({
			text: text,
			icon: Ext.MessageBox.INFO
		});
	};
	var _addTransientButton = function(menuId, config) {
		var b = null;
		if (arguments.length === 1) {
			config = arguments[0];
			b = NORTHCONTROLS.getTopToolbar().addButton(config);
		} else {
			b = NORTHCONTROLS.getTopToolbar().getComponent(menuId).menu.add(config);
		}
		b.trans = true;
	};
	var _cascadeRemove = function(menu) {
		menu.cascade(function() {
			if (this.trans) {
				this.ownerCt.remove(this);
			}
		});
	};
	/** checkPanel
	 *  called by notify function to determine if a panel displaying the
	 *  credentials specified has already been opened. Certain panels are not
	 *  accounted for here, as they allow more than one such panel of that type to
	 *  be opened. Note that this is in addition to, not replacement of, the
	 *  maxCount variable assigned in the Components.js configuration, that
	 *  governs the maximum number of panels allowed for a given type.
	 *  @param args {type} the type of panel.  Can be any of those specified in
	 *  NNLM.Rosters.Configs.
	 *  @param id {int} an identifier for a particular panel. *Not* a panel id,
	 *  but rather, the primary key for the record whose contents are displayed in
	 *  that panel.
	 **/
	var _checkPanel = function(pconfig) {
		var panelId = false;
		BODY.items.each(function(panel) {
			for (var k in pconfig) {
				if (pconfig.hasOwnProperty(k)) {
					if (panel[k] !== pconfig[k]) {
						//str += "\nItem failed with config " + k + " having a value of " + panel[k] +
						//", not " + pconfig[k];
						//_d(str, 3);
						return;
					}
				}
			}
			panelId = panel.id;
		});
		return panelId;
	};

	/* fashion a uniqueId for each content panel, to prevent namespace collision */
	var _createPanelId = function() {
		return 'panel-' + new Date().getTime().toString().substr(4) + Math.random().toString().replace(".", '').substr(0, 4);
	};
	var _d = function(mixed, type) {
		NNLM.Rosters.dump("\nDisplayManager(" + (new Date().format('i:s.u')) + '):' + mixed, type);
	};
	var _e = function(mixed) {
		if (typeof mixed === 'string') {
			mixed = {
				text: mixed
			};
		}
		mixed.icon = Ext.MessageBox.ERROR;
		_modal(mixed);
	};
	var _expandMenu = function(eventName) {
		Ext.getCmp(eventName + 'menu').expand();
	};
	var _getCurrentPanel = function() {
		if (!NNLM.Rosters.DisplayManager.init) {
			throw 'not initialized';
		}
		return (BODY.getActiveTab() || LANDING_PAGE);
	};
	var _isCurrentPanelLocked = function() {
		var currentPanel = _getCurrentPanel();
		if (!currentPanel) {
			return displaysLocked.indexOf(BODY.id);
		}
		return (
			displaysLocked.indexOf(BODY.id) >= 0 || displaysLocked.indexOf(currentPanel.id) >= 0);
	};
	var _lockCurrentPanel = function() {
		var currentPanel = _getCurrentPanel();
		if (!currentPanel) {
			return;
		}
		//_lockPanel(BODY.id);
	};
	var _lockPanel = function(id) {
		if (displaysLocked.indexOf(id) === -1) {
			_d('locking panel id: ' + id, 'info');
			//displaysLocked.push(id);
		}
	};
	var _modal = function(mixed) {
		if (Ext.isString(mixed)) {
			mixed = {
				text: mixed
			};
		}
		Ext.Msg.show({
			title: "DisplayManager: ",
			msg: String.format('<p><i>{0}</i></p>', mixed.text),
			buttons: Ext.Msg.OK,
			width: Math.max(400, mixed.length * 10),
			icon: (mixed.icon === undefined) ? Ext.MessageBox.INFO : mixed.icon
		});
	};
	var _removePanel = function(type) {
		var panel = BODY.findById(type + 'panel');
		if (panel !== null) {
			BODY.remove(panel);
		}
	};
	var _removeTransientButtons = function() {
		NORTHCONTROLS.getTopToolbar().items.each(function(o, i) {
			if (this.menu !== undefined) {
				_cascadeRemove(this.menu);
			}
		});
	};
	var _toggleProgress = function(text) {
		/*if(text === false){
            PROGRESS.reset();
            PROGRESS.hide();
            return;
        }
		text = (text === true) ? 'Talking to server...' : text;
        PROGRESS.show();
        PROGRESS.wait({
            text: text,
            duration: NNLM.get('MAX_CONN_TIME') * 1000,
            interval: 100
        });     */
	};

	/* true is 'on' */
	var _getSaveButtonState = function() {
		return !SOUTHCONTROLS.getTopToolbar().chute('save').disabled;
	};
	var _unlockPanel = function(id) {
		if (displaysLocked.indexOf(id) !== -1) {
			//_d('unlocking panel id: ' + id, 'info');
			displaysLocked.remove(id);
		}
	};
	var _unlockCurrentPanel = function() {
		var currentPanel = _getCurrentPanel();
		if (currentPanel === null) {
			return;
		}
		_unlockPanel(currentPanel.id);
		_unlockPanel(BODY.id);
	};
	var _unlockAllPanels = function() {
		_d("unlocking all panels", 'info');
		displaysLocked = [];
	};
	/** Begin Public Object **/
	var result = {
		ellipsis: function(t) {
			var maxLength = arguments.length > 1 ? arguments[1] : 20;
			if (t.length > maxLength) { //arbitrary choice
				t = t.substr(0, maxLength - 3) + '...';
			}
			return t;
		},
		/* err
		 *    requests display of an error message.
		 *    @param m {String} the message to display
		 */
		err: function(m) {
			this.notify('error', {
				text: m
			});
		},

		getToolbar: function(position) {
			return position === 'top' ? NORTHCONTROLS.getTopToolbar() : SOUTHCONTROLS.getTopToolbar();
		},
		getBody: function() {
			return BODY;
		},
		/** Constructor **/
		init: function() {
			_d("initializing...", 3);
			/** hardcoded links to primary interface elements. **/
			//MAIN = new NNLM.Rosters.Prototype1({renderTo: 'rosters'});
			MAIN = new NNLM.Rosters.Layout({
				renderTo: 'rosters'
			});
			var keys = jQuery.extend({},
				eventHandlers);
			for (key in keys) {
				keys[key] = true;
			}
			MAIN.addEvents(keys);
			_d('beginning event binding', 3);
			/* bind all events in eventHandlers object to the MAIN object defined here, so 
			any calls to NNLM.Rosters.DisplayManager.notify trigger an event, whose listener
			response would be the corresponding event in eventHandlers */
			for (var key in eventHandlers) {
				mixed = eventHandlers[key];
				//_d("binding listener for event " + key, 3);
				if (Ext.isFunction(mixed)) {
					MAIN.addListener(key, mixed, MAIN);
				} else if (Ext.isObject(mixed)) {
					if (mixed.fn === undefined) {
						throw "improper object construction in DisplayManager for eventhandler " + key;
					}
					mixed.buffer = (mixed.buffer === undefined) ? mixed.buffer : EVENT_DELAY;
					MAIN.addListener(key, mixed.fn, MAIN, {
						buffer: mixed.buffer
					});
				} else {
					throw "unknown type: " + typeof mixed;
				}
			}
			BODY = MAIN.findById('rosters_mainbody');
			HELP = MAIN.findById('rosters_mainhelp');
			NORTHCONTROLS = MAIN.getComponent('NORTHCONTROLS');
			SOUTHCONTROLS = MAIN.getComponent('SOUTHCONTROLS');
			WESTCONTROLS = MAIN.getComponent('WESTCONTROLS');
			LANDING_PAGE = BODY.chute('landing_page');
			//BODY.hideTabStripItem(LANDING_PAGE);
			BODY.getActions = null;
			SOUTHCONTROLS.getTopToolbar().insert(1, _actions.reset);
			SOUTHCONTROLS.getTopToolbar().insert(1, {
				xtype: 'tbseparator',
				permanent: true
			});
			SOUTHCONTROLS.getTopToolbar().insert(1, _actions.save);

			NORTHCONTROLS.getTopToolbar().chute('file').menu.add(_actions.save);
			NORTHCONTROLS.getTopToolbar().chute('file').menu.add(_actions.reset);
			SOUTHCONTROLS.getTopToolbar().doLayout();
			NORTHCONTROLS.getTopToolbar().doLayout();

			/*_addTransientButton('find', {
				text: 'foo'
			});*/
			_d('Rosters initialization is complete.', 1);
			this.init = true;
		},
		isReady: function() {
			return (this.init === true);
		},
		/** isViewChangeOk
		 * returns whether the current display has been locked by
		 * another object.
		 */
		isViewChangeOk: function() {
			var locked = NNLM.Rosters.DataManager.hasChanges(); //_isCurrentPanelLocked();
			var msg = "view chanje iz " + ((locked) ? "nok" : "ok");
			//_d(msg, 3);
			if (locked) {
				_d(msg, 'warn');
				NNLM.Rosters.DisplayManager.err('Your form has unsaved changes. Please save or reset this form before changing tabs.');
				return false;
			}
			_d(msg, 3);
			return true;
		},
		/*toggleButton: function(buttonName, b_value){
            return SOUTHCONTROLS.toggleButton(buttonName, b_value);
        },*/
		/* msg
		 *    requests display of a notification message.
		 *    Shorthand for same function in notify.
		 *    @param m {String} the message to display
		 */
		msg: function(m) {
			eventHandlers.statusmsg(m);
		},
		/* notify
		 *  This function is the primary means by which external components and
		 *   other managers talk to the display manager.  Notify is the callback
		 *   function for a variety of listeners throughout this application.
		 *   The scope of the executing object is set as default, so 'this',
		 *   within the context of the notify function will refer to the
		 *   calling object, not the display manager.
		 *    <pre>
		 *   owner:      a reference to the triggering object.
		 *   event:      {String} the name of the event
		 *   args:       an array consisting of any arguments passed to the
		 *               original event.
		 *    </pre>
		 */
		notify: function(eventName) {
			var args = Array.prototype.slice.call(arguments);
			if (Ext.isString(eventName)) {
				MAIN.fireEvent.apply(MAIN, args);
			} else {
				throw "malformed notify call: " + NNLM.printObject(eventName);
			}
		},
		addActions: function(o) {
			if (o === undefined) {
				o = [];
			}
			var b_actions = SOUTHCONTROLS.getTopToolbar();
			for (i = 0; i < o.length; i++) {
				if (b_actions.get(o[i].itemId) !== undefined) {
					//alert(b_actions.get(o[i].itemId));
					continue;
				}
				o[i].setIconClass('cog');
				var re = / /g;
				o[i].setText(o[i].getText().replace(re, '&nbsp;'));
				b_actions.insert(1, {
					xtype: 'tbseparator'
				});
				b_actions.insert(1, o[i]);
				b_actions.get(1).disable();
			}
			b_actions.doLayout();
		},
		clearDialogs: function() {
			msgDialogs.each(function(m) {
				if (!m.isVisible()) {
					msgDialogs.remove(m);
					return;
				}
				m.finish();
			});
		},
		clearActions: function() {
			var b_actions = SOUTHCONTROLS.getTopToolbar();
			for (var i = b_actions.items.length - 1; i >= 0; i--) {
				var c = b_actions.get(i);
				if (c.permanent === true) {
					continue;
				}
				b_actions.remove(c, true);
			}
			b_actions.doLayout();
		}
	};
	result.init();
	return result;
};
NNLM.Rosters.dump("Display Manager loaded.");
