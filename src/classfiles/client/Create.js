/**
 *     @class NNLM.Rosters.Create
 *     <p>This class is independently downloaded by the Rosters application during runtime, and
 *    houses all the operations performed by the Create panel.  A find panel is created
 *    using the NNLM.Rosters.Find.makeInstance method.</p>
 *
 *   @author     Aron Beal
 *   @copyright (c) 2008, by the University of Washington
 *   @date       May 6 2008
 *   @license All files in this web application are  licensed under the
 *   terms of the Open Source LGPL 3.0 license.
 *   License details: http://www.gnu.org/licenses/lgpl.html
 */
Ext.ns('NNLM.Rosters.Create');

NNLM.Rosters.Create.makeInstance = function(uniqueId) {
    var _dump = function(msg, type) {
        var time = new Date();
        NNLM.Rosters.dump('\n* Create (' + (new Date().format('i:s.u')) + '): ' + msg, type);
    };
    /**  _a
     *  display an alert dialog with this class prefixed.  used for debugging.
     *  @param msg {string} the message to display
     **/
    var _a = function(msg) {
        NNLM.Rosters.DisplayManager.notify('msg', {
            text: msg
        });
    };
    /**  _e
     *  display an error dialog with this class prefixed.  used for debugging.
     * @param msg {string} the message to display
     **/
    var _e = function(msg) {
        NNLM.Rosters.DisplayManager.err(msg);
    };
    var _record = null;
    var _functions = {
        'next': function() {
            MainPanel.fireEvent('next');
        },
        'prev': function() {
            MainPanel.fireEvent('prev');
        }
    };
    var _actions = {
        'next': {
            text: 'Next',
            tooltip: 'Advances the wizard to the next step in the process.'
        },
        'prev': {
            text: 'Previous',
            tooltip: 'Backs up the wizard to the prior step in the process.'
        }
    };
    for (var k in _actions) {
        _actions[k].itemId = k;
        _actions[k].handler = _functions[k].createCallback();
        _actions[k] = new Ext.Action(_actions[k]);
    }
    var _menus = {
        'beginning': [_actions.next],
        'middle': [_actions.next, _actions.prev],
        'end': [_actions.prev]
    };
    var _stores = {
        'people': NNLM.Rosters.DataManager.cloneStore('people', {
            listeners: {
                load: function(store, record, loadOptions) {
                    _dump('localpeople store loaded.  Result count: ' + store.getCount());
                    NNLM.Rosters.DisplayManager.notify("wait", false);
                },
                loadexception: function(e, o, msg) {
                    _e(msg.responseText);
                }
            }
        }),
        'list_descriptions': NNLM.Rosters.DataManager.cloneStore('list_descriptions', {
            listeners: {
                load: function(store, record, loadOptions) {
                    _dump('localpeople store loaded.  Result count: ' + store.getCount());
                    NNLM.Rosters.DisplayManager.notify("wait", false);
                },
                loadexception: function(e, o, msg) {
                    _e(msg.responseText);
                }
            }
        })
    };
    var _wizards = {
        'people': [{
            xtype: 'form',
            frame: true,
            flex: 1,
            itemId: 'form',
            trackResetOnLoad: true,
            defaults: {
                xtype: 'textfield',
                width: 'auto',
                msgTarget: 'side',
                blankText: 'This field requires a value.',
                maxLength: 100,
                maxLengthText: 'Cannot exceed 100 characters.',
                invalidText: 'The value in this field is either invalid or missing (only fields with * are required)'
            },
            items: [
                NNLM.Rosters.createSelect({
                    name: 'Prefix',
                    allowBlank: true,
                    width: 100,
                    emptyText: 'N/A'
                }, ['',
                    'Atty.',
                    'Coach',
                    'Dr.',
                    'Fr.',
                    'Gov.',
                    'Hon.',
                    'Ms.',
                    'Mr.',
                    'Mrs.',
                    'Ofc.',
                    'Pres.',
                    'Prof.',
                    'Rep.',
                    'Rev.'
                ]), {
                    name: 'FirstName',
                    fieldLabel: '* First Name',
                    allowBlank: false,
                    regex: /^[A-z]{2,}$/,
                    regexText: "This field is required.  Only letters are allowed"
                }, {
                    name: 'MiddleName',
                    fieldLabel: 'Middle Name',
                    regex: /^[A-z]*$/,
                    regexText: 'Only letters allowed in this field'
                }, {
                    name: 'LastName',
                    fieldLabel: '* Last Name',
                    allowBlank: false,
                    regex: /^[A-z]{2,}$/,
                    regexText: "This field is required.  Only letters are allowed."
                }, {
                    name: 'Suffix',
                    fieldLabel: 'Suffix',
                    regex: /^[A-z]*$/,
                    regexText: 'Only letters allowed in this field'
                }
            ],
            listeners: {
                'enter': function() {
                    if (this.isLastFieldFocused() && this.getForm().isValid()) {
                        MainPanel.fireEvent('next');
                    } else {
                        this.next(); //shifts to next field.
                    }
                },
                'next': function() {
                    if (!this.isValid()) {
                        _e("Errors exist!  Please correct them before continuing.");
                        return;
                    }
                    _dump('creating record here!', 3);
                    _record = new NNLM.Rosters.DataManager.STORES.people.recordType();
                    //persist values to the record on behalf of the form.
                    this.getForm().updateRecord(_record);
                    MainPanel.next();
                },
                'doreset': function() {
                    this.getForm().reset();
                }
            }
        }, {
            // grid column
            xtype: 'grid',
            anchor: '50% 100%',
            viewConfig: {
                forceFit: true,
                emptyText: 'No similar records found - proceed to next step.'
            },
            columns: [{
                header: "Potential Matches",
                dataIndex: 'PeopleID',
                renderer: function(value, metaData, record, rowIndex, colIndex, store) {
                    return "<div class='iconed user'><p class='title'>" + record.get("FirstName") + ' ' +
                        record.get('LastName') + "</p>(" + record.get('PeopleID') + ")</div>";
                }
            }],
            itemSelector: 'div.search-item',
            sm: new Ext.grid.RowSelectionModel({
                singleSelect: true
            }),
            store: _stores.people,
            stripeRows: true,
            listeners: {
                'dofilter': {
                    fn: function() {
                        if (_record === null) {
                            throw "record is null!"; //should have been established in prior step.
                        }
                        _record.data.fulltext = false;
                        this.store.setBaseParam('request', JSON.stringify(_record.data));
                        NNLM.Rosters.DisplayManager.notify('wait', true);
                        this.store.load({
                            callback: function(r, options, success) {
                                NNLM.Rosters.DisplayManager.notify('wait', false);
                                if (success) {
                                    _actions.next.enable();
                                }
                            },
                            scope: this
                        });
                    },
                    buffer: 100
                },
                'rowdblclick': function(grid, i, e) {
                    var r = this.store.getAt(i);
                    NNLM.Rosters.DisplayManager.notify('edit', {
                        PeopleID: r.get('PeopleID')
                    });
                },
                'enter': function() {
                    MainPanel.fireEvent('next');
                },
                'next': function() {
                    MainPanel.next();
                },
                'prev': function() {
                    NNLM.Rosters.DisplayManager.notify('wait', false);
                    this.store.empty();
                    _record = null;
                    MainPanel.prev();
                }
            }
        }, {
            bodyCssClass: 'BodyPanel',
            tpl: new Ext.XTemplate(
                '<tpl for=".">',
                '<div style="padding: 10%; font-size: large;">',
                '<h2>Record to be created:</h2>',
                '<p>Prefix         : {Prefix}</p>',
                '<p>First Name     : {FirstName}</p>',
                '<p>Middle Name    : {MiddleName}</p>',
                '<p>Last Name      : {LastName}</p>',
                '<p>Suffix         : {Suffix}</p>',
                '</div></tpl>').compile(),
            listeners: {
                'dofilter': function() {
                    if (!isset(this.body)) {
                        return;
                    }
                    this.tpl.overwrite(this.body, _record.data);
                    _record = NNLM.Rosters.DataManager.createRecord('people', {
                        Prefix: _record.get('Prefix'),
                        FirstName: _record.get('FirstName'),
                        MiddleName: _record.get('MiddleName'),
                        LastName: _record.get('LastName'),
                        Suffix: _record.get('Suffix')
                    });
                },
                'prev': function() {
                    NNLM.Rosters.DataManager.reset('people');
                    MainPanel.prev();
                }
            }
        }],
        'list_descriptions': [{
            xtype: 'form',
            itemId: 'form',
            frame: true,
            flex: 1,
            trackResetOnLoad: true,
            scroll: 'auto',
            defaults: {
                xtype: 'textfield',
                width: '85%'
            },
            items: [{
                    name: 'ListTitle',
                    fieldLabel: 'List Title',
                    allowBlank: false,
                    regex: /^.+$/,
                    regexText: "This field is required",
                    emptyText: '- Enter a title for your new list -'
                }, {
                    name: 'ListDescription',
                    xtype: 'textarea',
                    fieldLabel: 'List Description',
                    emptyText: '- Enter a description for your new list -'
                },
                NNLM.Rosters.createSelect({
                    name: 'ListVisibility',
                    fieldLabel: 'List Visibility'
                }, [{
                    name: 'Intranet',
                    value: 'intranet'
                }, {
                    name: 'Public',
                    value: 'public'
                }]), {
                    name: 'ListStartDate',
                    xtype: 'datefield',
                    fieldLabel: 'List Start Date',
                    emptyText: 'active on',
                    value: new Date()
                }, {
                    name: 'ListEndDate',
                    xtype: 'datefield',
                    emptyText: 'inactive on...',
                    fieldLabel: 'List End Date'
                }
            ],
            listeners: {
                'enter': function() {
                    if (this.isLastFieldFocused() && this.getForm().isValid()) {
                        MainPanel.fireEvent('next');
                    } else {
                        this.next();
                    }
                },
                'dofilter': function() {
                    _dump('destroying any temporary records', 3);
                    _record = null;
                },
                'next': function() {
                    if (!this.isValid()) {
                        _e("Errors exist!  Please correct them before continuing.");
                        return;
                    }
                    _dump('creating record here!', 3);
                    _record = new NNLM.Rosters.DataManager.STORES.list_descriptions.recordType();
                    //persist values to the record on behalf of the form.
                    this.getForm().updateRecord(_record);
                    NNLM.Rosters.DisplayManager.notify("wait", true);
                    MainPanel.next();
                },
                'doreset': function() {
                    this.getForm().reset();
                }
            }
        }, {
            // grid column
            xtype: 'grid',
            anchor: '50% 100%',
            viewConfig: {
                forceFit: true,
                emptyText: 'No similar records found - proceed to next step.'
            },
            columns: [{
                header: "Potential Matches",
                dataIndex: 'ListID',
                renderer: function(value, metaData, record, rowIndex, colIndex, store) {
                    return "<div class='iconed script'><p class='title'>" + record.get("ListTitle") + '</p>' +
                        '<p>' + record.get('ListDescription') + "</p>(" + record.get('ListID') + ")</div>";
                }
            }],
            itemSelector: 'div.search-item',
            sm: new Ext.grid.RowSelectionModel({
                singleSelect: true
            }),
            store: _stores.list_descriptions,
            stripeRows: true,
            listeners: {
                'dofilter': function() {
                    if (_record === null) {
                        throw "record is null!"; //should have been established in prior step.
                    }
                    _record.data.fulltext = true;
                    this.store.setBaseParam('request', JSON.stringify(_record.data));
                    NNLM.Rosters.DisplayManager.notify('wait', true);
                    this.store.load({
                        callback: function(r, options, success) {
                            NNLM.Rosters.DisplayManager.notify('wait', false);
                            if (success) {
                                _actions.next.enable();
                            }
                        },
                        scope: this
                    });
                },
                'rowdblclick': function(grid, i, e) {
                    var r = this.store.getAt(i);
                    NNLM.Rosters.DisplayManager.notify('list', {
                        ListID: r.get('ListID')
                    });
                },
                'enter': function() {
                    this.fireEvent('next');
                },
                'next': function() {
                    MainPanel.next();
                },
                'prev': function() {
                    NNLM.Rosters.DisplayManager.notify('wait', false);
                    this.store.empty();
                    _record = null;
                    MainPanel.prev();
                }
            }
        }, {
            bodyCssClass: 'BodyPanel',
            frame: true,
            autoScroll: true,
            tpl: new Ext.XTemplate(
                '<tpl for=".">',
                '<div style="padding: 10%">',
                '<table class="createsummary" summary="List creation information">',
                '<caption>List to be created:</caption>',
                '<tr><th>Title</th><td>{ListTitle}</td></tr>',
                '<tr><th>Description</th><td>{ListDescription}</td></tr>',
                '<tr><th>Visibility</th><td>{ListVisibility}</td></tr>',
                '<tr><th>Start Date</th><td>{[this.formatDate(values.ListStartDate)]}</td></tr>',
                '<tr><th>End Date</th><td>{[this.formatDate(values.ListEndDate)]}</td></tr>',
                '</table></div></tpl>', {
                    formatDate: function(date) {
                        if (Ext.isString(date)) {
                            if (date === '') {
                                return '[None]';
                            }
                            date = new Date(date);
                        }
                        return date.toDateString();
                    }
                }
            ).compile(),
            listeners: {
                'dofilter': function() {
                    if (!isset(this.body)) {
                        return;
                    }
                    _record = NNLM.Rosters.DataManager.createRecord('list_descriptions', {
                        ListTitle: _record.get('ListTitle'),
                        ListDescription: _record.get('ListDescription'),
                        ListVisibility: _record.get('ListVisibility'),
                        ListStartDate: _record.get('ListStartDate'),
                        ListEndDate: _record.get('ListEndDate')
                    });
                    this.tpl.overwrite(this.body, _record.data);
                },
                'next': function() {
                    MainPanel.next();
                },
                'prev': function() {
                    NNLM.Rosters.DataManager.destroyRecord('list_descriptions', _record);
                    MainPanel.prev();
                }
            }
        }]
    };
    /*var _sliders = {
        'people': {
            xtype: 'slider',
            width: 250,
            value:0,
            increment: 1,
            minValue: 0,
            maxValue: _wizards.people.length-1,
            listeners: {
                'change': function(slider, num, thumb){
                    //MainPanel.slideTo(num); use filter event instead!
                }
            }
        },
        'list_descriptions': {
            xtype: 'slider',
            width: 250,
            value:0,
            increment: 1,
            minValue: 0,
            maxValue: _wizards.list_descriptions.length-1
        }
    };*/
    var MainPanel = new Ext.Panel({
        layout: 'hbox',
        itemId: 'createPerson',
        iconCls: 'CreatePanelIcon',
        anchor: '100% 100%',
        activeItem: 0,
        closable: true,
        border: false,
        defaults: {
            flex: 1,
            border: false,
            frame: false
        },
        layoutConfig: {
            align: 'stretch',
            pack: 'start'
        },
        items: [{
            itemId: 'content',
            title: 'content',
            layout: 'card'
        }, {
            itemId: 'instructions',
            title: 'instructions',
            bodyCssClass: 'Wizard HelpPanel',
            tools: [{
                itemId: 'refresh',
                id: 'refresh',
                qtip: 'Refresh Page Data',
                handler: function() {
                    MainPanel.fireEvent('dofilter');
                }
            }]
        }],
        currentSlideNum: 0,
        getActions: function() {
            return null;
        },
        next: function() {
            this.setCurrentSlideNum(this.getActiveSlideNum() + 1);
        },
        prev: function() {
            this.setCurrentSlideNum(this.getActiveSlideNum() - 1);
        },
        getActiveSlideNum: function() {
            return MainPanel.chute('content').getLayout().getActiveSlideNum();
        },
        getActiveSlide: function() {
            return MainPanel.chute('content').getLayout().activeItem;
        },
        getSlideCount: function() {
            return this.chute('content').items.getCount();
        },
        setCurrentSlideNum: function(requested_i) {
            // begin validations                                                            
            var current_i = this.getActiveSlideNum();
            requested_i = Math.min(
                this.getSlideCount() - 1,
                Math.max(0, requested_i));
            _dump("setCurrentSlideNum called.  Requested slide:" + requested_i, 3);
            if (requested_i === current_i) {
                _dump('no change in slide', 3);
                return;
            }
            // end validations                  
            MainPanel.chute('content').getLayout().setActiveItem(requested_i);
            MainPanel.currentSlideNum = requested_i;
            MainPanel.fireEvent('dofilter');
        },
        setInterface: function(typeId) {
            _dump("Setting create interface to be of type " + typeId, 3);
            switch (typeId) {
                case 'people':
                    this.setTitle('Create a person');
                    this.setIconClass('iconed CreatePanelIcon user');
                    this.helpBase = NNLM.get('WIZARD_BASE_URL') + 'addperson/';
                    break;
                case 'list_descriptions':
                    this.setTitle('Create a list');
                    this.setIconClass('iconed CreatePanelIcon script_add');
                    this.helpBase = NNLM.get('WIZARD_BASE_URL') + 'addlist/';
                    break;
            }
            MainPanel.typeId = typeId;
            MainPanel.pkey = NNLM.Rosters.DataManager.getStoreKey(MainPanel.typeId);
            var c = MainPanel.chute('content');
            Ext.each(_wizards[typeId], function(item, i) {
                c.add(item);
            });
            c.getLayout().setActiveItem(0);
            MainPanel.enable();
            MainPanel.fireEvent('dofilter', 0);
        },
        listeners: {
            'tabchange': {
                fn: function(tabPanel, panel) {
                    _dump(' tabchange (MainPanel) (buffered)', 'event');
                    if (!MainPanel.loaded) {
                        return;
                    }
                    //do viewchange instead of binding directly to tabchange
                    //gives me more granular control as to when this message
                    //is passed up to the Display Manager.                        
                    NNLM.Rosters.DisplayManager.notify('viewchange');
                    MainPanel.fireEvent('dofilter');
                },
                buffer: 100
            },
            'enter': function() {
                this.getActiveSlide().fireEvent('enter');
            },
            'next': function() {
                NNLM.Rosters.DisplayManager.clearDialogs();
                NNLM.Rosters.DisplayManager.notify("wait", true);
                this.getActiveSlide().fireEvent('next');
            },
            'prev': function() {
                NNLM.Rosters.DisplayManager.clearDialogs();
                NNLM.Rosters.DisplayManager.notify("wait", true);
                this.getActiveSlide().fireEvent('prev');
            },
            'finish': function() {
                NNLM.Rosters.DisplayManager.notify('dosave');
            },
            'dofilter': {
                fn: function() {
                    if (this.typeId === undefined) {
                        return;
                    }
                    NNLM.Rosters.DisplayManager.notify("wait", false);
                    NNLM.Rosters.DisplayManager.clearActions();
                    if (this.getActiveSlideNum() === 0) {
                        NNLM.Rosters.DisplayManager.addActions(_menus.beginning);
                    } else if (this.getActiveSlideNum() === this.getSlideCount() - 1) {
                        NNLM.Rosters.DisplayManager.addActions(_menus.end);
                    } else {
                        NNLM.Rosters.DisplayManager.addActions(_menus.middle);
                    }
                    if (this.getActiveSlideNum() >= 0) {
                        _actions.prev.enable();
                    } else {
                        _actions.prev.disable();
                    }
                    if (this.getActiveSlideNum() < this.getSlideCount() - 1) {
                        _actions.next.enable();
                    } else {
                        _actions.next.disable();
                    }
                    var url = MainPanel.helpBase + (this.getActiveSlideNum() + 1) + '.html';
                    _dump("url: " + url, 'info');
                    MainPanel.chute('instructions').load(url);
                    this.getActiveSlide().fireEvent('dofilter');
                },
                buffer: 100
            },
            'dosave': function() {
                this.chute("content").fireEvent('dosave');
                if (_record === null) {
                    throw "record is null!"; //should have been established in prior step.
                }
                NNLM.Rosters.DataManager.save(this.typeId, function(success, record) {
                    if (!success) {
                        return;
                    }
                    var params = {};
                    params[MainPanel.pkey] = record.get(MainPanel.pkey);
                    _dump("callback. params passed to edit: " + NNLM.printObject(params), 2);
                    NNLM.Rosters.DisplayManager.notify('edit', params);
                    NNLM.Rosters.DisplayManager.notify('close', {
                        id: MainPanel.id
                    });
                });
            },
            'doreset': function() {
                this.setCurrentSlideNum(0);
                NNLM.Rosters.DataManager.reset(this.typeId);
                this.getActiveSlide().fireEvent('doreset');
            }
        }
    });
    return MainPanel;
};
NNLM.Rosters.dump("Account creation wizards loaded.");
NNLM.Rosters.Create.loaded = true;
