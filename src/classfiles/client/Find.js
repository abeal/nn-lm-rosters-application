/**
*     @class NNLM.Rosters.Find
*     <p>This class is independently downloaded by the Rosters application during runtime, and
*    houses all the operations performed by the find search panel.  A find panel is created
*    using the NNLM.Rosters.Find.makeInstance method.</p>
*
*   @author     Aron Beal
*   @copyright (c) 2008, by the University of Washington
*   @date       May 6 2008
*   @license All files in this web application are  licensed under the
*   terms of the Open Source LGPL 3.0 license.
*   License details: http://www.gnu.org/licenses/lgpl.html
*/
Ext.ns('NNLM.Rosters.Find');
NNLM.Rosters.Find.makeInstance = function(uniqueId){
    var _dump = function (msg, type) {
        var time = new Date().toTimeString();
        NNLM.Rosters.dump('\n* Edit (' + time + '): ' + msg, type);
    };
    /**  _a
        *  display an alert dialog with this class prefixed.  used for debugging.
        *  @param msg {string} the message to display
        **/
    var _a = function (msg) {
        NNLM.Rosters.DisplayManager.msg(msg);
    };
    /**  _e
        *  display an error dialog with this class prefixed.  used for debugging.
        * @param msg {string} the message to display
        **/
    var _e = function (msg) {
        NNLM.Rosters.DisplayManager.notify('error', {text:msg});
    };
	var _functions = {
		'editperson': function(){
			var id = MainPanel.chute('details').PeopleID;
	        if(id === undefined){
	            return;
	        }
	        NNLM.Rosters.DisplayManager.notify('edit',
	        {'PeopleID': id});
		},
		'editlist': function(){
			var id = MainPanel.chute('details').ListID;
	        if(id === undefined){
	            return;
	        }
	        NNLM.Rosters.DisplayManager.notify('edit',
	        {'ListID': id});
		},   
		'showpubliclist':  function(){
			var id = MainPanel.chute('details').ListID;
			if(id === undefined){
                return;
            }
			window.open("https://dev.staff.nnlm.gov/test/rosters/list.html?ListID="+id);
		},       
		'showpublicperson': function(){                                                   
			var pid = MainPanel.chute('grid').getSelectionModel().getSelected().get('PeopleID');
			window.open("https://dev.staff.nnlm.gov/test/rosters/?PeopleID="+pid);    
		}  
	};
	var _actions = {
		'editlist': {
            text: 'Edit List',      
			tooltip: 'Opens the selected list record in a new interface tab for editing.'   
        },
		'editperson': {
            text: 'Edit Record',                  
			tooltip: 'Opens the selected staff record in a new interface tab for editing.'
        },
		'showpubliclist': {
			text: 'Show list in public display',
            tooltip: "Opens a new browser window (or tab) that displays what this staff member's record will look like to the general public."
		},
		'showpublicperson':{
			text: 'Public View',         
			tooltip: "Opens a new browser window (or tab) that displays what this staff member's record will look like to the general public." 
		}
	}; 
	for (var k in _actions){               
		_actions[k].itemId = k;
		_actions[k].handler = _functions[k].createDelegate(this);    
		_actions[k] = new Ext.Action(_actions[k]);
	}
    var _stores = {
		
        /** roleStore,
         * a local read-only store that displays possible roles.
         **/
        'roles': new Ext.data.JsonStore({
            url: NNLM.get('SERVLET_BASE_URL') + 'roles/GetRoleTypes.servlet.php',
            fields: [{
                name: 'RoleType',
                type: 'string'
            },
            {
                name: 'RoleDescription',
                type: 'string'
            },
            {
                name: 'obsolete',
                type: 'boolean'
            }],
            idProperty: 'RoleType',
            root: 'results',
            totalProperty: 'count',
            baseParams: {
                query: ''
            },
            sortInfo: {
                field: 'RoleType',
                direction: 'ASC'
            },
            listeners: {                         
                load: function(store, record, loadOptions){
                    _dump('role temp store loaded.  Result count: ' + store.getCount(), 2);                                            
					store.sort('RoleType', 'ASC');                                            
                    NNLM.Rosters.DisplayManager.notify('wait', false);
                },
                'loadexception': function(sender, options, response, error) {
                    _dump('Response: ' + response.responseText);
                    NNLM.Rosters.dump(response);    
					throw 'Exception: ' + response.responseText;
                }
            }
        }),
		'regions': new Ext.data.JsonStore({
            url: NNLM.get('SERVLET_BASE_URL') + 'GetRegions.servlet.php',
            fields: [{
                name: 'Code',
                type: 'string'
            },
            {
                name: 'Name',
                type: 'string'
            }],
            idProperty: 'Code',
            root: 'results',
            totalProperty: 'count',
            baseParams: {
                query: ''
            },
            sortInfo: {
                field: 'Name',
                direction: 'ASC'
            },
            listeners: {
                load: function(store, record, loadOptions){
                    _dump('region temp store loaded.  Result count: ' + store.getCount(), 2);
                },         
                'loadexception': function(sender, options, response, error) {
                    _dump(response);
                    throw 'exception: ' + response.responseText;
                }
            }
        }),
        'people': NNLM.Rosters.DataManager.cloneStore('people',{
			baseParams: Ext.util.JSON.encode({params:{"booleanmode":false,fulltext: 'true'}}),
            listeners: {
                load: function(store, record, loadOptions){
                    _dump('people temp store loaded.  Result count: ' + store.getCount(), 2);
					_a('Your search returned '+store.getCount()+' results');     
					if(store.getCount() > 0){              
						MainPanel.chute('grid').getSelectionModel().selectFirstRow();
					}                
					store.sort([ { field : 'LastName', direction: 'ASC' }, { field : 'FirstName', direction: 'ASC' } ], 'ASC');                                              
                    NNLM.Rosters.DisplayManager.notify('wait', false);
                },
				'exception': function(proxy, type, action, options, response, arg){
				    NNLM.Rosters.DisplayManager.notify('exception', {response: response, ttl:0});
                }
            }
        }),
        'list_descriptions': NNLM.Rosters.DataManager.cloneStore('list_descriptions', {			
            listeners: {
                load: function(store, record, loadOptions){
                    _dump('list_descriptions temp store loaded.  Result count: ' + store.getCount(), 2);
					_a('Your search returned '+store.getCount()+' results');          
					if(store.getCount() > 0){              
						MainPanel.chute('grid').getSelectionModel().selectFirstRow();
					}                 
					store.sort([ { field : 'ListTitle', direction: 'ASC' }, { field : 'ListDescription', direction: 'ASC' } ], 'ASC');
                    NNLM.Rosters.DisplayManager.notify('wait', false);
                },
				'exception': function(proxy, type, action, options, response, arg){
				    NNLM.Rosters.DisplayManager.notify('exception', {response: response, ttl:0});
                }
            }
		}),
        'list_expanded': NNLM.Rosters.DataManager.cloneStore('lists')
    };
    var _colModels = {
        'people': new Ext.grid.ColumnModel({
            columns: [{
                dataIndex: 'PeopleID',
                renderer: function(value, metaData, record, rowIndex, colIndex, store){
                    return _templates.people.applyTemplate(record.data);
                }
            }]
        }),
        'list_descriptions': new Ext.grid.ColumnModel({
            columns: [{
                dataIndex: 'ListID',
                renderer: function(value, metaData, record, rowIndex, colIndex, store){
                    return _templates.list_descriptions.applyTemplate(record.data);
                }
            }]
        })
    };
    var _templates = {
        people: new Ext.XTemplate(
            '<tpl for=".">',
            '<div class="iconed user x-combo-list-item">',
            '<span class="title">{LastName}, {FirstName} ({PeopleID})</span>',
            '</div>',
            '</tpl>').compile(),
		list_descriptions: new Ext.XTemplate(
            '<tpl for=".">',
            '<div class="iconed list x-combo-list-item">',
            '<span class="title">{ListTitle} <span class="metadata">(List ID: {ListID})</span></span><br />',
            '<span class="metadata">{ListDescription}</span>',
            '</div>',
            '</tpl>'
		).compile()
    };
	var _menus = {
		'people': [_actions.editperson, _actions.showpublicperson],
		'list_descriptions': [_actions.editlist,_actions.showpubliclist]
	};
    var _searchfields = {
        people: {
			name: {
	            xtype: 'searchfield',
	            itemId: 'searchfield',
	            width: 400,
	            store: _stores.people,
	            emptyText: '[First and/or Last Name...]',
				displayParams: {'searchby':'name'},
	            listeners: {
	                beforesubmit: function(qEvent){
						this.store.removeAll();
	                    NNLM.Rosters.DisplayManager.notify('wait', true);
	                }
	            }
	        },
			libid: {
	            xtype: 'searchfield',
	            itemId: 'searchfield',
	            width: 100,
	            store: _stores.people,
	            emptyText: '[LIBID]',
				maxLength: 6,
                regex: /^[A-z0-9 ]*$/,
                regexText: "Only letters and spaces are allowed in this field.",
				displayParams: {'searchby':'libid'},
	            listeners: {
	                beforesubmit: function(qEvent){
						this.store.removeAll();
	                    NNLM.Rosters.DisplayManager.notify('wait', true);
	                }
	            }
	        },
			region: {
	            xtype: 'selectbox',
	            itemId: 'searchfield',
	            width: 400,
	            store: _stores.regions,
				mode: 'remote',
                valueField: 'Code',
                displayField: 'Name',
                emptyText: '[choose an RML/Center...]',
                itemSelector: 'div.search-item',
                tpl: new Ext.XTemplate(
                '<tpl for=".">',
                //'<tpl if="!obsolete">',
                '<div class="search-item">',
                '<p><strong>{Name}</strong><br /><span class="meta"> (Region {Code})</span></p>',
                '</div></tpl>').compile(),
	            listeners: {
	                select: {
						fn: function(combo, r, i){
		                    NNLM.Rosters.DisplayManager.notify('wait', true);
							var req = {
								'query': r.get('Code')
							};
							var p = {
								'searchby':'region'
							};
							MainPanel.chute('grid').getStore().reload({
					            params: {
						            _start: 0,
									request: JSON.stringify(req),
									params: JSON.stringify(p)
						        }
							});
		                },
						buffer: 100
					}
	            }
	        },
			role: {
	            xtype: 'selectbox',
	            itemId: 'searchfield',
	            width: 400,
	            store: _stores.roles,
				mode: 'remote',
                valueField: 'RoleType',
                displayField: 'RoleType',
                emptyText: '[choose a role...]',
                itemSelector: 'div.search-item',
                tpl: new Ext.XTemplate(
					'<tpl for="."><div class="search-item">',       
		            '<p><strong>{RoleType}</strong><br /><span class="metadata desc">{RoleDescription}</span></p>',   
		            '</div></tpl>'
				).compile(),
	            listeners: {
	                select: {
						fn: function(combo, r, i){
		                    NNLM.Rosters.DisplayManager.notify('wait', true);
							var req = {
								'query': r.get('RoleType')
							};
							var p = {
								'searchby':'role'
							};
							MainPanel.chute('grid').getStore().reload({
					            params: {
						            _start: 0,
									request: JSON.stringify(req),
									params: JSON.stringify(p)
						        }
							});
		                },
						buffer: 100
					}
	            }
	        },
			peopleid: {
	            xtype: 'searchfield',
	            itemId: 'searchfield',
	            width: 100,
	            store: _stores.people,
	            hidden: false,
	            emptyText: '[PeopleID]',
                regex: /^[0-9]*$/,
                regexText: "Only numbers are allowed in this field.",
				displayParams: {'searchby':'peopleid'},
	            listeners: {
	                beforesubmit: function(qEvent){
						this.store.removeAll();
	                    NNLM.Rosters.DisplayManager.notify('wait', true);
	                }
	            }
	        }
		},
        list_descriptions: {
			name: {
	            xtype: 'searchfield',
	            itemId: 'searchfield',
	            width: 400,
	            emptyText: 'Enter search terms...',
	            store: _stores.list_descriptions,
				displayParams: {'searchby':'query'},
	            listeners: {
	                beforesubmit: function(qEvent){
						this.store.removeAll();
	                    NNLM.Rosters.DisplayManager.notify('wait', true);
	                }
	            }
			}
        }
    };
    var _grids = {
        people: {
            xtype: 'grid',
            title: 'Search Results',
            itemId: 'grid',
            scroll: true,
            cm: _colModels.people,
            sm: new Ext.grid.RowSelectionModel({
                singleSelect:true,
                listeners: {
                    rowselect: function(sm, rowIdx, r){
                        MainPanel.load(r.get('PeopleID'));
						_actions.editperson.enable();
						_actions.showpublicperson.enable();
                    },
					rowdeselect: function(){						                          
						_actions.editperson.disable();
						_actions.showpublicperson.disable();     
					}
                }
            }),
            store: _stores.people,
            stripeRows: true,
            viewConfig: {
                forceFit:true,
                enableRowBody:true,
                emptyText: 'No results found'
            },
            listeners: {
                'doload': function(PeopleID){
                    this.PeopleID = PeopleID;
                },
				'dofilter': function(){
					if(this.PeopleID === undefined){
						return;
					} 
					if(this.store.getCount() > 0){
						_actions.editperson.enable();
						_actions.showpublicperson.enable();
					}                                                                                                
				},
				'rowdblclick': _functions.editperson.createDelegate(this)
            }
        },
        list_descriptions: {
            xtype: 'grid',
            title: 'Search Results',
            itemId: 'grid',
            scroll: true,
            cm: _colModels.list_descriptions,
            sm: new Ext.grid.RowSelectionModel({
                singleSelect:true,
                listeners: {
                    rowselect: function(sm, rowIdx, r){
						MainPanel.load(r.get('ListID'));
						_actions.editlist.enable();
						_actions.showpubliclist.enable();
                    },
					rowdeselect: function(){				    	                              
						_actions.editlist.disable();
						_actions.showpubliclist.disable();         
					}
                }
            }),
            store: _stores.list_descriptions,
            stripeRows: true,
            viewConfig: {
                forceFit:true,
                enableRowBody:true,
                emptyText: 'No results found'
            },
            listeners: {
                'doload': function(ListID){
                    this.ListID = ListID;
                },
				'dofilter': function(){
					if(this.ListID === undefined){
						return;
					}         
					if(this.store.getCount() > 0){
						_actions.editperson.enable();
						_actions.showpublicperson.enable();
					}                                                                                               
				},
				'rowdblclick': _functions.editlist.createDelegate(this)
            }
        }
    };
    var _details = {
        people: {
            itemId: 'details',
            title: 'Person Details',
            autoScroll: true,
			cls: 'rosters_public_record', 
			bodyStyle: "background-color: white;padding:5px;",   
			frame: true,
			html: '<p class="metadata">Choose a person to the left to preview</p>',     
            tools: [{                 
				id: 'refresh',
                itemId:'refresh',
                qtip: 'Refresh',
                handler: function(){
                    MainPanel.chute('details').fireEvent('dofilter');
                }
            }],
            listeners: {
                'doload': function(PeopleID){
					if(PeopleID === undefined){
						return;
					}
					this.PeopleID = PeopleID;                                       
                },
                'dofilter': function(){                     
					if(this.PeopleID === undefined){
						return;
					}					                                                           
					var q = {
						'PeopleID': this.PeopleID
					};
					var p = {
						'display': 'full',
						'visibility': 'intranet'
					};
                    this.load({
						url: NNLM.get('SERVLET_BASE_URL')+
                        'people/GetPersonHTML.php?'+NNLM.Rosters.DataManager.createQuery(q, p),
						timeout: 10
					});  
                }
            }
        },	
		list_descriptions:{
            itemId: 'details',
            title: 'List Details',
            autoScroll: true,                                           
			frame: false,                  
			bodyStyle: "background-color: white;",
			html: '<p class="metadata">Choose a list to the left to preview</p>',      
			tools: [{     
				id: 'refresh',
                itemId:'refresh',
                qtip: 'Refresh',
                handler: function(){
                    MainPanel.chute('details').fireEvent('dofilter');
                }
            }],
            listeners: {
                'doload': function(ListID){
                    this.ListID = ListID;                 
                },
				'dofilter': function(){    
					if(this.ListID === undefined){
						return;
					}      
					this.load(NNLM.get('SERVLET_BASE_URL')+'lists/GetListHTML.servlet.php?ListID='+this.ListID);	
				}	
            }
        }
        /*
		list_descriptions:{
            itemId: 'details',
            title: 'List Details',
            xtype: 'treepanel',
            autoScroll: true,
            animate: true,
            containerScroll: true,
			tbar: [
			{
				text: 'Edit this list',
				itemId: 'edit',
				disabled: true,
				handler: _functions.editSelectedList
			}],
            loader: new Ext.tree.TreeLoader({
                dataUrl: NNLM.get("SERVLET_BASE_URL")+'lists/getList.servlet.php',
                nodeParameter: 'ListID'
            }),
            root: {
                nodeType: 'async',
                allowChildren: true,
                cls: 'emptynode',
                text: "[Search for a list using the toolbar searchbox.]",
                id: -1,
                expandable: true,
                expanded: true
            },
            listeners: {
                'doload': function(ListID){
                    this.ListID = ListID;
					//this.setRootNode(this.emptyNode);
                },
				'dofilter': function(){
					if(this.ListID === undefined){
						return;
					}
					var r = _stores.list_descriptions.getById(this.ListID);
					if(!r){
						return;
					}
					var root = new Ext.tree.AsyncTreeNode({
	                    text: r.get('ListTitle'),
	                    draggable:false, // disable root node dragging
	                    id: r.get('ListID'),
	                    expandable: true,
	                    expanded: true
	                });
					this.setRootNode(root);				
				}	
            }
        }
		*/
    };
    var MainPanel = new Ext.Panel({
        layout:'hbox',
        layoutConfig: {
            align: 'stretch',
            pack: 'start'
        },
        iconCls: 'iconed magnifier',
        border: false,
        type: 'find',
		help: 'Find.html',
        closable: true,     
		tbar: ['-'],
        setInterface: function(typeId, searchby){
            _dump('setInterface called.  TypeID: ' + typeId, 3);        
			MainPanel.typeId = typeId;
            var tb = MainPanel.getTopToolbar();
			var title = '';
			var description = '';      
            switch(typeId){
            case 'people':
				switch(searchby){
				case 'libid':
					title = 'Find by LIBID';
	                description = 'Find a person by DOCLINE LIBID';  
					MainPanel.help = 'find-libid.html';
					break;
				case 'role': 
				    _stores.roles.load();
					title = 'Find by role';
	                description = 'Find a person by a role assigned to their position';    
					MainPanel.help = 'find-role.html';
					break;
				case 'region':
					_stores.regions.load();
					title = 'Find by RML/Center';
	                description = 'Find a person by their RML/Center';          
					MainPanel.help = 'find-region.html';
					break;       
				case 'peopleid':
					_stores.regions.load();
					title = 'Find by PeopleID';
	                description = 'Find a person by their assigned PeopleID';    
					MainPanel.help = 'find-peopleid.html';
					break;     
				case 'name':
				default:
					searchby = 'name';
					title = 'Find by name';
		           	description = 'Find a person by their first and/or last name';  
					MainPanel.help = 'find-person.html';
				}                                         
                this.pkey = 'PeopleID';                                               
                //tb.add("Enter the person's name:");
                break;
            case 'list_descriptions':	
				switch(searchby){
				case 'name':
				default:
					searchby = 'name';
					title = 'Find by title';      
					MainPanel.help = 'find-list.html';
		           	description = 'Find a list by terms in the title or description';
				}                                   
                this.pkey = 'ListID';                                      
                //tb.add("Enter search terms for the list:");
                break;
            }      
            this.setTitle(title);        
			tb.add(_searchfields[typeId][searchby]); 
			tb.add(' ');
			tb.add({
				xtype: 'tbtext',
				style: 'color: #999',
				text: description
			});       
            MainPanel.add(_grids[typeId]);     
            MainPanel.add(_details[typeId]);
        },
        defaults: {
            deferredRender: false,
            flex: 1
        },
        listeners: { 
			'tabchange': {
				fn: function(tabPanel,	 panel) {
	                _dump(' tabchange (MainPanel) (buffered)', 'event');         
	                NNLM.Rosters.DisplayManager.notify('viewchange');
					this.fireEvent('dofilter');
				},
				buffer: 100
	        },            
            'doload': function(ID){
                this[this.pkey] = ID;
				this.rainEvent('doload', ID);
            },
            'dofilter': function(){          
				NNLM.Rosters.DisplayManager.notify('showhelp', {help:MainPanel.help});
				NNLM.Rosters.DisplayManager.clearActions();       
				NNLM.Rosters.DisplayManager.addActions(_menus[MainPanel.typeId]);                                 
                this.rainEvent('dofilter');
            }
        },
		load: function(ID){
			this.fireEvent('doload', ID);
			this.fireEvent('dofilter');
		}
    });
    return MainPanel;
};
NNLM.Rosters.Find.loaded = true;