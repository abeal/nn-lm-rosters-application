/* Conditionally included code that allows proper IE functionality.  Thanks go to Paul Sowden */
Element = function() {};
var __IEcreateElement = document.createElement;
document.createElement = function(tagName){
    var element = __IEcreateElement(tagName);
    var iface = new Element();
    for (method in iface) {
        element[method] = iface[method];
    }
    return element;
};
if(window.console === undefined){
	window.console = function(){};
}