/**
 *     @class NNLM.Rosters.Edit
 * <p>This class is independently downloaded by the Rosters application during runtime,
and houses all the operations performed by the edit search panel. An Edit panel is
created using the NNLM.Rosters.Find.makeInstance method.</p>
 *
 *   @author     Aron Beal
 *   @license All files in this web application are  licensed under the
 *   terms of the Open Source LGPL 3.0 license.
 *   License details: http://www.gnu.org/licenses/lgpl.html.
 */
/** BOOKMARK
 * collapsible panel 3 does not save properly.  Events not triggering?
 */
/*global Ext, NNLM */
Ext.ns('NNLM.Rosters.Edit');

NNLM.Rosters.Edit.makeInstance = function(uniqueId)
{ /* shortcuts to record constructors */
  var GLOBALS = {}; /** FUNCTIONS **/
  /**  _dump
   *  display a console message with this class prefixed.  used for debugging.
   *  @param msg {string} the message to display.
   **/
  var _dump = function(msg, type)
  {
    var time = new Date();
    NNLM.Rosters.dump('\n* Edit(' + (new Date().format('i:s.u')) + '): ' + msg, type);
  };
  /**  _a
   *  display an alert dialog with this class prefixed.  used for debugging.
   *  @param msg {string} the message to display.
   **/
  var _a = function(msg)
  {
    NNLM.Rosters.DisplayManager.msg(msg);
  };
  /**  _e
   *  display an error dialog with this class prefixed.  used for debugging.
   * @param msg {string} the message to display.
   **/
  var _e = function(msg)
  {
    _dump(msg, 'error');
    NNLM.Rosters.DisplayManager.err(msg);
  };
  /** _c
   *  display message as confirmation dialog.  Execute callback if 'OK' button
   *  is pressed.
   *  @param msg {string} the message to display.
   *  @param callback {function} the callback function to execute if the dialog i
   *  confirmed.
   **/
  var _c = function(msg, callback)
  {
    callback = callback === undefined ? null : callback;
    NNLM.Rosters.DisplayManager.notify('confirm',
    {
      title: 'Edit Person',
      text: msg,
      callback: callback
    });
  };
  var _p = function(msg, callback)
  {
    callback = callback === undefined ? null : callback;
    NNLM.Rosters.DisplayManager.notify('prompttext',
    {
      title: 'Edit Person',
      text: msg,
      callback: callback
    });
  };
  /** _u
   * creates a user dialog message to inform them of an action the
   * system has taken on their behalf.  Uses the configurable parameter
   * 'MESSAGE_DELAY_LENGTH' to determine how long the message remains
   * visible
   * @param msg {string} the message to display.
   * @param p {Ext.Component} the component which should be used to render the
   * message.
   */
  var _u = function(msg, p)
  {
    NNLM.Rosters.DisplayManager.notify('wait',
    {
      wait: true,
      text: msg,
      panel: p
    });
  }; /* Regular expressions that define what each account type in the accounts panel should accept as data. */
  var _accountRegexes = {
    shell: /^[-_.a-zA-Z0-9]+$/i,
    shellText: "Valid shell accounts are letters and numbers only.",
    nnlm_user: /[a-z0-9_\.\-@]/i,
    nnlm_userText: "Valid NN/LM user accounts are allowed any character in an email address.",
    drupal: /^[-_A-z0-9. ]+$/i,
    drupalText: "With Drupal accounts, spaces are allowed, punctuation is not allowed except for periods, hyphens, and underscores.",
    nnlm_mysql: /^.*$/i,
    nnlm_mysqlText: "With MySQL accounts, it is best to use only ascii characters.",
    email: /^(\w+)([\-+.][\w]+)*@(\w[\-\w]*\.){1,5}([A-Za-z]){2,6}$/,
    emailText: "The value entered does not appear to be a valid email address",
    facebook: /^(\w+)([\-+.][\w]+)*@(\w[\-\w]*\.){1,5}([A-Za-z]){2,6}$/,
    facebookText: "Valid Facebook accounts should be an email address.",
    skype: /^[-_A-z0-9.]+$/i,
    skypeText: "Valid skype accounts are letters and numbers only.",
    phone: /^[-0-9.\(\)]+$/i,
    phoneText: "Valid phone numbers can contain the following (in addition to numbers): hyphens, parentheses, periods",
    fax: /^[-0-9.\(\)]+$/i,
    faxText: "Valid fax numbers can contain the following (in addition to numbers): hyphens, parentheses, periods",
    twitter: /[a-z0-9_\.\-@]/i,
    twitterText: "Valid Twitter accounts are allowed any character in an email address.",
    linkedin: /[a-z0-9_\.\-@]/i,
    linkedinText: "LinkedIn accounts are allowed any character in an email address.",
    ntrpuser: /[a-z0-9_\.\-@]/i,
    ntrpuserText: "National Training Center accounts are allowed any character in an email address.",
    wordpress: /(((^https?)|(^ftp)):\/\/([\-\w]+\.)+\w{2,3}(\/[%\-\w]+(\.\w{2,})?)*(([\w\-\.\?\\\/+@&#;`~=%!]*)(\.\w{2,})?)*\/?)/i,
    wordpressText: "A wordpress entry must be a valid URL.  This would be a link to a blog where you are a contributor.",
    moodle: /^[-_.a-zA-Z0-9]+$/,
    moodleText: "A valid moodle account is allowed any character in an email address",
    staffwiki: /[a-z0-9_\.\-@]/i,
    staffwikiText: "Valid StaffWiki user accounts are allowed any character in an email address.",
    googleplus: /[a-z0-9_\.\-@]/i,
    googleplusText: "Valid Google Plus user accounts are allowed any character in an email address."
  };
  //standalone functions that perform an edit-global service.  The functions in these sections are either
  //called externally, or via delegate creation (which is in turn appended to an action)
  var _functions = { /* adds a new account entry to the DataManager. */
    'addaccount': function()
    {
      var grid = MainPanel.chute('accounts');
      var tb = grid.getBottomToolbar();
      var at = tb.chute('accountType');
      var av = tb.chute('accountValue');
      var as = tb.chute('accountVisibility');
      if (!(at.validate() && av.validate()))
      {
        _a("You must both choose an account type, and enter the account value in the bottom toolbar.  Both are required entries.");
        return;
      }
      grid.stopEditing();
      NNLM.Rosters.DataManager.createRecord('accounts',
      {
        PeopleID: MainPanel.PeopleID,
        AccountVisibility: as.getValue(),
        AccountTypeID: at.getValue(),
        AccountValue: av.getValue()
      });
    },
    'addposition': function()
    {
      if (!NNLM.Rosters.DisplayManager.isViewChangeOk())
      {
        return;
      }
      _p('Please enter a position title',

      function(field)
      {
        var params = {
          Title: field.getValue(),
          PeopleID: MainPanel.PeopleID,
          StartDate: new Date().format('Y-m-d')
        };
        var r = NNLM.Rosters.DataManager.createRecord('positions', params, function(success)
        {

          NNLM.Rosters.DisplayManager.notify('wait', false);
          var affectedPanel = MainPanel.chute('positions');
          r.data.PositionID = parseInt(r.get('PositionID'), 10); //comes back as string, and is not converted, because EXT is stupid at times.
          var p = createPositionPanel(r);
          p.fireEvent('doload', r.get('PositionID'));
          _a('A new position has been created (and saved).  Please assign a library from the drop down, ' + ' if applicable. (just start typing the library name)');
          affectedPanel.setActiveTab(positions.items.length - 1);
        });
        NNLM.Rosters.DataManager.save('positions');
      });
    },
    'addrole': function()
    {
      var grid = MainPanel.chute('positions').getActivePosition().chute('positionRoles');
      var tb = grid.getBottomToolbar();
      if (!tb.chute('roleType').validate())
      {
        _a("You must choose a role type to add from the drop down on the bottom row.");
        return;
      }
      if (tb.chute('roleType').obsolete === true)
      {
        _e('This role type is obsolete, and is displayed only for historical purposes.' + '  Lists are now used to represent this role type.  This message (and the associated obsolete role) will disappear in the next version.');
        return;
      }
      var id = tb.chute('roleType').getValue();
      grid.stopEditing();
      NNLM.Rosters.DataManager.createRecord('roles',
      {
        PositionID: MainPanel.chute('positions').getActivePosition().PositionID,
        RoleType: id,
        preventDuplicates: true
      });
      grid.startEditing((grid.getStore().getCount() - 1), 2);
    },
    'autofill': function()
    {
      var pan = MainPanel.chute('positions').getActivePosition().chute('panels');
      pan.chute('positionAddrForm').expand();
      if (pan.getLayout().activeItem.itemId !== 'addressInfo')
      {
        if (!_setPositionPanel(3))
        {
          return;
        }
      }
      var f = pan.chute('addressInfo');
      var defaultInst = ' ';
      var skip = false;
      f.getForm().items.each(function()
      {
        if (this.name === 'INST_INST' && this.getValue() !== '')
        {
          skip = this.getValue();
        }
      });
      if (skip)
      {
        return;
      }
      var r = NNLM.Rosters.DataManager.STORES.positions.getAt(0);
      if (!r)
      {
        _e('record for position could not be found');
        return;
      }
      if (r.get('LIBID'))
      {
        var drecord = NNLM.Rosters.DataManager.getRecordById("docline", r.get('LIBID'));
        if (!drecord)
        {
          _e('docline record could not be found');
          return;
        }
        f.getForm().items.each(function()
        {
          if (this.name === 'INST_INST' && this.getValue() === '')
          {
            this.setValue(defaultInst);
          }
          if (this.name === 'STREET_INST' && drecord.get('STREET2_INST') !== '')
          {
            this.setValue(drecord.get(this.name) + ', ' + drecord.get('STREET2_INST'));
          }
          else
          {
            this.setValue(drecord.get(this.name));
          }

        });
        f.fireEvent('change', [f]);
      }
      else
      {
        f.getForm().items.each(function()
        {
          if (this.name === 'INST_INST' && this.getValue() === '')
          {
            this.setValue(defaultInst);
          }
        });
      }
      pan.chute('positionAddrView').fireEvent('dofilter');
    },
    'clearoverride': function()
    {
      var pan = MainPanel.chute('positions').getActivePosition().chute('panels');
      if (pan === undefined || pan.getLayout().activeItem.itemId !== 'addressInfo')
      {
        _e("WRONGO");
        return;
      }
      var form = pan.chute('addressInfo');
      form.getForm().items.each(function()
      {
        this.setValue(null);
      });
      form.fireEvent('change', [form]);
      form.fireEvent('dofilter');
      form.chute('positionAddrForm').collapse();
    },
    'deleteaccount': function(mixed)
    {
      if (mixed === '')
      {
        var r = MainPanel.chute('accounts').getSelectionModel().getSelected();
        NNLM.Rosters.DataManager.destroyRecord('accounts', r);
      }
      else
      {
        NNLM.Rosters.DataManager.destroyRecordById('accounts', mixed);
      }
    },
    'deleterole': function(mixed)
    {
      if (mixed === '')
      {
        var r = MainPanel.chute('positions').getActivePosition().chute('positionRoles').getSelectionModel().getSelected();
        if (!r)
        {
          _e("record not found.");
          return;
        }
        NNLM.Rosters.DataManager.destroyRecord('roles', r);
      }
      else
      {
        NNLM.Rosters.DataManager.destroyRecordById('roles', mixed);
      }
    },
    'editlocker': function()
    {
      var pan = MainPanel.chute('lists');
      var r = NNLM.Rosters.DataManager.getRecordById("people", pan.PeopleID);
      if (r)
      {
        NNLM.Rosters.DisplayManager.notify('list',
        {
          ListID: r.get('MyListID')
        });
      }
      else
      {
        _e("Couldn't find this user's locker!");
      }
    },
    'editselectedlist': function()
    {
      var p = MainPanel.chute('lists').chute('belongGrid');
      if (!p.getSelectionModel().hasSelection())
      {
        _a("You must first select a list to edit.");
        return;
      }
      var r = p.getSelectionModel().getSelected();
      if (r)
      {
        NNLM.Rosters.DisplayManager.notify('list',
        {
          ListID: r.get('ListID')
        });
      }
      else
      {
        _e("No list was selected");
      }
    },
    'googlepics': function()
    {
      var p = MainPanel.chute('google-pictures');
      if (p.collapsed)
      {
        p.expand();
        return;
      }
      p.collapse();
    },
    'refreshlibidlabel': function(count)
    {
      count = (count === undefined) ? 0 : count + 1;
      if (count > 5)
      {
        _e("could not retrieve DOCLINE information for the requested library.  Network issues?  Try reloading.");
        return;
      }
      var v = MainPanel.chute('positions').getActivePosition().chute('positionInfo').chute('libidcombo').getValue();
      var p = MainPanel.chute('positions').getActivePosition().chute("positionInfo").chute('posmetadata');
      var r = NNLM.Rosters.DataManager.getRecordById('docline', v);
      if (!r)
      {
        if (!v)
        {
          return;
        }
        NNLM.Rosters.DataManager.load(
        {
          storeName: 'docline',
          params: {
            LIBID: v
          },
          callback: function()
          {
            NNLM.Rosters.DisplayManager.notify('relay',
            {
              target: 'refreshlibidlabel',
              args: count
            });
          }
        });
        return;
      }
      p.tpl.overwrite(p.el, r.data);
    },
    'removeaccount': function()
    {
      var grid = MainPanel.chute('accounts');
      if (grid.getSelectionModel().hasSelection() === false)
      {
        _a('You must select one or more accounts to remove, first.');
        return;
      }
      var selections = grid.getSelectionModel().getSelections();
      grid.stopEditing();
      NNLM.Rosters.DataManager.destroyRecords('accounts', selections);
    },
    'removeposition': function(b, e)
    {
      if (!NNLM.Rosters.DisplayManager.isViewChangeOk())
      {
        return;
      }
      var currentPosition = MainPanel.chute('positions').getActivePosition();
      if (MainPanel.isPrimaryPosition(currentPosition.PositionID))
      {
        _e("You cannot delete someone's primary position.  You must" + ' first reassign another position to be the primary.');
        return;
      }
      var p = NNLM.Rosters.DataManager.getRecordById('positions', currentPosition.PositionID);
      NNLM.Rosters.DisplayManager.notify('prompt',
      {
        title: 'Destroying position ',
        type: 'destroy',
        text: "Are you sure you wish to destroy the currently active position?  This action is irreversible.",
        callback: function()
        {
          NNLM.Rosters.DataManager.destroyRecordById('positions', currentPosition.PositionID);
          NNLM.Rosters.DataManager.save('positions');
        }
      });
    },
    'removerole': function()
    {
      if (!_setPositionPanel(1))
      {
        return;
      }
      var grid = MainPanel.chute('positions').getActivePosition().chute('positionRoles');
      if (grid === 'undefined')
      {
        _e("I'm sorry, but a fatal error has occured with this operation.  Please reload the application.");
      }
      var sm = grid.getSelectionModel();
      if (sm.hasSelection() === false)
      {
        _e('You must select a role before you can delete it.');
        return;
      }
      var d = sm.getSelections();
      for (var i = 0; i < d.length; i++)
      {
        dump('Removing role ' + d[i].get('RoleType'));
        NNLM.Rosters.DataManager.destroyRecord('roles', d[i]);
      }
    },
    'setgooglepicture': function(mixed)
    {
      var p = MainPanel.chute('info').chute('Picture');
      p.setValue(mixed);
      p.fireEvent('change', p);
      MainPanel.chute('info').chute('info-picture-display').fireEvent('dofilter');
    },
    'setprimary': function()
    {
      var p = MainPanel.chute('positions');
      var requestedPrimaryID = p.getActiveTab().PositionID;
      if (requestedPrimaryID === undefined)
      {
        _e('An error has occurred.  Please reload the application');
        return;
      }
      var r = NNLM.Rosters.DataManager.getRecordById("people", MainPanel.PeopleID);
      if (!r)
      {
        _e('An error has occurred.  Please reload the application');
        return;
      }
      if (r.get('PrimaryPosition') === requestedPrimaryID)
      {
        _a("This is already the member's primary position.  This aspect is indicated in the position tab with an image of a star, and the term 'Primary' in parenthesis next to the title.");
        return;
      }
      r.set('PrimaryPosition', requestedPrimaryID);
      p.primaryPositionID = requestedPrimaryID;
      MainPanel.chute('positions').fireEvent("dofilter");
    }
  };
  //Edit-global Ext.Actions that are repurposed into one or more visual components.
  var _actions = {
    'addaccount': {
      text: 'Add account',
      tooltip: "After entering an account value, and choosing an account type, use this action.  It will copy your " + "changes to the local store.  Don't forget to save your changes when you're done."
    },
    /* Adds a new position (persisting, creates modal prompt). **/
    'addposition': {
      text: 'Add Position',
      tooltip: "This control adds a new position to the current record.  After entering a position title (the " + "only required element of a new position) you will be taken to a new tab where you can modify the new " + "position further. <strong>Caution:</strong> This control automatically saves its changes."
    },
    /* Adds a new role to the position record for the forefront position */
    'addrole': {
      text: 'Add role',
      tooltip: "After choosing a role from the drop down at the bottom of the center panel, use this control to " + "copy the role to the local store.  Don't forget to save your changes."
    },
    /* fills the form fields in the docline override address panel with the contents of the associated docline record for the
    forefront position. Can change the visual display.  Triggers a visual error if no libid is currently assigned.*/
    'autofill': {
      text: 'Change Address',
      tooltip: "Whenever you override the DOCLINE&#174; address, you need to override ALL the fields.  This control " + "copies the docline record into the form, allowing minor modifications to be made without a lot of typing."
    },
    /* Resets any override to the docline default by clearing any entries in the rosters db for address. Non-persisting. */
    'clearoverride': {
      text: 'Default Address',
      tooltip: "This control removes any address stored for this record.  If a DOCLINE&trade; LIBID is assigned, the adress reverts to the institutional address that the LIBID refers to.  Don't forget to save the change."
    },
    /* Opens the list that represents the locker for the individual at the forefront display */
    'editlocker': {
      text: 'Edit locker',
      tooltip: 'This control opens your locker in a new tab for editing.'
    },
    /* Opens the selected list in a new tab */
    'editselectedlist': {
      text: 'Edit list',
      tooltip: 'This control opens the selected list in a new tab.'
    },
    /* Show/hide google image suggestions panel. */
    'googlepics': {
      text: 'Toggle images',
      tooltip: 'This control toggles the Google&#174; image suggestions panel.'
    },
    /* removes an existing account entry from the DataManager. */
    'removeaccount': {
      text: 'Remove account',
      tooltip: "This control removes any selected account entry from the local store.  Don't forget to " + "save your changes"
    },
    /* removes an existing position from the DataManager. */
    'removeposition': {
      text: 'Remove Position',
      tooltip: 'This control destroys the current position.  Use with care.'
    },
    /* removes the selected role from the forefront position */
    'removerole': {
      text: 'Delete the selected role(s)',
      tooltip: "This control removes any selected role entry from the local store.  Don't forget to " + "save your changes"
    },
    /* Sets the position panel at the forefront to be primary. */
    'setprimary': {
      text: 'Set as Primary Position',
      tooltip: "This control sets the position currently at the forefront to be the user's primary position"
    }
  };
  for (var k in _actions)
  {
    _actions[k].itemId = k;
    _actions[k].handler = _functions[k].createDelegate(this);
    _actions[k] = new Ext.Action(_actions[k]);
  }
  /** checkform
   * convenience function that tells whether a form is both dirty and contains
   * valid entries, a condition for saving.
   * @param myform {Ext.form.BasicForm} the form to be checked.
   * @return {boolean} true if the form can be saved, false otherwise.
   */
  var _checkForm = function(myform)
  {
    if (!myform)
    {
      _e('form could not be accessed');
      return false;
    }

    if (!myform.isValid())
    {
      _dump('Form is invalid.');
      return false;
    }
    if (myform.autoPersist !== undefined && !myform.isDirty())
    {
      _dump('Form contains no changes to be saved.');
      return false;
    }
    return true;
  };
  //toggle function for the positions tab.
  var _setPositionPanel = function(number)
  {
    var l = MainPanel.chute('positions');
    if (l.getActiveTab().chute('panels').getLayout().getActiveItemNum() === number)
    {
      return true;
    }
    if (!NNLM.Rosters.DisplayManager.isViewChangeOk())
    {
      return false;
    }
    l.getActiveTab().chute('panels').getLayout().setActiveItem(number);
    l.fireEvent('dofilter');
    return true;
  };
  //toggle function for the lists tab.
  var _setListsPanel = function(number)
  {
    if (!NNLM.Rosters.DisplayManager.isViewChangeOk())
    {
      return false;
    }
    var l = MainPanel.chute('lists');
    l.chute('panels').getLayout().setActiveItem(number);
    l.fireEvent('dofilter');
    return true;
  };
  var _doclineLoad = function(PositionID)
  {
    var r = NNLM.Rosters.DataManager.getRecordById("positions", PositionID);
    if (!r)
    {
      throw "position not found!";
    }
    var libid = r.get('LIBID');
    if (libid !== '')
    {
      _dump("fetching libid " + libid, 3);
      NNLM.Rosters.DataManager.load(
      {
        storeName: 'docline',
        params: {
          LIBID: libid
        }
      });
    }
  };
  var _stores = {
    /** accountTypeStore
     * used to retrieve the account types for image matching
     */
    'accountTypeStore': new Ext.data.JsonStore(
    {
      url: NNLM.get('SERVLET_BASE_URL') + 'accounts/GetAccountTypes.servlet.php',
      autoLoad: true,
      fields: [
      {
        name: 'AccountTypeID',
        type: 'int'
      },
      {
        name: 'AccountType',
        type: 'string'
      },
      {
        name: 'AccountTypeDescription',
        type: 'string'
      },
      {
        name: 'AccountImageIcon',
        type: 'string'
      }],
      idProperty: 'AccountTypeID',
      root: 'results',
      totalProperty: 'count',
      baseParams: {
        query: ''

      },
      listeners: {
        'activate': function(store, records, options)
        {
          accounts.get(1).view.refresh();
        },
        'loadexception': function(sender, options, response, error)
        {
          throw ('accountTypeStore exception');
        },
        'load': function()
        {
          //var a = accounts.getComponent('accountGrid');
          //a.enable();
          /*if (a.viewReady === true) {
                        a.view.refresh();
                    }*/
        }
      }
    }),

    /** libidStore
     *   a local store used for read-only retrieval of LIBIDS.  Used for local
     *   combo boxes in the edit interface.
     **/
    'libidStore': new Ext.data.JsonStore(
    {
      url: NNLM.get('SERVLET_BASE_URL') + 'GetLibids.servlet.php',
      fields: [
      {
        name: 'LIBID',
        type: 'string'
      },
      {
        name: 'INST_INST',
        type: 'string'
      },
      {
        name: 'DEPT_INST',
        type: 'string'
      }],
      idProperty: 'LIBID',
      root: 'results',
      totalProperty: 'count',
      baseParams: {
        query: ''
      },
      sortInfo: {
        field: 'LIBID',
        direction: 'ASC'
      },
      listeners: {
        'loadexception': function(sender, options, response, error)
        {
          _dump('Response: ' + response.responseText, 3);
          throw 'exception: ' + error;
        }
      }
    }),
    'lockerStore': new Ext.data.JsonStore(
    {
      // store configs
      autoDestroy: true,
      url: NNLM.get('SERVLET_BASE_URL') + 'lists/mylist_watch.servlet.php',
      storeId: 'watchingStore',
      // reader configs
      root: 'results',
      record: 'row',
      successProperty: 'success',
      idProperty: 'ListEntryID',
      fields: NNLM.Rosters.DataManager.getConfig('lists'),
      listeners: {
        'beforeload': function()
        {
          this.setBaseParam('PeopleID', this.PeopleID);
        }
      }

    }),
    'listBelongStore': new Ext.data.JsonStore(
    {
      // store configs
      autoDestroy: true,
      url: NNLM.get('SERVLET_BASE_URL') + 'lists/mylist_belong.servlet.php',
      storeId: 'watchingStore',
      // reader configs
      root: 'results',
      record: 'row',
      successProperty: 'success',
      idProperty: 'ListEntryID',
      fields: NNLM.Rosters.DataManager.getConfig('list_descriptions'),
      listeners: {
        'beforeload': function()
        {
          this.setBaseParam('request', this.PeopleID);
        }
      }
    })
  };
  var nullRecord = new _stores.accountTypeStore.recordType(
  {
    AccountTypeID: -1,
    AccountType: 'null',
    AccountTypeDescription: "No account type selected!",
    AccountImageIcon: "/images/social_networking/question_mark_32.png"
  });

  var _menus = {
    'info': [_actions.googlepics],
    'accounts': [_actions.addaccount, _actions.removeaccount],
    'positions': [_actions.setprimary, _actions.addposition, _actions.removeposition],
    'positionInfo': [],
    'positionRoles': [_actions.addrole, _actions.removerole],
    'addressInfo': [_actions.autofill, _actions.clearoverride],
    'lists': [_actions.editselectedlist]
  };
  var _templates = {
    accountButtonTpl: new Ext.XTemplate('<tpl for="."><p class="delete_link"><a href="#" onclick="NNLM.Rosters.DisplayManager.notify(\'relay\', {target: \'deleteaccount\', args:\'{AccountID}\'}); return false">Delete</a>', '<tpl if="AccountTypeID === 3"><p class="delete_link"><a href="#" onclick="window.open(\'https://dev.staff.nnlm.gov/passwd/admin/?uname=abeal&state=2\'); return false">Set Password</a></tpl>', '</p></tpl>').compile(),
    accountChoiceTpl: new Ext.XTemplate('<tpl for="."><div class="search-item account_grid_entry"><img src="{AccountImageIcon}" alt="{AccountTypeDescription}" style="vertical-align: middle;" /><p style="display: inline; background-color: transparent;">{AccountTypeDescription}</p><br style="clear: both" /></div></tpl>').compile(),
    accountImageTpl: new Ext.XTemplate('<tpl for="."><img class="search-item" title="{AccountTypeDescription}" src="{AccountImageIcon}" alt="{AccountTypeDescription}" /></tpl>').compile(),
    accountDeleteTpl: new Ext.XTemplate('<tpl for="."><p class="delete_link"><a href="#" onclick="NNLM.Rosters.DisplayManager.notify(\'relay\', {target: \'deleteaccount\', args:\'{AccountID}\'}); return false">Delete</a></p></tpl>').compile(),
    roleDeleteTpl: new Ext.XTemplate('<tpl for="."><p class="delete_link"><a href="#" onclick="NNLM.Rosters.DisplayManager.notify(\'relay\', {target: \'deleterole\', args:\'{RoleID}\'}); return false">Delete</a></p></tpl>').compile(),
    accountTpl: new Ext.XTemplate('<tpl for="."><div class="account_grid_entry">',
    //'<p class="delete_link"><a href="#" onclick="NNLM.Rosters.DataManager.destroyRecordByIndex(\'accounts\', {[xcount]}); return false">Delete</a></p>',
    '<img src="{AccountImageIcon}" alt="{AccountTypeDescription}" />', '<p class="AccountValue">{AccountValue}</p><p class="AccountDescription">({AccountTypeDescription}) {AccountVisibility}</p>', '<p class="AccountComments">{AccountComments}</p>', '<tpl if="this.isEmpty(AccountID);">', '</tpl></div></tpl>',
    {
      isEmpty: function(p)
      {
        return isset(p);
      }
    }).compile(),
    noAddress: new Ext.XTemplate('<tpl if="!LIBID">', '<tpl if="!INST_INST">', '<div class="position_record">', '<h2>No address supplied</h2>', '<p>No address has been supplied for this position. Assigning a LIBID to this position ' + 'will cause the address of the LIBID instituion to be used as default.  You may ' + 'then override aspects of this address, or simply supply your own now, if no LIBID' + ' applies to this position.</p>', '</div>', '</tpl>', '</tpl>').compile(),
    dtpl: new Ext.XTemplate('<tpl if="LIBID">', '<h3>DOCLINE default address (based on assigned LIBID; used if no address is supplied).</h3>', '<div class="position_record">', '<a href="http://nnlm.gov/members/results.html?libid={LIBID}" target="_blank">', '<span class="institution">{INST_INST}</span>', '<tpl if="DEPT_INST"> <span class="department">{DEPT_INST}</span></tpl></a>', '<div class="address">', '{STREET_INST}', '<tpl if="STREET2_INST"> {STREET2_INST}</tpl>', '<br />{CITY_INST}, {STATE_ETC_CODE_INST}, {ZIP_MAIL_CODE_INST}', '</div></div>', '</tpl>').compile(),
    ptpl: new Ext.XTemplate('<tpl if="INST_INST">', '<h3>Supplied address:</h3>', '<div class="position_record">', '<span class="institution">{INST_INST}</span>', '<tpl if="DEPT_INST"> <span class="department">{DEPT_INST}</span></tpl>', '<div class="address">', '{STREET_INST}', '<tpl if="STREET2_INST"> {STREET2_INST}</tpl>', '<br />{CITY_INST}, {STATE_ETC_CODE_INST}, {ZIP_MAIL_CODE_INST}', '</div>', '</tpl>').compile(),
    posmetadata: new Ext.XTemplate('<tpl for="."><p class="posmetadata">', '({LIBID}) {INST_INST}<br />{DEPT_INST}', '</p></tpl>'),
    possearch: new Ext.XTemplate('<tpl for="."><div class="search-item">', '<p class="posmetadata">({LIBID}) {INST_INST}', '<tpl if="DEPT_INST">, {DEPT_INST}</tpl>', '</p>', '</div></tpl>'),
    roleStoreTpl: new Ext.XTemplate('<tpl for="."><div class="search-item" style="padding:5px;">', '<strong>{RoleType}</strong><br /><span class="metadata">{RoleDescription}</span>', '</div></tpl>').compile()
  };
  /** overview
   *   panel used for displaying an overall summary of the person, including name,
   *   contact information and address for their primary position, and
   *   all accounts.
   **/
  var overview = {
    help: 'edit-overview.html',
    itemId: 'overview',
    bodyStyle: 'padding: 5px',
    title: 'Public View',
    bodyCssClass: 'overview',
    frame: false,
    border: true,
    autoScroll: true,
    tools: [
    {
      itemId: 'refresh',
      id: 'refresh',
      qtip: 'Refresh Page Data',
      handler: function()
      {
        MainPanel.getComponent('overview').fireEvent('dofilter');
      }
    }],
    listeners: {
      'doload': function(PeopleID)
      {
        this.PeopleID = PeopleID;
      },
      'dofilter': {
        fn: function()
        {
          if (!isset(this.PeopleID))
          {
            throw "People ID not set!";
          }
          var r = NNLM.Rosters.DataManager.getRecordById("people", this.PeopleID);

          q = {
            'PeopleID': this.PeopleID
          };
          p = {
            'display': 'full',
            'visibility': 'public'
          };
          this.load(
          {
            url: NNLM.get('SERVLET_BASE_URL') + 'people/GetPersonHTML.php',
            params: {
              request: JSON.stringify(q),
              params: JSON.stringify(p)
            },
            text: "Updating display to most current data..."
          });
        },
        buffer: 500
      }
    }
  };
  /** overview_i
   *  same as overview panel, but displays intranet-only visible information as well
   **/
  var overview_i = {
    help: 'edit-overview-i.html',
    title: 'Intranet View',
    itemId: 'overview_i',
    bodyCssClass: 'overview',
    bodyStyle: 'padding: 5px',
    frame: false,
    border: true,
    autoScroll: true,
    tools: [
    {
      itemId: 'refresh',
      id: 'refresh',
      qtip: 'Refresh Page Data',
      handler: function()
      {
        MainPanel.getComponent('overview').fireEvent('dofilter');
      }
    }],
    listeners: {
      'doload': function(PeopleID)
      {
        this.PeopleID = PeopleID;
      },
      'dofilter': {
        fn: function()
        {
          if (!isset(this.PeopleID))
          {
            throw "People ID not set!";
          }
          var r = NNLM.Rosters.DataManager.getRecordById("people", this.PeopleID);

          q = {
            'PeopleID': this.PeopleID
          };
          p = {
            'display': 'full',
            'visibility': 'intranet'
          };
          this.load(
          {
            url: NNLM.get('SERVLET_BASE_URL') + 'people/GetPersonHTML.php',
            params: {
              request: JSON.stringify(q),
              params: JSON.stringify(p)
            },
            text: "Updating display to most current data..."
          });
        },
        buffer: 500
      }
    }
  };
  /* info
   *  panel displaying and allowing modification of information in
   *  the rosters.people table.
   */
  var info = {
    title: 'Info',
    itemId: 'info',
    id: 'info-' + uniqueId,
    help: 'edit-info.html',
    layout: 'border',
    category: 'info',
    defaults: {
      border: false,
      frame: false,
      flex: 1
    },
    items: [
    {
      itemId: 'info-picture-display',
      region: 'west',
      frame: true,
      width: 220,
      border: true,
      tpl: new Ext.XTemplate('<tpl if="this.exists(Picture)">', '<img class="profile-picture" src="{Picture}" alt="Photo of {FirstName} {LastName}" />', '</tpl>', '<tpl if="!this.exists(Picture)">', '<img class="profile-picture" src="' + NNLM.get('NO_IMAGE_AVAILABLE') + '" alt="No Photo Available for {FirstName} {LastName}" />', '</tpl>').compile(),
      updateDetail: function(data)
      {
        if (!isset(this.body))
        {
          return;
        }
        this.tpl.overwrite(this.body, data);
      },
      listeners: {
        'doload': function(PeopleID)
        {
          this.PeopleID = PeopleID;
        },
        'dofilter': function()
        {
          var r = NNLM.Rosters.DataManager.getRecordById("people", this.PeopleID);
          this.updateDetail(r.data);
        },
        'doreset': function()
        {
          this.fireEvent('dofilter');
        }
      }
    },
    {
      xtype: 'form',
      itemId: 'info-details',
      region: 'center',
      trackResetOnLoad: true,
      autoPersist: function(field)
      {
        if (!NNLM.Rosters.DataManager.updateRecord('people', MainPanel.PeopleID, this.getForm()))
        {
          throw "error during autopersist";
        }
      },
      defaults: {
        xtype: 'textfield'
      },
      items: [
      {
        xtype: 'displayfield',
        name: 'LastUpdt',
        fieldLabel: 'Last Updated'
      },
      {
        xtype: 'displayfield',
        name: 'PeopleID',
        fieldLabel: 'People ID'
      },
      NNLM.Rosters.createSelect(
      {
        name: 'Prefix',
        allowBlank: true,
        emptyText: 'N/A'
      }, ['', 'Atty.', 'Coach', 'Dr.', 'Fr.', 'Gov.', 'Hon.', 'Ms.', 'Mr.', 'Mrs.', 'Ofc.', 'Pres.', 'Prof.', 'Rep.', 'Rev.']),
      {
        fieldLabel: 'First',
        name: 'FirstName',
        regex: /^[A-z ]+$/,
        regexText: 'Only letters and spaces are allowed in this field.'
      },
      {
        fieldLabel: 'Middle',
        name: 'MiddleName',
        regex: /^[A-z ]+$/,
        regexText: 'Only letters and spaces are allowed in this field.'
      },
      {
        fieldLabel: 'Last',
        name: 'LastName',
        regex: /^[A-z ]+$/,
        regexText: 'Only letters and spaces are allowed in this field.'
      },
      {
        fieldLabel: 'Suffix',
        name: 'Suffix',
        regex: /^[A-z,. ]+$/,
        regexText: 'Only letters, commas, or periods are allowed in this field.',
        width: 50
      },
      {
        xtype: 'textfield',
        itemId: 'Picture',
        name: 'Picture',
        fieldLabel: 'Photo',
        width: '85%',
        emptyText: 'Enter an image url, or select an option below',
        regexText: 'Absolute URLs only, please. (must start with http...)',
        regex: /^http(s?):\/\//
      }],
      listeners: {
        'enter': function(currentField)
        {
          _dump(' enter (Edit info tab)', 'event');
          this.nextOrSubmit();
        },
        'doload': function(PeopleID)
        {
          this.PeopleID = PeopleID;
        },
        'dofilter': function()
        {
          _dump("dofilter", 3);
          var r = NNLM.Rosters.DataManager.getRecordById("people", this.PeopleID);
          //alert('loading form with r: ' + r.get('MiddleName'));
          this.getForm().loadRecord(r);
          _actions.googlepics.enable();
        },
        'dosave': function()
        {
          NNLM.Rosters.DataManager.save('people');
        },
        'doreset': function()
        {
          var me = this;
          _dump(" doreset (info)", 'event');
          NNLM.Rosters.DataManager.reset('people', function()
          {
            _dump(" doreset (info) - completed", 'event');
            var r = NNLM.Rosters.DataManager.getRecordById("people", me.PeopleID);
            //alert('loading record.  r value: ' + r.get('FirstName'));
            me.getForm().loadRecord(r);
          });
        }
      }
    },
    {
      title: 'Alternate image suggestions',
      frame: true,
      border: true,
      autoScroll: true,
      region: 'east',
      collapsible: true,
      collapsed: false,
      collapseMode: 'mini',
      width: 200,
      itemId: 'google-pictures',
      iconCls: 'iconed google',
      tpl: new Ext.XTemplate('<img class="googlePicture" src="' + NNLM.get('NO_IMAGE_AVAILABLE') + '" alt="No image available" ', ' onclick="NNLM.Rosters.DisplayManager.notify(\'relay\', {target: \'setgooglepicture\', args:\'{null}\'})"/>', '<tpl for=".">', '<img class="googlePicture" src="{tbUrl}"', 'title="{contentNoFormatting}" alt="{contentNoFormatting}"', ' onclick="NNLM.Rosters.DisplayManager.notify(\'relay\', {target: \'setgooglepicture\', args:\'{tbUrl}\'})" />', '</tpl>').compile(),
      fbar: [],
      updatePictures: function()
      {
        if (this.imageSearch.results.length === 0)
        {

          this.tpl.overwrite(this.body,
          {
            url: 'images/noimage.gif'
            //contentNoFormatting: 'No images found for this person'
          });
          return;
        }
        //for (i = 0; i < this.imageSearch.results.length; i++) {
        //enable line to see Google search result details
        //_dump(NNLM.po(this.imageSearch.results[i]));
        //}
        this.update(this.imageSearch.results);
      },
      listeners: {
        'afterrender': function()
        {
          this.getFooterToolbar().add(google.search.Search.getBranding());
          this.imageSearch = new google.search.ImageSearch();
        },
        'doload': function(PeopleID)
        {
          this.PeopleID = PeopleID;
        },
        'dofilter': function()
        {
          var r = NNLM.Rosters.DataManager.getRecordById("people", this.PeopleID);


          // Restrict to medium sized images only
          /*this.imageSearch.setRestriction(
                    google.search.ImageSearch.RESTRICT_IMAGESIZE,
                    google.search.ImageSearch.IMAGESIZE_MEDIUM);*/
          this.imageSearch.setSiteRestriction('nnlm.gov');
          this.imageSearch.setResultSetSize(
          google.search.Search.LARGE_RESULTSET);
          //callback set here.
          this.imageSearch.setSearchCompleteCallback(this,
          this.updatePictures, [this.imageSearch]);
          this.imageSearch.execute(r.get('FirstName') + ' ' + r.get('LastName'));
        },
        'relay': function()
        {
          this.collapse();
        }
      }
    }],
    listeners: {
      'enter': function(component)
      {
        this.chute('info-details').fireEvent('enter', [component]);
      },
      'dofilter': function()
      {
        NNLM.Rosters.DisplayManager.clearActions();
        NNLM.Rosters.DisplayManager.addActions(_menus[this.itemId]);
        this.rainEvent('dofilter');
      },
      'doload': function(PeopleID)
      {
        this.PeopleID = PeopleID;
        this.rainEvent('doload', PeopleID);
      },
      'dosave': function(b, e)
      {
        _dump(' dosave', 'event');
        var record = NNLM.Rosters.DataManager.getRecordById("people", MainPanel.PeopleID);
        if (record === false)
        {
          return;
        }
        this.rainEvent('dosave');
        //this.getComponent('info-details').
        //this.getComponent('info-picture').fireEvent('dofilter');
      },
      'doreset': function()
      {
        this.rainEvent('doreset');
      },
      'relay': function(mixed)
      {
        this.rainEvent('relay', [mixed]);
      }
    }
  };
  /* accounts
   *  panel for displaying and allowing modification of accounts belonging
   *  to this individual
   */
  var accounts = {
    title: 'Accounts',
    itemId: 'accounts',
    xtype: 'editorgrid',
    //cls: 'position_panel',
    help: 'edit-accounts.html',
    stripeRows: true,
    store: NNLM.Rosters.DataManager.STORES.accounts,
    emptyText: 'No accounts yet assigned',
    clicksToEdit: 2,
    deferEmptyText: false,
    bbar: [
    {
      xtype: 'tbtext',
      text: 'TYPE:'
    },
    {
      xtype: 'selectbox',
      itemId: 'accountType',
      editable: false,
      fieldLabel: 'Choose an account type',
      width: 300,
      bodyStyle: 'margin: 5px',
      store: _stores.accountTypeStore,
      mode: 'remote',
      valueField: 'AccountTypeID',
      displayField: 'AccountTypeDescription',
      triggerAction: 'all',
      emptyText: 'pull down to select an account type',
      itemSelector: 'div.search-item',
      allowBlank: false,
      tpl: _templates.accountChoiceTpl,
      listeners: {
        'select': {
          fn: function(sb, r, i)
          {
            //_a("type: " + r.get('AccountType') + ', regex: ' +  _accountRegexes[r.get('AccountType')]);
            if (_accountRegexes[r.get('AccountType')] === undefined)
            {
              _e("The account type " + r.get("AccountType") + " does not have a regex for evaluation!");
            }
            var f = MainPanel.chute('accounts').getBottomToolbar().chute('accountValue');
            f.regex = _accountRegexes[r.get('AccountType')];
            f.regexText = _accountRegexes[r.get('AccountType') + 'Text'];
          },
          buffer: 100
        }
      }
    },
    {
      xtype: 'tbtext',
      text: 'VISIBILITY: '
    },
    NNLM.Rosters.createSelect(
    {
      name: 'AccountVisibility',
      itemId: 'accountVisibility'
    }, [
    {
      name: 'Intranet',
      value: 'intranet'
    },
    {
      name: 'Public',
      value: 'public'
    }]),
    {
      xtype: 'tbtext',
      text: 'VALUE:'
    },
    {
      xtype: 'textfield',
      itemId: 'accountValue',
      allowBlank: false,
      listeners: {
        'enter': _functions.addaccount.createDelegate(this)
      }
    }],
    viewConfig: {
      forceFit: true,
      emptyText: 'No accounts yet assigned.',
      deferEmptyText: false
    },
    initEvents: function()
    {
      Ext.grid.EditorGridPanel.superclass.initEvents.call(this);
      this.relayEvents(this.store, ['destroy', 'save', 'update']);
    },
    sm: new Ext.grid.RowSelectionModel(
    {}),
    cm: new Ext.grid.ColumnModel(
    {
      defaults: {
        hideable: false
      },
      columns: [
      {
        header: '<span>Type</span>',
        dataIndex: 'AccountTypeID',
        sortable: false,
        width: 30,
        renderer: function(value, metaData, record, rowIndex, colIndex, store)
        {
          var acctR = _stores.accountTypeStore.getById(parseInt(record.get('AccountTypeID'), 10));
          if (acctR === undefined)
          {
            return '<p class="error">Not defined</p>';
          }
          var composite = Ext.apply(
          {}, record.data);
          composite = Ext.apply(composite, acctR.data);
          return _templates.accountImageTpl.applyTemplate(composite);
        }
      },
      {
        header: '<span class="editable">Account Value</span>',
        dataIndex: 'AccountValue',
        sortable: true,
        allowBlank: false,
        editor: {
          xtype: 'textfield'
        }
        //x-grid3-row-alt
      },
      {
        header: '<span class="editable">Account Visibility</span>',
        dataIndex: 'AccountVisibility',
        width: 40,
        sortable: true,
        editor: NNLM.Rosters.createSelect(
        {
          name: 'AccountVisibility',
          fieldLabel: '* Account Visibility'
        }, [
        {
          name: 'Intranet',
          value: 'intranet'
        },
        {
          name: 'Public',
          value: 'public'
        }])
      },
      {
        header: '<span class="editable">Account Comments</span>',
        dataIndex: 'AccountComments',
        editor: {
          xtype: 'textfield'
        }
      },
      {
        header: '<span>Verified?</span>',
        width: 35,
        dataIndex: 'AccountVerified',
        sortable: false,
        renderer: function(value, metaData, record, rowIndex, colIndex, store)
        {
          switch (value)
          {
          case -1:
            return 'N/A';
          case 1:
            return '<img src="/rosters/images/icons/flag_green.png" title="This account has been verified to exist on NN/LM servers" alt="Account Verified" />';
          case 0:
          default:
            return '<img src="/rosters/images/icons/flag_red.png" alt="Account Verification Failed" title="No record of this account was actually found on NN/LM servers.  Please contact Web-STOC to remedy this."/>';
          }
        }
      },
      {
        dataIndex: 'AccountID',
        header: 'Controls',
        width: 35,
        sortable: false,
        renderer: function(value, metaData, record, rowIndex, colIndex, store)
        {
          return _templates.accountDeleteTpl.applyTemplate(record.data);
        }
      }

      ]
    }),
    listeners: {
      'celldblclick': function(grid, ri, ci, e)
      {
        if (!grid.getColumnModel().isCellEditable(ci, ri))
        {
          return;
        }
        grid.startEditing(ri, ci);
      },
      'dofilter': function()
      {
        NNLM.Rosters.DisplayManager.clearActions();
        NNLM.Rosters.DisplayManager.addActions(_menus[this.itemId]);
        _actions.addaccount.enable();
        _actions.removeaccount.enable();
      },
      'doload': function(PositionID)
      {
        this.PositionID = PositionID;
      },
      'dosave': function()
      {
        NNLM.Rosters.DataManager.save('accounts');
      },
      'doreset': function()
      {
        NNLM.Rosters.DataManager.reset('accounts');
      }
    }
  };
  /* positions
   *  panel for displaying and allowing modification of positions belonging
   *  to this individual
   */
  var positions = {
    title: 'Positions',
    xtype: 'tabpanel',
    itemId: 'positions',
    help: 'edit-positions.html',
    getActivePosition: function()
    {
      return this.getActiveTab();
    },
    enableTabScroll: true,
    activeTab: 0,
    resizeTabs: false,
    // turn on tab resizing
    minTabWidth: 45,
    maxTabWidth: 200,
    frame: false,
    border: false,
    listeners: {
      'afterreset': function()
      {
        _dump("afterreset positions", 'info');
        this.fireEvent('dofilter');
      },
      'enter': function()
      {
        _dump(' enter (positions tab)', 'event');
        this.getActiveTab().fireEvent('enter');
      },
      'tabchange': function(tabPanel, tab)
      {
        _dump(' tabchange (positions panel)', 'event');
        MainPanel.fireEvent('dofilter');
      },
      'dofilter': function()
      {
        _dump(' dofilter (Positions panel)', 'event');
        if (!MainPanel.loaded)
        {
          _dump('Mainpanel not loaded.  Returning', 'error', 3);
          return;
        }
        NNLM.Rosters.DisplayManager.clearActions();
        NNLM.Rosters.DisplayManager.addActions(_menus[this.itemId]);
        _actions.addposition.enable();
        _actions.removeposition.enable();
        _actions.setprimary.enable();
        if (this.items.length === 0)
        {
          return;
        }
        var person_r = NNLM.Rosters.DataManager.getRecordById('people', MainPanel.PeopleID);
        var p = this;
        this.items.each(function()
        {
          var position_r = NNLM.Rosters.DataManager.getRecordById('positions', this.PositionID);
          if (!position_r)
          {
            _a("position " + this.PositionID + ' removed.');
            if (p.getActiveTab().PositionID === this.PositionID)
            {
              p.setActiveTab(0);
            }
            this.destroy();
            return;
          }
          if (this.PositionID !== person_r.get('PrimaryPosition'))
          {
            this.removeClass('primary');
            this.setTitle(position_r.get('Title'));
            this.setIconClass('');
          }
          else
          {
            this.addClass('primary');
            this.setTitle(position_r.get('Title') + ' (Primary)');
            this.setIconClass('star');
          }
        });
        this.getActivePosition().fireEvent('dofilter');
      },
      'doload': function(PeopleID)
      {
        _dump(' doload (positions).  PeopleID: ' + PeopleID, 'event');
        // Create panels for all positions.
        this.PeopleID = PeopleID;
        this.primaryPositionID = NNLM.Rosters.DataManager.getRecordById('people', PeopleID).get('PrimaryPosition');
        NNLM.Rosters.DataManager.STORES.positions.query('PeopleID', PeopleID, false).each(function(r)
        {
          // Create separate panels for each position this person holds.
          if (r.get('PeopleID') === PeopleID)
          {
            _dump("Adding position panel for person " + r.get('PeopleID') + ",  PositionID " + r.get('PositionID'), 2);
            var p = createPositionPanel(r);
            p.fireEvent("doload", r.get('PositionID'));
          }
        });
        var person_r = NNLM.Rosters.DataManager.getRecordById('people', MainPanel.PeopleID);
        this.items.each(function()
        {
          var position_r = NNLM.Rosters.DataManager.getRecordById('positions', this.PositionID);
          if (this.PositionID !== person_r.get('PrimaryPosition'))
          {
            _actions.setprimary.disable();
            this.removeClass('primary');
            this.setTitle(position_r.get('Title'));
            this.setIconClass('');
          }
          else
          {
            _actions.setprimary.enable();
            this.addClass('primary');
            this.setTitle(position_r.get('Title') + ' (Primary)');
            this.setIconClass('primary');
          }
        });
      },
      'dosave': function()
      {
        _dump(' positions dosave', 'event');
        if (this.items.length === 0)
        {
          return;
        }
        NNLM.Rosters.DataManager.save('positions');
        NNLM.Rosters.DataManager.save('people');
        this.getActiveTab().fireEvent('dosave');
      },
      'doreset': function()
      {
        _dump('positions doreset', 'event');
        if (this.items.length === 0)
        {
          return;
        }
        NNLM.Rosters.DataManager.reset('positions');
        NNLM.Rosters.DataManager.reset('roles');
        NNLM.Rosters.DataManager.reset('people');
        this.getActiveTab().fireEvent('doreset');
        this.fireEvent('dofilter');
      }
    }
  };


  /** createPositionPanel
   * Creates a distinct tab panel for a given position.
   * @param args {Ext.data.Record}
   * @return {Ext.Panel}
   */
  var createPositionPanel = function(record)
  {
    if (!record.get('PositionID'))
    {
      _e('Required attribute PositionID is not set as an argument for the ' + 'createPositionPanel function.  Cannot create new panel.');
      return false;
    }
    var panelID = 'position-' + record.get('PositionID');
    _dump("Creating panel for position ID: " + record.get('PositionID')); /* Beginning of new panel construction */
    var panels = [
    //Positions Info subpanel
    {
      xtype: 'form',
      itemId: 'positionInfo',
      title: 'Position Information',
      cls: 'position_subpanel',
      id: panelID + '-positionInfo',
      help: 'edit-positions-info.html',
      trackResetOnLoad: true,
      padding: '5',
      layout: 'vbox',
      layoutConfig: {
        align: 'stretch',
        pack: 'start'
      },
      //Reset ops return to stored data, instead of blank
      autoPersist: function(field)
      {
        var pid = MainPanel.chute('positions').getActivePosition().PositionID;
        NNLM.Rosters.DataManager.updateRecord('positions', pid, this.getForm());
      },
      defaults: {
        layout: 'form',
        autoWidth: true,
        autoHeight: true,
        margin: '30',
        bodyStyle: 'background-color: white'
      },
      items: [
      {
        xtype: 'fieldset',
        title: 'DOCLINE affiliation (if any)',
        flex: 1,
        items: [
        {
          xtype: 'label',
          itemId: 'posmetadata',
          tpl: _templates.posmetadata,
          listeners: {
            doreset: function()
            {}
          }
        },
        {
          name: 'LIBID',
          itemId: 'libidcombo',
          fieldLabel: 'LIBID',
          autoWidth: true,
          xtype: 'combo',
          width: 500,
          store: _stores.libidStore,
          mode: 'remote',
          valueField: 'LIBID',
          displayField: 'LIBID',
          forceSelection: true,
          loadingText: "Retrieving libraries...",
          itemSelector: 'div.search-item',
          tpl: _templates.possearch,
          listeners: {
            'dofilter': _functions.refreshlibidlabel,
            doreset: function()
            {
              this.fireEvent('dofilter');
            },
            'select': function(combo, r, i)
            {
              this.fireEvent('dofilter');
            },
            'blur': function(f)
            {
              this.fireEvent('dofilter');
            },
            'beforeclear': function(f)
            {
              _a('"' + f.getRawValue() + '" is not a legitimate LIBID choice.  If you believe this to be an error, make sure the library is not closed, and that it is a docline member.  These are conditions of assignment.');
            }
          }
        }],
        listeners: {
          'dofilter': function()
          {
            this.rainEvent('dofilter');
          },
          'doreset': function()
          {
            this.rainEvent('doreset');
          }
        }
      },
      {
        xtype: 'fieldset',
        title: 'Position Details',
        flex: 2,
        items: [
        {
          xtype: 'label',
          itemId: 'posmetadata',
          tpl: new Ext.XTemplate('<tpl for=".">', '<p class="posmetadata">', 'Position ID: {PositionID}<br />', 'Region: {Region}', '</p>', '</tpl>'),
          listeners: {
            'dofilter': function()
            {
              var r = NNLM.Rosters.DataManager.STORES.positions.getAt(0);
              this.tpl.overwrite(this.el, r.data);
            },
            'doreset': function()
            {
              this.fireEvent('dofilter');
            }
          }
        },
        {
          fieldLabel: 'Title',
          name: 'Title',
          xtype: 'textarea',
          autoHeight: true,
          width: 200
        },
        NNLM.Rosters.createSelect(
        {
          name: 'PositionVisibility',
          fieldLabel: 'Position Visibility'
        }, [
        {
          name: 'Intranet',
          value: 'intranet'
        },
        {
          name: 'Public',
          value: 'public'
        }]),
        NNLM.Rosters.createSelect(
        {
          name: 'PositionRanking',
          fieldLabel: 'Position Type'
        }, ['Director', 'Assistant / Associate Director', 'Professional', 'Other', 'Support', 'Student / Temp']),
        {
          xtype: 'datefield',
          fieldLabel: 'Start Date',
          name: 'StartDate',
          allowBlank: false,
          minValue: '01/01/1969',
          minText: 'No dates before Jan 01, 1969, please.'
        },
        {
          xtype: 'datefield',
          fieldLabel: 'End Date',
          name: 'EndDate',
          minValue: '01/01/1969',
          minText: 'No dates before Jan 01, 1969, please.',
          emptyText: 'Not set'
        }]
      }
      /*{
        xtype: 'fieldset',
        x: '50%',
        y: '5%',
        title: 'Position Contact Info',
        defaults: {
                  xtype: 'textfield',
                  allowBlank: true
              },
        items: [{
                  fieldLabel: 'Phone Country Code',
                  name: 'PHONE_COUNTRY_CODE_INST',
                  width: 20,
                  columnWidth: 0.1,
                  regex: /^\d*$/,
                  regexText: 'Country Code: Only numbers are allowed in this field.  No punctuation.'
              },
              {
                  fieldLabel: 'Phone Area Code',
                  name: 'PHONE_AREA_CODE_INST',
                labelAlign: 'top',
                  width: 60,
                  columnWidth: 0.2,
                  regexText: 'Area Code: Three digits are required here.  No parentheses, please',
                  regex: /^(\d{3})?$/,
          listeners: {change: function(field, newvalue, oldvalue){
            var re = /^.*(\d{3}).*$/;
            if(field.getValue().match(re) !== null){
              field.setValue(RegExp.$1);
            }
          }}
              },
              {
                  fieldLabel: 'Phone',
                  name: 'PHONE_INST',
                  width: 100,
                  columnWidth: 0.3,
          emptyText: 'XXX-XXXX',
                  regexText: 'Phone Number: 7 digits and a hyphen are required here.',
                  regex: /^(\d{3})-(\d{4})?$/,
          listeners: {
            change: function(field, newvalue, oldvalue){
              var re = /^(\d{3})(\d{4})$/;
              if(field.getValue().match(re) !== null){
                field.setValue(RegExp.$1 + '-' + RegExp.$2);
              }
            }
          }
              },
              {
                  fieldLabel: 'Phone Extension',
                  name: 'PHONE_EXT_INST',
                  width: 60,
                  columnWidth: 0.2,
                  regexText: 'Phone extension: Only numbers are allowed in this field.',
                  regex: /^\d*$/
              },{
                  fieldLabel: 'Email Address',
          xtype: 'textfield',
                  name: 'Email',
                  vtype: 'email',
          width: 200
        }]
      }*/
      ],
      listeners: {
        'enter': function()
        {
          this.next();
        },
        'doload': function(PositionID)
        {
          _dump(' doload (positions info form).  PositionID: ' + PositionID, 'event');
          this.PositionID = PositionID;
          //this.setEventForwarding(panelID);
        },
        'dofilter': function()
        {
          NNLM.Rosters.DisplayManager.notify('showhelp',
          {
            help: this.help
          });
          var r = NNLM.Rosters.DataManager.STORES.positions.getAt(0);
          this.getForm().loadRecordNoEvents(r);
          NNLM.Rosters.DisplayManager.addActions(_menus[this.itemId]);
          this.rainEvent('dofilter');
          _dump('after', 3);
        },
        'dosave': function()
        {
          _dump(" dosave (position info)", 'event');
          _doclineLoad(this.PositionID);
        },
        'doreset': function()
        {
          var r = NNLM.Rosters.DataManager.STORES.positions.getAt(0);
          this.getForm().reset();
          this.rainEvent('doreset');
        }
      }
    },
    // Position Roles subpanel
    {
      itemId: 'positionRoles',
      xtype: 'editorgrid',
      title: 'Position Roles',
      cls: 'position_subpanel',
      bbar: [
      {
        xtype: 'tbtext',
        text: 'Assign new role: '
      },
      {
        xtype: 'selectbox',
        itemId: 'roleType',
        editable: false,
        fieldLabel: 'Choose a Role',
        allowBlank: false,
        width: 300,
        bodyStyle: 'margin: 5px',
        store: NNLM.Rosters.DataManager.STORES.role_types,
        mode: 'remote',
        valueField: 'RoleType',
        displayField: 'RoleType',
        emptyText: 'pull down to select a role',
        itemSelector: 'div.search-item',
        tpl: _templates.roleStoreTpl,
        listeners: {
          'select': {
            fn: function(combo, r, i)
            {
              var c = this;
              if (r.get('obsolete') === true)
              {
                c.obsolete = true;
                c.markInvalid('This role type is obsolete, and is displayed only for historical purposes.' + '  Lists are now used to represent this role type. This message (and the associated obsolete role) will disappear in the next version.');

              }
              else
              {
                c.obsolete = false;
                _functions.addrole();
              }
            },
            buffer: 100
          }
        }
        //</tpl></tpl>').compile()
      }],
      help: 'edit-positions-roles.html',
      columnWidth: 0.4,
      store: NNLM.Rosters.DataManager.STORES.roles,
      clicksToEdit: 2,
      deferEmptyText: false,
      viewConfig: {
        forceFit: true,
        emptyText: 'No roles yet assigned.',
        deferEmptyText: false
      },
      initEvents: function()
      {
        Ext.grid.EditorGridPanel.superclass.initEvents.call(this);
        this.relayEvents(this.store, ['destroy', 'save', 'update']);
      },
      sm: new Ext.grid.RowSelectionModel(
      {}),
      cm: new Ext.grid.ColumnModel(
      {
        columns: [
        {
          header: 'Role Type',
          dataIndex: 'RoleType',
          renderer: function(value, metadata, dataRecord, ri, ci, s)
          {
            var type_r = NNLM.Rosters.DataManager.getRecordById('role_types', value);
            if (type_r === undefined)
            {
              return '<p class="error">Not defined</p>';
            }
            var composite = Ext.apply(
            {}, dataRecord.data);
            composite = Ext.apply(composite, type_r.data);
            return _templates.roleStoreTpl.applyTemplate(composite);
          }
        },
        {
          header: '<p class="editable">Comments (including scope)</p>',
          dataIndex: 'Scope',
          editor: {
            xtype: 'textfield',
            allowBlank: true
          }
        },
        {
          dataIndex: 'RoleID',
          header: 'Controls',
          width: 35,
          editable: false,
          hideTrigger: true,
          renderer: function(value, metaData, dataRecord, rowIndex, colIndex, store)
          {
            return _templates.roleDeleteTpl.applyTemplate(dataRecord.data);
          }
        }],
        isCellEditable: function(col, row)
        {
          var dataRecord = NNLM.Rosters.DataManager.STORES.roles.getAt(row);
          if (dataRecord.get('readonly'))
          {
            // replace with your condition
            return false;
          }
          return Ext.grid.ColumnModel.prototype.isCellEditable.call(this, col, row);
        }
      }),
      listeners: {
        'celldblclick': function(grid, ri, ci, e)
        {
          if (!grid.getColumnModel().isCellEditable(ci, ri))
          {
            return;
          }
          grid.startEditing(ri, ci);
        },
        'dofilter': function()
        {
          NNLM.Rosters.DisplayManager.notify('showhelp',
          {
            help: this.help
          });
          NNLM.Rosters.DisplayManager.addActions(_menus[this.itemId]);
          _actions.addrole.enable();
          _actions.removerole.enable();
        },
        'doload': function(PositionID)
        {
          this.PositionID = PositionID;
        },
        'dosave': function()
        {
          NNLM.Rosters.DataManager.save('roles');
        }
      }
    },
    // Position address subpanel
    {
      xtype: 'form',
      layout: 'border',
      itemId: 'addressInfo',
      title: 'Position Address',
      trackResetOnLoad: true,
      cls: 'position_subpanel',
      help: 'edit-positions-address.html',
      defaults: {
        flex: 1,
        layout: 'form'
      },
      autoPersist: function(field)
      {
        if (NNLM.Rosters.DataManager.updateRecord('positions', this.PositionID, this.getForm()))
        {
          NNLM.Rosters.DisplayManager.notify("setvalid");
        }
      },
      items: [
      {
        xtype: 'fieldset',
        itemId: 'positionAddrView',
        layout: 'form',
        region: 'center',
        defaults: {
          xtype: 'panel',
          border: false
        },
        items: [
        {
          itemId: 'none'
        },
        {
          itemId: 'docline'
        },
        {
          itemId: 'positions'
        }],
        LIBID: record.data.LIBID,
        dtpl: _templates.dtpl,
        ptpl: _templates.ptpl,
        ntpl: _templates.noAddress,
        // startingMarup as a new property
        // add a method which updates the detail
        updateDoclineDetail: function(dataRecord)
        {
          if (!dataRecord)
          {
            return;
          }
          var body = this.chute('docline').body;
          if (record === undefined || !dataRecord.get('LIBID'))
          {
            _dump("NO docline record available.", 'error', 3);
            this.dtpl.overwrite(body,
            {
              LIBID: false
            });
            return;
          }
          this.dtpl.overwrite(body, dataRecord.data);
        },
        updatePositionDetail: function(dataRecord)
        {
          var body = this.chute('positions').body;
          if (dataRecord === undefined || !dataRecord.get('INST_INST'))
          {
            this.ptpl.overwrite(body,
            {
              INST_INST: false
            });
            return;
          }
          //_actions.clearoverride.enable();
          this.ptpl.overwrite(body, dataRecord.data);
        },
        listeners: {
          'doload': function(PositionID)
          {
            this.PositionID = PositionID;
            _doclineLoad(this.PositionID);
          },
          'dofilter': function()
          {
            var r = NNLM.Rosters.DataManager.STORES.positions.getAt(0);
            var libid = r.get('LIBID');
            var none = this.chute('none').body;
            _dump('dofilter libid: ' + libid, 3);
            this.ntpl.overwrite(none, record.data);
            if (libid !== '')
            {
              var docrecord = NNLM.Rosters.DataManager.getRecordById("docline", libid);
              if (!docrecord)
              {
                _a("The full DOCLINE record is still loading.  Please stand by...");
                NNLM.Rosters.DataManager.STORES.docline.addListener('load', function(store, data)
                {
                  this.fireEvent('dofilter');
                }, this,
                {
                  single: true
                });
              }
              else
              {
                this.updateDoclineDetail(docrecord);
                this.updatePositionDetail(r);
              }
            }
            else
            {
              this.updatePositionDetail(r);
            }
          },
          'dosave': function()
          {
            this.fireEvent('dofilter');
          },
          'doreset': function()
          {
            this.fireEvent('dofilter');
          }
        }
      },
      {
        // Docline Override subpanel
        xtype: 'fieldset',
        itemId: 'positionAddrForm',
        layout: 'form',
        region: 'east',
        width: 500,
        frame: true,
        collapsible: true,
        collapsed: true,
        collapseMode: 'mini',
        title: 'New position address',
        defaults: {
          xtype: 'textfield',
          width: '90%'
        },
        items: [
        {
          fieldLabel: 'Institution',
          xtype: 'textarea',
          itemId: 'Institution',
          emptyText: 'Note: you *must* provide an institution entry if you wish to override.',
          name: 'INST_INST',
          regex: /^[\-A-z0-9 _&]*$/
        },
        {
          fieldLabel: 'Department',
          xtype: 'textarea',
          name: 'DEPT_INST'
        },
        {
          fieldLabel: 'Street',
          xtype: 'textarea',
          name: 'STREET_INST'
        },
        {
          fieldLabel: 'City',
          name: 'CITY_INST'
        },
        {
          fieldLabel: 'State',
          name: 'STATE_ETC_CODE_INST'
        },
        {
          fieldLabel: 'Zip',
          name: 'ZIP_MAIL_CODE_INST'
        }]
      }],
      listeners: {
        'doload': function(PositionID)
        {
          this.PositionID = PositionID;
          //this.rainEvent('doload', PositionID);
        },
        'dofilter': function()
        {
          NNLM.Rosters.DisplayManager.notify('showhelp',
          {
            help: this.help
          });
          NNLM.Rosters.DisplayManager.addActions(_menus[this.itemId]);
          var r = NNLM.Rosters.DataManager.STORES.positions.getAt(0);
          if (r.get('INST_INST') !== null)
          { //TODO: flush this out to all fields!
            _actions.clearoverride.enable();
            _actions.autofill.enable();
          }
          else
          {
            _actions.clearoverride.disable();
            _actions.autofill.disable();
          }
          _dump('Loading position id ' + r.get('PositionID') + ' into position panel PositionInfo');
          this.getForm().loadRecordNoEvents(r);
          NNLM.Rosters.DisplayManager.notify('showhelp',
          {
            help: this.help
          });
          this.rainEvent('dofilter');
        },
        'dosave': function()
        {
          this.chute('positionAddrForm').collapse();
        },
        'doreset': function()
        {
          this.getForm().reset();
          this.fireEvent('dofilter');
          this.chute('positionAddrForm').collapse();
        }
      }
    }];
    var newpanel = {
      title: '[Default title]',
      xtype: 'panel',
      id: panelID,
      layout: 'hbox',
      layoutConfig: {
        align: 'stretch',
        pack: 'start'
      },
      help: 'edit-positions.html',
      closable: false,
      defaults: {
        frame: false,
        border: false
      },
      items: [
      {
        xtype: 'panel',
        layout: 'vbox',
        itemId: 'controls',
        flex: 2,
        bodyCssClass: 'MenuPanel',
        layoutConfig: {
          align: 'stretch',
          pack: 'start',
          defaultMargins: {
            top: 5,
            right: 0,
            bottom: 5,
            left: 15
          }
        },
        defaults: {
          xtype: 'button',
          flex: 1,
          bodyStyle: 'padding: 5px',
          frame: false,
          border: false
        },
        items: [
        {
          xtype: 'panel',
          bodyCssClass: 'MenuPanel'
        },
        {
          text: 'Position Information',
          cls: 'edit_position_info_b',
          handler: _setPositionPanel.createCallback(0)
        },
        {
          text: 'Position Roles',
          cls: 'edit_position_roles_b',
          handler: _setPositionPanel.createCallback(1)
        },
        {
          text: 'Position Address',
          cls: 'edit_position_address_b',
          handler: _setPositionPanel.createCallback(2)
        },
        //new Ext.Button(_actions.setprimary),
        {
          xtype: 'panel',
          bodyCssClass: 'MenuPanel'
        }],
        listeners: {
          'dofilter': function()
          {
            var number = this.ownerCt.chute('panels').getLayout().getActiveItemNum();
            this.items.each(function(item, i)
            {
              if (i === (number + 1))
              {
                item.toggle(true);
              }
              else if (item.xtype === 'button')
              {
                item.toggle(false);
              }
            });
          }
        }
      },
      {
        itemId: 'panels',
        xtype: 'panel',
        layout: 'card',
        header: false,
        flex: 10,
        activeItem: 0,
        frame: false,
        border: false,
        items: panels,
        listeners: {
          'enter': function()
          {
            _dump(' enter (Position panels)', 'event');
            this.getLayout().activeItem.fireEvent('enter');
          }
        }
      }],
      listeners: {
        'enter': function()
        {
          _dump(' enter (position subtab)', 'event');
          this.chute('panels').fireEvent('enter');
        },
        'check': function(checkbox, checked)
        {
          _dump(' check (Positions activeTab)', 'event');
          if (!checked)
          {
            return false;
          }
          MainPanel.fireEvent('valid', checkbox.findParentByType('form').getForm());
          return true;
        },
        'doload': function(PositionID)
        {
          _dump(' doload (positions panel).  PositionID: ' + PositionID, 'event');
          this.PositionID = PositionID;
          this.chute('panels').rainEvent('doload', this.PositionID);
        },
        'dofilter': function()
        {
          //filter roles store for only roles applicable to this position.
          _dump(' dofilter (Positions activeTab)', 'event');
          if (!isset(this.PositionID))
          {
            throw 'ERROR! Position ID not set, returning';
          }
          NNLM.Rosters.DataManager.STORES.positions.clearFilter();
          NNLM.Rosters.DataManager.STORES.positions.filter('PositionID', this.PositionID);
          NNLM.Rosters.DataManager.STORES.roles.clearFilter();
          NNLM.Rosters.DataManager.STORES.roles.filter('PositionID', this.PositionID);
          _dump('Filtered roles by Position ID, new count: ' + NNLM.Rosters.DataManager.STORES.roles.getCount(), 3);
          this.chute('controls').fireEvent('dofilter');
          this.chute('panels').getLayout().activeItem.fireEvent('dofilter');
        },
        'dosave': function()
        {
          this.chute('panels').getLayout().activeItem.fireEvent('dosave');
          this.chute('controls').fireEvent('dosave');
        },
        'doreset': function()
        {
          this.chute('panels').getLayout().activeItem.fireEvent('doreset');
          this.chute('controls').fireEvent('doreset');
        }
      }
    };
    var activePanel = MainPanel.chute('positions');
    newpanel = activePanel.add(newpanel);
    //_dump("New panel generated for position " + record.get("PositionID"), 'info');
    return newpanel;
  };
  /* lists
   *  panel for displaying information about lists related to this member.
   */
  var lists = {
    title: 'Lists',
    id: 'lists-' + uniqueId,
    itemId: 'lists',
    help: 'edit-lists.html',
    category: 'lists',
    layout: 'hbox',
    layoutConfig: {
      align: 'stretch',
      pack: 'start'
    },
    bodyCssClass: 'rostersMenuPanel',
    defaults: {
      frame: false,
      border: false
    },
    items: [
    {
      xtype: 'panel',
      layout: 'vbox',
      itemId: 'controls',
      flex: 3,
      bodyCssClass: 'MenuPanel',
      layoutConfig: {
        align: 'stretch',
        pack: 'start',
        defaultMargins: {
          top: 5,
          right: 0,
          bottom: 5,
          left: 15
        }
      },
      defaults: {
        xtype: 'button',
        flex: 1
      },
      items: [
      {
        xtype: 'panel',
        frame: false,
        border: false,
        bodyCssClass: 'MenuPanel'
      },
      /*{
                text: 'My Locker',
        itemId: 'mylocker',
                handler: _setListsPanel.createCallback(0)
            },*/
      {
        text: 'Lists I belong to',
        itemId: 'listsbelong',
        handler: _setListsPanel.createCallback(0)
      },
      {
        xtype: 'panel',
        flex: 3,
        frame: false,
        border: false,
        bodyCssClass: 'MenuPanel'
      }],
      listeners: {
        'doload': function(PeopleID)
        {
          var r = NNLM.Rosters.DataManager.getRecordById("people", PeopleID);
          this.PeopleID = PeopleID;
          /*
          var lb = this.chute('mylocker');
          lb.setText(r.get('FirstName') + ' ' + r.get('LastName') + "'s locker");
          if(NNLM.Rosters.DataManager.getCurrentUser().get('PeopleID') !== PeopleID){
            lb.setText(lb.getText() + ' (private)');
            lb.setDisabled(true);
          }
          */
          this.chute('listsbelong').setText('Lists ' + r.get('FirstName') + ' ' + r.get('LastName') + " belongs to");
        },
        'dofilter': function()
        {
          var number = this.ownerCt.chute('panels').getLayout().getActiveItemNum();
          this.items.each(function(item, i)
          {
            if (i === (number + 1))
            {
              item.toggle(true);
            }
            else if (item.xtype === 'button')
            {
              item.toggle(false);
            }
          });
        }
      }
    },
    {
      itemId: 'panels',
      xtype: 'panel',
      layout: 'card',
      flex: 10,
      activeItem: 0,
      defaults: {
        viewConfig: {
          forceFit: true
        },
        scroll: 'auto',
        border: false
      },
      items: [
      /*{
              title: 'Default',
        iconCls: 'iconed folder_user',
              xtype: 'grid',
              itemId: 'watchingGrid',
        cls: 'position_subpanel',
              store: _stores.lockerStore,
        bbar: [_actions.editlocker],
              emptyText: 'You are not watching lists.',
        height: 200,
              sm: new Ext.grid.RowSelectionModel(),
              cm: new Ext.grid.ColumnModel({
                  columns: [
                  {
                      dataIndex: 'ListEntryID',
                      renderer: function(value, metaData, record, rowIndex, colIndex, store) {
                          switch (record.get('ListMemberTable')) {
                          case 'people':
                              return "<p class='iconed user'>" + record.get('DescriptiveText') + '</p>';
                              break;
                          case 'list_descriptions':
                              return "<p class='iconed script'>" + record.get('DescriptiveText') + '</p>';
                              break;
                          default:
                              return "<p class='error'>Undefined list member table!</p>";
                          }
                          return '';
                          //should never reach.
                      }
                  }
                  ]
              }),
              listeners: {
                  'doload': function(PeopleID) {
                      var r = NNLM.Rosters.DataManager.getRecordById("people", PeopleID);
            this.PeopleID = PeopleID;
                      this.ListID = r.get('MyListID');
                      this.store.PeopleID = PeopleID;
            this.setTitle(r.get('FirstName') + ' ' + r.get('LastName') + "'s locker");
                  },
                  'dofilter': function() {
                this.store.load({add:false});
                  },
                  'rowdblclick': function(grid, i, e) {
                      var r = this.store.getAt(i);
                      switch (r.get('ListMemberTable')) {
                      case 'people':
                          NNLM.Rosters.DisplayManager.notify('edit', {
                              PeopleID: r.get('ListMemberTableID')
                          });
                          break;
                      case 'list_descriptions':
                          NNLM.Rosters.DisplayManager.notify('list', {
                              ListID: r.get('ListMemberTableID')
                          });
                          break;
                      default:
                      }
                  }
              }
          },*/
      {
        title: 'Default',
        xtype: 'grid',
        itemId: 'belongGrid',
        cls: 'position_subpanel',
        store: _stores.listBelongStore,
        emptyText: 'You do not yet belong to any lists.',
        sm: new Ext.grid.RowSelectionModel(),
        cm: new Ext.grid.ColumnModel(
        {
          columns: [
          {
            dataIndex: 'ListEntryID',
            renderer: function(value, metaData, record, rowIndex, colIndex, store)
            {
              return "<div class='list'><p class='title'>" + record.get('ListTitle') + "</p><p class='metadata'>" + record.get('ListDescription') + '</p></div>';
            }
          }]
        }),
        listeners: {
          'rowdblclick': function(grid, i, e)
          {
            var r = this.store.getAt(i);
            NNLM.Rosters.DisplayManager.notify('list',
            {
              ListID: r.get('ListID')
            });
          },
          'doload': function(PeopleID)
          {
            this.PeopleID = PeopleID;
            this.store.PeopleID = PeopleID;
            var r = NNLM.Rosters.DataManager.getRecordById("people", PeopleID);
            this.setTitle('Lists ' + r.get('FirstName') + ' ' + r.get('LastName') + " belongs to");
          },
          'dofilter': function()
          {
            NNLM.Rosters.DisplayManager.notify('wait', true);
            this.store.load(
            {
              add: false,
              text: "Updating display to most current data...",
              callback: function()
              {
                NNLM.Rosters.DisplayManager.notify('wait', false);
              }
            });
          }
        }
      }],
      listeners: {
        'doload': function(PeopleID)
        {
          this.PeopleID = PeopleID;
          this.rainEvent('doload', PeopleID);
        },
        'enter': function()
        {
          _dump(' enter (Position panels)', 'event');
          this.getLayout().activeItem.fireEvent('enter');
        }
      }
    }],
    listeners: {
      //Note to self: this panel uses a local store, and will be
      // out of sync with any operations done using the lists interface.
      //need to add a tabchange listener to reload the data from the
      //server at some point.
      'valid': function(field)
      {
        field.findParentByType('grid').store.filter(
        {
          property: 'ListDescription',
          value: field.getValue(),
          anyMatch: true,
          //optional, defaults to true
          caseSensitive: false
          //optional, defaults to true
        });
      },
      'doload': function(PeopleID)
      {
        this.PeopleID = PeopleID;
        this.rainEvent('doload', PeopleID);
      },
      'dofilter': function()
      {
        //filter roles store for only roles applicable to this position.
        _dump(' dofilter (lists)', 'event');
        if (!isset(this.PeopleID))
        {
          throw 'ERROR! PeopleID not set, returning';
        }
        NNLM.Rosters.DisplayManager.clearActions();
        NNLM.Rosters.DisplayManager.addActions(_menus[this.itemId]);
        _actions.editselectedlist.enable();
        if (NNLM.Rosters.DataManager.getCurrentUser().get('PeopleID') === this.PeopleID)
        {
          //_actions.editlocker.enable();
        }
        else
        {
          //_actions.editlocker.disable();
        }
        this.getComponent('controls').fireEvent('dofilter');
        this.getComponent('panels').getLayout().activeItem.fireEvent('dofilter');
      },
      'dosave': function()
      {
        this.chute('panels').getLayout().activeItem.fireEvent('dosave');
      },
      'doreset': function()
      {
        this.chute('panels').getLayout().activeItem.fireEvent('doreset');
      }
    }
  };

  /** MainPanel
   * The 'public' interface for the edit component, this object is what is
   * actually returned from the createInstance invocation.  It contains the
   * object that is intended to be directly inserted into the interface, and
   * houses all the others.
   */
  var MainPanel = new Ext.TabPanel(
  {
    id: 'MainPanel-' + uniqueId,
    iconCls: 'iconed user',
    bodyCssClass: 'EditPanel',
    frame: false,
    border: false,
    closable: true,
    title: '...',
    type: 'edit',
    PeopleID: undefined,
    defaults: {
      frame: false
    },
    anchor: '0 100%',
    activeItem: 0,
    items: [overview, overview_i, info, positions, accounts, lists],
    plugins: new Ext.ux.TabCloseMenu(),
    listeners: {
      // all data change events come in from subpanel event relaying.
      //passing form directly instead of regular valid event arg signature.
      //this allows calling the form
      'enter': function(component)
      {
        _dump(' enter (Edit Mainpanel)', 'event');
        this.getActiveTab().fireEvent('enter', [component]);
      },
      'tab': function(component)
      {
        _dump(' tab (Edit Mainpanel)', 'event');
        this.getActiveTab().fireEvent('tab', [component]);
      },
      'tabchange': {
        fn: function(tabPanel, panel)
        {
          _dump(' tabchange (MainPanel) (buffered)', 'event');
          if (!MainPanel.loaded)
          {
            return;
          }
          //do viewchange instead of binding directly to tabchange
          //gives me more granular control as to when this message
          //is passed up to the Display Manager.
          panel = (panel === undefined) ? this.getActiveTab() : panel;
          NNLM.Rosters.DisplayManager.notify('viewchange');
          this.fireEvent('dofilter');
        },
        buffer: 100
      },
      'afterload': function()
      {
        //this.fireEvent('dofilter');
      },
      'aftersave': function(success)
      {
        _dump(" aftersave (MainPanel)", 'event');
        var r = NNLM.Rosters.DataManager.getRecordById("people", this.PeopleID);
        NNLM.Rosters.DisplayManager.msg("Changes saved for '" + r.get('FirstName') + ' ' + r.get('LastName') + "'.");
        this.fireEvent('dofilter');
      },
      'afterreset': function()
      {
        _dump('edit afterreset', 'info');
        this.rainEvent('afterreset');
      },
      'doload': function(PeopleID)
      {
        _dump(' doload (MainPanel).  PeopleID: ' + PeopleID, 'event');
        MainPanel.PeopleID = PeopleID;
        this.rainEvent('doload', PeopleID);
        //_dump('Mainpanel loaded.', 'info');
        this.loaded = true;
      },
      'dofilter': {
        fn: function()
        {
          NNLM.Rosters.DisplayManager.clearActions();
          _dump(' dofilter (MainPanel) (buffered).  PeopleID: ' + MainPanel.PeopleID, 'event');
          if (!MainPanel.loaded || !MainPanel.rendered)
          {
            _dump('dofilter: Mainpanel not loaded.  Returning', 'error');
            return;
          }
          if (this.PeopleID === undefined)
          {
            return;
          }
          NNLM.Rosters.DataManager.filterBy(MainPanel.PeopleID);
          var r = NNLM.Rosters.DataManager.getRecordById("people", this.PeopleID);
          var data = new Ext.util.MixedCollection();
          data.addAll(r.data);
          //tbtitle.tpl.overwrite(tbtitle.el, data.map);
          MainPanel.setTitle(NNLM.Rosters.DisplayManager.ellipsis(
          r.get('LastName') + ', ' + r.get('FirstName')));
          NNLM.Rosters.DisplayManager.notify('showhelp',
          {
            help: this.getActiveTab().help
          });
          this.getActiveTab().fireEvent('dofilter');
        },
        buffer: 200
      },
      'dosave': function(b, e)
      {
        _dump(' dosave (MainPanel)', 'event');
        if (!MainPanel.loaded)
        {
          _dump('dosave: Mainpanel not loaded.  Returning', 'error');
          return;
        }
        this.getActiveTab().fireEvent('dosave', this, b, e);
      },
      'doreset': function()
      {
        _dump(' doreset (MainPanel)', 'event');
        if (!MainPanel.loaded)
        {
          _dump('doreset: Mainpanel not loaded.  Returning', 'error');
          return;
        }
        this.getActiveTab().fireEvent('doreset');
      },
      'relay': function(o)
      {
        if (!MainPanel.loaded)
        {
          _dump('relay: Mainpanel not loaded.  Returning', 'error');
          return;
        }
        if (_functions[o.target] === undefined)
        {
          throw 'Relay called a non-existing target';
        }
        _functions[o.target](o.args);
      }
    },
    /** isPrimaryPosition
     * convenience function to check a passed positionID against the person's
     * established primary position id.
     * @param PositionID {mixed} A given position id.  May be int or string
     * representation.
     * @return {boolean} true if it is their primary position, false otherwise.
     */
    isPrimaryPosition: function(PositionID)
    {
      return parseInt(NNLM.Rosters.DataManager.getRecordById("people", MainPanel.PeopleID).get('PrimaryPosition'), 10) === parseInt(PositionID, 10);
    },
    load: function(params)
    {
      if (!isset(params.PeopleID))
      {
        _e('PeopleID argument was not passed properly.');
        return;
      }
      _dump('Executing Edit load() operation.', 2);
      var r = NNLM.Rosters.DataManager.getRecordById("people", params.PeopleID);
      //alert("PeopleID: " + params.PeopleID + ", r: " + r);
      //change tab title to match the person's name.
      this.fireEvent('doload', params.PeopleID.toString());
      NNLM.Rosters.DisplayManager.msg('Displaying record for ' + r.get('FirstName') + ' ' + r.get('LastName'));
    }
  });
  return MainPanel;
};
NNLM.Rosters.dump('Editing system loaded.');
NNLM.Rosters.Edit.loaded = true;
