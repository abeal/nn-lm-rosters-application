/**
 *   Rosters application component file
 *
 *   This file contains a series of custom components for use
 *   with the NN/LM Rosters application.  It also contains any
 *   extensions and overrides done to the Ext base code for
 *   purposes of the rosters application.
 *
 *   @author     Aron Beal
 *   @license All files in this web application are  licensed under the
 *   terms of the Open Source LGPL 3.0 license.
 *   License details: http://www.gnu.org/licenses/lgpl.html.
 */

/* Any and all Ext basic initializations overrides done here. */
/** START general overrides */
/*Array.prototype.clone = function() {
    return this.slice(0);
};*/
/** END general overrides */
/* coopt ext example code for easy dialog boxes. */
Ext.example = function() {
    var msgCt;
    var pending = 0;

    function createBox(t, s, c) {
        return ['<div class="bubblemsg">',
            '<div class="x-box-tl"><div class="x-box-tr"><div class="x-box-tc"></div></div></div>',
            '<div class="x-box-ml"><div class="x-box-mr"><div class="x-box-mc"><h3 class="', c, ' iconed computer">', t, '</h3><span class="', c, '">', s, '</span></div></div></div>',
            '<div class="x-box-bl"><div class="x-box-br"><div class="x-box-bc"></div></div></div>',
            '</div>'
        ].join('');
    }

    function createCloseBox(t, s, c) {
        return ['<div class="bubblemsg">',
            '<div class="x-box-tl"><div class="x-box-tr"><div class="x-box-tc"></div></div></div>',
            '<div class="x-box-ml"><div class="x-box-mr"><div class="x-box-mc"><img src="/rosters/images/icons/close_window.gif" alt="click to close" title="click to close" style="float: right" /><h3 class="', c, ' iconed computer">', t, '</h3><span class="', c, '">', s, '</span></div></div></div>',
            '<div class="x-box-bl"><div class="x-box-br"><div class="x-box-bc"></div></div></div>',
            '</div>'
        ].join('');
    }
    return {
        msg: function(format) {
            var arr = Array.prototype.slice.call(arguments, 0);
            var o = {
                cls: 'msg',
                text: ''
            };
            if (typeof arr[0] === 'string') {
                o.text = arr.pop();
            } else {
                var p = arr.pop();
                for (var k in p) {
                    if (p.hasOwnProperty(k)) {
                        o[k] = p[k];
                    }
                }
            }
            if (!isset(o.text)) {
                throw 'Undefined text';
            }
            if (!isset(o.title)) {
                o.title = (o.cls === 'error') ? 'Rosters error message' : 'Rosters status message';
            }
            if (o.delay === undefined || o.delay === 'auto') {
                var wc = o.text.split(' ').length;
                o.delay = Math.ceil(wc / 2.33);
            }
            if (o.cls === 'error') {
                o.delay = 0;
            }
            if (!msgCt) {
                msgCt = Ext.DomHelper.append(document.body, {
                    id: 'msg-div'
                }, true);
            }
            var body = document.getElementById('rosters_mainbody');
            if (!isset(body)) {
                return null;
            }
            msgCt.alignTo(body, 'b-b', [0, -180]);

            var m = (o.delay !== 0) ? Ext.DomHelper.append(msgCt, {
                html: createBox(o.title, '<p style="margin-top: 0; padding-top: 0;">' + o.text + '</p>', o.cls)
            }, true) : Ext.DomHelper.append(msgCt, {
                html: createCloseBox(o.title, '<p style="margin-top: 0; padding-top: 0;">' + o.text + '</p>', o.cls)
            }, true);
            Ext.example.toggleMessageConsole(true);
            if (!Ext.isNumber(o.delay)) {
                throw "Invalid delay argument to messaging system";
            }
            m.finish = function() {
                this.stopFx();
                if (!Ext.isIE) {
                    this.purgeAllListeners();
                }
                this.ghost("b", {
                    remove: true,
                    callback: Ext.example.toggleMessageConsole.createCallback(false)
                });
            };
            m.on('click', m.finish);
            switch (o.delay) {
                case 0:
                    m.fadeIn('b');
                    break;
                default:
                    m.fadeIn('b').pause(o.delay).ghost("b", {
                        remove: true,
                        callback: Ext.example.toggleMessageConsole.createCallback(false)
                    });
                    break;
            }
            return m;
        },
        toggleMessageConsole: function(bool) {
            if (!msgCt) {
                msgCt = Ext.DomHelper.append(document.body, {
                    id: 'msg-div'
                }, true);
            }
            //NNLM.Rosters.dump("toggle message panel called", 'warn');
            bool = (!isset(bool)) ? false : bool;
            pending = (bool) ? (pending + 1) : (pending - 1);
            if (pending > 0) {
                msgCt.setStyle('z-index', 20000);
            } else {
                msgCt.setStyle('z-index', -1);
            }
        }
    };
}();
/** START Ext overrides */
Ext.XTemplate.override({
    exists: function(o) {
        return o !== null && (o.toString() !== 'undefined') && o.toString() !== '';
    },
    getPic: function(pic) {
        return (!isset(pic) || pic === '') ? 'http://nnlm.gov/icons/NNLMBlueLogo140.gif' : pic;
    }
});
Ext.Component.override({
    findParentByItemId: function(id) {
        var candidate = this.ownerCt;
        while (candidate !== undefined) {
            if (candidate.itemId === id) {
                return candidate;
            }
            candidate = candidate.ownerCt;
        }
        return null;
    },
    getParentById: function(itemId) {
        return this.findParentByItemId(itemId);
    },
    getChildById: function(itemId) {
        return (this.itemId === itemId) ? this : null;
    },
    ladder: function(itemId) {
        return this.findParentByItemId(itemId);
    },
    chute: function(itemId) {
        return this.getChildById(itemId);
    },
    /* blink
	blinks the element text for the specified duration.
	@param duration {int} the time to blink the text (seconds)
	@param speed {int} the speed (blinks/sec) to blink
	*/
    blink: function(duration, speed) {
        var me = this.getEl();
        var ratio = (1 / speed);
        //NNLM.Rosters.dump("ratio: " + ratio, 'info');
        me.stopFx();
        var ival = window.setInterval(function() {
            if (me.isVisible()) {
                me.hide(false);
            } else {
                me.show();
            }
        }, ratio * 500);
        window.setTimeout(function() {
            me.stopFx();
            window.clearInterval(ival);
            me.show();
        }, duration * 1000);
    }
});
Ext.Container.override({
    getParentById: function(itemId) {
        var result = null;
        if (this.itemId === itemId || this.id === itemId) {
            return this;
        }
        if (this.ownerCt === null || this.ownerCt === undefined || this.ownerCt.getParentById === undefined) {
            return null;
        }
        return this.ownerCt.getParentById(itemId);
    },
    getChildById: function(itemId) {
        var result = null;
        var doCascade = true;
        this.cascade(function() {
            if (!doCascade) {
                return false;
            }
            if (this.itemId === itemId || this.id === itemId) {
                doCascade = false;
                result = this;
                return false;
            }
            return true;
        });
        return result;
        /*
		var result = null;
		if(this.itemId === itemId || this.id === itemId){
			return this;
		}
		if(this.items.length === 0){
			return null;
		}
		for(var i = 0; i < this.items.length; i++){
			NNLM.dump('Checking id: ' + this.get(i).id);
			if(this.get(i).getChildById !== undefined){
				result = this.get(i).getChildById(itemId);
			}
			if(result !== null){
				return result;
			}
		}
		return null;*/
    },
    getChildById_ci: function(itemId) {
        itemId = itemId.toLowerCase();
        var result = null;
        var doCascade = true;
        this.cascade(function() {
            if (!doCascade) {
                return false;
            }
            //NNLM.dump("comparison: " + itemId + ", id: " + this.id + " itemId: " + this.itemId);
            if (this.itemId && this.itemId.toLowerCase() === itemId || this.id.toLowerCase() === itemId) {
                doCascade = false;
                result = this;
                return false;
            }
            return true;
        });
        return result;
    },
    ladder: function(itemId) {
        return this.getParentById(itemId);
    },
    chute: function(itemId) {
        return this.getChildById(itemId);
    },
    chutei: function(itemId) {
        return this.getChildById_ci(itemId);
    },
    rainEvent: function(s_eventName, o_args) {
        this.items.each(function() {
            this.fireEvent(s_eventName, o_args);
        });
    }
});
Ext.layout.CardLayout.override({
    setActiveItemId: function(itemId) {
        var candidates = this.activeItem.ownerCt.items.items;
        for (var i = 0; i < candidates.length; i++) {
            if (candidates[i].itemId === itemId) {
                this.setActiveItem(i);
            }
        }
    },
    slideTransition: function(itemId, slideOutEdge, slideInEdge) {
        this.setActiveItem(this.container.getComponent(itemId));
    },
    next: function() {
        var cardOwner = this.activeItem.ownerCt;
        for (i = 0; i < cardOwner.items.length; i++) {
            if (cardOwner.get(i).id === this.activeItem.id) {
                var n = (i + 1) % cardOwner.items.length;
                this.slideTransition(n, 'l');
                return;
            }
        }
    },
    prev: function() {
        var cardOwner = this.activeItem.ownerCt;
        for (i = 0; i < cardOwner.items.length; i++) {
            if (cardOwner.get(i).id === this.activeItem.id) {
                var p = (i - 1) % cardOwner.items.length;
                if (p < 0) {
                    p = cardOwner.items.length - 1;
                }
                this.slideTransition(p, 'r');
                return;
            }
        }
    }
});
Ext.TabPanel.override({
    listeners: {
        'beforetabchange': function(tabPanel, newtab, currenttab) {
            if (!this.rendered) {
                throw 'not rendered!';
            }
            if (!NNLM.Rosters.DisplayManager.isViewChangeOk()) {
                return false;
            }
            return true;
        }
    }
});
Ext.Panel.override({
    getButton: function(buttonName) {
        if (!this.rendered) {
            return false;
        }
        var button = false;
        var tbar = this.getTopToolbar();
        var bbar = this.getBottomToolbar();
        if (!button && isset(this.buttons)) {
            button = this.buttons.getComponent(buttonName);
        }
        if (!button && isset(tbar)) {
            button = tbar.getComponent(buttonName);
        }
        if (!button && isset(bbar)) {
            button = bbar.getComponent(buttonName);
        }
        if (!button) {
            return false;
        }
        return button;
    },
    toggleButton: function(button, bool_state) {
        button = (Ext.isString(button)) ? this.getButton(button) : button;
        if (!button) {
            NNLM.Rosters.dump('button not found in panel ' +
                this.title, 'error');
            return false;
        }
        button.setEnabled(bool_state);
        return true;
    },
    //stub implementations to be overridden by panels who care.
    doDelayedTask: function(fn, delay) {
        if (this.altask === undefined) {
            this.altask = new Ext.util.DelayedTask(function() {
                fn();
                delete this.altask;
            });
            this.altask.delay(delay_amt);
        } else {
            this.altask.delay(delay_amt);
        }
    },
    findSiblingById: function(id) {
        return this.ownerCt.chute(id);
    },
    getExpandedSubPanel: function() {
        for (var i = 0; i < this.items.length; i++) {
            if (this.getComponent(i).collapsed === false) {
                return this.getComponent(i);
            }
        }
        return null;
    },
    listeners: {
        'afterrender': function() {
            this.addEvents('doload', 'dosave', 'doreset', 'dofilter', 'change');
        }
    },
    toggle: function() {
        if (this.collapsed) {
            this.expand();
            return true;
        }
        this.collapse();
        return false;
    }
});
Ext.grid.GridPanel.override({
    getSelectedRecordId: function() {
        var sel = this.getSelectionModel().getSelected();
        return sel ? sel.get('ListID') : -1;
    }
});
Ext.Button.override({
    setEnabled: function(bool) {
        if (this.disabled !== bool) {
            return;
        }
        /*if(bool === true){
            this.getEl().frame("FF0000", 1, {
                duration: 1
            });
        }*/
        this.setDisabled(!bool);
    },
    toggleIconCls: function() {
        var tmp = '';
        if (this.iconCls_a !== undefined) {
            tmp = this.iconCls;
            this.setIconClass(this.iconCls_a);
            this.iconCls_a = tmp;
        }
    },
    getOwner: function() {
        if (this.ownerCt !== undefined && this.ownerCt.xtype === 'toolbar') {
            return this.ownerCt.getOwner();
        }
        return this.findParentByType('panel');
    },
    getMenuSubButton: function(itemId) {
        if (this.menu === undefined) {
            return null;
        }
        return this.menu.chute(itemId);
    },
    toggleMenuOption: function(buttonName, bool_state) {
        var b = this.chute(buttonName);
        if (b === null) {
            throw 'menu button ' + buttonName + ' not found for menu button ' + this.itemId;
        }
        b.setEnabled(bool_state);
    }
});
Ext.data.Store.override({
    myremoved: 0,
    empty: function(silent) {
        this.removeAll(silent);
    },
    exists: function(o) {
        var mc = this.queryBy(function(r, id) {
            for (var k in o) {
                if (o.hasOwnProperty(k)) {
                    if (r.get(k) === undefined) {
                        throw ('invalid key ' + k + ' requested');
                    }
                    if (r.get(k) !== o[k]) {
                        return false;
                    }
                }
            }
            return true;
        });
        return (mc.getCount() > 0);
    },
    getRemovedCount: function() {
        return this.removed;
    },
    getLast: function() {
        return this.getAt(this.getCount() - 1);
    },
    isDirty: function() {
        var dirty = false;
        if (this.myremoved > 0) {
            NNLM.Rosters.dump("Store: records have been removed.  Dirty.", "warn");
            return true;
        }
        if (this.modified && this.modified.length > 0) {
            NNLM.Rosters.dump("Store: modified property is present.  Dirty.", "warn");
            return true;
        }
        Ext.each(this.data, function() {
            if (dirty) {
                return;
            }
            dirty = dirty || Boolean(this.phantom);
            if (dirty) {
                NNLM.Rosters.dump("Store: Phantom record found.  Dirty.", "warn");
            }
            dirty = dirty || Boolean(this.dirty);
            if (dirty) {
                NNLM.Rosters.dump("Store: Dirty record found.  Dirty.", "warn");
            }
        });
        if (!dirty) {
            NNLM.Rosters.dump("Store: no modified property, dirty, or phantom records found.  Clean.", "info");
        }
        return dirty;
    },
    purgePhantomRecords: function() {
        var is = [];
        //NNLM.Rosters.dump("purging phantom records", 'info');
        for (var i = 0; i < this.getCount(); i++) {
            is[i] = (this.getAt(i).phantom === true);
        }
        for (i = this.getCount() - 1; i >= 0; i--) {
            if (is[i] === true) {
                //_dump("purging record with value " + this.getAt(i).get('AccountValue'));
                this.removeAt(i);
            }
        }
    },
    removeBy: function(property, value) {
        _dump("Removing all items with " + property + " value of " + value, 3);
        var data = this.snapshot || this.data;
        var mc = data.filterBy(function(item) {
            if (item.get(property) === value) {
                is.push(i);
            }
            return true;
        }, scope || this);
        mc.each(function(record) {
            data.remove(record);
        });
    },
    isModified: function(r) {
        return (this.modified.indexOf(r) === -1);
    }
});
Ext.Toolbar.override({
    empty: function() {
        for (var i = 0; i < this.items.length; i++) {
            this.remove(this.get(0));
        }
    },
    toggleButton: function(buttonName, bool_state) {
        var b = this.chute(buttonName);
        if (b !== undefined && b !== null) {
            b.setEnabled(bool_state);
        }
    },
    toggleAllButtons: function(bool_state) {
        this.items.each(function() {
            this.setDisabled(!bool_state);
        });
    },
    toggleAllMenuOptions: function(menuId, bool_state) {
        var menuButton = this.chutei(menuId);
        if (menuButton === undefined || menuButton === null) {
            NNLM.dump('menu id ' + menuId + ' is null');
            return;
        }
        if (menuButton.menu === undefined || menuButton.menu === null) {
            NNLM.dump('menu id ' + menuId + ' has no defined menu');
            return;
        }
        menuButton.menu.items.each(function() {
            this.setDisabled(!bool_state);
        });
    },
    toggleMenuOption: function(menuId, buttonName, bool_state) {
        var menuButton = this.chutei(menuId);
        if (menuButton === null) {
            NNLM.dump('menu id ' + menuId + ' is null');
            return;
        }
        if (menuButton.menu === null) {
            NNLM.dump('menu id ' + menuId + ' has no defined menu');
            return;
        }
        var b = menuButton.menu.chutei(buttonName);
        if (b === null) {
            NNLM.dump("can't find the button " + buttonName + ' under the menu ' + menuId);
            return;
        }
        b.setDisabled(!bool_state);
    },
    getOwner: function() {
        result = this.ownerCt.ownerCt;
        if (result === null || result === undefined) {
            //not guaranteed to be a good result here.
            result = this.findParentByType('panel');
        }
        return result;
    }
});
Ext.form.RadioGroup.override({
    initEvents: function() {
        Ext.form.RadioGroup.superclass.initEvents.call(this);
        //overriding this upon init seems to be required for this build (3.2.2)
        this.originalValue = null;
    },
    isDirty: function() {
        if (this.disabled || !this.rendered) {
            return false;
        }
        var dirty = String(this.getValue()) !== String(this.originalValue);
        /* NNLM.Rosters.dump("Dirty test for field " + this.name + ": " + dirty +
                          ', NEW: ' + this.getValue() + ', OLD: ' +
                          this.originalValue);*/
        return dirty;
    },
    getValue: function() {
        var out = null;
        this.eachItem(function(item) {
            if (item.checked) {
                out = item;
            }
            return (!item.checked);
        });
        return (out === null) ? out : out.inputValue;
    },
    setValueForItem: function(val) {
        val = String(val).split(',')[0];
        var me = this;
        this.eachItem(function(item) {
            if (item.checked) {
                me.originalValue = item.inputValue;
            }
            item.setValue(val === item.inputValue);
        });
    }
});
Ext.form.FormPanel.override({
    /* I have two types of forms here - those that I just need to use to offer fields
	to the user so they may create something themselves, and those that should automatically
	persist changes to a corresponding record and ignite the save button.  This setting is
	meant to differentiate between the two. Implementation will be a function that
	handles persisting all fields in this form to the record.*/
    autoPersist: null,
    /**
     *  setEventForwarding OBSOLETE
     *  iterates through a form's elements, and sends events appropriate to the
     *  form element type to the panel designated by targetId.
     *  @param form {Ext.form.FormPanel} the formpanel containing the form elements.
     *  @param targetId {String} the id of the target panel to forward forms to.
     *  the form panel provided must be contained within the target panel.
     *  @param arguments[2] {array}{ optional } an optional third argument
     *  that takes a list of string event names, and does the binding
     *  only with those events, instead of the normal set of events
     *  to be bound to a form.
     *  @return {void}
     **/
    /*setEventForwarding: function(targetId) {
		return;
        var target = null;
        if (typeof targetId === undefined) {
            target = this;
        }
        else if (typeof targetId === 'object') {
            target = targetId;
        }
        else if (this.id === targetId) {
            target = this;
        }
        else {
            this.findParentBy(function(container, thisComponent) {
                if (container.id === targetId) {
                    target = container;
                }
            });
        }
        if (target === null) {
            throw 'SEF: target not found! Critical application failure!';
        }
        //second argument is array of events to be forwarded.  Used for
        //non-form component specific event relaying.
        if (arguments.length > 1) {
            var evtName = '';
            for (var i = 0; i < arguments[1].length; i++) {
                target.relayEvents(component, [arguments[1][i]]);
            }
            return;
        }
        if (this.items.length === 0) {
            return;
        }
        this.items.each(function() {
            //'this' refers to a form component, if we've reached this point.
            switch (this.xtype) {
            case 'displayfield':
                break;
            case 'combo':
            case 'selectbox':
                //_dump('SEF: binding form event "select" to ' +
                //targetId + ' from combobox "' + this.name + '"');
                target.relayEvents(this, ['select', 'blur', 'valid', 'invalid']);
                break;
            case 'textfield':
            case 'textarea':
            case 'datefield':
                //_dump('SEF: binding form event "valid" to ' +
                //targetId + ' from ' + this.xtype + ' "' + this.name + '"');
                target.relayEvents(this, ['valid', 'change', 'invalid']);
                break;
            case 'checkboxgroup':
            case 'radiogroup':
                target.relayEvents(this, ['change']);
                break;
            default:
                throw 'SEF: unrecognized form item "' + this.xtype +
                '", no binding.  This: ' + this;
            }
        });
    },*/
    isDirty: function() {
        var dirty = false;
        this.suspendEvents(false);
        dirty = this.getForm().isDirty();
        this.resumeEvents();
        return dirty;
    },
    isValid: function() {
        var valid = false;
        this.suspendEvents(false);
        this.getForm().suspendEvents(false);
        valid = this.getForm().isValid();
        this.getForm().resumeEvents();
        this.resumeEvents();
        return valid;
    },
    bindRecord: function(store, id) {
        this.boundRecord = store.getById(id);
        if (this.boundRecord === null) {
            throw "Record could not be bound.";
        }
        return this.boundRecord;
    },
    getBoundRecord: function() {
        if (this.boundRecord === undefined) {
            return null;
        }
        return this.boundRecord;
    },
    listeners: {
        'valid': {
            fn: function(field) {
                NNLM.Rosters.dump("valid (formpanel generic)", 'event');
            },
            buffer: 200
        },
        'change': function(field) {
            NNLM.Rosters.dump("change (formpanel generic)", 'event');
            if (this.autoPersist === null) {
                return;
            }
            if (this.isValid()) {
                NNLM.Rosters.dump("autopersisting.", 3);
                this.autoPersist(field);
            }
            if (this.isDirty()) {
                NNLM.Rosters.DisplayManager.notify('isdirty', true);
            }
        },
        'afterrender': function() {
            this.addEvents(['select', 'change', 'check', 'blur', 'valid', 'invalid', 'dirty']);
        },
        'focus': function(f) {
            NNLM.Rosters.dump('focus.  Setting focused item to be : ' + f.id, 'event');
            this.focusedItem = f.id;
        }
    },
    isLastFieldFocused: function() {
        return (this.items.last().id === this.focusedItem);
    },
    prev: function() {
        if (this.items.getCount() < 2 || this.items.first().id === this.focusedItem) {
            return;
        }
        this.items.each(function(o, j) {
                if (o.id === this.focusedItem) {
                    this.items.itemAt(j - 1).focus();
                }
            },
            this);
    },
    next: function() {
        if (this.items.getCount() < 2 || this.items.last().id === this.focusedItem) {
            return;
        }
        for (i = 0; i < this.items.length; i++) {
            var item = this.items.get(i);
            if (item.id === this.focusedItem) {
                item = this.items.itemAt(i + 1);
                item.focus();
                return;
            }
        }
    },
    nextOrSubmit: function(component) {
            if (this.isLastFieldFocused()) {
                if (this.isDirty()) {
                    NNLM.Rosters.DisplayManager.notify('dosave');
                }
            } else {
                this.next();
            }
        }
        /*keys: [
        {
            key: Ext.EventObject.TAB,
            fn: function() {
                NNLM.Rosters.DisplayManager.notify('tab', {
                    target: this
                });
            }
        },
        {
            key: Ext.EventObject.ENTER,
            fn: function() {
                NNLM.Rosters.DisplayManager.notify('enter', {
                    target: this
                });
            }
        }
        ] */
});
Ext.form.BasicForm.override({
    clearFields: function() {
        this.items.each(function() {
            this.setRawValue(null);
        });
    },
    hasNonEmptyValues: function() {
        var hasvalue = false;
        if (this.items === undefined || this.items.length === 0) {
            return false;
        }
        this.items.each(function(f) {
            if (f.getValue() !== '') {
                hasvalue = true;
            }
        });
        return hasvalue;
    },
    trackResetOnLoad: true,
    loadRecordNoEvents: function(record) {
        this.suspendEvents(false);
        this.loadRecord(record);
        this.resumeEvents();
    },
    resetNoEvents: function() {
        this.suspendEvents(false);
        this.reset();
        this.resumeEvents();
    }
});
Ext.form.Field.override({
    /*Overriding the isDirty function, as original fn returns false if the
     form is disabled, which is undesirable, as a few forms are
     disabled after the form is cleared. */
    msgTarget: 'qtip',
    //  We know that we want Field's events to bubble directly to the FormPanel.
    getBubbleTarget: function() {
        if (!this.formPanel) {
            this.formPanel = this.findParentByType('form');
        }
        return this.formPanel;
    },
    isLastField: function() {
        return this.findParentByType('form').isLastFieldFocused();
    },
    bubbleEvents: ['focus'],
    listeners: {
        'valid': function() {
            var bt = this.getBubbleTarget();
            if (bt !== null) {
                bt.fireEvent('valid', this);
            }
        },
        'change': function(field, newValue, oldValue) {
            var bt = this.getBubbleTarget();
            if (bt !== null) {
                bt.fireEvent('change', this);
            }
        },
        specialkey: function(field, e) {
            // e.HOME, e.END, e.PAGE_UP, e.PAGE_DOWN,
            // e.TAB, e.ESC, arrow keys: e.LEFT, e.RIGHT, e.UP, e.DOWN
            switch (e.getKey()) {
                case e.ENTER:
                    NNLM.Rosters.DisplayManager.notify('enter', {
                        target: field
                    });
                    field.fireEvent("enter");
                    break;
                default:
                    //NNLM.Rosters.dump("Key "+e.getKey()+"not bound", ');
                    break;
            }
        }
    }
});
Ext.form.ComboBox.override({
    initComponent: function() {
        Ext.form.ComboBox.superclass.initComponent.call(this);
        this.addEvents('beforeclear', 'clear');
    },
    assertValue: function() {
        //FULL OVERRIDE - ADDING 'clear' EVENT
        var val = this.getRawValue(),
            rec;

        if (this.valueField && Ext.isDefined(this.value)) {
            rec = this.findRecord(this.valueField, this.value);
        }
        if (!rec || rec.get(this.displayField) !== val) {
            rec = this.findRecord(this.displayField, val);
        }
        if (!rec && this.forceSelection) {
            if (val.length > 0 && val !== this.emptyText) {
                this.fireEvent('beforeclear', this);
                this.el.dom.value = Ext.value(this.lastSelectionText, '');
                this.fireEvent('clear', this);
                this.applyEmptyText();
            } else {
                this.clearValue();
            }
        } else {
            if (rec && this.valueField) {
                // onSelect may have already set the value and by doing so
                // set the display field properly.  Let's not wipe out the
                // valueField here by just sending the displayField.
                if (this.value === val) {
                    return;
                }
                val = rec.get(this.valueField || this.displayField);
            }
            this.setValue(val);
        }
    }
});
Ext.layout.CardLayout.override({
    getActiveItemNum: function() {
        var current = this.activeItem;
        if (current === null) {
            throw "No active item found for card layout!";
        }
        var parent = current.ownerCt;
        for (var i = 0; i < parent.items.length; i++) {
            if (parent.get(i).id === current.id) {
                return i;
            }
        }
        throw "No active item found for card layout!";
    },
    getActiveSlideNum: function() {
        return this.getActiveItemNum();
    },
    isLastSlide: function() {
        return (this.getActiveItemNum() === this.activeItem.ownerCt.items.length - 1);
    },
    isFirstSlide: function() {
        return (this.getActiveItemNum() === 0);
    },
    slideNext: function() {
        if (this.activeItem === null) {
            return;
        }
        var activeItemNum = this.getActiveItemNum();
        var next = Math.min(this.activeItem.ownerCt.items.length - 1, activeItemNum + 1);
        if (activeItemNum === next) {
            return;
        }
        var parent = this.activeItem.ownerCt;
        this.activeItem.getEl().stopFx().fadeOut({
            duration: 0.2,
            callback: function() {
                parent.getLayout().setActiveItem(next);
                parent.get(next).getEl().fadeIn({
                    duration: 0.2
                });
            }
        });
        //this.setActiveItem(next);
    },
    slidePrev: function() {
        if (this.activeItem === null) {
            return;
        }
        var activeItemNum = this.getActiveItemNum();
        var prev = Math.max(0, activeItemNum - 1);
        if (activeItemNum === prev) {
            return;
        }
        var parent = this.activeItem.ownerCt;
        this.activeItem.getEl().stopFx().fadeOut({
            duration: 0.2,
            callback: function() {
                parent.getLayout().setActiveItem(prev);
                parent.get(prev).getEl().fadeIn({
                    duration: 0.2
                });
            }
        });
    },
    slideTo: function(num) {
        if (num === undefined) {
            throw 'num argument passed to slideTo function is undefined!';
        }
        if (this.activeItem === null) {
            return;
        }
        num = parseInt(num, 10);
        var activeItemNum = this.getActiveItemNum();
        var parent = this.activeItem.ownerCt;
        if (activeItemNum === num || num > parent.items.length || num < 0) {
            return;
        }
        this.activeItem.getEl().stopFx().fadeOut({
            duration: 0.2,
            callback: function() {
                parent.getLayout().setActiveItem(num);
                parent.get(num).getEl().fadeIn({
                    duration: 0.2
                });
            }
        });
    }
});
Ext.grid.RowSelectionModel.override({
    getSelectedRowIndex: function() {
        if (!this.hasSelection()) {
            return -1;
        }
        for (var i = 0; i < this.grid.getStore().getCount(); i++) {
            if (this.isSelected(i)) {
                return i;
            }
        }
        //should not reach.
        return -1;
    }
});
/**END Ext overrides*/

/** START Ext extensions */
/**
 * Makes a ComboBox more closely mimic an HTML SELECT.  Supports clicking and dragging
 * through the list, with item selection occurring when the mouse button is released.
 * When used will automatically set {@link #editable} to false and call {@link Ext.Element#unselectable}
 * on inner elements.  Re-enabling editable after calling this will NOT work.
 *
 * @author Corey Gilmore
 * http://extjs.com/forum/showthread.php?t=6392
 *
 * @history 2007-07-08 jv
 * Slight mods for Ext 2.0
 *
 **/
Ext.ux.SelectBox = function(config) {
    this.searchResetDelay = 1000;
    config = config || {};
    config = Ext.apply(config || {}, {
        editable: false,
        forceSelection: true,
        rowHeight: false,
        lastSearchTerm: false,
        triggerAction: 'all',
        lazyRender: true,
        mode: 'local'
    });
    Ext.ux.SelectBox.superclass.constructor.apply(this, arguments);
};
Ext.extend(Ext.ux.SelectBox, Ext.form.ComboBox, {
    lazyInit: false,
    forceSelection: true,
    allowBlank: false,
    initEvents: function() {
        Ext.ux.SelectBox.superclass.initEvents.apply(this, arguments);
        // you need to use keypress to capture upper/lower case and shift+key, but it doesn't work in IE
        this.el.on('keydown', this.keySearch, this, true);
        this.cshTask = new Ext.util.DelayedTask(this.clearSearchHistory, this);
        this.lastSelectedIndex = 0;
    },

    keySearch: function(e, target, options) {
        var raw = e.getKey(),
            key = String.fromCharCode(raw),
            startIndex = 0;
        if (!this.store.getCount()) {
            return;
        }

        switch (raw) {
            case Ext.EventObject.HOME:
                e.stopEvent();
                this.selectFirst();
                return;

            case Ext.EventObject.END:
                e.stopEvent();
                this.selectLast();
                return;

            case Ext.EventObject.PAGEDOWN:
                this.selectNextPage();
                e.stopEvent();
                return;

            case Ext.EventObject.PAGEUP:
                this.selectPrevPage();
                e.stopEvent();
                return;
        }

        // skip special keys other than the shift key
        if ((e.hasModifier() && !e.shiftKey) || e.isNavKeyPress() || e.isSpecialKey()) {
            return;
        }
        if (this.lastSearchTerm === key) {
            startIndex = this.lastSelectedIndex;
        }
        this.search(this.displayField, key, startIndex);
        this.cshTask.delay(this.searchResetDelay);
    },

    onRender: function(ct, position) {
        this.store.on('load', this.calcRowsPerPage, this);
        Ext.ux.SelectBox.superclass.onRender.apply(this, arguments);
        if (this.mode === 'local') {
            this.calcRowsPerPage();
        }
    },


    render: function(ct) {
        Ext.ux.SelectBox.superclass.render.apply(this, arguments);
        if (Ext.isSafari) {
            this.el.swallowEvent('mousedown', true);
        }
        this.el.unselectable();
        this.innerList.unselectable();
        this.trigger.unselectable();
        this.innerList.on('mouseup',
            function(e, target, options) {
                if (target.id && target.id === this.innerList.id) {
                    return;
                }
                this.onViewClick(false);
            },
            this);

        this.innerList.on('mouseover',
            function(e, target, options) {
                if (target.id && target.id === this.innerList.id) {
                    return;
                }
                this.lastSelectedIndex = this.view.getSelectedIndexes()[0] + 1;
                this.cshTask.delay(this.searchResetDelay);
            },
            this);

        this.trigger.un('click', this.onTriggerClick, this);
        this.trigger.on('mousedown',
            function(e, target, options) {
                e.preventDefault();
                this.onTriggerClick();
            },
            this);

        this.on('collapse',
            function(e, target, options) {
                Ext.getDoc().un('mouseup', this.collapseIf, this);
            },
            this, true);

        this.on('expand',
            function(e, target, options) {
                Ext.getDoc().on('mouseup', this.collapseIf, this);
            },
            this, true);
    },

    clearSearchHistory: function() {
        this.lastSelectedIndex = 0;
        this.lastSearchTerm = false;
    },

    selectFirst: function() {
        this.focusAndSelect(this.store.data.first());
    },

    selectLast: function() {
        this.focusAndSelect(this.store.data.last());
    },

    selectPrevPage: function() {
        if (!this.rowHeight) {
            return;
        }
        var index = Math.max(this.selectedIndex - this.rowsPerPage, 0);
        this.focusAndSelect(this.store.getAt(index));
    },

    selectNextPage: function() {
        if (!this.rowHeight) {
            return;
        }
        var index = Math.min(this.selectedIndex + this.rowsPerPage, this.store.getCount() - 1);
        this.focusAndSelect(this.store.getAt(index));
    },

    search: function(field, value, startIndex) {
        field = field || this.displayField;
        this.lastSearchTerm = value;
        var index = this.store.find.apply(this.store, arguments);
        if (index !== -1) {
            this.focusAndSelect(index);
        }
    },

    focusAndSelect: function(record) {
        var index = Ext.isNumber(record) ? this.store.indexOf(record) : record;
        this.select(index, this.isExpanded());
        this.onSelect(this.store.getAt(record), index, this.isExpanded());
    },

    calcRowsPerPage: function() {
        if (!isset(this.store)) {
            return;
        }
        if (this.store.getCount() > 0) {
            var n = Ext.fly(this.view.getNode(0));
            this.rowHeight = (n === null) ? 50 : n.getHeight();
            this.rowsPerPage = this.maxHeight / this.rowHeight;
        } else {
            this.rowHeight = false;
        }
    },

    listeners: {
        'select': {
            fn: function(combo, r, i) {
                if (i > this.store.getCount()) {
                    return;
                }
                var k = this.valueField || this.displayField;
                var newValue = this.store.getAt(i).get(k);
                this.i = (this.i === undefined) ? 0 : this.i;
                var oldValue = this.store.getAt(this.i).get(k);
                if (oldValue !== newValue) {
                    NNLM.dump('old value: ' + oldValue + ', new value: ' + newValue);
                    this.fireEvent('change', combo, oldValue, newValue);
                    this.i = i;
                }
                this.setValue(r.get(k));
                this.collapse();
            },
            buffer: 50
        }
    }
});
Ext.reg('selectbox', Ext.ux.SelectBox);

NNLM.Rosters.createSelect = function(config, data) {
    var len = 10;

    if (typeof data[0] === 'string') {
        var tmp = [];
        for (i = 0; i < data.length; i++) {
            len = Math.max(len, data[i].length);
            tmp.push({
                name: data[i],
                value: data[i]
            });
        }
        data = tmp;
        tmp = undefined;
    }
    return new Ext.form.ComboBox({
        xtype: 'combo',
        itemId: config.itemId,
        mode: 'local',
        triggerAction: 'all',
        forceSelection: true,
        editable: false,
        allowBlank: (config.allowBlank === true) ? true : false,
        emptyText: (config.emptyText !== undefined) ? config.emptyText : '',
        name: config.name,
        hiddenName: config.name,
        fieldLabel: (config.fieldLabel === undefined) ? config.name : config.fieldLabel,
        displayField: 'name',
        value: data[0].value,
        valueField: 'value',
        tpl: '<tpl for=".">' +
            '<div class="x-combo-list-item">' +
            '{name}&nbsp;' +
            '</div></tpl>',
        store: new Ext.data.JsonStore({
            fields: ['name', 'value'],
            data: data
        }),
        listeners: (config.listeners !== undefined) ? config.listeners : {}
    });
};

Ext.app.SearchField = Ext.extend(Ext.form.TwinTriggerField, {
    initEvents: function() {
        Ext.form.TwinTriggerField.superclass.initEvents.call(this);
        this.addEvents(['beforesubmit', 'loaded', 'submit']);
        var me = this;
        this.store.on('load',
            function() {
                me.fireEvent('loaded');
            });
        this.keymap = new Ext.KeyMap(this.id, {
            key: [10, 13],
            fn: this.onTrigger2Click,
            scope: this
        });
    },
    validationEvent: false,
    validateOnBlur: false,
    trigger1Class: 'x-form-clear-trigger',
    trigger2Class: 'x-form-search-trigger',
    hideTrigger1: true,
    width: 180,
    hasSearch: false,
    displayParams: {},
    setDisplayParam: function(k, v) {
        this.displayParams[k] = v;
    },
    setDisplayParams: function(o) {
        this.displayParams = o;
    },
    minLength: 1,
    onTrigger1Click: function() {
        if (this.hasSearch) {
            this.el.dom.value = '';
            this.triggers[0].hide();
            this.hasSearch = false;
        }
    },

    onTrigger2Click: function() {
        var v = this.getRawValue();
        if (v.length < this.minLength) {
            NNLM.Rosters.DisplayManager.err('Minimum length required for this searchfield is ' +
                this.minLength + ' characters');
            return false;
        }
        this.fireEvent('beforesubmit');
        this.store.reload({
            params: {
                _start: 0,
                request: v,
                params: JSON.stringify(this.displayParams)
            }
        });
        this.fireEvent('submit');
        this.hasSearch = true;
        this.triggers[0].show();
        return true;
    }
});
Ext.reg('searchfield', Ext.app.SearchField);

Ext.ux.RostersMenuPanel = Ext.extend(Ext.Panel, {
    cls: 'rostersMenuPanel',
    baseCls: 'x-corners',
    padding: '2px',
    layout: 'hbox',
    anchor: '100% 100%'
});
Ext.reg('rostersmenupanel', Ext.ux.RostersMenuPanel);

Ext.ux.ToolbarLink = Ext.extend(Ext.BoxComponent, {
    autoEl: {
        tag: 'p',
        cls: 'pseudolink',
        html: 'Ready.'
    },
    initEvents: function() {
        Ext.BoxComponent.superclass.initEvents.call(this);
        this.addEvents(['click']);
    },
    setText: function(t) {
        this.getEl().dom.firstChild.nodeValue = t;
    },
    listeners: {
        afterrender: function() {
            var me = this;
            this.getEl().addListener('click', function(e, t, config) {
                me.fireEvent('click', [e, t, config]);
            });
        }
    }
});
Ext.reg('tblink', Ext.ux.ToolbarLink);
/* End ext extensions */

/* START NNLM Config objects */

/** Template
    Contains all basic config options.
    @extends Ext.Panel
*/
NNLM.Rosters.Template = function(config) {
    NNLM.Rosters.Template.superclass.constructor.call(this, Ext.apply({
            closable: true,
            text: 'loading...',
            autoScroll: true,
            scripts: true,
            border: false,
            frame: false
        },
        config));
};
/** BodyTemplate
 *   @extends NNLM.Rosters.Template
 *   @abstract
 *   Template Object: duplicate this skeleton to make further objects.
 */
NNLM.Rosters.BodyTemplate = function(config) {
    NNLM.Rosters.BodyTemplate.superclass.constructor.call(this, Ext.apply({
            cls: 'BodyPanel',
            title: 'Body Template',
            layout: 'anchor',
            defaults: {
                border: false,
                frame: false,
                autoWidth: true
            },
            listeners: {
                close: function() {}
            }
        },
        config));
};
NNLM.Rosters.WizardTemplate = function(config) {
    NNLM.Rosters.WizardTemplate.superclass.constructor.call(this, Ext.apply({
            closable: true,
            cls: 'WizardPanel',
            title: 'Example Wizard',
            layout: 'card',
            activeItem: 0,
            // make sure the active item is set on the container config!
            defaults: {
                // applied to each contained panel
                border: false
            },
            bbar: [{
                        id: 'move-prev',
                        text: 'Back',
                        handler: function() {},
                        disabled: true
                    },
                    ' ',
                    // greedy spacer so that the buttons are aligned to each side
                    {
                        id: 'move-next',
                        text: 'Next',
                        handler: function() {}
                    }
                ]
                //Reusable config options here
        },
        config));
};
NNLM.Rosters.FormTemplate = function(config) {
    NNLM.Rosters.FormTemplate.superclass.constructor.call(this, Ext.apply({
            items: [{
                layout: 'column',
                border: false,
                items: [{
                    columnWidth: 0.5,
                    layout: 'form',
                    border: false,
                    items: [{
                        xtype: 'textfield',
                        fieldLabel: 'First Name',
                        name: 'first',
                        anchor: '95%'
                    }, {
                        xtype: 'textfield',
                        fieldLabel: 'Company',
                        name: 'company',
                        anchor: '95%'
                    }]
                }, {
                    columnWidth: 0.5,
                    layout: 'form',
                    border: false,
                    items: [{
                        xtype: 'textfield',
                        fieldLabel: 'Last Name',
                        name: 'last',
                        anchor: '95%'
                    }, {
                        xtype: 'textfield',
                        fieldLabel: 'Email',
                        name: 'email',
                        vtype: 'email',
                        anchor: '95%'
                    }]
                }]
            }, {
                xtype: 'tabpanel',
                plain: true,
                activeTab: 0,
                items: [{
                    title: 'Personal Details',
                    layout: 'form',
                    defaults: {
                        width: 230
                    },
                    defaultType: 'textfield',

                    items: [{
                        fieldLabel: 'First Name',
                        name: 'first',
                        allowBlank: false,
                        value: 'Jack'
                    }, {
                        fieldLabel: 'Last Name',
                        name: 'last',
                        value: 'Slocum'
                    }, {
                        fieldLabel: 'Company',
                        name: 'company',
                        value: 'Ext JS'
                    }, {
                        fieldLabel: 'Email',
                        name: 'email',
                        vtype: 'email'
                    }]
                }]
            }],

            buttons: [{
                text: 'Save'
            }, {
                text: 'Cancel'
            }]
        },
        config));
};
/* individual panel configurations here. */
NNLM.Rosters.Configs = {
    /** find
     *  type: panel
     *  Find a person by name or people id.
     **/
    'find': {
        title: 'Find',
        maxCount: 3

    },
    /** create
     *  type: panel
     **/
    'create': {
        title: 'Create',
        help: 'Create.html',
        maxCount: 1

    },
    /** edit
     *  type: panel
     **/
    'edit': {
        title: 'Edit',
        help: 'Edit.html',
        maxCount: 5

    },
    /** my_profile */
    'my_profile': {
        title: 'My Profile',
        cls: 'MyProfile',
        help: 'MyProfile.html'
    },
    'list': {
        title: 'List Management',
        help: 'List.html',
        maxCount: 5
    }
};
NNLM.Rosters.registerTemplates = function() {
    //order matters!
    //Panel->Template->BodyTemplate, FormTemplate
    Ext.extend(NNLM.Rosters.Template, Ext.Panel, {});
    Ext.extend(NNLM.Rosters.BodyTemplate, NNLM.Rosters.Template, {});
    Ext.extend(NNLM.Rosters.WizardTemplate, NNLM.Rosters.Template, {});

    Ext.extend(NNLM.Rosters.FormTemplate, Ext.FormPanel, {});
    Ext.extend(NNLM.Rosters.FormTemplate, NNLM.Rosters.Template, {});
};
//function for testing
function randomString(len) {
    var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz ';
    var s = '';
    for (var i = 0; i < len; i++) {
        var rnum = Math.floor(Math.random() * chars.length);
        s += chars.substring(rnum, rnum + 1);
    }
    return s;
}

/* END NNLM config objects */
/** Begin Main Layout ***************************************/
NNLM.Rosters.Layout = Ext.extend(Ext.Panel, {
    layout: 'border',
    monitorResize: true,
    height: NNLM.get('APPLICATION_HEIGHT'),
    defaults: {
        deferredRender: false,
        border: false,
        frame: true
    },
    initComponent: function() {
        var _actions = {
            'about': new Ext.Action({
                text: 'About Rosters',
                iconCls: 'iconed help',
                handler: function() {
                    NNLM.Rosters.DisplayManager.notify("about");
                }
            }),
            /**
             * shows the message console
             * The message console shows all communication from the server, as well as all
             * local notices, warnings, and errors generated in response to user action.
             * It is governed by the Display Manager.
             */
            'console': new Ext.Action({
                text: 'Message&nbsp;History',
                iconCls: 'iconed computer',
                permanent: true,
                itemId: 'console',
                handler: function() {
                    NNLM.Rosters.DisplayManager.notify('console');
                }
            }),
            /** Links externally to the change password page. */
            'changepassword': new Ext.Action({
                text: 'Change my intranet password',
                iconCls: 'iconed computer_key',
                permanent: true,
                itemId: 'changepassword',
                handler: function() {
                    window.open("https://staff.nnlm.gov/passwd");
                    //NNLM.Rosters.DisplayManager.notify('changepassword');
                }
            }),
            'changepasswordadmin': new Ext.Action({
                text: 'Change another user\'s intranet password (restricted)',
                iconCls: 'iconed computer_key',
                permanent: true,
                itemId: 'changepasswordadmin',
                handler: function() {
                    var id = NNLM.Rosters.DisplayManager.getBody().getActiveTab().PeopleID;
                    var uname = false;
                    if (id !== undefined) {
                        var r = null;
                        NNLM.Rosters.DataManager.STORES.accounts.findBy(function(record) {
                            if (record.get('PeopleID') !== id) {
                                return false;
                            }
                            if (record.get('AccountTypeID') !== 3) {
                                return false;
                            }
                            r = record;
                            return true;
                        });
                        uname = r.get('AccountValue');
                    }
                    if (uname !== false) {
                        window.open("/passwd/admin/?state=2&uname=" + uname);
                    } else {
                        window.open("/passwd/admin");
                    }
                    //NNLM.Rosters.DisplayManager.notify('changepassword');
                }
            }),
            /**
             * creates a new staff record.
             * This action initiates the wizard that guides the user through the process of
             * creating a new staff record.
             */
            'createperson': new Ext.Action({
                text: 'Create a new staff record',
                itemId: 'createperson',
                iconCls: 'iconed user_add',
                handler: function() {
                    NNLM.Rosters.DisplayManager.notify('createperson');
                }
            }),
            /**
             * creates a new list.
             * This action guides the user through the creation of a new list.  This is necessarily
             * done in wizard form to avoid list duplication.
             */
            'createlist': new Ext.Action({
                text: 'Create a new list',
                itemId: 'createlist',
                iconCls: 'iconed script_add',
                handler: function() {
                    NNLM.Rosters.DisplayManager.notify('createlist');
                }
            }),
            'dolayout': new Ext.Action({
                text: 'Refresh interface layout',
                iconCls: 'iconed paintcan',
                handler: function() {
                    NNLM.Rosters.DisplayManager.notify('dolayout');
                }
            }),
            /**
             * Edits the current user's record
             * The currently logged in user will have a record in the system already.  This opens
             * an edit panel for that record.
             */
            'edit': new Ext.Action({
                text: 'Edit my record',
                itemId: 'edit',
                iconCls: 'editPerson',
                handler: function() {
                    NNLM.Rosters.DisplayManager.notify('edit');
                }
            }),
            /**
             * Edits the current user's locker
             * Each user has a list that is entirely their own, and is generated with each new user.
             * This list is termed a 'locker', even though it is functionally the same.  This opens
             * a panel to that list for editing.
             */
            'editlist': new Ext.Action({
                text: 'Edit my locker',
                itemId: 'editlist',
                iconCls: 'editList',
                handler: function() {
                    NNLM.Rosters.DisplayManager.notify('list');
                }
            }),
            'errormessage': new Ext.Action({
                text: 'Create new error message bubble',
                itemId: 'errormessage',
                iconCls: 'iconed table_multiple',
                handler: function() {
                    var lipsum = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.".substr(0, Math.floor(Math.random() * 449));
                    NNLM.Rosters.DisplayManager.notify('statusmsg', {
                        text: lipsum,
                        cls: 'error'
                    });
                }
            }),
            /**
             * searches for a list by name
             * This opens a panel that allows searching for a list by keyword, or description.
             */
            'findlist': new Ext.Action({
                text: 'Find a list by title or description',
                itemId: 'findlist',
                handler: function() {
                    NNLM.Rosters.DisplayManager.notify('find', {
                        typeId: 'list_descriptions'
                    });
                }
            }),
            /**
             * searches for a person by name
             * This opens a panel that allows searching for a person by part or all of their name.
             */
            'findpersonbyname': new Ext.Action({
                text: 'Search for people by staff member name',
                itemId: 'findpersonbyname',
                handler: function() {
                    NNLM.Rosters.DisplayManager.notify('findpersonbyname');
                }
            }),
            /**
             * searches for a person by LIBID
             * This opens a panel that allows searching for a person by DOCLINE LIBID.  The LIBID
             * must already be assigned to one of their positions for it to be findable.
             */
            'findpersonbylibid': new Ext.Action({
                text: 'Search for people by Docline LIBID',
                itemId: 'findpersonbylibid',
                handler: function() {
                    NNLM.Rosters.DisplayManager.notify('findpersonbylibid');
                }
            }),
            /**
             * searches for a person by role
             * This opens a panel that allows searching for a person by a role.  The role must be
             * assigned to one of their assigned positions in order to be findable.
             */
            'findpersonbyrole': new Ext.Action({
                text: 'Search for people by assigned role',
                itemId: 'findpersonbyrole',
                handler: function() {
                    NNLM.Rosters.DisplayManager.notify('findpersonbyrole');
                }
            }),
            /**
             * searches for a person by peopleid
             * This opens a panel that allows searching for a person by PeopleID.  This is the system
             * unique identifier for a person.
             */
            'findpersonbypeopleid': new Ext.Action({
                text: 'Search for people by PeopleID',
                itemId: 'findpersonbypeopleid',
                handler: function() {
                    NNLM.Rosters.DisplayManager.notify('findpersonbypeopleid');
                }
            }),
            /**
             * searches for a person by region
             * This opens a panel that allows searching for a person by which region they belong to.  This
             * polls their assigned positions to determine this.
             */
            'findpersonbyregion': new Ext.Action({
                text: 'Search for people by RML/Center',
                itemId: 'findpersonbyregion',
                handler: function() {
                    NNLM.Rosters.DisplayManager.notify('findpersonbyregion');
                }
            }),
            /**
             *	Opens the old rosters system in a new window.
             */
            'oldrosters': new Ext.Action({
                text: 'Open old rosters interface',
                itemId: 'findpersonbyregion',
                iconCls: 'iconed bomb',
                handler: function() {
                    alert('Please be aware that the old Rosters interface is in the process of being retired.  This action will be removed at some future date.');
                    window.open("https://staff.nnlm.gov/rosters_old/");
                }
            }),
            'testmessage': new Ext.Action({
                text: 'Create new test message bubble',
                iconCls: 'iconed table_multiple',
                itemId: 'testmessage',
                handler: function() {
                    var lipsum = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.".substr(0, Math.floor(Math.random() * 449));
                    NNLM.Rosters.DisplayManager.notify('statusmsg', {
                        text: lipsum,
                        cls: 'msg'
                    });
                }
            }),
            'testsuite': new Ext.Action({
                text: 'Run test suite',
                itemId: 'testsuite',
                iconCls: 'iconed table_multiple',
                handler: function() {
                    NNLM.Rosters.DisplayManager.gmdl();
                    //NNLM.Rosters.DisplayManager.notify('prompt', {text: "I am siva",type: 'destroy'});
                    /*var a = new Ext.Action({
						text: 'foo',
						handler: function(){
							alert('foo');
						}
					});
					var b = new Ext.Action({
						text: 'bar',
						handler: function(){
							alert('bar');
						}
					});
					var c = new Ext.Action({
						text: 'baz',
						handler: function(){
							alert('baz');
						}
					});
					var menu = {
						'a': [a,b],
						'b': [b,c],
						'c': [a,c]
					};
					var random = Math.floor(Math.random()*4);
					random = (random === 0) ? 'a' : (random === 1) ? 'b' : 'c';
					NNLM.Rosters.DisplayManager.msg("setting to " + random);
					NNLM.Rosters.DisplayManager.clearActions();
					NNLM.Rosters.DisplayManager.addActions(menu[random]);   */
                    //NNLM.Rosters.DisplayManager.msg('No test suite currently defined');
                    //NNLM.Rosters.DisplayManager.msg(NNLM.Rosters.DataManager.STORES.lists.getById(12167).get('ListEntryEndDate'));
                    //var BODY = NNLM.Rosters.DisplayManager.getBody();
                    //BODY.setActiveTab(BODY.items.length - 1);
                    //NNLM.Rosters.DisplayManager.notify('edit', {PeopleID:4003});
                    //NNLM.Rosters.DisplayManager.notify('edit', {PeopleID:4});
                    //NNLM.Rosters.DisplayManager.notify('list',{ListID:3323});
                    //NNLM.Rosters.DisplayManager.notify('list',{ListID:715});
                    //NNLM.Rosters.DisplayManager.notify('edit',{PeopleID:4436});
                    //NNLM.Rosters.DisplayManager.notify('list', {ListID:715}); //Michael boer
                    //NNLM.Rosters.DisplayManager.notify('edit',{PeopleID: 932});
                    //NNLM.Rosters.DisplayManager.notify('edit',{PeopleID: 104});
                    /*var SC = ODY = MAIN.findById('rosters_mainbody');
		            HELP = MAIN.findById('rosters_mainhelp');
		            NORTHCONTROLS = MAIN.getComponent('NORTHCONTROLS');
		            SOUTHCONTROLS = MAIN.getComponent('SOUTHCONTROLS');
		            WESTCONTROLS = MAIN.getComponent('WESTCONTROLS');
		            PROGRESS = SOUTHCONTROLS.topToolbar.getComponent("progress")*/
                    //var f = NNLM.Rosters.DisplayManager.getBody().getActiveTab().chute('form');
                    /*f.get(1).setValue("Joseph");
					f.get(3).setValue("Flagerty");*/
                    /*f.get(0).setValue("My test list");
					f.get(1).setValue("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi quis sapien purus. Quisque dapibus pulvinar tincidunt. Maecenas quis lectus nisi. Aenean luctus lectus et neque vulputate hendrerit. Aenean lobortis, nibh nec placerat sollicitudin, sapien elit auctor magna, eu aliquam ante quam gravida purus. Curabitur purus elit, tempus et porttitor in, tincidunt in justo. Ut hendrerit vestibulum volutpat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc augue quam, dignissim eu dapibus vitae, tincidunt in nisl. Suspendisse rhoncus nulla vel odio mattis varius. Phasellus ullamcorper elementum lectus, vitae viverra massa porttitor sit amet. Nulla egestas lacinia neque ac pellentesque.");*/

                }
            }),
            /**
             * toggles the help panel
             */
            'togglehelp': new Ext.Action({
                text: 'Show/Hide help panel',
                itemId: 'togglehelp',
                iconCls: 'iconed help',
                handler: function() {
                    NNLM.Rosters.DisplayManager.notify('togglehelp', false);
                }
            }),
            /**
             * opens the staff wiki in a new window
             */
            'wikiopen': new Ext.Action({
                text: 'Go to Rosters Documentation',
                itemId: 'wikiopen',
                iconCls: 'iconed wikilink',
                handler: function() {
                    window.open("https://staff.nnlm.gov/wiki/Rosters_User_Documentation");
                }
            })
        };
        Ext.apply(this, {
            xtype: 'panel',
            defaults: {
                xtype: 'panel',
                border: false
            },
            items: [
                /*north region.  Contains title and navigation controls */
                {
                    region: 'north',
                    itemId: 'NORTHCONTROLS',
                    tbar: [{
                            text: 'File',
                            xtype: 'button',
                            itemId: 'file',
                            iconCls: 'iconed folder',
                            menu: []
                        }, {
                            text: 'Find/Edit',
                            itemId: 'find',
                            iconCls: 'iconed magnifier',
                            menu: {
                                items: [{
                                        xtype: 'tbtext',
                                        cls: 'groupmenu',
                                        text: 'Search for people'
                                    },
                                    _actions.findpersonbyregion,
                                    _actions.findpersonbyname,
                                    _actions.findpersonbyrole, {
                                        xtype: 'tbtext',
                                        cls: 'groupmenu',
                                        text: 'Search for lists'
                                    },
                                    _actions.findlist, {
                                        xtype: 'tbtext',
                                        cls: 'groupmenu',
                                        text: 'Advanced searches'
                                    },
                                    _actions.findpersonbylibid,
                                    _actions.findpersonbypeopleid
                                ]
                            }
                        }, {
                            text: 'Create',
                            itemId: 'create',
                            iconCls: 'iconed bullet_add',
                            menu: {
                                items: [_actions.createperson, _actions.createlist]
                            }
                        },
                        /*{
                    text: 'Delete',
                    itemId: 'delete',
                    iconCls: 'iconed bullet_delete',
                    menu: {
                        items: [
                        {
                            text: 'Not yet implemented',
							disabled: true,
							iconCls: 'iconed',
                            handler: function() {
                                NNLM.Rosters.DisplayManager.msg(NNLM.get('MSG_STATUS_POSITION'));
                            }
                        }
                        ]
                    }
                },*/
                        {
                            text: 'My Shortcuts',
                            itemId: 'my_shortcuts',
                            menu: {
                                items: [_actions.edit, _actions.changepassword /*, _actions.editlist*/ ]
                            }
                        }, {
                            text: 'Help',
                            itemId: 'help',
                            iconCls: 'iconed help',
                            menu: {
                                items: [_actions.togglehelp, _actions.wikiopen, _actions.console, _actions.about]
                            }
                        }, '->', {
                            text: 'Admin',
                            iconCls: 'iconed wrench',
                            menu: {
                                items: [{
                                        text: 'Show transaction log',
                                        iconCls: 'iconed book',
                                        handler: function() {
                                            NNLM.Rosters.DisplayManager.notify('transactionlog');
                                        }
                                    }, _actions.console, _actions.changepasswordadmin, _actions.oldrosters
                                    /*{
							xtype: 'tbtext',
							cls: 'groupmenu',
					        text: 'Testing actions'
						},_actions.dolayout, _actions.testsuite, _actions.testmessage, _actions.errormessage   */
                                ]
                            }
                        }, {
                            xtype: 'tbitem',
                            cls: 'apptitle',
                            html: '<h1>NN/LM Rosters Application</h1><p style="color: #333;">Powered by ExtJS</p>'
                        }
                    ]
                },
                /*{
                    region: 'west',
                    width: 200,
                    layout: 'fit',
                    id: 'rosters_shortcuts',
                    title: 'Shortcut menu',
                    split: true,
                    collapsible: true,
                    collapsed: true,
                    collapseMode: 'mini',
                    bodyCssClass: 'HelpPanel'
                },*/
                /*center region.  Contains main rosters interface.  This is subdivided
into two primary sub-interfaces: one for list management, and one
for people management.*/
                {
                    region: 'center',
                    id: 'rosters_mainbody',
                    xtype: 'tabpanel',
                    //bodyStyle: 'background-color:#999; border: 1px inset 999;',
                    border: false,
                    frame: true,
                    bodyStyle: 'background-color: #ccc;',
                    bubbleEvents: ['tabchange'],
                    defaults: {
                        xtype: 'tabpanel',
                        enableTabScroll: true,
                        frame: false,
                        border: false
                    },
                    items: [{
                        tools: [{
                            itemId: 'refresh',
                            id: 'refresh',
                            qtip: 'Refresh Page Data',
                            handler: function() {
                                this.load(NNLM.get('BODY_BASE_URL') + 'Welcome.html');
                            }
                        }],
                        xtype: 'panel',
                        itemId: 'landing_page',
                        title: 'Welcome',
                        bodyCssClass: 'HelpPanel',
                        autoLoad: NNLM.get('BODY_BASE_URL') + 'Welcome.html',
                        closable: false
                    }]
                },
                /*east region.  Contains help panel.*/
                {
                    region: 'east',
                    width: 200,
                    layout: 'fit',
                    id: 'rosters_mainhelp',
                    title: 'Help',
                    split: true,
                    collapsible: true,
                    collapseMode: 'mini',
                    bodyCssClass: 'HelpPanel',
                    bbar: [_actions.wikiopen]
                },
                /*south region.  Contains any controls used across all center panels*/
                {
                    region: 'south',
                    xtype: 'panel',
                    itemId: 'SOUTHCONTROLS',
                    id: 'rosters_southcontrols',
                    header: true,
                    tbar: [{
                        xtype: 'tbfill',
                        permanent: true
                    }, {
                        xtype: 'tbseparator',
                        permanent: true
                    }, _actions.console]
                }
            ]
        });
        // Call parent (required)
        NNLM.Rosters.Layout.superclass.initComponent.apply(this, arguments);
        this.addEvents('dosave', 'doreset');
    }
});
NNLM.Rosters.initRequest = function() {
    NNLM.Rosters.DisplayManager.notify('showhelp', {
        help: 'Welcome.html'
    });
    NNLM.Rosters.DisplayManager.getBody().setActiveTab(0);
    var args = NNLM.decodeQueryString();
    if (!jQuery.isEmptyObject(args) && args.action !== undefined) {
        var action = args.action;
        delete args.action;
        NNLM.Rosters.DisplayManager.notify(action, args);
    }
};
NNLM.Rosters.init = function() {
    Ext.QuickTips.init();
    Ext.state.SessionProvider = Ext.extend(Ext.state.CookieProvider, {
        readCookies: function() {
            if (this.state) {
                for (var k in this.state) {
                    if (typeof this.state[k] === 'string') {
                        this.state[k] = this.decodeValue(this.state[k]);
                    }
                }
            }
            return Ext.apply(this.state || {},
                Ext.state.SessionProvider.superclass.readCookies.call(this));
        }
    });
    Ext.BLANK_IMAGE_URL = '/scripts/ext/resources/images/default/s.gif';

    //Ext.Direct.addProvider(NNLM.Rosters.RPC);
    Ext.state.Manager.setProvider(
        new Ext.state.SessionProvider({
            state: Ext.appState
        }));
    NNLM.Rosters.registerTemplates();
    jQuery('#progress').remove();
    jQuery('#noscript').remove();
    NNLM.Rosters.DataManager = new NNLM.Rosters.DataManager();
    NNLM.Rosters.DisplayManager = new NNLM.Rosters.DisplayManager();
    NNLM.Rosters.DisplayManager.msg("Rosters system initialized.");
    NNLM.Rosters.DisplayManager.msg("Loading current user data...please wait...");
    window.RostersErr = window.setTimeout(function() {
        window.clearInterval(window.RostersInit);
        throw "One or more required elements could not be fetched.";
    }, 10000);
    //poll until all the init callbacks are complete.  Sort of graceless; maybe I'll get something better later.
    var initcount = 10;
    window.RostersInit = window.setInterval(function() {
        NNLM.Rosters.dump("testing load state: " + (NNLM.Rosters.DataManager.isReady() && NNLM.Rosters.DisplayManager.isReady()), 3);
        if ((NNLM.Rosters.DataManager.isReady() && NNLM.Rosters.DisplayManager.isReady())) {
            NNLM.Rosters.dump("All rosters components loaded.  Starting requested actions.", 1);
            NNLM.Rosters.DisplayManager.notify("datachanged", false); //reset now that current user info has been added
            NNLM.Rosters.DisplayManager.msg("Ready.");
            window.clearInterval(window.RostersInit);
            window.clearTimeout(window.RostersErr);
            NNLM.Rosters.initRequest();
        }
    }, 500);
};
NNLM.Rosters.dump('Custom components initialized and component overrides established.');
