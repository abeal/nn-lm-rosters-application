/******************************************
*   Global Environment (ini settings)     *
*******************************************/
Ext.ns('NNLM.Rosters');
NNLM.options = {};
NNLM.get = function (optionname) {
    if (NNLM.options[optionname] !== undefined) {
        return NNLM.options[optionname];
    }
    return false;
};
NNLM.set = function (optionname, optionvalue) {
    NNLM.options[optionname] = optionvalue;
};
NNLM.list = function(variables, values, scope){
    for(var i = 0; i < variables.length && i < values.length; i++){
        (scope || this)[variables[i]] = values[i];
    }
};
NNLM.set('APP_ROOT', '/rosters/');
NNLM.set('VERSION_NUMBER', '@@@version_stamp@@@');
NNLM.set('MAX_CONN_TIME', 10);
NNLM.set('MAX_HISTORY_LENGTH', 5);
NNLM.set('APPLICATION_HEIGHT', 550); //in pixel
NNLM.set('TIMEOUT', 5000); //5 second ajax request timeout
NNLM.set('HTML_BASE_URL', NNLM.get('APP_ROOT') + 'views/');
NNLM.set('SCRIPT_BASE_URL', NNLM.get('APP_ROOT') + 'classfiles/client/');
NNLM.set('SERVLET_BASE_URL', NNLM.get('APP_ROOT') + 'servlet/');
NNLM.set('MENU_BASE_URL', NNLM.get('HTML_BASE_URL') + 'menu/');
NNLM.set('BODY_BASE_URL', NNLM.get('HTML_BASE_URL') + 'body/');
NNLM.set('WIZARD_BASE_URL', NNLM.get('BODY_BASE_URL') + 'wizards/');
NNLM.set('HELP_BASE_URL', NNLM.get('HTML_BASE_URL') + 'help/');
NNLM.set('LOADING_HTML', "<p style='border: 1px dotted #e7e7e7;'><strong><img src='/rosters/images/extanim32.gif' style='vertical-align: middle; padding-right: 10px' />Loading...please wait</strong><br />");
NNLM.set('MESSAGE_DELAY_LENGTH', 5.0);
NNLM.set('DEBUG',true);
NNLM.set('DEV',true);
NNLM.set('MSG_NOT_IMPLEMENTED', "The selection you've made is not yet implemented");
NNLM.set('MSG_STATUS_POSITION', "Please note that status and error messages are displayed in the lower right of this interface. If something is not working as expected, you should check there first to see if there's a message you're missing.");
NNLM.set('NO_IMAGE_AVAILABLE', '/rosters/images/noimage.gif');
NNLM.set('PUBLIC_LIST', 'https://dev.staff.nnlm.gov/test/rosters/list.html?ListID=');
NNLM.set('PUBLIC_PERSON', 'https://dev.staff.nnlm.gov/test/rosters/index.html?PeopleID=');
//transfer variables set in the php environment.



NNLM.Rosters.dump = function (msg){
	var style = 'color: #000;';
	if(window.console === undefined){return;}
	if(window.dumpTimer === undefined && window.console !== undefined){		
		if(Ext.isIE){
			window.console.log("******************* BATCH BEGIN *******************");
		}
		else{		
			window.console.log("%cBATCH BEGIN", 'background-color: black; color: #fff;');
		}
	}
	else{
		window.clearTimeout(window.dumpTimer);
		//delete window.dumpTimer;
	}
	window.dumpTimer = window.setTimeout(function(){
		if(Ext.isIE){
			window.console.log("******************* BATCH END *******************");
		}
		else{	
			window.console.log("%cBATCH END", 'background-color: black; color: #fff;');	
		}
	}, 5000);
    if(arguments.length > 1){
		switch(arguments[1]){	
		case 1:
			style = 'color: #333;';
			break;
		case 2:
			style = 'color: #666;';
			break;
		case 3:
			style = 'color: #999;';
			break;
		case 'focus':
			style = 'background-color: #eef; color: #00f; font-weight: bold';
			break;
		case 'block':
			style = 'background-color: black; color: #fff;';
			break;
		case 'info':
			style = 'color: #161;  background-color: #EFE';
			break;
		case 'warn':
			style = 'color: #F90; background-color: #FFE';
			break;
		case 'error':
			style = 'color: #FAA;';
			break;
		case 'exception':
			style = 'color: #F00; background-color: #FEE';
			break;
		case 'event':
			style = 'color: #bbf;';
			break;
		case 'debug':
		case null:
		default:
			style = 'color: #000;';
		}
	}
	if(Ext.isIE){
		window.console.log(msg);
	}
	else{
		window.console.log('%c'+msg, style);
	}
};
function isset(obj){
    return (typeof obj !== 'undefined' && obj !== null);
}
NNLM.Rosters.emptyFn = function(){}; //for establishing default fn value when callbacks are absent.

//general exception handling.
window.onerror = function(e){
	NNLM.Rosters.dump(e, 'exception');
	if(NNLM.Rosters.DisplayManager !== undefined){
		NNLM.Rosters.DisplayManager.err('The following application exception has occurred: <br />"<strong>'+ e + '</strong>".<br />Please reload the Rosters application.');
	}
};                     
Ext.Ajax.timeout = 120000; //2 minutes.
NNLM.Rosters.dump("Environment initialized.");