<?php
require_once("utils.php");

class Find{
	function getPersonDocline($PeopleID){
		EchoRequest::writeToTerminal($_REQUEST, "\nGetPersonDocline: Incoming request data\n");
		$required_parameters = array('PeopleID');
		
		/** perform a translation to People_ID if necessary */
		perform_PID_conversion();
		/* check for required parameters.  
		Note that variables are standardized as lowercase.*/
		foreach($required_parameters as $param){
			if(@!isset($_REQUEST[$param])){
				err(BAD_REQ);
			}
			${strtolower($param)} = $_REQUEST[$param];
		}
		$query_string = "CALL rosters.getPersonDocline('$people_id')";
		runQueryAndWriteOutput($query_string);
	}
}
?>