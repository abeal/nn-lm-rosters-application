<?php
class Requester{
	private function err($msg){
		
		return "alert('$msg');";
		/*
		Ext.Msg.show({
			title: "Server Message: ",
			msg: String.format("<p><i>{0}</i></p>", $msg),
			buttons: Ext.Msg.OK,
			width: 400,
			icon: Ext.MessageBox.NOTICE, 
			fn: function(){}
		})'; */
	}
	public function getJS($script){
		if(is_array($script)){
			$script_output = '';
			foreach($script as $s){
				$script_output .= $this->getJS($s);
			}
			return $script_output;
		}
		$mydir = dirname(realpath("../client/$script"));
		if(!file_exists("$mydir/$script")){
			return "alert('Requester Error: Script not found: $mydir/$script')";
		}
		ob_start();
		readfile("$mydir/$script");
		return ob_get_clean();
	}
}
?>