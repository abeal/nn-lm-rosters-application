<?php
/* LoadPosition.servlet.php
Retrieves a list of positions using a People ID
*/
require_once("../utils.php");
require_once('MySQLi.php.inc');
/* begin processing */
$allowed_parameters = array(
    'PositionID'=>"/NUMERIC/",
    'PeopleID'=>"/NUMERIC/",
    'Title'=>"/TEXT/"
);

function processPosition($PositionID){   
	$PositionID = ''.$PositionID; //cast as string.
	if(!ctype_digit($PositionID) || $PositionID == 0){
		throw new UserDataException("Position ID $PositionID does not appear to be a valid position.  Please notify Web-STOC of this issue.");
	}   
	$PositionID = intval($PositionID);
	dump("Processing id $PositionID");
	$count = Utils::simpleQuery("
		SELECT COUNT(*)
		FROM ".DATABASE.".positions p
		WHERE p.PositionID = $PositionID");
	if($count < 1){
		dump("PositionID $PositionID not found!");
		Utils::err(NO_RES);
	}
	list($PeopleID, $Title) = Utils::singleRowQuery("
		SELECT p.PeopleID, p.Title
		FROM ".DATABASE.".positions p
		WHERE p.PositionID = $PositionID", false);
	
	dump("Deleting PositionID $PositionID..");
	$query_string = "DELETE FROM ".DATABASE.".positions WHERE PositionID = $PositionID";
	$mysqli = MySQLiC::getDBConnection('master');
	dump("Query String: $query_string");
	$mysqli->query($query_string);
	$affected = $mysqli->affected_rows;
	dump("Affected rows: $affected");
	if($affected <= 0){
	    Utils::err(SQL_ERR, $mysqli->error);
	}
	$mysqli->close();
	Utils::logTransaction('positions', $PeopleID, 'DELETE', "Title of removed item: $Title");
	return $affected;
}

dump("Incoming request data:");
dump($_REQUEST);

$request = Utils::getRequest();
Utils::sanity_check($request, $allowed_parameters);

if(ctype_digit($request)){
	$request = array('PositionID'=>$request);
}      
if(Utils::is_assoc($request)){
	$count = 0;
	if(isset($request['PositionID'])){
		$count = processPosition($request['PositionID']);
	}
	else if(isset($request['Title']) && isset($request['PeopleID'])){
		$request['PositionID'] = Utils::simpleQuery("SELECT * FROM rosters.positions WHERE PeopleID = $request[PeopleID] AND Title = '$request[Title]'");
		$count = processPosition($request['PositionID']);
	}
	else{
		throw new UserDataException("No search parameters were found.");
	}
	Utils::simple_response(($count > 0));
}
else{
	$count = 0;
	foreach($request as $PositionID){
		processPosition($PositionID);
		$count += 1;
	}
	Utils::simple_response(($count > 0));
}
Utils::err(BAD_REQ, "Unrecognized request value: ".print_r($request, true).".");
?>
