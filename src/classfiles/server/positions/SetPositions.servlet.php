<?php
/* GetPositions.servlet.php
Retrieves a list of positions using a People ID
*/
require_once("utils.php");

//DO NOT RUN UNTIL REWRITE!

//data sanity checks
foreach($data['results'] as $arr){	
	if(!isset($arr['type'])){
		throw_error('update type is not set');
	}
	if(!isset($arr['Affiliations_ID'])){
		throw_error('Affiliations ID is not set');
	}
}
//database connection assurance

//update database entry

//send response
$result = array(
	'success'=>true
);
foreach($data['results'] as $arr){	
	if($arr['type'] != 'u'){
		continue;
	}
	if(!isset($arr['data'])){
		throw_error('no data found for Affiliation ID ' + $arr['Affiliations_ID']);
	}
	foreach($arr['data'] as $arr2){
		$Affiliations_ID = $arr['Affiliations_ID'];
		foreach($arr2 as $k=>$v){
			${$k} = $v;
		}
		if(!(
			isset($Affiliations_ID) &&
			isset($name) &&
			isset($old) && 
			isset($newval))){
				continue;
		}
		$query_string = "
			UPDATE general.affiliations a
			SET a.$name = '$newval'
			WHERE a.Affiliations_ID = '$Affiliations_ID'";
		$result[]= $query_string;
		
	}
	/*
	$query_string = "
		SELECT a.*, p.FirstName, p.LastName 
		FROM general.affiliations a
		INNER JOIN general.people p
		ON (a.People_ID = p.People_ID)
		WHERE Affiliations_ID = '$arr[Affiliations_ID]'";
	$resource = mysql_query($query_string) or trigger_error(mysql_error());
	while($arr = mysql_fetch_assoc($resource)){
		$result[]= $arr;
	}
	mysql_free_result($resource);
	*/
}
EchoRequest::write_to_terminal($result, false);
write_array($result);
?>
