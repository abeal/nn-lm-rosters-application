<?php
/** Note: this servlet is required to make decisions about what
the 'proper' information is to be returned.  It will be an amalgam
of information from both the general and the docline databases.  The rule
is, the general database takes precedence unless no information is present for
a person, in which case the docline database information will be used as
a default.
*/
require_once("../utils.php");
require_once('MySQLi.php.inc');


$required_parameters = array(
    'PositionID'=>"/NUMERIC/"
);

$request = Utils::getRequest();

Utils::sanity_check($request, 'positions');//replace eventually with more strict checking.

//EndDate and StartDate get sent regardless of empty.  Easiest point to correct
//this issue is right here...maybe trace down *why* it's doing this later.
if(isset($request['StartDate']) && $request['StartDate'] == ''){
    unset($request['StartDate']);
}
if(isset($request['EndDate']) && $request['EndDate'] == ''){
    unset($request['EndDate']);
}
if(isset($request['LIBID'])){
	$request['Region'] = Utils::fetchRegion($request['LIBID']);
}
dump("Decoded request data:");
dump($request);
//Utils::sanity_check($request, $required_parameters);

/* check to insure the position exists */
$query_string = "
    SELECT Title, PeopleID
    FROM ".DATABASE.".positions
    WHERE PositionID = '$request[PositionID]'";

$mysqli = MySQLiC::getDBConnection('inserter');
if(!$result = $mysqli->query($query_string)){
    dump("An critical error has occurred in the SavePosition servlet.");
    Utils::err(SQL_ERR, $mysqli->error);
}
if($result->num_rows == 0){
    Utils::err(INVALID_REQ, "PositionID '$request[PositionID]' does not exist.");
}
else{
    dump("Position $request[PositionID] found.  Modifying...");
    list($Title, $PeopleID) = $result->fetch_row();
    $result->close();
}
$mysqli->close();
/* position does exist - save the changes to it. */

foreach(Utils::getFields($request, 'positions') as $k=>$v){
    if($k == 'PositionID'){
        continue; //won't need to update the primary key, obviously.
    }
    if($v == '' || $v == 'NULL'){
        $tmp []= "$k=NULL";
    }
    else{
        $tmp []= "$k='$v'";
    }
}
//add a last updated date
$tmp[] = "LastUpdt='".date("Y-m-d H:i:s")."'";

$query_string = "
    UPDATE ".DATABASE.".positions
    SET " . implode(', ', $tmp).
    " WHERE PositionID = '$request[PositionID]'";
$mysqli = MySQLiC::getDBConnection('updater');
if(isset($fields['Title'])){
    $Title = $fields['Title'];
}
dump("query string: $query_string");
if(!$mysqli->query($query_string)){
    dump($mysqli->error);
    Utils::err(SQL_ERR, $mysqli->error);
}
else{
    dump("query executed successfully.");
    Utils::logTransaction('positions', $PeopleID, 'UPDATE', "ID: ($request[PositionID]), affected position title: \"$Title\".");

}
$response = ($mysqli->affected_rows != 0) ? true : false;
dump("Affected rows: ".$mysqli->affected_rows);
$result->close();
$mysqli->close();
//update the region
$libid = Utils::simpleQuery("SELECT LIBID FROM ".DATABASE.".positions WHERE PositionID = $request[PositionID]");
$query_string = "
    UPDATE ".DATABASE.".positions
    SET Region = '" .Utils::fetchRegion($libid).
    "' WHERE PositionID = '$request[PositionID]'";
$mysqli = MySQLiC::getDBConnection('updater');
if(isset($fields['Title'])){
    $Title = $fields['Title'];
}
dump("query string: $query_string");
if(!$mysqli->query($query_string)){
    dump($mysqli->error);
    Utils::err(SQL_ERR, $mysqli->error);
}
else{
    dump("query executed successfully.");

}
$response = ($mysqli->affected_rows != 0) ? true : false;
dump("Affected rows: ".$mysqli->affected_rows);
$result->close();
$mysqli->close();
$query_string = "
    SELECT
        p.CITY_INST,
        p.COUNTY_INST,
        p.DEPT_INST,
        p.Email,                                         
        p.INST_INST,
        p.LastUpdt,
        p.LIBID,
        p.PeopleID,
        p.PHONE_AREA_CODE_INST,
        p.PHONE_COUNTRY_CODE_INST,
        p.PHONE_EXT_INST,
        p.PHONE_INST,
        p.PositionID,
        p.PositionRanking,
        p.PositionVisibility,
        p.PROVINCE_INST,
		p.Region,                                               
		DATE_FORMAT(p.StartDate, '%Y-%m-%d') as StartDate,
	    DATE_FORMAT(p.EndDate, '%Y-%m-%d') as EndDate,
        p.STATE_ETC_CODE_INST,
        p.STREET2_INST,
        p.STREET_INST,
        p.Title,
        p.ZIP_MAIL_CODE_INST
    FROM ".DATABASE.".positions p
    WHERE p.PositionID = '$request[PositionID]'";
Utils::runQueryAndWriteOutput($query_string);
?>
