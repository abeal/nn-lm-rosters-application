<?php
/** Note: this servlet is required to make decisions about what
the 'proper' information is to be returned.  It will be an amalgam
of information from both the general and the docline databases.  The rule
is, the general database takes precedence unless no information is present for
a person, in which case the docline database information will be used as
a default.
*/

require_once("../utils.php");
//$name = $_REQUEST['query'];
$People_ID = '4003';
$matches = array();
$query_string = "
    SELECT *
    FROM general.people p
    WHERE p.People_ID = '$People_ID'
    ORDER BY p.LastName, p.FirstName";
$resource = mysql_query($query_string) or Utils::err(SQL_ERR, true);
if(!$resource){
    Utils::err(GEN_ERR);
}
$query_string = "
    SELECT
    a.Active,
	a.Main,
	a.List,
	a.ListOrder,
	a.Title,
	pld.CITY_INST as City,
	pld.COUNTRY_CODE_INST as Country,
	pld.COUNTY_INST as County,
	pld.DEPT_INST as Department,
	pld.INST_INST as Institution,
	pld.LIBID as Libid,
	pld.PHONE_AREA_CODE_INST as Phone_Area_Code,
	pld.PHONE_COUNTRY_CODE_INST as Phone_Country_Code,
	pld.PHONE_EXT_INST as Phone_Extension,
	pld.PHONE_INST as Phone,
	pld.PROVINCE_INST as Province,
	pld.STATE_ETC_CODE_INST as State,
	pld.STREET2_INST as Street2,
	pld.STREET_INST as Street1,
	pld.ZIP_MAIL_CODE_INST as Zip
	/* rc.NAME AS Region,
	ltc.DESCRIPTION as Library_Type */
    FROM general.affiliations a
    INNER JOIN docline.PUBLIC_LIBRARY_DOCUSERS pld
    ON (pld.LIBID = a.ID)
    /* LEFT JOIN docline.PUBLIC_REGION_CODES rc
	ON (pld.REGION_CODE = rc.REGION_CODE)
	LEFT JOIN docline.PUBLIC_LIBRARY_LEVEL_CODES llc
	ON (pld.LIB_LEVEL_CODE = llc.LIB_LEVEL_CODE)
	LEFT JOIN docline.PUBLIC_LIBRARY_TYPE_CODES ltc
	ON (pld.LIB_TYPE_CODE = ltc.LIB_TYPE_CODE) */
    WHERE a.People_ID = '$People_ID'
    ORDER BY pld.LIBID";
$resource2 = mysql_query($query_string) or Utils::err(SQL_ERR, true);
if(!$resource2){
    Utils::err(GEN_ERR);
}
 /*   print json_encode(array("error"=>mysql_num_rows($resource).":QS: $query_string"));
    exit;*/
$results = array();
/* tpl corresponds to an Ext x-template that is used
as a preferred format to display the data on the client side. */


header("Content-type: text/xml");
$oXMLout = new XMLWriter();
$oXMLout->openMemory();
$oXMLout->startElement("results");
$oXMLout->writeElement("count", mysql_num_rows($resource));
while($row = mysql_fetch_assoc($resource)){
	$oXMLout->startElement("row");
	foreach($row as $k=>$v){
    	$oXMLout->writeElement($k, $v);
    }
    $oXMLout->endElement();
}
$oXMLout->endElement();
$oXMLout->endElement();
print $oXMLout->outputMemory();
mysql_free_result($resource);
?>
