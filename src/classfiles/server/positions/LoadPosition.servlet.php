<?php
/* LoadPosition.servlet.php
Retrieves a list of positions using a People ID
*/
require_once("../utils.php");
require_once('MySQLi.php.inc');
$required_parameters = array(
    'PeopleID'=>"/NUMERIC/",
    'Title'=>'/COMPLEXTEXT/'
);
dump("Incoming request data:");
dump($_REQUEST);

$request = Utils::getRequest();

if(@isset($request['create']) && $request['create'] == true){
    unset($request['create']);
    $required_parameters['Title'] = "/COMPLEXTEXT/";
    Utils::sanity_check($request, $required_parameters, true);
    //first, check for this position's existence for this position.  Duplicates not allowed.
    $mysqli = MySQLiC::getDBConnection('inserter');

    //Kind of a redundant query as far as retrieval goes,
    //but I need to check that the person actually exists.
    $query_string = "
        SELECT PeopleID
        FROM ".DATABASE.".people
        WHERE PeopleID = '$request[PeopleID]'";
    $result = $mysqli->query($query_string);
    list($PeopleID) = $result->fetch_row();
    if($result->num_rows == 0){
        Utils::err(INVALID_REQ, "The person '$request[PeopleID]' does not exist.");
    }
    list($PeopleID) = $result->fetch_row();
    $result->close();
    $mysqli->close();

    $fields = Utils::getFields($request, 'positions');
    if(!array_key_exists('StartDate', $fields)){
        $fields['StartDate'] = date("Y-m-d");
    }
    $query_string = "
        INSERT INTO ".DATABASE.".positions
        (" . implode(', ', array_keys($fields)) . ")
        VALUES ('" . implode("', '", $fields) . "')";

    dump("QS (Insert): ".$query_string);
    $mysqli = MySQLiC::getDBConnection('inserter');
    $PositionID = false;
    if($mysqli->real_query($query_string)){
        $PositionID = $mysqli->insert_id;
        Utils::logTransaction('positions', $request['PeopleID'], 'CREATE', "new position title: \"$fields[Title]\".");
    }
    else{
        dump("Warning: query failed: " . $mysqli->error);
    }
    $mysqli->close();        /* DATE_FORMAT(p.StartDate, '%b %e, %Y') as StartDate,
    DATE_FORMAT(p.EndDate, '%b %e, %Y') as EndDate,  	
		DATE_FORMAT(p.StartDate, '%Y-%m-%d') as StartDate,
	    DATE_FORMAT(p.StartDate, '%Y-%m-%d') as EndDate, */
    $query_string = "
        SELECT
            p.CITY_INST,
            p.COUNTY_INST,
            p.DEPT_INST,
            p.Email,    
			DATE_FORMAT(p.StartDate, '%Y-%m-%d') as StartDate,
		    DATE_FORMAT(p.EndDate, '%Y-%m-%d') as EndDate,
            p.INST_INST,
            p.LastUpdt,
            p.LIBID,
            p.PeopleID,
            p.PHONE_AREA_CODE_INST,
            p.PHONE_COUNTRY_CODE_INST,
            p.PHONE_EXT_INST,
            p.PHONE_INST,
            p.PositionID,
            p.PositionRanking,
            p.PositionVisibility,
            p.PROVINCE_INST,
			p.Region,                                            
            p.STATE_ETC_CODE_INST,
            p.STREET2_INST,
            p.STREET_INST,
            p.Title,
            p.ZIP_MAIL_CODE_INST
        FROM ".DATABASE.".positions p
        WHERE p.PositionID = '$PositionID'";
    Utils::runQueryAndWriteOutput($query_string);
    die();
}
/* check for required parameters.
Note that variables are standardized as lowercase.*/
Utils::sanity_check($request, $required_parameters);

$query_string = "
SELECT
    p.PositionID,
    p.PositionVisibility,
    p.PositionRanking,
    p.PeopleID,
    p.Title,
    p.Email,   
	DATE_FORMAT(p.StartDate, '%Y-%m-%d') as StartDate,
    DATE_FORMAT(p.EndDate, '%Y-%m-%d') as EndDate,                                     
	p.Region,
    p.LIBID,
    p.INST_INST,
    p.DEPT_INST,
    p.STREET_INST,
    p.STREET2_INST,
    p.CITY_INST,
    p.STATE_ETC_CODE_INST,
    p.ZIP_MAIL_CODE_INST,
    p.COUNTY_INST,
    p.PROVINCE_INST,
    p.PHONE_COUNTRY_CODE_INST,
    p.PHONE_AREA_CODE_INST,
    p.PHONE_INST,
    p.PHONE_EXT_INST,
    p.LastUpdt
FROM ".DATABASE.".positions p
    INNER JOIN ".DATABASE.".people pe
    ON (pe.PeopleID = p.PeopleID)";
$people_fields = Utils::getValidFieldNames("people");
$position_fields = Utils::getValidFieldNames("positions");
foreach($request as $k=>$v){
    if(array_key_exists($k, $people_fields)){
        $tmp []= "pe.$k='$v'";
    }
    else if (array_key_exists($k, $position_fields)){
        $tmp []= "p.$k='$v'";
    }
}
//lookup positionid by peopleid and libid.
$query_string .= "
WHERE ".implode(" AND ", $tmp);
$query_string .= "
ORDER BY pe.PrimaryPosition, p.PositionRanking, p.LIBID";
$query_string = str_replace("?", $request['PeopleID'], $query_string);

Utils::runQueryAndWriteOutput($query_string);
?>
