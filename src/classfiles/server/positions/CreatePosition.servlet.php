<?php
/** Note: this servlet is required to make decisions about what
the 'proper' information is to be returned.  It will be an amalgam
of information from both the general and the docline databases.  The rule
is, the general database takes precedence unless no information is present for
a person, in which case the docline database information will be used as
a default.
*/

require_once("../utils.php");
require_once('MySQLi.php.inc');
$allowed_parameters = array(
    'PeopleID'=>"/NUMERIC/",
    'Title'=>"/TEXT/"
);                         
function insert_record($l){
	global $allowed_parameters;
	dump("pre sanity check");
	Utils::sanity_check($l, $allowed_parameters, true);
	dump("post sanity check");
    $fields = Utils::getFields($l, 'positions');
    //first, check for this position's existence for this position.  Duplicates not allowed.
    $count = Utils::simpleQuery("
		SELECT COUNT(*)
        FROM ".DATABASE.".people
        WHERE PeopleID = '$fields[PeopleID]'");
    if($count == 0){
        Utils::err(INVALID_REQ, "The person '$fields[PeopleID]' does not exist.");
    }             
    if(!array_key_exists('StartDate', $fields)){
        $fields['StartDate'] = date("Y-m-d");
    }
    $query_string = "
        INSERT INTO ".DATABASE.".positions
        (" . implode(', ', array_keys($fields)) . ")
        VALUES ('" . implode("', '", $fields) . "')";   
	dump("All checks passed.  Insert string: " . $query_string);
    $mysqli = MySQLiC::getDBConnection('inserter');
    $pid = false;
    if($mysqli->real_query($query_string)){       
		dump("Record creation successful.  New PositionID: $pid");
        $pid = $mysqli->insert_id;
        Utils::logTransaction('positions', $fields['PeopleID'], 'CREATE', "position title: \"$fields[Title]\".");
    }
    else{
        dump("Warning: query failed: " . $mysqli->error);
    }
    $response = ($mysqli->affected_rows == 0) ? false : true;
    $mysqli->close();            
    return $pid;
}


/* Begin processing */
$request = Utils::getRequest();
dump("Create request.  Data: ");
dump($request);
$pid = insert_record($request);
$query_string = "
    SELECT
        p.CITY_INST,
        p.COUNTY_INST,
        p.DEPT_INST,
        p.Email,
        DATE_FORMAT(p.EndDate, '%b %e, %Y') as EndDate,
        p.INST_INST,
        p.LastUpdt,
        p.LIBID,
        p.PeopleID,
        p.PHONE_AREA_CODE_INST,
        p.PHONE_COUNTRY_CODE_INST,
        p.PHONE_EXT_INST,
        p.PHONE_INST,
        p.PositionID,
        p.PositionRanking,
        p.PositionVisibility,
        p.PROVINCE_INST,
		p.Region,
        DATE_FORMAT(p.StartDate, '%b %e, %Y') as StartDate,
        p.STATE_ETC_CODE_INST,
        p.STREET2_INST,
        p.STREET_INST,
        p.Title,
        p.ZIP_MAIL_CODE_INST
    FROM ".DATABASE.".positions p
    WHERE p.PositionID = '$pid'";
Utils::runQueryAndWriteOutput($query_string);
?>
