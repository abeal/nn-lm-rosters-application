<?php
/** Note: this servlet is required to make decisions about what
the 'proper' information is to be returned.  It will be an amalgam
of information from both the general and the docline databases.  The rule
is, the general database takes precedence unless no information is present for
a person, in which case the docline database information will be used as
a default.
*/
require_once("../utils.php");
function update_record($fields){
    global $me;
    $fields = getFields($fields, 'positions');
    $pid = $fields['PositionID'];
    unset($fields['PositionID']);
    $tmp = array();
    $query_string = "
        SELECT COUNT(*)
        FROM rosters.positions
        WHERE PositionID = '$pid'";
    DB::query($query_string);
    list($count) = DB::getResultRow();
    if($count == 0){
        err(INVALID_REQ, "PositionID '$pid' does not exist.");
    }
    foreach($fields as $k=>$v){
        $tmp []= "$k='$v'";
    }
    $query_string = "
        UPDATE rosters.positions
        SET " . implode(', ', $tmp).
        " WHERE PositionID = '$pid'";
    EchoRequest::writeToTerminal("QS (Update): ".$query_string, $me);
    DB::query($query_string, 'updater');
    if(DB::getLastError() != ''){
        err(BAD_REQ, DB::getLastError());
    }
    return DB::getAffectedRows();

}

/* start processing */
EchoRequest::writeToTerminal("Incoming request data", $me);
EchoRequest::writeArrayToTerminal($_REQUEST, $me);
$request = json_decode($_REQUEST['request'], true);
if(!sanity_check($request, 'positions')){
    err(BAD_REQ);
}
if(!array_key_exists('PositionID', $request)){
    err(BAD_REQ, "Required attribute 'PositionID' not set");
}

simple_response(update_record($request));
?>
