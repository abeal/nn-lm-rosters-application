<?php
/** Note: this servlet is required to make decisions about what
the 'proper' information is to be returned.  It will be an amalgam
of information from both the general and the docline databases.  The rule
is, the general database takes precedence unless no information is present for
a person, in which case the docline database information will be used as
a default.
*/

require_once("../utils.php");
$required_parameters = array(
    'Prefix'=>"/TEXT/",
    'FirstName'=>"/TEXT/",
    'MiddleName'=>"/TEXT/",
    'LastName'=>"/TEXT/",
    'Suffix'=>"/TEXT/"
);
$result =  json_decode($_REQUEST['request'], true);
sanity_check($result, $required_parameters);
dump("Creation data:", $me);
dump(print_r($result, true), $me);
$fields = getFields($request, 'people');
//first, check for this position's existence for this position.  Duplicates not allowed.
$query_string = "
    INSERT INTO rosters.people
    (" . implode(', ', array_keys($fields)) . ")
    VALUES ('" . implode("', '", $fields) . "')";
$mysqli = MySQLiC::getDBConnection('inserter');
dump("query string: $query_string");
if(!$mysqli->query($query_string)){
    dump($mysqli->error);
    err(SQL_ERR, $mysqli->error);
}
else{
    dump("query executed successfully.");
    $response = ($mysqli->affected_rows != 0) ? true : false;
    dump("Affected rows: ".$mysqli->affected_rows);
    $pid = $mysqli->insert_id;
    log_transaction('people', $pid, 'INSERT');
}
$result->close();
$mysqli->close();
$query_string = "
    SELECT
        p.FirstName,
        p.LastName,
        p.LastUpdt,
        p.MiddleName,
        p.PeopleID,
        p.Picture,
        p.Prefix,
        p.PrimaryPosition,
        p.Suffix
    FROM rosters.people p
    WHERE p.PeopleID = '$pid'";
runQueryAndWriteOutput($query_string);
?>
