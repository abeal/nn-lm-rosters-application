/**
*   @class NNLM.Rosters.DataManager
*     <p>The DataManager is responsible for maintaining a series of stores, and  connecting
*   to the servlets that supply them with data.</p>
*   The data in the NNLM Rosters application is present in two places - on the server, in the
*   rosters database, and on the client, in the form of Ext Store objects.  In order to coordinate
*   store behavior, all stores and server communication happens within this component.  Any major
*   user record that is interacted with by multiple components resides in one or more of the
*   stores in this component, especially to include all data that is allowed to undergo
*   editing.
*   Stores may exist outside of the DataManager, but
*   they will be simple in nature, not allowed to exist outside of whatever component
*   created them, and read-only.</p>
*
*   @author     Aron Beal
*   @copyright (c) 2008, by the University of Washington
*   @date       May 6 2008
*   @license All files in this web application are  licensed under the
*   terms of the Open Source LGPL 3.0 license.
*   License details: http://www.gnu.org/licenses/lgpl.html
*/
/*
All data stores are currently placed in the global scope. This is likely to change
once I figure out a good tradeoff between reuse of objects, and the necessity for
unique instances.
The configs are defined separately, as they may be reused in these unique instances.
*/
Ext.ns('NNLM.Rosters');
NNLM.Rosters.DataManager = function(){
    /* a series of listeners that dictate behavior.  Each listener is
    added to every store under the datamanager's control */
    var lastID = 0;
    var Listeners = {
        /* Listeners defined here are common for all stores. */
        /**
         * @event datachanged
         * Fires when the data cache has changed in a bulk manner (e.g., it has been sorted, filtered, etc.) and a
         * widget that is using this Store as a Record cache should refresh its view.
         * @param {Store} this
         */
        'datachanged': function(store){
            //_dump('datachanged event occurred');
        },

        /**
         * @event metachange
         * Fires when this store's reader provides new metadata (fields). This is currently only supported for JsonReaders.
         * @param {Store} this
         * @param {Object} meta The JSON metadata
         */
        'metachange': function(store, meta){
            _dump('metachange event occurred');
        },

        /**
         * @event add
         * Fires when Records have been {@link #add}ed to the Store
         * @param {Store} this
         * @param {Ext.data.Record[]} records The array of Records added
         * @param {Number} index The index at which the record(s) were added
         */
        'add': function(store, record, addedIndex){
            _dump('add event occurred.  index: ' + addedIndex);
        },

        /**
         * @event remove
         * Fires when a Record has been {@link #remove}d from the Store
         * @param {Store} this
         * @param {Ext.data.Record} record The Record that was removed
         * @param {Number} index The index at which the record was removed
         */
        'remove': function(store, record, removedIndex){
            _dump('remove event occurred');
            return true;
        },

        /**
         * @event update
         * Fires when a Record has been updated
         * @param {Store} this
         * @param {Ext.data.Record} record The Record that was updated
         * @param {String} operation The update operation being performed.  Value may be one of:
         *
         *   Ext.data.Record.EDIT
         *   Ext.data.Record.REJECT
         *   Ext.data.Record.COMMIT
         *
         */
        'update': function(store, record, op){
            _dump('update event occurred.  Store: ' + store.name + ", op: " + op);
            if (op !== Ext.data.Record.COMMIT){
                return false;
            }
            return true;
        },

        /**
         * @event clear
         * Fires when the data cache has been cleared.
         * @param {Store} this
         */
        'clear': function(store){
            //_dump('clear event occurred');
        },

        /**
         * @event beforeload
         * Fires before a request is made for a new data object.  If the beforeload handler returns
         * false the {@link #load} action will be canceled.
         * @param {Store} this
         * @param {Object} options The loading options that were specified (see {@link #load} for details)
         */
        'beforeload': function(store, loadOptions){
            //_dump('beforeload event occurred');
        },

        /**
         * @event load
         * Fires after a new set of Records has been loaded.
         * @param {Store} this
         * @param {Ext.data.Record[]} records The Records that were loaded
         * @param {Object} options The loading options that were specified (see {@link #load} for details)
         */
        'load': function(store, record, loadOptions){
            //_dump('Load event triggered.');
        },
        /**
         * @event loadexception
         * Fires if an exception occurs in the Proxy during loading.
         * Called with the signature of the Proxy's "loadexception" event.
         */
        'exception': function(e, o, response){
            _dump('exception event occurred:' + response);
        },

        /**
         * @event beforesave
         * Fires before a network save request fires.  If the handler returns false, the action will be cancelled.
         * @param {Store} this
         * @param {Record/Record[]} The record or Array of records being saved
         */
        'beforesave': function(store, record){
            _dump('beforesave event occurred.');
        },

        'save': function(store, batch, data){
            _dump('save event occurred.  data response: ' + NNLM.po(data));
            NNLM.Rosters.DisplayManager.notify('aftersave');
        },

        /**
         * @event saveexception
         * Fires when a network save exception occurs.
         * @param {DirectProxy} proxy
         * @param {Object} result
         * @param {Ext.Direct.ExceptionEvent}
         */
        'saveexception': function(proxy, result, exception){
            _dump('saveexception event occurred');
        },

        /**
         * @event beforedestroy
         * Fires before a record will be destroyed
         * @param {Store} this
         * @param {Record/Record[]} rs, record(s) to be destroyed
         */
        'beforedestroy': function(store, rs){
            _dump('beforedestroy event occurred');
        },

        /**
         * @event destroy
         * Fires after a record has been destroyed
         * @param {Store} this
         * @param {Object} result
         * @param {Ext.Direct.Event} response
         */
        'destroy': function(store, result, response){
            _dump('destroy event occurred');
        },

        /**
         * @event destroyexception
         * Fires when a destroy exception occurred
         * @param {Store} this
         * @param {Ext.data.Record[]} rs
         * @param {Ext.Direct.Event} response
         */
        'destroyexception': function(store, rs, response){
            _dump('destroyexception event occurred: ' + NNLM.printObject(response));
        },

        /**
         * @event beforecreate
         * Fires before a network create request occurs
         * @param {Store} this
         * @param {Record/Record[]}
         */
        'beforecreate': function(store, record){
            _dump('beforecreate event occurred');
        },

        /**
         * @event create
         * Fires after network create request occurs
         * @param {Store} this
         * @param {Object} result
         * @param {Ext.Direct.ExceptionEvent} response
         */
        'create': function(store, result, response){
            _dump('create event occurred');
        },

        /**
         * @event createexception
         * Fires after network create exception occurs
         * @param {DirectProxy} this
         * @param {Record} record
         * @param {Ext.Direct.ExceptionEvent}
         */
        'createexception': function(proxy, record, exception){
            _dump('createexception event occurred: ' + exception);
        }
    };
    /* a series of listeners that dictate behavior.  Each listener is
    added to every store proxy under the datamanager's control */
    var ProxyListeners = {
        /**
         * @event beforeload
         * Fires before a request to retrieve a data object.
         * @param {DataProxy} this The proxy for the request
         * @param {Object} params The params object passed to the {@link #request} function
         */
        'beforeload': function(proxy, params){
            //_dump('proxy beforeload called');
        },

        /**
         * @event load
         * Fires before the load method's callback is called.
         * @param {DataProxy} this The proxy for the request
         * @param {Object} o The request transaction object
         * @param {Object} options The callback's options property as passed to the {@link #request} function
         */
        'load': function(proxy, o, options){
            //_dump('proxy load called');
        },
        /**
         * @event exception
         * Fires if an exception occurs in the Proxy during a remote request.
         * This event is relayed through a corresponding
         * {@link Ext.data.Store}.{@link Ext.data.Store#exception exception},
         * so any Store instance may observe this event.
         * See also:
         * http://www.extjs.com/deploy/dev/docs/?class=Ext.data.DataProxy
         */
        'exception': function(proxy, type, action, options, response, arg){
            if(arg === null || arg === undefined){
                return;
            }
            if(response === undefined ||
               response.raw === undefined ||
               response.raw.results === undefined ||
               response.raw.results.error === undefined){
                if(response.responseText !== undefined){
                    _e('Proxy exception: ' + response.responseText);
                    return;
                }
                _e("Proxy exception.  (Cannot determine nature - the error message returned was unintelligible or absent.)" +
                   " Whatever you just did, it didn't take.  Try again.");
                return;
            }
            _e("proxy exception: "  + response.raw.results.error);
        },
        /**
         * @event beforewrite
         * Fires before a request is generated for one of the actions Ext.data.Api.actions.create|update|destroy
         * @param {DataProxy} this The proxy for the request
         * @param {String} action [Ext.data.Api.actions.create|update|destroy]
         * @param {Record/Array[Record]} rs The Record(s) to create|update|destroy.
         * @param {Object} params The request params object.  Edit params to add parameters to the request.
         */
        'beforewrite': function(proxy, action, r, params){
        },

        /**
         * @event write
         * Fires before the request-callback is called
         * @param {DataProxy} this The proxy that sent the request
         * @param {String} action [Ext.data.Api.actions.create|upate|destroy]
         * @param {Object} data The data object extracted from the server-response
         * @param {Object} response The decoded response from server
         * @param {Record/Record{}} rs The records from Store
         * @param {Object} options The callback's options property as passed to the {@link #request} function
         */
        'write': function(proxy, action, data, response, r, options){
            _dump('proxy write called.');
            /*for(var k in data){
                if(isset(r.data[k])){
                    r.set(k, data[k]); //override any local values with updates from the server.
                }
            }*/
            //alert('role id: ' + r.data.RoleID);
        }
    };
    /* Configurations define what store records look like, and mirror
    database fields. While in cases it would have been cleaner to name
    entries in the configurations with different names or different
    casing, the entries here echo the database side fields so that the
    mapping will be clear when revisiting this code later. */
    var Configurations = {
        /* matches the table rosters.people */
        people:[
            {fieldLabel: false, name: 'PeopleID', type: 'int'},
            {name: 'Prefix', type: 'string'},
            {fieldLabel: 'First Name', name: 'FirstName', type: 'string'},
            {fieldLabel: 'Middle Name', name: 'MiddleName', type: 'string'},
            {fieldLabel: 'Last Name', name: 'LastName', type: 'string'},
            {name: 'Suffix', type: 'string'},
            {fieldLabel: 'Primary Position', name: 'PrimaryPosition', type: 'int'},
            {fieldLabel: 'Picture URL', name: 'Picture', type: 'string'},
            {fieldLabel: 'Date Last Updated', name: 'LastUpdt', type: 'string'},
            {fieldLabel: "User List", name: 'MyListID', type: 'int'}
        ],
        /* matches the table rosters.positions */
        positions:[
            {fieldLabel: false, name: "PositionID", type: "int"},
            {fieldLabel: 'Position Visibility', name: "PositionVisibility", type: "string"},
            {fieldLabel: 'Position Ranking', name: "PositionRanking", type: "string"},
            {fieldLabel: false, name: "PeopleID", type: "int"},
            {name: "Title", type: "string"},
            {name: "Email", type: "string"},
            {fieldLabel: 'Start Date', name: "StartDate", type: "date"},
            {fieldLabel: 'End Date', name: "EndDate", type: "date"},
            {name: "LIBID", type: "string"},
            {fieldLabel: 'Institution', name: "INST_INST", type: "string"},
            {fieldLabel: 'Department', name: "DEPT_INST", type: "string"},
            {fieldLabel: 'Street', name: "STREET_INST", type: "string"},
            {fieldLabel: 'Street 2', name: "STREET2_INST", type: "string"},
            {fieldLabel: 'City', name: "CITY_INST", type: "string"},
            {fieldLabel: 'State', name: "STATE_ETC_CODE_INST", type: "string"},
            {fieldLabel: 'Zip', name: "ZIP_MAIL_CODE_INST", type: "string"},
            {fieldLabel: 'County', name: "COUNTY_INST", type: "string"},
            {fieldLabel: 'Province', name: "PROVINCE_INST", type: "string"},
            {fieldLabel: 'Telephone Country Code', name: "PHONE_COUNTRY_CODE_INST", type: "string"},
            {fieldLabel: 'Telephone Area Code', name: "PHONE_AREA_CODE_INST", type: "string"},
            {fieldLabel: 'Telephone', name: "PHONE_INST", type: "string"},
            {fieldLabel: 'Telephone Extension', name: "PHONE_EXT_INST", type: "string"},
            {fieldLabel: 'Date this record was last updated', name: "LastUpdt", type: "date"}
        ],
        roles: [
            {name: 'RoleID', type: 'int'},
            {name: 'RoleType', type: 'string'},
            {name: 'RoleDescription', type: 'string'},
            {name: 'PositionID', type: 'int'},
            {name: 'RoleVisibility', type: 'string'},
            {name: 'Scope', type: 'string'}
        ],
        accounts:[
            {name: 'AccountID', type: 'int'},
            {name: 'PeopleID', type: 'int'},
            {name: 'AccountTypeID', type: 'int'},
            {fieldLabel: 'Account Visibility', name: 'AccountVisibility', type: 'string'},
            {fieldLabel: 'Account Value', name: 'AccountValue', type: 'string'}
        ],
        /* matches part of the table docline.PUBLIC_LIBRARY_DOCUSERS. */
        docline:[
            {name: "PeopleID", type: "int"},
            {name: "LIBID", type: "string"},
            {fieldLabel: 'Institution', name: "INST_INST", type: "string"},
            {fieldLabel: 'Department', name: "DEPT_INST", type: "string"},
            {fieldLabel: 'Street', name: "STREET_INST", type: "string"},
            {fieldLabel: 'Street 2', name: "STREET2_INST", type: "string"},
            {fieldLabel: 'City', name: "CITY_INST", type: "string"},
            {fieldLabel: 'State', name: "STATE_ETC_CODE_INST", type: "string"},
            {fieldLabel: 'Zip', name: "ZIP_MAIL_CODE_INST", type: "string"},
            {fieldLabel: 'County', name: "COUNTY_INST", type: "string"},
            {fieldLabel: 'Province', name: "PROVINCE_INST", type: "string"},
            {fieldLabel: 'Telephone Country Code', name: "PHONE_COUNTRY_CODE_INST", type: "int"},
            {fieldLabel: 'Telephone Area Code', name: "PHONE_AREA_CODE_INST", type: "int"},
            {fieldLabel: 'Telephone', name: "PHONE_INST", type: "string"},
            {fieldLabel: 'Telephone Extension', name: "PHONE_EXT_INST", type: "string"}
        ],
        list_descriptions:[
            {name: 'ListID', type: "int"},
            {name: 'ListTitle', type: 'string'},
            {name: 'ListDescription', type: 'string'},
            {name: 'ListVisibility', type: 'string'},
            {name: 'ListStartDate', type: 'date'},
            {name: 'ListEndDate', type: 'date'}
        ],
        lists:[
            {name: 'ListEntryID', type: 'int'},
            {name: 'ListID', type: 'int'},
            {name: 'ListMemberTable', type: 'string'},
            {name: 'ListMemberTableID', type: 'int'},
            {name: 'Notes', type: 'string'},
            {name: 'DescriptiveText', type: 'string'}
        ]
    };
    /* Constructors define individual stores under the DataManager's
    control. There should be a store constructor for every database
    field that the DataManager communicates with.  Any outside objects
    that wish to independently communicate with the database should do
    so by first cloning a constructor in the DataManager using the
    cloneStore method.*/

    var Constructors = {
        docline: {
            name: 'docline',
            proxy: {
                api: {
                    read : NNLM.get('SERVLET_BASE_URL') + 'GetDocline.servlet.php'
                }
            },
            reader: {
                idProperty: 'LIBID'
            }
        },
        people: {
            name: 'people',
            proxy: {
                api: {
                    read : NNLM.get('SERVLET_BASE_URL') + 'people/LoadPerson.servlet.php',
                    create : NNLM.get('SERVLET_BASE_URL') + 'people/CreatePerson.servlet.php',
                    update: NNLM.get('SERVLET_BASE_URL') + 'people/SavePerson.servlet.php',
                    destroy: NNLM.get('SERVLET_BASE_URL') + 'people/DestroyPerson.servlet.php'
                }
            },
            reader: {
                idProperty: 'PeopleID'
            }
        },
        positions: {
            name: 'positions',
            proxy: {
                api: {
                    read : NNLM.get('SERVLET_BASE_URL') + 'positions/LoadPosition.servlet.php',
                    create : NNLM.get('SERVLET_BASE_URL') + 'positions/CreatePosition.servlet.php',
                    update: NNLM.get('SERVLET_BASE_URL') + 'positions/SavePosition.servlet.php',
                    destroy: NNLM.get('SERVLET_BASE_URL') + 'positions/DestroyPosition.servlet.php'
                }
            },
            reader: {
                idProperty: 'PositionID'
            }
        },
        roles: {
            name: 'roles',
            proxy: {
                api: {
                    read : NNLM.get('SERVLET_BASE_URL') + 'roles/LoadRoles.servlet.php',
                    create : NNLM.get('SERVLET_BASE_URL') + 'roles/CreateRole.servlet.php',
                    update: NNLM.get('SERVLET_BASE_URL') + 'roles/SaveRole.servlet.php',
                    destroy: NNLM.get('SERVLET_BASE_URL') + 'roles/DestroyRole.servlet.php'
                }
            },
            reader: {
                idProperty: 'RoleID'
            }
        },
        accounts: {
            name: 'accounts',
            proxy: {
                api: {
                    read : NNLM.get('SERVLET_BASE_URL') + 'accounts/LoadAccount.servlet.php',
                    create : NNLM.get('SERVLET_BASE_URL') + 'accounts/CreateAccount.servlet.php',
                    update: NNLM.get('SERVLET_BASE_URL') + 'accounts/SaveAccount.servlet.php',
                    destroy: NNLM.get('SERVLET_BASE_URL') + 'accounts/DestroyAccount.servlet.php'
                }
            },
            reader: {
                idProperty: 'AccountID'
            }
        },
        list_descriptions:{
            name: 'list_descriptions',
            proxy: {
                api: {
                    read : NNLM.get('SERVLET_BASE_URL') + 'lists/LoadListDescription.servlet.php',
                    create : NNLM.get('SERVLET_BASE_URL') + 'lists/CreateListDescription.servlet.php',
                    update: NNLM.get('SERVLET_BASE_URL') + 'lists/SaveListDescription.servlet.php',
                    destroy: NNLM.get('SERVLET_BASE_URL') + 'lists/DestroyListDescription.servlet.php'
                }
            },
            reader: {
                idProperty: 'ListID'
            }
        },
        lists: {
            name: 'lists',
            proxy: {
                api: {
                    read : NNLM.get('SERVLET_BASE_URL') + 'lists/LoadList.servlet.php',
                    create : NNLM.get('SERVLET_BASE_URL') + 'lists/CreateList.servlet.php',
                    update: NNLM.get('SERVLET_BASE_URL') + 'lists/SaveList.servlet.php',
                    destroy: NNLM.get('SERVLET_BASE_URL') + 'lists/DestroyList.servlet.php'
                }
            },
            reader: {
                idProperty: 'ListEntryID'
            }
        }
    };
    /* callbacks store the callback functions for collective store loads.  Any
    *   'load' request will have a callback of some sort, even if it's only an
    *   empty function.  This callback will be triggered only when all stores have returned.
    *   This is an object hash, with a unique id as a key, and an object hash as value.
    *   That value hash, in turn, contains 'count' and 'fn', representing the number of store
    *   requests still outstanding to complete the load, and the function to process as a callback.
    *   This integer is incremented whenever a store load for that callback is established, and
    *   decremented whenever said store load completes.
    */
    var Callbacks = {},

     _a = function (msg) {
        NNLM.Rosters.DisplayManager.notify('msg', {
            text: 'DataManager: ' + msg
        });
    },
    /** addListener
    *   @private
    *   adds a listener to a named store
    *   @param storeName {String} the texutal name of the store.
    *   @returns {Boolean} false if the event could not be added, true otherwise.
    */
    _addListener = function(storeName, eventName, handler, originator){
        if(!isset(this.STORES[storeName])){
            return false;
        }
        this.STORES[storeName].addListener(eventName, handler, originator);
        return true;
    };
    _createStore = function(config, extended_config){
        var result = Ext.extend(Ext.data.GroupingStore, {
            storeId: config.name,
            proxy: new Ext.data.HttpProxy({
                api: {
                    read : config.proxy.api.read,
                    create : config.proxy.api.create,
                    update: config.proxy.api.update,
                    destroy: config.proxy.api.destroy
                },
                listeners: ProxyListeners
            }),
            reader: new Ext.data.JsonReader({
                idProperty: config.reader.idProperty,
                root: 'results',
                totalProperty: 'count',
                record: 'row',
                successProperty: 'success'
            }, Configurations[config.name]),
            writer: new Ext.data.JsonWriter({
                returnJson: true,
                writeAllFields: false
            }),
            paramsAsHash: true,
            batchSave: false,
            baseParams: {
                request: ''
            },
            listeners: Listeners,
            sortInfo: {field: config.reader.idProperty, direction: 'ASC'}
        });
        if(extended_config === undefined){
            return new result();
        }
        else{
            return new result(extended_config);
        }
    },
    /** _dump
    *   @private
    *   dumps a string message to the Firefox console, with some text and formatting indicating
    *   the message originated from this component.
    *   @param msg {String} the message to be displayed.
    */
    _dump = function(msg){
        //commenting out DataManager event dump messages for clarity in console.
        //NNLM.Rosters.dump('\n* DataManager(' + (new Date().format('i:s.u')) + '): ' + msg);
    },
    /** _e
    *   @private
    *   displays an error box specific to the DataManager with a user notification message.
    *
    */
    _e = function(msg){
        NNLM.Rosters.DisplayManager.err('DataManager Error: ' + msg);
    },
   /* Begin Public Object */
    result = {

        STORES: {}, //Contains all managed stores
        /** clearFilters
        * clears filters on all stores.
        * @returns {void}
        */
        clearFilters: function(){
            _dump("clearFilters called");
            this.STORES.accounts.clearFilter();
            this.STORES.people.clearFilter();
            this.STORES.positions.clearFilter();
            this.STORES.roles.clearFilter();
            this.STORES.lists.clearFilter();
            _dump("clearfilters operation performed"+
                  ". Postop store counts: " +
                  "\nPeople: " + this.STORES.people.getCount() +
                  "\nPositions: " + this.STORES.positions.getCount() +
                  "\nRoles: " + this.STORES.roles.getCount() +
                  "\nAccounts: " + this.STORES.accounts.getCount()
                  );
        },
        /** cloneStore
        *   creates a new store (empty) that is a clone of one of the stores managed by the Data Manager.
        *   This is necessary because some components will access the same servlets as the
        *   data manager stores do, but whose data is entirely transitory; it is only used for
        *   an intermediary step - to  choose records to add to one of the main stores.  These stores
        *   only live within their Ext host component, and are destroyed when that component is.
        *   @param storeName {string} the name of the store to be cloned.  This
        *   must be one of the stores managed by the datamanager.
        *   @param config {object} a key-value hash object containing additional
        *   parameters to assign to the cloned store.
        *   @returns {Ext.data.Store}
        */
        cloneStore: function(storeName, config){
            if(!isset(config)){
                config = {};
            }
            config.autoDestroy = true;
            if(!isset(Constructors[storeName])){
                _e("Fatal error: The data store constructor does not exist, and could not be created!");
                return false;
            }
            return _createStore(Constructors[storeName], config);
        },
        /** cloneRecord
         *  clones a basic record from a named store.  Record is set with default values for all fields.
         *  @param storeName {String} The name of the store whose record should be cloned.
         */
        cloneRecord: function(storeName){
            if(!isset(this.STORES[storeName])){
                _e('Fatal error: The data store ' + storeName + ' does not exist, and could not be created!');
                return false;
            }
            var mystore = this.STORES[storeName];
            return new mystore.recordType({});
        },
        /** createQuery
         * turns a javascript object into a request string suitable for passing to
         * a store's servlet.
         * @param o {Object} the parameters to be wrapped for delivery.
         * @returns {String} the JSON string result.
         */
        createQuery: function(o){
            var result = 'query='+ JSON.stringify(o);
            return result;
        },
        /** createRecord
         * returns a new record of the type specified by the first parameter.  Accepts
         * as arguments the values in the object in the second parameter.  Uses remote loading
         * servlets to create the record.
         *  @param storeName {String} the name of the store where the record will be added.
         *  @param _params {object} an object containing additional parameters to define
         *  some starting values for the store.
         *  @param fn {function} an optional callback function to execute once the server
         *  returns the definition and the record is created.  This function will be called
         *  with the newly instantiated record as an argument, or false if the record creation
         *  was a failure.
         *  @returns {Ext.data.Record}
         */
        createRecord: function(storeName, _params, fn){
            if(!isset(NNLM.Rosters.DataManager.STORES[storeName])){
                _e('Fatal error: The data store ' + storeName + ' does not exist, and could not be created!');
                return false;
            }
            if(fn === undefined){
                fn = function(record){
                    _dump('default callback: ' + record);
                };
            }
            /* basic idea: create three callback functions, one for successful server write, one
              for server exception, and one for store commit.  bind the listeners to the appropriate
              stores (and proxies), and then unbind them during callback resolution, in addition to
              performing whatever effects they require. */
            var write = function(store, action, result, res, rs){
                /* with a write callback, we have successfully returned from server without errors.
                  We can remove all listeners to the store and the proxy for this operation. */
                _dump('proxy writeback');
                if(res.success){
                    _dump('result: ' + NNLM.printObject(result));
                    //_dump('res: ' + NNLM.printObject(res));
                    for(var p in res.raw.results[0]){
                        //copy over server established values to new record.
                        if(res.raw.results[0].hasOwnProperty(p)){
                            rs.data[p] = res.raw.results[0][p];
                        }
                    }
                    NNLM.Rosters.DataManager.STORES[storeName].commitChanges();
                    //listeners cleared on 'update' event.
                }
                else{
                    NNLM.Rosters.DataManager.STORES[storeName].rejectChanges();
                    fn(false);
                    clearListeners();
                }
            };
            var exception = function(proxy, type, action, options, response, arg){
                var str = '';
                for(var key in arg){
                    if(typeof arg[key] !== 'function'){
                        str += key + ': ';
                        if(typeof arg[key] == 'object'){
                           str +=  NNLM.printObject(arg[key]);
                        }
                        else{
                            str += arg[key];
                        }
                    }
                }
                try{
                    if(response.raw === undefined){
                        _e(str);
                    }else{
                        _e(response.raw.results.error);
                    }
                }
                catch(e){
                    _e('Exception: server response could not be parsed.  Text: ' + response.error);
                }
                /* with an exception, we remove the write listener whose job it is to remove this callback,
                  as well as the record we added and this listener itself. */
                clearListeners();
                return false;
            };
            var update = function(recordArray, options, success){
                if(success){
                    NNLM.Rosters.DataManager.STORES[storeName].commitChanges();
                }
                clearListeners();
                fn(success, recordArray);
            };
            var clearListeners = function(){
                NNLM.Rosters.DisplayManager.notify(
                'toggleprogress', {text: 'Creating new record...', v:false});
                NNLM.Rosters.DataManager.STORES[storeName].proxy.removeListener('write', write, NNLM.Rosters.DataManager);
                NNLM.Rosters.DataManager.STORES[storeName].removeListener('exception', exception, NNLM.Rosters.DataManager);
                NNLM.Rosters.DataManager.STORES[storeName].removeListener('update', update, NNLM.Rosters.DataManager);
            };
            this.STORES[storeName].proxy.addListener('write', write, this);
            this.STORES[storeName].addListener('exception', exception, this);
            //this.STORES[storeName].addListener('update', update, this);
            _params.create = true;
            this.STORES[storeName].load({
                params: {'request': JSON.stringify(_params)},
                callback: update,
                scope: this,
                add: true
            });
            NNLM.Rosters.DisplayManager.notify(
                'toggleprogress', {text: 'Creating new record...', v:true});
        },
        /** destroyRecord
         * destroys a record in the named store, and calls a callback function once the server returns with results.
         * @param storeName {string} the name of the store.
         * @param record {Ext.data.Record} the record object to be removed.
         * @param fn {function} any callback function to be executed upon completion.
         * @returns {boolean} true if the request is successfully sent the server, false otherwise.  Note that this
         * does not include the server's success in deleting the record.  The callback function passed should take this
         * into account - it is passed one parameters:
         *      success {boolean} whether or not the operation was a success
         */
        destroyRecord: function(storeName, record, fn){
            if(!isset(this.STORES[storeName])){
                _e('Fatal error: The data store ' + storeName + ' does not exist, and could not be created!');
                return false;
            }
            if(fn === undefined){
                fn = function(success){
                    _dump('default callback: ' + success);
                };
            }
            if(Ext.isNumber(record)){
                record = this.STORES[storeName].getById(record);
            }
            var callback = function(store, r, i){
                clearListeners();
                fn(true);
                return true;
            };
            var exception = function(proxy, type, action, options, response, arg){
                var str = '';
                for(var key in arg){
                    if(typeof arg[key] !== 'function'){
                        str += key + ': ';
                        if(typeof arg[key] == 'object'){
                           str +=  NNLM.printObject(arg[key]);
                        }
                        else{
                            str += arg[key];
                        }
                    }
                }
                clearListeners();
                fn(false);
                _e("there's been an exception.");
                _dump("Exception: " + str);
                return false;
            };
            var clearListeners = function(){
                NNLM.Rosters.DisplayManager.notify('toggleprogress', {v:false});
                NNLM.Rosters.DataManager.STORES[storeName].removeListener('exception', exception, NNLM.Rosters.DataManager);
                NNLM.Rosters.DataManager.STORES[storeName].removeListener('remove', callback, NNLM.Rosters.DataManager);
            };
            this.STORES[storeName].addListener('remove', callback, this);
            this.STORES[storeName].addListener('exception', exception, this);
            NNLM.Rosters.DisplayManager.notify(
                'toggleprogress', {text: 'Removing record...', v:true});
            this.STORES[storeName].remove(record);
            return true;
        },
        /** destroyRecordById
         * convenience function for calling destroyRecord with just a record id.  Executes
         * any passed callback function with the results of the query once it returns from the server.  This function
         * will not be executed if the record id passed is not found.
         * @param storeName {string} the name of the store
         * @param id {int} the integer that refers to the record's primary key
         * @param fn {object} a callback function to be executed once the deletion results
         * are returned from the server.  The function is executed with a single argument,
         * @returns {boolean} true if the request is successfully sent the server, false otherwise.  Note that this
         * does not include the server's success in deleting the record.  The callback function passed should take this
         * into account - it is passed one parameters:
         *      success {boolean} whether or not the operation was a success
         */
        destroyRecordById: function(storeName, id, fn){
            var recordi = this.STORES[storeName].findBy(function(r){
                if(r.data.PositionID == id){
                    return true;}
            });
            if(recordi === -1){
                _e("record " + id + " not found");
                return false;
            }
            var record = this.STORES[storeName].getAt(recordi);
            if(!isset(record)){
                _e("record " + id + " not found");
                fn(false);
                return false;
            }
            return this.destroyRecord(storeName, record, fn);
        },

        /** filterBy is a convenience function that filters all stores by
         * a given PeopleID.  When the PeopleID is not a key in that store, the
         * function does the appropriate cross-referencing to determine the correct
         * key(s) related the to the peopleID, an filters by those instead
         * @param PeopleID {int} the peopleID to filter by.
         * @returns {void}
         */
        filterBy: function(PeopleID){
            PeopleID = parseInt(PeopleID, 10);
            var myfilter = [{
                property     : 'PeopleID',
                value        : PeopleID,
                exactMatch     : true
            }];
            this.STORES.accounts.filter(myfilter);
            this.STORES.people.filter(myfilter);
            this.STORES.positions.filter(myfilter);
            var libids = [];
            var roles = [];
            this.STORES.positions.each(function(record){
                libids.push(record.get('LIBID'));
                roles.push(record.get('PositionID'));
            });
            //warning - this filter will blow up quick with large local cache.
            //hoping it's a non-issue (small ops only.)
            /*
             this.STORES.docline.filterBy(function(record, id){
                return (libids.indexOf(id) !== -1);
            });
            */
            this.STORES.roles.filterBy(function(record, id){
                return (roles.indexOf(record.get('PositionID')) !== -1);
            });
            _dump("filterBy operation performed, using PeopleID " + PeopleID +
                  ". Postop store counts: " +
                  "\nPeople: " + this.STORES.people.getCount() +
                  "\nPositions: " + this.STORES.positions.getCount() +
                  "\nRoles: " + this.STORES.roles.getCount() +
                  "\nAccounts: " + this.STORES.accounts.getCount()
                  );
        },
        /** filterStore filters a single store, using the data managers
         * innate knowledge of that store's primary key, and applying
         * all appropriate conversions and checks to incoming data.
         * @param storeName {string} the name of the store.
         * @param value {int} the primary key value to be filtered by.
         */
        filterStore: function(storeName, value){
            value = parseInt(value, 10);
            var key = 'PeopleID';
            switch(storeName){
            case 'list_descriptions':
            case 'lists':
                key = 'ListID'
                break;
            case 'roles':
            case 'role_descriptions':
                key = 'PositionID';
            }
            this.STORES[storeName].filter([{
                property     : key,
                value        : value,
                exactMatch     : true
            }]);
        },
        /** getColumnModel(storeName)
         * returns a basic column model using the store's configuration, usable
         * in a gridPanel
         *   @param storeName {string} the name of the store configuration to query
         *   @returns {Ext.grid.ColumnModel}
         */
        getColumnModel: function(storeName){
            var config = this.getConfig(storeName);
            if(!config){
                return [];
            }
            var fields = arguments.length > 1 ? arguments[1] : jQuery.keys(config);
            var result = [];
            for(var i in fields){
                if(config.hasOwnProperty(i)){
                    var o = {
                        'name': config[i].name
                    };
                    o.header = (isset(config.fieldLabel)) ? config.fieldLabel : config.name;
                    result.push(o);
                }
            }
            return new Ext.grid.ColumnModel(result);
        },
        /** getConfig
        *   returns the configuration array for a given store.
        *   @param storeName {String} the name of the store to be retrieved.  See also:
        *
        *   @return {Array} the store configuration referenced by the passed parameter,
        *   or an empty array if
        *   the referenced store does not exist.
        */
        getConfig: function(storeName){
            if(!isset(Configurations[storeName])){
                _e("Fatal error: The data store configuration does not exist, and could not be created!");
                return false;
            }
            return Configurations[storeName];
        },
        /** getFieldNames
         *   returns a form configuration object for a given store.
         *   @param storeName {string} the name of the store configuration to query
         *   @returns {array} list of field names, or empty array if something went wrong.
         */
        getFieldConfig: function(storeName){
            var config = this.getConfig(storeName);
            if(!config){
                return [];
            }
            var result = [];
            for(var i in config){
                if(config.hasOwnProperty(i)){
                    var o = {
                        'name': config[i].name
                    };
                    if(isset(config.fieldLabel)){
                        if(config.fieldLabel !== false){
                            o.fieldLabel = config.fieldLabel;
                        }
                    }
                    else{
                        o.fieldLabel = o.name;
                    }
                    result.push(o);
                }
            }
            return result;
        },
        /** getRecord
        *   retrieves a composite record object from the stores.
        *   @param PeopleID {Number} The PeopleID of the record of the person to retrieve.
        *   @return {Object Hash} the results from the store in question, or an empty hash if none.
        *       Entries in the object hash will be Ext.data.MixedCollections of Ext.data.Record objects.
        */
        getRecords: function(PeopleID){
            var results = {},
            queryByFn = function(r, id){
                return (r.data.PeopleID === PeopleID);
            };
            for (var storeName in this.STORES){
                if(this.STORES.hasOwnProperty(storeName)){
                    if(this.STORES[storeName] === false){
                        continue;
                    }
                    results[storeName] = this.STORES[storeName].queryBy(queryByFn);
                }
            }
            return results;
        },
        getRecordsById: function(storeName, id){
            var storeKey = NNLM.Rosters.DataManager.getStoreKey(storeName);
            var recordi = this.STORES[storeName].findBy(function(r){
                //alert(r.data.PositionID);
                if(r.data[storeKey] == id){
                    return true;}
                return false;
            });
            if(recordi === -1){
                return false;
            }
            return this.STORES[storeName].getAt(recordi);
        },
        /** getStoreNames
        *   provides the index by which an outside source would query a particular store
        *   using the Ext getById call.
        *   @param storeName {string} the name of the store to provide the key for
        *   @returns {string} the value of the key for that store.
        */
        getStoreKey: function(storeName){
            return Constructors[storeName].reader.idProperty;
        },
        /** Constructor
        *   @private
        */
        init: function(){
            /*
            create a new store for each constructor, and bind the datamanager's
            event listeners to the store, so each store calls each listener at the
            appropriate time. */
            var mystore = false;
            for(var indx in Constructors){
                if(Constructors.hasOwnProperty(indx)){
                    mystore = _createStore(Constructors[indx], {
                        autoLoad: false
                    });
                    for(var indx2 in Listeners){
                        if(Listeners.hasOwnProperty(indx)){
                            mystore.addListener(indx2, Listeners[indx2]);
                        }
                    }
                    this.STORES[indx] = mystore;
                    mystore.name = indx;
                }
            }
            NNLM.Rosters.dump("Data Manager initialized.");
        },
        /* load
        *  This function is the primary means by which external components and
        *   other managers talk to the data manager.  Load takes a single argument - a
        *   set of parameters, that are designed to be used by the Ext Stores contained
        *   within the data manager.
        *
        *   @param obj {Object Hash}:      any arguments to pass to
        *               the store load.  Arguments may be one of the following:
        *
        *               obj.params: the query parameters to be loaded.  Will equate
        *               to a key=value transmission via XMLHttpRequest.  Example:
        *               obj.params = {foo:bar} would send the key value pair "foo=bar".
        *
        *               Note: The parameter 'PeopleID' is required.
        *
        *               obj.callback: a callback function to be executed on load completion.
        *               This will be called immediately following the DataManager callback.
        */
        load: function(obj){
            if(!isset(obj.params)){
                _e('no parameters passed.  Invalid query.');
                return false;
            }
            if(!isset(obj.params.extid)){
                obj.params.extid = (new Date()).getTime();
            }
            var doCallback = function(fnID, fnStore, records, options, success){
                if(!isset(Callbacks[fnID])){
                    _e('Invalid callback id referenced');
                    return;
                }
                Callbacks[fnID].count--;
                if(Callbacks[fnID].count <= 0){
                    NNLM.Rosters.DisplayManager.notify('wait',{wait:false});
                    Callbacks[fnID].fn(records, options, success);
                }
                else{
                    _dump('load finished - no callback.  ' + Callbacks[fnID].count + ' remaining,');
                }
            };
            var storesToLoad = (obj.storeName !== undefined) ? [obj.storeName.toString()] : jQuery.keys(this.STORES);
            var toLoadCount = storesToLoad.length;
            _dump("Count of stores to be loaded: " + toLoadCount);
            var fnID = new Date().getTime().toString().substr(4) +
            Math.random().toString().replace(".", '').substr(0,4);
            Callbacks[fnID] = {
                count: toLoadCount,
                fn: isset(obj.callback) ? obj.callback : function(){_dump('default function');},
                success: true
            };
            //multi store load below here.
            var myscope = isset(obj.scope) ? obj.scope: this;
            NNLM.Rosters.DisplayManager.notify('wait',{
                wait:true,
                text:"A moment while the full record is fetched from the server..."
            });
            Ext.each(storesToLoad, function(name){
                obj.params.extid = (new Date()).getTime();
                _dump("loading store " + name);
                var getInfo = (function(myId, myStore){
                    return {id: myId, store: myStore};
                }).createDelegate(myscope, [fnID, name]);
                NNLM.Rosters.DataManager.STORES[name].load({
                    params: NNLM.Rosters.DataManager.createQuery(obj.params),
                    scope: myscope,
                    callback: function(records, options, success){
                        var info = getInfo();
                        delete getInfo;
                        doCallback(info.id, info.store, records, options, success);
                    },
                    add: true
                });
            });
        }
    };
    result.init();
    return result;
};
NNLM.Rosters.dump("Data Manager loaded.");
