/**
utility script to convert other request parameters to people_id
*/
require_once("env.php");
require_once("init.php");
require_once("utils.php");
require_once("sqlnobody.php.inc");
require_once("EchoRequest.servlet.php");
function perform_PID_conversion(){
    $optional_parameters = array('nnlm_user');
    foreach($optional_parameters as $tmp){
        if(@isset($_REQUEST[$tmp])){
            EchoRequest::write_raw_to_terminal("Translating optional parameter $tmp to People_ID");
            $param = $tmp;//record index value of parameter passed.
            $result = mysql_query("
            SELECT p.People_ID
            FROM general.people p
            WHERE p.$param = '$_REQUEST[$tmp]'", $link) or err(SQL_ERR);
            list($people_id) = mysql_fetch_row($result);
            mysql_free_result($result);
            $_REQUEST['People_ID'] = $people_id;
            unset($_REQUEST[$tmp]);
        }
    }
}
