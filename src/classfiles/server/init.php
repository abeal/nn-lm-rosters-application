<?php
require_once('env.php');
require_once('utils.php');
/** @var DEBUG (int) One of three possible value
*    0=no UF::debugging
*     1=Any query strings being executed are displayed.
*    2=query, function outputs, POST input
*    3=query, function outputs, and all flow control display statements.
*/
define("DEBUG", 3);

/**
* init.php
* Constant definitions and initialization for mapping application.
* <hr>
*
* This file is intended for use with the NNLM Rosters application.
* @author Aron Beal <abeal@u.washington.edu>
* @version 1.1
*/
/*
#############################################################
Set all environment variables for staff, debug, etc.
Include all necessary class definition
#############################################################
*/
ini_set("register_globals", 0);
ini_set("output_buffering", 1);
ini_set("display_errors", 0);
$_SERVER['SERVER_NAME'] = isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : 'dev.staff.nnlm.gov';
$_SERVER['REMOTE_ADDR'] = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : false;
//require_once('sqlnobody.php.inc');
/** @var DEV (bool) A "DEV" build is a stable build that exists on the dev server, and
* should be considered promotable anytime. (Note: the above two
* features are hoped to be phased out in favor of cvs or subversion.) */
define("DEV", (preg_match("/dev\.(staff\.)?nnlm\.gov/",
    $_SERVER['SERVER_NAME'])) ? true : false);
define("STAFF", preg_match("/(dev\.)?staff\.nnlm\.gov/",
    $_SERVER['SERVER_NAME']));
define("URIBASE", ((STAFF) ? "https://" : "http://") .
    $_SERVER['SERVER_NAME'] . preg_replace("/[^\/]+$/",
    "", $_SERVER['PHP_SELF']));
/**
Environment variables and default
****************************
*/
if(DEV && STAFF){
    // /staff.nnlm.gov/ copy
    define("NNLM_AUTH_KEY", md5("nnlmmaps".$_SERVER['REMOTE_ADDR']));
}
else if(!DEV && STAFF){
    //india's /i3/ copy
    define("NNLM_AUTH_KEY", md5("nnlmmaps".$_SERVER['REMOTE_ADDR']));
}
else{
    //hotel's copy.
    define("NNLM_AUTH_KEY", false);
}
if(DEBUG > 0){
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
}
$_ENV['debug'] = DEBUG; //for debugging class recognition

//default libid:
$libids = (isset($_GET['libid'])) ? $_GET['libid'] : 'WAUWAS';
/*
function dl_once($dobjname){
    //DEPRECATED
    //warning: linux based; needs to be .dll in windows systems.
    //$suffix = ".so";
    //if(!extension_loaded($dobjname)){dl($dobjname.$suffix);}
}
*/
?>
NNLM.set('DEBUG','<?php print DEBUG; ?>');
NNLM.set('DEV','<?php print DEV; ?>');
NNLM.set('STAFF', <?php print STAFF; ?>);
NNLM.set('PeopleID', -1);

/* 'NNLM_AUTH_KEY': '<?php print NNLM_AUTH_KEY; ?>',
 'URIBASE': '<?php print $_ENV['url']; ?>',
 'PeopleID': false */
<?php
if(isset($_SERVER['PHP_AUTH_USER'])){
    require_once('MySQLi.php.inc');
    $mysqli = MySQLiC::getDBConnection('nobody');
    $query_string = "
    SELECT p.PeopleID
    FROM rosters.people p
    INNER JOIN http_auth.users u
    ON (u.People_ID = p.PeopleID)
    WHERE u.user  = '$_SERVER[PHP_AUTH_USER]'";
    $result = $mysqli->query($query_string);
    list($pid) = $result->fetch_row();
    $mysqli->close();?>
    NNLM.set('PeopleID', '<?php print $pid; ?>');
    <?php
}
?>
