
<?php
/* GetAccountTypes.servlet.php
Retrieves a list of possible rosters accounts
*/
require_once("../utils.php");
$query_string = "
    SELECT AccountTypeID,
        AccountType,
        AccountTypeDescription,
        AccountImageIcon
    FROM ".DATABASE.".account_types 
	ORDER BY AccountTypeDescription";
Utils::runQueryAndWriteOutput($query_string);
?>
