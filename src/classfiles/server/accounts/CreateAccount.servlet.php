<?php
/** Note: this servlet is required to make decisions about what
the 'proper' information is to be returned.  It will be an amalgam
of information from both the general and the docline databases.  The rule
is, the general database takes precedence unless no information is present for
a person, in which case the docline database information will be used as
a default.
*/
require_once("../utils.php");
require_once('MySQLi.php.inc');
$required_parameters = array(
    'PeopleID'=>"/NUMERIC/",
    'AccountTypeID'=>"/NUMERIC/",
    'AccountVisibility'=>"/(public|intranet)/",
    'AccountValue'=>"/COMPLEXTEXT/",
	'AccountComments'=>"/COMPLEXTEXT/"
);
/*
+---------------+---------------------------+
| AccountTypeID | AccountTypeDescription    |
+---------------+---------------------------+
|             1 | Drupal Account            | 
|             3 | Intranet/Extranet Account | 
|             5 | NN/LM Shell Account       | 
|             7 | Email Address             | 
|             9 | Phone Number              | 
|            11 | Fax Number                | 
|            13 | NN/LM MySQL Account       | 
|            15 | Facebook Username         | 
|            17 | Skype Username            | 
|            19 | Twitter Username          | 
|            21 | Wordpress URL             | 
|            23 | LinkedIn Username         | 
|            25 | Staff Wiki Account        | 
|            27 | Moodle Account            | 
|            29 | Google+ Account           | 
+---------------+---------------------------+
*/
function processAccount($l){
	global $required_parameters;
	Utils::sanity_check($l, $required_parameters);
	foreach(array('PeopleID', 'AccountTypeID', 'AccountVisibility', 'AccountValue') as $k){
		if(!isset($l[$k])){
			Utils::err(BAD_REQ);
		}
	}
	dump("new account parameters are sane.");
	$fields = Utils::getFields($l, 'accounts');
	//first, check for this position's existence for this position.  Duplicates not allowed.
	$count = Utils::simpleQuery("
	    SELECT COUNT(*)
	    FROM ".DATABASE.".people
	    WHERE PeopleID = '$fields[PeopleID]'");
	if($count === 0){
	    Utils::err(INVALID_REQ, "The person '$fields[PeopleID]' does not exist.");
	}
	dump("new account person exists.");
	list($count, $description) = Utils::singleRowQuery("
	    SELECT COUNT(*), at.AccountTypeDescription
	    FROM rosters.accounts a
		INNER JOIN rosters.account_types at
		ON (at.AccountTypeID = a.AccountTypeID)
	    WHERE a.PeopleID = '$fields[PeopleID]'
		AND a.AccountTypeID = '$fields[AccountTypeID]'
		AND at.Singleton IS TRUE", false);
	if($count != 0){
	    Utils::err(DUPLICATE, "Only one account of type '$description' is allowed per individual.  Account was not added.");
		return;
	}
	$dupes = array();
	foreach(array_keys($fields) as $k){
		array_push($dupes, "$k = '$fields[$k]'");
	}
	$query_string = "
	    INSERT INTO ".DATABASE.".accounts
	    (" . implode(', ', array_keys($fields)) . ")
	    VALUES ('" . implode("', '", $fields) . "')
		ON DUPLICATE KEY UPDATE " . implode(',', $dupes);
	dump("Query: " . $query_string);
	$mysqli = MySQLiC::getDBConnection('updater');
	if(!$mysqli->query($query_string)){
	    dump("Warning: query failed: " . $mysqli->error);
	    Utils::err(BAD_REQ, $mysqli->error);
	}
	$affected = ($mysqli->affected_rows == 0) ? false : $mysqli->insert_id;
	dump("Affected rows: " + $mysqli->affected_rows. ", id: $affected");
	$mysqli->close();
	dump("insertion successful.");
    Utils::logTransaction('accounts', $fields['PeopleID'], 'CREATE', "account addition: $l[AccountValue]");  
  	$type = $fields['AccountTypeID'];         
	$value = $fields['AccountValue'];
	/*if($type == 'nnlm_user' && $value != ''){          
		$query_string = "INSERT INTO http_auth.users u (user, AccountTypeID, AccountVisibility, AccountValue
		WHERE People_ID = $pid AND user = '$value' LIMIT 1";
		dump("Removed http_auth account with value $value");
	}*/     
	return $affected;
}

/* Begin processing */
$request = Utils::getRequest();
dump("Create request.  Data: ");
dump($request);
$ids = array();
if(Utils::is_assoc($request)){
	$ids []= processAccount($request);
}
else{
	for($i = 0; $i < count($request); $i++){
		$ids []= processAccount($request[$i]);
	}
}
$qsc = array();
for($i = 0; $i < count($ids); $i++){
	if($ids[$i] == false){
		continue;
	}
	$qsc []= 'AccountID = '.$ids[$i];
}
$query_string = "SELECT a.AccountID,
	a.PeopleID,
	a.AccountTypeID,
	a.AccountVisibility,
	a.AccountValue,
	a.AccountComments,
	at.AccountType,
	at.Singleton,
	-1 as AccountVerified
FROM ".DATABASE.".accounts a
INNER JOIN ".DATABASE.".account_types at
ON (at.AccountTypeID = a.AccountTypeID) 
WHERE " . implode(' OR ', $qsc);
$output = array('success'=>true);
$mysqli = MySQLiC::getDBConnection();
if($result = $mysqli->query($query_string, MYSQLI_STORE_RESULT)){
    dump("Num rows: " . $result->num_rows);
    while ($row = $result->fetch_assoc()) {
		$row['Locker'] = ($row['ListID'] == $mylid) ? true : false;
		Utils::verifyAccount($row);
        $output[]= $row;
    }
}
else{
    dump("Warning: query failed: " . $mysqli->error);
    $mysqli->close();
    Utils::err(BAD_REQ);
}
$mysqli->close();
Utils::write_array($output);
?>
