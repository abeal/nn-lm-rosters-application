<?php
/* LoadPosition.servlet.php
Retrieves a list of accounts using a People ID
*/
require_once("../utils.php");
require_once('MySQLi.php.inc');

/* begin processing */
$required_parameters = array(
    'AccountID'=>"/NUMERIC/",
    'query'=>"/NUMERIC/"
);
$count = 0;
function processAccount($aID){
	//position delete request, delete by role type
	global $count;
	$mysqli = MySQLiC::getDBConnection();
	$type = 'unknown';
	if(!is_numeric($aID)){
		Utils::err(BAD_ARG);
	}
	$query_string = "SELECT a.PeopleID, at.AccountType, a.AccountValue
		FROM rosters.account_types at
	    INNER JOIN rosters.accounts a
	    ON (at.AccountTypeID = a.AccountTypeID)
	    WHERE AccountID = '$aID' LIMIT 1";          
	$row = Utils::singleRowQuery($query_string, false);    
	if(!$row){               		
		Utils::err(NO_RES);
	}
	list($pid, $type, $value) = $row;
	$mysqli = MySQLiC::getDBConnection('master');
	dump("Deleting account id $aID");
	$query_string = "DELETE FROM rosters.accounts WHERE AccountID = '$aID'";
	dump("Query string: $query_string");
	if(!$mysqli->query($query_string)){
	    Utils::err(SQL_ERR, $mysqli->error);
	}
	else{
	    Utils::logTransaction('accounts', $pid, 'DELETE', "Account type removed: $type, Value: $value");
	}
	$count += $mysqli->affected_rows;  
	/*if($type == 'nnlm_user' && $value != ''){          
		$query_string = "DELETE FROM http_auth.users u WHERE People_ID = $pid AND user = '$value' LIMIT 1";
		dump("Removed http_auth account with value $value");
	} */            
	$mysqli->close();
	return true;
}

/* begin processing */
/* begin processing */

dump("Incoming request data:");
dump($_REQUEST);

$request = Utils::getRequest();
if(ctype_digit($request)){
	$request = array('AccountID'=>$request);
}
if(Utils::is_assoc($request)){
	if(isset($request['query'])){
    	$request['AccountID'] = $request['query'];
	}
	processAccount($request['AccountID']);
	Utils::simple_response(true);
}
else{
	foreach($request as $AccountID){
		processAccount($AccountID);
	}
	Utils::simple_response(true);
}
Utils::err(BAD_REQ, "Unrecognized request value: ".print_r($request, true).".");




/* Possibly considering adding code in here later to make modifications
  to the http_auth table if the account being removed is an intranet/
  extranet account. */

?>
