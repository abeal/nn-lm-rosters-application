<?php
/* LoadPosition.servlet.php
Retrieves a list of positions using a People ID
*/
require_once("../utils.php");
require_once('MySQLi.php.inc');
$required_parameters = array(
    'PeopleID'=>"/NUMERIC/",
    'AccountID'=>"/NUMERIC/",
    'AccountTypeID'=>"/NUMERIC/",
    'AccountTypeDescription'=>"/TEXT/"
);
dump("Incoming request data:");
dump($_REQUEST);

$request = Utils::getRequest();
Utils::sanity_check($request, $required_parameters);
$query_string = "
    SELECT
        a.AccountID,
        a.PeopleID,
        a.AccountTypeID,
        a.AccountVisibility,
        a.AccountValue,
		a.AccountComments,
		at.AccountType,
		at.Singleton,
		-1 as AccountVerified
    FROM rosters.accounts a
	INNER JOIN rosters.account_types at
	ON (at.AccountTypeID = a.AccountTypeID)
    INNER JOIN rosters.people p
        ON (p.PeopleID = a.PeopleID)
    WHERE p.PeopleID = '$request[PeopleID]'
";     
//Utils::runQueryAndWriteOutput($query_string);
$output = array('success'=>true);
$mysqli = MySQLiC::getDBConnection();
if($result = $mysqli->query($query_string, MYSQLI_STORE_RESULT)){
    dump("Num rows: " . $result->num_rows);
    while ($row = $result->fetch_assoc()) {
		$row['Locker'] = ($row['ListID'] == $mylid) ? true : false;
		Utils::verifyAccount($row);
        $output[]= $row;
    }
}
else{
    dump("Warning: query failed: " . $mysqli->error);
    $mysqli->close();
    Utils::err(BAD_REQ);
}
$mysqli->close();
Utils::write_array($output);        	
?>
