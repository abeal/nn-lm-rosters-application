<?php

require_once("../utils.php");
require_once('MySQLi.php.inc');

$required_parameters = array(
    'AccountID'=>"/NUMERIC/"
);

/* check to insure the position exists */
function processAccount($request){
	Utils::sanity_check($request, 'accounts');
	$count = Utils::simpleQuery("SELECT COUNT(*)
	FROM ".DATABASE.".accounts
	WHERE AccountID = '$request[AccountID]'");
	if($count == 0){
	    Utils::err(INVALID_REQ, "The individual with AccountID '$request[AccountID]' does not exist.");
	}
	else{
	    dump("AccountID $request[AccountID] found.  Modifying...");
	}
	/* position does exist - save the changes to it. */
	$tmp = array();
	foreach(Utils::getFields($request, 'accounts') as $k=>$v){
	    if($k == 'AccountID'){continue;} //won't need to update the primary key, obviously.
	    if($v == ''){
	        $tmp []= "$k=NULL";
	    }
	    else{
	        $tmp []= "$k='$v'";
	    }
	}
	//add a last updated date   
	$pid = Utils::simpleQuery("SELECT PeopleID FROM rosters.accounts WHERE AccountID = $request[AccountID]");
	$query_string = "
	    UPDATE ".DATABASE.".accounts
	    SET " . implode(', ', $tmp).
	    " WHERE AccountID = '$request[AccountID]'";
	$mysqli = MySQLiC::getDBConnection('updater');
	dump("query string: $query_string");
	if(!$mysqli->query($query_string)){
	    dump($mysqli->error);
	    Utils::err(SQL_ERR, $mysqli->error);
	}
	else{
	    dump("query executed successfully.");
	    Utils::logTransaction('accounts', $pid, 'UPDATE', "Account ID updated: $request[AccountID]");
	}
	$response = ($mysqli->affected_rows != 0) ? true : false;
	dump("Affected rows: ".$mysqli->affected_rows);
	$mysqli->close(); 
	//update http_auth tables if applicable.

		if($type == 'nnlm_user'){
			dump("NNLM ACCOUNT");
		}
}
$request = Utils::getRequest();
dump("Decoded request data:");
dump($request);
if(Utils::is_assoc($request)){
	if(isset($request['query'])){
    	$request['AccountID'] = $request['query'];
	}
	processAccount($request);
	$query_string = "SELECT 
        a.AccountID,
        a.PeopleID,
        a.AccountTypeID,
        a.AccountVisibility,
        a.AccountValue,
		a.AccountComments,
		at.AccountType,
		at.Singleton,
		-1 as AccountVerified
		FROM ".DATABASE.".accounts a
		INNER JOIN rosters.account_types at
		ON (at.AccountTypeID = a.AccountTypeID)
		WHERE AccountID = $request[AccountID]"; //output completed update
}
else{
	$aids = array();
	foreach($request as $r){
		$aids []= 'AccountID = '.$r['AccountID'];
		processAccount($r);
	}
	$query_string = "SELECT 
        a.AccountID,
        a.PeopleID,
        a.AccountTypeID,
        a.AccountVisibility,
        a.AccountValue,
		a.AccountComments,
		at.AccountType,
		at.Singleton,
		-1 as AccountVerified
		FROM ".DATABASE.".accounts a
		INNER JOIN rosters.account_types at
		ON (at.AccountTypeID = a.AccountTypeID)
		WHERE " . implode(' OR ', $aids); //output completed update
}
$output = array('success'=>true);
$mysqli = MySQLiC::getDBConnection();
if($result = $mysqli->query($query_string, MYSQLI_STORE_RESULT)){
    dump("Num rows: " . $result->num_rows);
    while ($row = $result->fetch_assoc()) {
		$row['Locker'] = ($row['ListID'] == $mylid) ? true : false;
		Utils::verifyAccount($row);
        $output[]= $row;
    }
}
else{
    dump("Warning: query failed: " . $mysqli->error);
    $mysqli->close();
    Utils::err(BAD_REQ);
}
$mysqli->close();
Utils::write_array($output);
?>
