<?php
require_once("env.php");
require_once("init.php");
require_once("utils.php");
$foo = array("foo", "bar", "baz");

/* tpl corresponds to an Ext x-template that is used
as a preferred format to display the data on the client side. */

header("Content-type: text/xml");
$oXMLout = new XMLWriter();
$oXMLout->openMemory();
$oXMLout->startElement("results"); //<results>
$oXMLout->writeElement("count", count($foo)); //<count>$num</count>
foreach($foo as $v){
	$oXMLout->startElement("row");
	$oXMLout->writeElement("foo", $v); //<foo>$v</foo>
	$oXMLout->endElement();
}
$oXMLout->endElement(); //</results>
print $oXMLout->outputMemory();
?>
