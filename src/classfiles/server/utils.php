<?php
/*
* utils.php
* utility functions common to all servlets.
*/



require_once ("env.php");
require_once ('MySQLi.php.inc');
MySQLiC::useSingleServer();
require_once ("scripts/Exceptions.php");
require_once ("scripts/EchoRequest.servlet.php");
require_once ('scripts/rosters_stored_procedures.class.php');
EchoRequest::setClassesOfInterest('CurrentUser');

/**
 * handles all php errors
 *
 * @param $errno {Int}
 *		The error number
 *	@param $errst {String}
 *		The string error message
 *	@param $errline
 *		The line number where the error occurred.
 */
function error_handler($errno, $errstr = 'n/a', $errfile = 'n/a', $errline = 'n/a') {
  dump("ERROR: $errstr");
  $bt = array_pop(debug_backtrace());
  $err = array(
    'extid' => Utils::getExtID(),
    'success' => FALSE,
    'error' => $errstr,
    'file' => $bt['file'],
  );
  Utils::write_array($err, TRUE);
  die();
}

/**
 * handles all php exceptions
 *
 * @param $e {Exception}
 *		The exception that was thrown.
 */
function exception_handler($e) {
  dump("ERROR: " . $e->getMessage());
  $bt = array_pop(debug_backtrace());
  $err = array(
    'extid' => Utils::getExtID(),
    'success' => FALSE,
    'error' => $e->getMessage(),
    'file' => $e->getFile(),
    'line' => $e->getLine(),
  );
  $err = json_encode($err);
  header("Content-type: application/json");
  print $err;
  die();
}
set_exception_handler("exception_handler");
set_error_handler("error_handler", E_ERROR);

define("DEV", FALSE);
define("NO_RES", 1);
define("SQL_ERR", 2);
define("NO_DATA_SENT", 3);
define("GEN_ERR", 4);
define("BAD_REQ", 5);
define("INVALID_REQ", 6);
define("ERR", 7);
define("ACCESS_RESTRICTED", 8);
define("OUTPUT_TYPE", "JSON");
define("DATABASE", "rosters");

/**
 * writes content to port 50000, which can be then listened to
 *
 *	See the EchoRequest class for more details.
 *	@param $msg {string}
 *		The message to display
 *	@param $src {string}
 *		The message source file.
 */
function dump($msg, $src = FALSE) {
  if (!DEV) {
    return;
  }
  $src = ($src == FALSE) ? basename($_SERVER['SCRIPT_FILENAME']) : $src;
  static $time = 0;
  $bt = array_pop(debug_backtrace());
  if (is_array($msg)) {
    EchoRequest::writeArrayToTerminal($msg, $src);
  }
  else {
    EchoRequest::writeToTerminal('Line ' . $bt['line'] . ': ' . $msg, $src);
  }
}
dump('Bootstrapping UTILS');

/**
 *	The Utils class is responsible for all the functions that
 * are common between the various rosters servlets.  It handles things
 *	like the code to write content in the proper output format,
 * as well as executing simple queries of the database.
 */
class Utils {
  private static $json_query = NULL;
  private static $json_params = NULL;
  private static $json_extid = NULL;
  private static $pid = NULL;
  private static $lid = NULL;
  public static function escape_array(&$mixed) {
    if (!is_array($mixed)) {
      throw new Exception("wrong data type type passed to escape_array function: " . gettype($mixed));
    }
    $mysqli = MySQLiC::getDBConnection('inserter');
    if (self::is_assoc($mixed)) {
      foreach ($mixed as $k => $v) {
        $mixed[$k] = $mysqli->real_escape_string($v);
      }
    }
    else {
      for ($i = 0; $i < count($mixed); $i++) {
        $mixed[$i] = $mysqli->real_escape_string($mixed[$i]);
      }
    }
    $mysqli->close();
    return $mixed;
  }
  public static function escape_string($mixed) {
    if (is_array($mixed)) {
      throw new Exception("wrong data type type passed to escape_string function: " . gettype($mixed));
    }
    $mysqli = MySQLiC::getDBConnection('inserter');
    $result = $mysqli->real_escape_string($mixed);
    $mysqli->close();
    return $result;
  }

  /**
   * err
   * causes an error message to be returned from the server
   *
   * @param $case {int} the type of error being returned.
   * Valid types are (constant values):
   * NO_RES: no results were found for the parameters sent.  Note that this is
   * a successful query.
   * SQL_ERR: A problem exists with the SQL query that was executed.
   * NO_DATA_SENT: No data was sent from the client.
   * BAD_REQ: An invalid parameter was supplied, or an invalid value for a correct
   * parameter.
   * GEN_ERR: Some other type of error not covered under these other categories
   * has occurred.
   * @param $msg {string} more information about the nature of the error, if any
   * exists.
   *
   * @return {special} Executing this function will halt processing. Does not
   * return.
   */
  public static function err($case = 7, $msg = "") {
    $errno = 0;
    $err = '';
    switch ($case) {
      case ACCESS_RESTRICTED:
        $err = 'You do not have sufficient permissions to view the requested record.';
        break;

      case ERR:
        $err = 'The following error has occurred:';
        break;

      case NO_RES:
        //create no leaf element
        $result = array(
          'success' => TRUE,
        );
        self::write_array($result);
        exit;
        break;

      case SQL_ERR:
        $err = addslashes("A mysql error has occurred: " . $msg);
        break;

      case NO_DATA_SENT:
        $err = 'No data was sent.  Did you enter something in the search box?';
        break;

      case BAD_REQ:
        $err = 'Bad request.';
        break;

      case DUPLICATE:
        $err = "";
        break;

      case GEN_ERR:
      default:
        $err = 'An undefined error has occurred.  This application cannot continue.';
    }
    if ($msg != "") {
      $err .= " $msg";
    }
    error_handler(0, $err);
    exit(1);
  }

  /**
   *  fetchRegion
   *	gets the region, given a 6 character LIBID
   *	Tests the general.regions table for an exact match, indicating an RML or National Center.  If
   *	not found, the libid can be assigned based on the first two character match from the DOCLINE
   *	database.
   * 	@param $LIBID {string} the six character DOCLINE libid to be looked up.
   */
  public static function fetchRegion($LIBID) {
    $exact = self::singleRowQuery("SELECT COUNT(*) FROM general.regions WHERE LIBID = '$LIBID'");
    if ($exact > 0) {
      return self::simpleQuery("SELECT Region_Code FROM general.regions WHERE LIBID = '$LIBID'");
    }
    $sec = substr($LIBID, 0, 2);
    return self::simpleQuery("SELECT REGION_CODE FROM docline.PUBLIC_STATE_ETC_CODES  WHERE STATE_ETC_CODE = '$sec'");
  }

  /**
   *  fetchAllRegions
   *	Returns a list of all regions and region codes, zero-padded.
   * 	@param $LIBID {string} the six character DOCLINE libid to be looked up.
   */
  public static function fetchAllRegions($LIBID) {
    $results = array();
    // master
    //$query_string = "SELECT REGION_CODE, NAME FROM `docline`.`PUBLIC_REGION_CODES` ORDER BY REGION_CODE";
    // Supplemental
    $query_string2 = "SELECT Libid, Shortname from `general`.`regions` ORDER BY Regions_ID";
    // using elevated permissions to restrict ops to dbhost.
    $mysqli = MySQLiC::getDBConnection('inserter');
    if (!$result = $mysqli->query($query_string2)) {
      dump("An error has occurred in the fetchAllRegions function.");
      $err = $mysqli->error;
      $mysqli->close();
      throw new SQL_Error($err);
    }
    while (list($code, $name) = $result->fetch_row()) {
      //$code = str_pad($code, 2, '0', STR_PAD_LEFT);
      $results[$code] = $name;
    }
    $result->close();
    /*
		if(!$result = $mysqli->query($query_string)){
	        dump("An error has occurred in the fetchAllRegions function.");
			$err = $mysqli->error;
		    $mysqli->close();
	        throw new SQL_Error($err);
	    }
	    while(list($code, $name) = $result->fetch_row()){
			$code = str_pad($code, 2, '0', STR_PAD_LEFT);
	        // overrides any prior value.
	        $results[$code] = $name;
	    }    */





    return $results;
  }

  /**
   *	getCurrentUser
   *	@return {array} associative hash containing the rosters.people info for the current logged in user.
   */
  public static function getCurrentUser() {
    if (!isset($_SERVER['PHP_AUTH_USER'])) {
      throw new RostersException("Cannot determine the currently logged in user.  Does this user have an intranet/extranet account?  Is it valid?");
    }
    return self::singleRowQuery("
			SELECT p.* 
			FROM rosters.people p
			INNER JOIN rosters.accounts a
			ON (a.AccountTypeID = 3 AND a.PeopleID = p.PeopleID)
			WHERE a.AccountValue = '$_SERVER[PHP_AUTH_USER]'");
  }

  /**
   *	getCurrentUserID
   *	@return {int} the PeopleID of the currently logged in use.  Returns -1 if nobody is logged in.
   */
  public static function getCurrentUserID() {
    if (self::$pid != NULL) {
      return self::$pid;
    }
    if (!isset($_SERVER['PHP_AUTH_USER'])) {
      self::$pid = -1;
    }
    else {
      dump('current user: ' . $_SERVER['PHP_AUTH_USER']);
      $query_string = "
				SELECT a.PeopleID
				FROM rosters.accounts a
				WHERE a.AccountTypeID = 3 
				AND a.AccountValue = '$_SERVER[PHP_AUTH_USER]'";
      $pid = self::simpleQuery($query_string);
      dump("pid: " . $pid);
      if (ctype_digit($pid)) {
        self::$pid = $pid;
      }
    }
    return self::$pid;
  }

  /**
   * 	getCurrentUserLID
   *	@return {int} the personal locker id of the currently logged in user.  Returns -1 if nobody is logged in.
   */
  public static function getCurrentUserLID() {
    // TODO - fix this to work with http_auth instead.
    return - 1;
    if (self::$lid != NULL) {
      return self::$lid;
    }
    if (!isset($_SERVER['PHP_AUTH_USER'])) {
      self::$lid = -1;
    }
    else {
      self::$lid = self::simpleQuery("
		        SELECT p.MyListID 
				FROM rosters.people p
				INNER JOIN rosters.accounts a
				ON (p.PeopleID = a.PeopleID) 
				INNER JOIN rosters.account_types at 
				ON (at.AccountTypeID = a.AccountTypeID) 
				WHERE AccountType = 'nnlm_user' 
				AND AccountValue = '$_SERVER[PHP_AUTH_USER]'");
    }
    return self::$lid;
  }

  /**
   * getExtID
   *
   * @return the EXTID that is required by contract to be in every servlet request.
   * @throws exception if the id is not found.
   */
  public static function getExtID() {
    if (self::$json_extid != NULL) {
      return self::$json_extid;
    }
    if (isset($_REQUEST['extid']) && is_numeric($_REQUEST['extid'])) {
      self::$json_extid = $_REQUEST['extid'];
    }
    else {
      return NULL;
    }
    return self::$json_extid;
  }

  /**
   *  getFields
   *  retrieves the field names for the named table in the rosters database, and
   *  provides the caller with an array consisting of only those entries that match
   *  table name entries.
   *  @param $request_data_arr {associative array} the array containing the data
   *  to be returned
   *  @param $target_table {string} the name of the table in the rosters database
   * to query for field names
   *  @return {associative array} the entries in $request_data_arr whose keys
   *  match the field names in the target table. Outputs an error and
   *  kills processing if bad data is passed.
   **/
  public static function getFields($request_data_arr, $target_table) {
    $result = array();
    dump("target table: " . $target_table);
    $valid_field_keys = self::getValidFieldNames(trim($target_table));
    if (count($request_data_arr) == 0) {
      throw new UserDataException("No data was passed!");
    }
    if (count($valid_field_keys) == 0) {
      throw new RostersException("No valid field keys found for table $target_table!");
    }
    dump("Request array: ");
    dump(print_r($request_data_arr, TRUE));
    foreach ($request_data_arr as $k => $v) {
      dump("testing key $k: " . isset($v));
      if (array_key_exists($k, $valid_field_keys)) {
        $result[$k] = $v;
      }
    }
    if (count($result) == 0) {
      dump('Caution: No valid field names were supplied (valid fields: ' .
        implode(',', array_keys($valid_field_keys)) .
        '), (fields given: ' . implode(',', array_keys($request_data_arr)) . ').' .
        ' This may be ok.'
      );
    }
    //dump("final fields: " . print_r($result, true));
    return $result;
  }

  /**
   *	getRequest
   *	Parses the $_REQUEST array and returns a populated object with the correct variables.
   *	@return {object} an object containing the key=>value pairs present in the $_REQUEST
   *	array.  Barely any data sanitization is performed in this function; the individual servlets are
   *	still mostly responsible for that.  This does expand some shorthand, however; for the following values:
   *	v: Visibility.  Accepted values: p(ublic), i(ntranet)
   *	d: Display.  The requested output type for the person.  Accepted values: t(erse), s(hort), f(ull)
   *	s: Sort.  How the output is to be sorted.  ORDER MATTERS!
   *   Accepted values: r(egional), a(lphabetical), f(irstname), l(astname), p (listorder), q(listorder, reverse)
   **/
  public static function getRequest() {
    if (self::$json_query != NULL) {
      dump('already established query, returning...');
      return self::$json_query;
    }
    self::$json_query = array();
    dump("getRequest called.  Raw request data: " . print_r($_REQUEST, TRUE));
    if (isset($_REQUEST['query'])) {
      //remove once I have time to find all these 'query' tags on the client, and standardize them.
      $_REQUEST['request'] = $_REQUEST['query'];
    }
    if (isset($_REQUEST['results'])) {
      //remove once I have time to find all these 'query' tags on the client, and standardize them.
      $_REQUEST['request'] = $_REQUEST['results'];
    }
    if ($_REQUEST['request'] != '') {
      self::$json_query = json_decode($_REQUEST['request'], TRUE);
      if (is_null(self::$json_query) && is_string($_REQUEST['request'])) {
        dump("IS STRING");
        $_REQUEST['request'] = str_replace('""', '"', '{"query":"' . $_REQUEST['request'] . '"}');
        //TODO: add a sanitization for extid.
        self::$json_query = json_decode($_REQUEST['request'], TRUE);
      }
      elseif (is_int(self::$json_query)) {
        $_REQUEST['request'] = '{"query":' . $_REQUEST['request'] . '}';
        //TODO: add a sanitization for extid.
        self::$json_query = json_decode($_REQUEST['request'], TRUE);
      }
    }
    //eliminate all shorthand in the request.
    if (Utils::is_assoc(self::$json_query)) {
      foreach (self::$json_query as $k => $v) {
        switch ($k) {
          case 'v':
            //visibility
            unset(self::$json_query[$k]);
            switch ($v) {
              case 'p':
                self::$json_query['visibility'] = 'public';
                break;

              case 'i':
                self::$json_query['visibility'] = 'intranet';
                break;

              default:
                self::$json_query['visibility'] = $v;
            }
            break;

          case 'd':
            //display
            unset(self::$json_query[$k]);
            switch ($v) {
              case 't':
                self::$json_query['display'] = 'terse';
                break;

              case 's':
                self::$json_query['display'] = 'short';
                break;

              case 'f':
                self::$json_query['display'] = 'full';
                break;

              default:
                self::$json_query['display'] = $v;
            }
            break;

          case 's':
            //display
            unset(self::$json_query[$k]);
            self::$json_query['sort'] = $v;
            break;

          default:
        }
      }
    }
    dump("PROCESSED REQUEST DATA:");
    dump(self::$json_query);
    return self::$json_query;
  }

  /**
   *	getRequestParams
   *	returns an object containing any parameters passed in the server $_REQEST object
   *	Parameters are any portion of passed data that indicates how the data should be processed.  This is
   *	compared to request data, which indicates which data should be retrieved.  Examples of parameters
   *	might be display type, sort order, or fulltext searching requests.
   *	@return {array} an associative array containing any key=>value data pairs passed in the request params array.
   *	returns an empty array if nothing is found.
   */
  public static function getRequestParams() {
    if (self::$json_params != NULL) {
      return self::$json_params;
    }
    self::$json_params = (isset($_REQUEST['params']) ? json_decode($_REQUEST['params'], TRUE) : array());
    dump("PROCESSED PARAMS DATA:");
    dump(self::$json_params);
    return self::$json_params;
  }
  /* extracts the success variable from the result, and places it appropriately. */





  public static function _getSuccess(&$arr) {
    $success = (isset($arr['success'])) ? $arr['success'] : FALSE;
    unset($arr['success']);
    return $success;
  }

  /**
   *	getValidFieldNames
   *    helper function for retrieving field names from the rosters database.
   *    @param tablename {string} the name of the table to retrieve field names from.
   *    @return {array} an associative array with the keys being the field
   *    names,and the values being null. Triggers error message and kills processing
   *    in the event of an error.
   */
  public static function getValidFieldNames($tablename) {
    if (!preg_match("/^[a-z_]+$/", $tablename)) {
      throw new RostersException("getValidFieldNames: Illegal value supplied for table name: $tablename");
    }
    $mysqli       = MySQLiC::getDBConnection('inserter');
    $output       = array();
    $query_string = "
	    SELECT COLUMN_NAME, DATA_TYPE
	    FROM information_schema.COLUMNS
	    WHERE TABLE_SCHEMA = 'rosters' AND TABLE_NAME = '$tablename';
	    ";
    if (!$result = $mysqli->query($query_string)) {
      dump("An error has occurred in the GetValidFieldNames function.");
      $err = $mysqli->error;
      $result->close();
      $mysqli->close();
      throw new SQL_Error($err);
    }
    while (list($colname, $datatype) = $result->fetch_row()) {
      switch ($datatype) {
        case "tiyint":
        case 'smallint':
        case 'mediumint':
        case 'int':
        case 'bigint':
          $output[$colname] = '/NUMERIC/';
          break;

        case 'date':
          $output[$colname] = '/DATE/';
          break;

        case 'timestamp':
          $output[$colname] = '/TIMESTAMP/';
          break;

        default:
          $output[$colname] = '/COMPLEXTEXT/';
      }
    }
    $result->close();
    $mysqli->close();
    return $output;
  }

  /**
   * 	inline
   *   strips all tab and newline characters from the string.  Replaces multiple spaces with single ones.
   *	@param str {string} the string to replace.
   *	@return {string} the modified string.
   **/
  public static function inline($str) {
    return preg_replace("|[\s\n\t]+|", ' ', $str);
  }

  /** Utils::is_assoc
   *   returns whether or not the passed array is associative or normal.
   *   @param $_array {array} the array to be examined.
   *   @return {bool} true if the array is associative, false otherwise.
   */
  public static function is_assoc($array) {
    return (is_array($array) && 0 !== count(array_diff_key($array, array_keys(array_keys($array)))));
  }

  /** logTransaction
   * updates the transaction log with the passed parameters as entries.
   *
   * @param $tk {VARCHAR(25)} the name of the table modified
   * @param $mkv {MEDIUMINT(8) unsigned} the primary key of the
   * record modified.  Note that this record may not exist if the action take
   * was a DELETE operaion.
   * @param ActionTaken {ENUM('CREATE','UPDATE','DELETE')} The action
   * performed on the specified record.
   * See also http://en.wikipedia.org/wiki/Create,_read,_update_and_delete
   */
  public static function logTransaction($tm, $mkv, $at, $c = '') {
    $pid    = self::getCurrentUserID();
    $mysqli = MySQLiC::getDBConnection('updater');
    $c      = addSlashes($c);
    dump("
		Logging transaction.  Values:
			Updater ID      : $pid
			Table modified  : $tm
			Record modified : $mkv
			Action Taken    : $at
			Comments        : $c
		");
    $query_string = "INSERT INTO  " . DATABASE . ".transaction_log
		(ModifierID,TableModified,ModifiedID,ActionTaken,Comments)
		VALUES ('$pid','$tm','$mkv','$at','$c')
	    ON DUPLICATE KEY UPDATE
	        ModifiedID='$mkv',
	        ActionTaken='$at',
	            Comments='$c'";

    dump("TL query: $query_string");
    $id = FALSE;
    if ($mysqli->query($query_string)) {
      dump("Transaction log updated.");
      $id = $mysqli->insert_id;
    }
    else {
      dump("Warning: transaction log update failed: " . $mysqli->error);
    }
    $mysqli->close();
    return $id;
  }

  /** nameCrunch
   *  processes a textual string with the assumption that it is a user inputted
   *  person's name.  Attempts to extract name components based on general American
   *  English language assumptions.
   *  @param $query {string} the user provided string containing the person's name.
   *  @return {assoc. array} an array with keys corresponding to field names in the
   *  people table, and values being the function's best guess as to what portion,
   *  if any, of the supplied string represented that particular value.
   */
  public static function nameCrunch($query) {
    $result = array();
    dump("crunching name " . $query);
    $query = trim($query);
    if (strpos($query, ',')) {
      //1 comma: [honorific] lastname, firstname middle
      dump("comma split.");
      $parts = explode(',', $query);
      dump("Query parts:" . print_r($parts, TRUE));
      array_push($parts, array_shift($parts));
      dump("Query parts:" . print_r($parts, TRUE));
    }
    else {
      dump("nocomma");
      $parts = explode(' ', $query);
    }
    foreach ($parts as $k => $v) {
      $parts[$k] = trim($v);
    }
    //1: firstname only
    //2: first, then last name (commas already processed)
    //3: first, middle, last
    //4: prefix, first, middle, last (had to pick one; will mess up some cases)
    //5: prefix, first, middle, last, suffix
    //more: count only the first 5.

    dump("Query parts:" . print_r($parts, TRUE));
    switch (count($parts)) {
      case 1:
        $result['FirstName'] = trim($parts[0]);
        break;

      case 2:
        $result['FirstName'] = trim($parts[0]);
        $result['LastName'] = trim($parts[1]);
        break;

      case 3:
        $result['FirstName'] = trim($parts[0]);
        $result['MiddleName'] = trim($parts[1]);
        $result['LastName'] = trim($parts[2]);
        break;

      case 4:
      case 5:
      default:
        $result['Prefix'] = trim($parts[0]);
        $result['FirstName'] = trim($parts[1]);
        $result['MiddleName'] = trim($parts[2]);
        $result['LastName'] = trim($parts[3]);
        if (isset($parts[4])) {
          $result['Suffix'] = trim($parts[4]);
        }
    }
    dump("Name crunch results: (pref, first, mid, last, suf): \nPrefix: $result[Prefix]," .
      "\nFirst:  $result[FirstName],\nMiddle: $result[MiddleName], \nLast:   $result[LastName], \nSuffix: $result[Suffix]"
    );
    return $result;
  }

  /** runQueryAndWriteOutput
   *  convenience function to execute a query string with 'nobody' level
   *  privileges, and write the output (in JSON format) to the requesting
   *  browser.
   *  @param $query_string {string} the query to be executed.
   *  @return {special} halts execution upon completion.
   */
  public static function runQueryAndWriteOutput($query_string) {
    dump(self::getExtID());
    dump("RQAWO Query: $query_string");
    $output = array('success' => TRUE, 'results' => array(), 'extid' => self::getExtID(), 'count' => 0);
    // using elevated permissions to restrict ops to dbhost.
    $mysqli = MySQLiC::getDBConnection('inserter');
    if ($result = $mysqli->query($query_string, MYSQLI_STORE_RESULT)) {
      dump("Num rows: " . $result->num_rows);
      $output['count'] = $result->num_rows;
      while ($row = $result->fetch_assoc()) {
        $output['results'][] = $row;
      }
    }
    else {
      $err = $mysqli->error;
      $mysqli->close();
      throw new SQL_Error($err);
    }
    $mysqli->close();
    $output = json_encode($output);
    header("Content-type: application/json");
    dump("Server response: " . $output);
    print $output;
    exit;
  }

  /** sanity_check
   *  takes a data array, and purges it of key-value pairs that are not in the list of valid keys
   *  for the array.
   *  @param $request_data_array {array} the data array to be purged.
   *  @param $mixed {array|string} The valid keys.  If an array is passed, the keys
   *  will be valid value keys, and the values will be regular expressions to be used
   *  to validate the data present in the passed request_data_arr.
   *  If a string is passed instead, the string is assumed to be a tablename in
   *  the rosters database, and the valid keys are determined from fields in that
   *  table.
   *  @return {special} return value is void if there is no issue with the data.
   *  Otherwise, the function will trigger an error message to standard out and
   *  halt execution.
   */
  public static function sanity_check(&$request_data_arr, $mixed, $all_required = FALSE) {
    $valid_field_keys = FALSE;
    if (is_array($mixed)) {
      $valid_field_keys = $mixed;
    }
    // is string, otherwise
    elseif (is_string($mixed)) {
      $valid_field_keys = self::getValidFieldNames($mixed);
    }
    dump("Keys: " . print_r($valid_field_keys, TRUE));
    if (count($valid_field_keys) == 0) {
      throw new Exception("Sanity check left with nothing to work with.");
    }
    $anySaneRequest = TRUE;
    // always an allowed input.
    $valid_field_keys['extid'] = '/NUMERIC/';
    // use for escaping parameters.
    $mysqli = MySQLiC::getDBConnection('inserter');
    foreach ($request_data_arr as $k => $v) {
      $msg = "Checking key $k: ";
      //if the key exists in request_data_arr, it must match the pattern
      //in valid_field_keys
      if (array_key_exists($k, $valid_field_keys)) {
        if ($valid_field_keys[$k] == NULL) {
          $valid_field_keys[$k] = '/COMPLEXTEXT/';
        }
        if ($valid_field_keys[$k][0] != '/') {
          //value to be matched is a literal value.  Do direct comparison.
          if ($valid_field_keys[$k] != $request_data_arr[$k]) {
            throw new UserDataException("The supplied value " .
              "'$request_data_arr[$k]' for the key $k was not a valid value."
            );
          }
        }
        switch ($valid_field_keys[$k]) {
          case '/BOOLEAN/':
            //special case: peopleid on client is not known.
            $msg .= " (Boolean):";
            if (!is_bool($request_data_arr[$k])) {
              throw new UserDataException("The supplied value " .
                "'$request_data_arr[$k]' for the key $k was not a valid boolean value."
              );
            }
            break;

          case '/NUMERIC/':
            $msg .= " (Numeric):";
            //special case: peopleid on client is not known.
            if (!is_numeric($request_data_arr[$k])) {
              throw new UserDataException("The supplied value " .
                "'$request_data_arr[$k]' for the key $k was not a valid numeric value."
              );
            }
            break;

          case '/DATE/':
            $msg .= " (Date):";

            if ($request_data_arr[$k] == '') {
              $msg .= " WARNING: empty string - unsetting value";
              $request_data_arr[$k] = 'NULL';
            }
            else {

              dump("Original date value: " . $request_data_arr[$k]);
              $request_data_arr[$k] = preg_replace("/T.*$/", "", $request_data_arr[$k]);
              $converted = $request_data_arr[$k];
              dump("Converted value: " . $converted);
              if ($converted == FALSE) {
                throw new UserDataException("The supplied value " .
                  "'$request_data_arr[$k]' for the key $k was not a valid date value."
                );
              }
              $msg .= " NOTE: converted value $request_data_arr[$k] to date $converted";
              $request_data_arr[$k] = $converted;
            }
            break;

          case '/TEXT/':
            $msg .= " (Text):";
            if (!preg_match("/^[A-z0-9., ]*$/", $request_data_arr[$k])) {
              throw new UserDataException("The supplied value " .
                "'$request_data_arr[$k]' for the key $k was not a " .
                "valid text value."
              );
            }
            break;

          case '/COMPLEXTEXT/':
            $msg .= " (Complex Text):";
            $request_data_arr[$k] = $mysqli->real_escape_string($request_data_arr[$k]);
            dump($request_data_arr[$k]);
            break;

          case '/URL/':
            if (!preg_match("/(?i)\b((?:[a-z][\w-]+:(?:/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?«»“”‘’]))/", $request_data_arr[$k])) {
              throw new UserDataException("The supplied value " .
                "'$request_data_arr[$k]' for the key $k was not a " .
                "valid URL value."
              );
            }
            break;

          case '/TIMESTAMP/':
            if (strtotime($request_data_arr[$k]) === FALSE) {
              throw new UserDataException("The supplied value " .
                "'$request_data_arr[$k]' for the key $k was not a " .
                "valid timestamp."
              );
            }
            break;

          default:
            $msg .= " (Custom):";
            //validation match is normal regular expression
            if (!preg_match($valid_field_keys[$k], $request_data_arr[$k])) {
              throw new UserDataException("The supplied value " .
                "'$request_data_arr[$k]' for the key $k was not a " .
                "valid value."
              );
            }
        }

        $msg .= " OK";
        dump($msg);
      }
      else {
        dump("The key $k does not exist in the valid field keys array, skipping");
      }
    }
    $mysqli->close();
    // not required.
    unset($valid_field_keys['extid']);
    if ($all_required) {
      //The key in valid_field_keys *must* be present in the input array.
      foreach ($valid_field_keys as $k => $v) {
        if (!array_key_exists($k, $request_data_arr)) {
          throw new UserDataException("The required key $k was not found in this request.");
        }
      }
    }
    dump("Sanity check passed.  Keys tested: " . implode(", ", array_keys($valid_field_keys)));
    return TRUE;
  }

  /** simpleQuery
   * Same
   *
   * @param $query_string {string} the SQL string to execute. Should only have one value in SELECT clause.
   **/
  public static function simpleQuery($query_string) {
    try {
      // using elevated permissions to restrict ops to dbhost.
      $mysqli = MySQLiC::getDBConnection('inserter');
      if ($mysqli->errno) {
        throw new Exception($mysqli->error);
      }
      dump("Executing Simple Query: " . $query_string);
      if (!$result = $mysqli->query($query_string)) {
        $mysqli->close();
        return FALSE;
      }
      list($single) = $result->fetch_row();
      dump('Simple query result: ' . $single);
      $result->close();
      $mysqli->close();
      return $single;
    }
    catch(Exception$e) {
      throw new SQL_Error($e->getMessage());
    }
  }

  public static function simple_response($mixed) {
    $results = array('extid' => self::getExtID(), 'success' => TRUE, 'count' => 0, 'results' => NULL);
    switch (gettype($mixed)) {
      case 'boolean':
        $results['success'] = $mixed;
        break;

      case 'string':
        $results['message'] = $mixed;
        break;

      case 'integer':
        dump('integer: ' . $mixed);
        $results['count'] = $mixed;
        break;

      case 'array':
        dump('array');
        if (isset($mixed['error'])) {
          $results['message'] = $mixed['error'];
        }
    }
    $results = json_encode($results);
    if ($results['extid'] !== 'test') {
      header("Content-type: application/json");
    }
    dump("Server response: " . $results);
    print $results;
    exit(0);
  }

  /** 
   * Runs a simple query.  Returns a single row.
   *
   * @param $query_string {string} the sql to execute.
   * @param $assoc {boolean} whether the result should be an normal or associative array. (default is true - return assoc)
   **/
  public static function singleRowQuery($query_string, $assoc = TRUE) {
    try {
      // using elevated permissions to restrict ops to dbhost.
      $mysqli = MySQLiC::getDBConnection('inserter');
      if (!$m_result = $mysqli->query($query_string)) {
        return FALSE;
      }
      if ($assoc) {
        $result = $m_result->fetch_assoc();
      }
      else {
        $result = $m_result->fetch_row();
      }
      $m_result->close();
      $mysqli->close();
      return $result;
    }
    catch(Exception$e) {
      throw new SQL_Error($e->getMessage());
    }
  }
  public static function verifyAccount(&$row) {
    if ($row['Singleton']) {
      switch ($row['AccountType']) {
        case 'drupal':
          break;

        case 'nnlm_user':
          $query_string = "SELECT COUNT(DISTINCT user)
					FROM http_auth.users where user = '$row[AccountValue]'";
          dump('Query: ' . $query_string);
          $row['AccountVerified'] = Utils::simpleQuery($query_string);
          break;

        case 'shell':
          /*
				$connection = ssh2_connect('hotel.hsl.washington.edu', 22);
				if (!$connection) {
				  dump('Connection failed');
				}
				if ($connection) {
				  dump('Connection succeeded');
				}
				$connection->disconnect();*/
          break;

        case 'nnlm_mysql':
          $query_string = "SELECT COUNT(DISTINCT User)
					FROM mysql.user where User = '$row[AccountValue]'";
          dump('Query: ' . $query_string);
          $row['AccountVerified'] = Utils::simpleQuery($query_string);
          break;
      }
      dump('verifying ' . $row['AccountType'] . ' account with value ' . $row['AccountValue'] . ': $row[AccountVerified]');
    }
  }

  /** 
   * checks the system path, and determines which file would actually be included
   * by a require or include statement
   *
   * @param relative_path {string} the relative pathname of the file in question.
   * This can simply be a filename.
   *
   * @return {mixed} The string corresponding to the absolute path to the file
   * that would be referenced, or false, if no viable candidate file was found
   * in the system include path.
   */
  public static function which_file($relative_path) {
    foreach (explode(':', ini_get('include_path')) as $prefix) {
      $path = $prefix . '/' . $relative_path;
      if (file_exists($path)) {
        return $path;
      }
    }
    return FALSE;
  }
	/**
	*	writes an array to the output socket
	* (expecting standard XMLHttpRequest over
	*  port 80) in JSON format
	* @param $arr {a. array} The array to be written
	*	@return {void} (prints output to STDOUT)
	*/
  public static function write_array($arr, $nested = TRUE) {
    $results = array('success' => self::_getSuccess($arr));

    if ($results['success']) {
      if ($nested) {
        $results['count'] = count($arr);
        $results['results'] = array();
        foreach ($arr as $k => $v) {
          $results['results'][$k] = $v;
        }
      }
      else {
        foreach ($arr as $k => $v) {
          if (is_numeric($v)) {
            $v = integer($v);
          }
          $results[$k] = $v;
        }
      }
    }
    else {
      if (is_array($arr) && count($arr) != 0) {
        $results['results'] = $arr;
      }
      $results['count'] = 0;
    }
    $results['extid'] = self::getExtID();
    if (isset($arr['error'])) {
      $results['message'] = $arr['error'];
    }
    dump("Outputting results: " . json_encode($results));
    if ($results['extid'] !== 'test') {
      header("Content-type: application/json");
    }
    $results = json_encode($results);
    print $results;
  }
}
dump("\n*********************************************\n***************** NEW REQUEST ***************\n*********************************************");

