<?php
/* use reflection apis to establish class and function names, 
to be passed to application for RPC execution.
*/
require_once('utils.php');
$prefix = preg_replace("|[^/]+$|", '', __FILE__);
$filelist = array(
	"Edit.php",
	"Find.php",
	"Requester.php"
);
$API = array(); //used by client side, converted to json and send down the pipe.
$RPC = array(); // same contents, slightly different structure as API. Used by router.php
$pointer = false;
/* create a config object containing class and method names of 
select server side classes (RPC candidates) to pass as an argument later to 
the Ext.Direct.addProvider method. This will allow these remote methods
to be invoked from the client side in code.  
Note: we must buffer output so that javascript headers may 
be sent later.  We don't want to actually output anything required
in the below code.

We also here must create an $API object for use by the router.php file
that must direct calls to appropriate locations.
*/
ob_start();
foreach($filelist as $fil){
	$fil= $prefix . $fil;
	//$md5 = md5_file($fil);
	
	$name = preg_replace("|^.*/([^/]+)\.php$|", "$1", $fil);
	require_once($fil);
	$ref = new ReflectionClass($name);
	$methods = array();
	$RPC[$name]['methods'] = array();
	foreach($ref->getMethods() as $ReflectionMethod){
		$methods[] = array(
			'name'=>$ReflectionMethod->getName(),
			'len'=>$ReflectionMethod->getNumberOfRequiredParameters()
		);
		$RPC[$name]['methods'][$ReflectionMethod->getName()] = array(
			'len'=>$ReflectionMethod->getNumberOfRequiredParameters()
		);
	}
	$API[$name] = $methods;
}
ob_end_clean();
?>