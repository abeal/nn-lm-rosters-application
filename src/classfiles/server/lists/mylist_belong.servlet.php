<?php
require_once("../utils.php");
require_once('MySQLi.php.inc');
$required_parameters = array(
    'PeopleID'=>"/NUMERIC/",
	'query'=>"/NUMERIC/"
);

dump("Incoming request data:");
dump($_REQUEST);

$request = Utils::getRequest();
Utils::sanity_check($request, $required_parameters);
if(isset($request['query'])){
	$request['PeopleID'] = $request['query'];
}
if(!isset($request['PeopleID'])){
    $request['PeopleID'] = Utils::getCurrentUserID();
}
$query_string = Rosters_Stored_Procedures::getListAssignations($request['PeopleID'], 'intranet');
dump("Query string: $query_string");
Utils::runQueryAndWriteOutput($query_string);
?>
