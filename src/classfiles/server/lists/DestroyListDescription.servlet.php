<?php
/* LoadPosition.servlet.php
Retrieves a list of positions using a People ID
*/
require_once("../utils.php");
require_once('MySQLi.php.inc');
/* begin processing */
$required_parameters = array(
    'ListID'=>"/NUMERIC/"
);
dump("Incoming request data:");
dump($_REQUEST);

$request =  Utils::getRequest();

sanity_check($request, $required_parameters, true);


$query_string = "SELECT COUNT(*) FROM ".DATABASE.".list_descriptions WHERE ListID = '$request[ListID]'";
$mysqli = MySQLiC::getDBConnection();
if(!$result = $mysqli->query($query_string)){
    Utils::err(SQL_ERR, $mysqli->error);
}
list($count) = $result->fetch_row();
$result->close();
$mysqli->close();
if($count <= 0){
    Utils::err("List id $request[ListID] not found");
}
dump("Deleting list id $request[ListID]");

/* List ID is confirmed.  Delete both the entry in list_descriptions, and everything
  *in* the list (matching ListID entries in the rosters.lists table)*/
$mysqli = MySQLiC::getDBConnection('master');
$query_string = "DELETE FROM ".DATABASE.".list_descriptions WHERE ListID = '$request[ListID]'";
dump("Query String: $query_string");
$mysqli->query($query_string);
$affected = $mysqli->affected_rows;
dump("Affected rows: $affected");
if($affected <= 0){
    Utils::err(SQL_ERR, $mysqli->error);
}
else{
    $count+= $affected;
    //delete associated roles if successful.
    $query_string = "DELETE FROM ".DATABASE.".lists WHERE ListID = '$request[ListID]'";
    $mysqli->query($query_string);
    dump("Additional roles associated with listid $request[ListID] deleted: " .$mysqli->affected_rows);
}
$mysqli->close();
Utils::simple_response($count);
?>
