<?php
/** Note: this servlet is required to make decisions about what
the 'proper' information is to be returned.  It will be an amalgam
of information from both the general and the docline databases.  The rule
is, the general database takes precedence unless no information is present for
a person, in which case the docline database information will be used as
a default.
*/

require_once("../utils.php");
require_once('MySQLi.php.inc');

$required_parameters = array(
    'create'=>"/BOOLEAN/",
    'query'=>"/COMPLEXTEXT/",
	'PeopleID'=>"/NUMERIC/",
	'ListID'=>"/NUMERIC/",
    'ListTitle'=>"/COMPLEXTEXT/",
    'ListDescription'=>"/COMPLEXTEXT/"
);
$request = Utils::getRequest();

$mylid = Utils::getCurrentUserLID();
if(isset($request['query'])){
    if(is_numeric($request['query'])){
        $request['ListID'] = $request['query'];
    }
}
if(isset($request['ListID']) && $request['ListID'] === 'self'){
	$request['ListID'] = $mylid;
}
/* check for required parameters.
Note that variables are standardized as lowercase.*/
dump("Incoming request data:");
dump($request);
Utils::sanity_check($request, $required_parameters);
Utils::sanity_check($_REQUEST, array(
	'_searchMode'=>"/TEXT/", 
	'_start'=>'/NUMERIC/'));
$params = array_slice($_REQUEST, 0);
dump("Request parameters: ");
dump(print_r($params, true));


//Case 'query': coming from the find list dialog
if(isset($request['query'])){
	dump("searchmode: query");
	$request['queries'] = array();
	if($params['_searchMode'] == 'tight'){
		$request['queries'][]= '+'.implode(' +', explode(' ', $request['query']));
	}
	else{
		$request['queries'][]= '+' . implode('* +', explode(' ', $request['query']));
		$request['queries'][]= '+' . implode(' +', explode(' ', $request['query'])) . '*';
	}
	for($i = 0; $i < count($request['queries']); $i++){
		$request['queries'][$i] = "MATCH (ListTitle, ListDescription)
	    AGAINST('".$request['queries'][$i]."' IN BOOLEAN MODE)";
	}
   	$query_string = "
    SELECT
        ListID,
        ListTitle,
        ListDescription,
        ListVisibility,
        ListStartDate,
        ListEndDate
    FROM ".DATABASE.".list_descriptions
	WHERE (" .implode(' OR ', $request['queries']) . ")
	AND (ListVisibility <> 'private' OR ListID = $mylid)";
	Utils::runQueryAndWriteOutput($query_string);
	exit;
}
//This case may be obsolete.  I need to check for it in code when I get a chance.
else if(isset($request['queries'])){
	
	dump("searchmode: queries");
	$request['queries'] = array();
	if($params['_searchMode'] == 'tight'){
		$request['queries'][]= '+'.implode(' +', explode(' ', $request['ListTitle']));
	}
	else{
		$request['queries'][]= '+' . implode('* +', explode(' ', $request['ListTitle']));
		$request['queries'][]= '+' . implode(' +', explode(' ', $request['ListTitle'])) . '*';
	}
	for($i = 0; $i < count($request['queries']); $i++){
		$request['queries'][$i] = "MATCH (ListTitle, ListDescription)
	    AGAINST('".$request['queries'][$i]."' IN BOOLEAN MODE)";
	}
    $query_string = "
    SELECT
    ListID,
    ListTitle,
    ListDescription,
    ListVisibility,
    ListStartDate,
    ListEndDate
    FROM ".DATABASE.".list_descriptions
    WHERE (ListVisibility <> 'private' OR ListID = $mylid)
	AND (". implode(' OR ', $request['queries']) . ')';
	Utils::runQueryAndWriteOutput($query_string);
	exit;
}

//Direct request by ListID.  Run the query and do an access check on the results.
else if(isset($request['ListID'])){
	dump("searchmode: ListID");
       $query_string = "
       SELECT
           ListID,
           ListTitle,
           ListDescription,
           ListVisibility,
           ListStartDate,
           ListEndDate
       FROM ".DATABASE.".list_descriptions l
       WHERE (ListVisibility <> 'private' OR ListID = $mylid) 
		AND ListID = '$request[ListID]'";
}

//Direct request by PeopleID.  Run the query and do an access check on the results.
else if(isset($request['PeopleID'])){
	dump("searchmode: PeopleID");	
    $query_string = "SELECT
            l.ListID,
            l.ListTitle,
            l.ListDescription,
            l.ListVisibility,
            l.ListStartDate,
            l.ListEndDate
        FROM ".DATABASE.".list_descriptions l
        INNER JOIN ".DATABASE.".people p
        ON (p.MyListID = l.ListID)
        WHERE p.PeopleID = '$request[PeopleID]' AND (l.ListVisibility <> 'private' OR l.ListID = $mylid) ";
}
else if(isset($request['ListTitle'])){
	if($request['fulltext']){
		$request['ListTitle'] .= '*';
	}
	$query_string = "
		SELECT
	            l.ListID,
	            l.ListTitle,
	            l.ListDescription,
	            l.ListVisibility,
	            l.ListStartDate,
	            l.ListEndDate
	        FROM ".DATABASE.".list_descriptions l
        WHERE (ListVisibility <> 'private' OR ListID = $mylid) 
		AND MATCH(l.ListTitle, l.ListDescription)
        AGAINST ('$request[ListTitle]' IN BOOLEAN MODE)";
}
else{
	Utils::err(BAD_REQ, 'No known request type was passed to the LoadListDescription servlet');
}
dump("Executing query string: $query_string");
$output = array('success'=>true);
$mysqli = MySQLiC::getDBConnection();
if($result = $mysqli->query($query_string, MYSQLI_STORE_RESULT)){
    dump("Num rows: " . $result->num_rows);
    while ($row = $result->fetch_assoc()) {
		$row['Locker'] = ($row['ListID'] == $mylid) ? true : false;
        $output[]= $row;
    }
}
else{
    dump("Warning: query failed: " . $mysqli->error);
    $mysqli->close();
    Utils::err(BAD_REQ);
}
$mysqli->close();
Utils::write_array($output);
/*
 Example output:
{
	"success": true,
	"count": 3,
	"results": [
		{
			"ListID": "7683",
			"ListTitle": "Sascha Rogers's list",
			"ListDescription": null,
			"ListVisibility": "intranet",
			"ListStartDate": "2010-12-14",
			"ListEndDate": null
		},
		{
			"ListID": "4111",
			"ListTitle": "Rivkah Sass's list",
			"ListDescription": null,
			"ListVisibility": "intranet",
			"ListStartDate": "2010-12-14",
			"ListEndDate": null
		},
		{
			"ListID": "3031",
			"ListTitle": "Ann B. Sasser's list",
			"ListDescription": null,
			"ListVisibility": "intranet",
			"ListStartDate": "2010-12-14",
			"ListEndDate": null
		}
	],
	"extid": null
}

*/
?>
