<?php
/** Note: this servlet is required to make decisions about what
the 'proper' information is to be returned.  It will be an amalgam
of information from both the general and the docline databases.  The rule
is, the general database takes precedence unless no information is present for
a person, in which case the docline database information will be used as
a default.
*/
require_once("../utils.php");
require_once('MySQLi.php.inc');


$required_parameters = array(
    'ListID'=>"/NUMERIC/"
);

$request = Utils::getRequest();

Utils::sanity_check($request, 'list_descriptions');
dump("Decoded request data:");
dump($request);
Utils::sanity_check($request, $required_parameters, true);

/* check to insure the position exists */
$query_string = "
    SELECT COUNT(*)
    FROM ".DATABASE.".list_descriptions
    WHERE ListID = '$request[ListID]'";

$mysqli = MySQLiC::getDBConnection();
if(!$result = $mysqli->query($query_string)){
    dump("An critical error has occurred in the SaveListDescription servlet.");
    Utils::err(SQL_ERR, $mysqli->error);
}
list($count) = $result->fetch_row();
$result->close();
$mysqli->close();
if($count == 0){
    Utils::err(INVALID_REQ, "ListID '$request[ListID]' does not exist.");
}
else{
    dump("ListID $request[ListID] found.  Modifying...");
}
dump("start date: " . $request['ListStartDate']);
dump("end date: " . $request['ListStartDate']);
/* position does exist - save the changes to it. */
$changed = array();
foreach(Utils::getFields($request, 'list_descriptions') as $k=>$v){
    if($k == 'ListID'){
        continue; //won't need to update the primary key, obviously.
    }
    if($v == '' || $v == 'NULL'){
        $tmp []= "$k=NULL";
    }
    else{
        $tmp []= "$k='$v'";
    }
	$changed []= $k;
}

$query_string = "
    UPDATE ".DATABASE.".list_descriptions
    SET " . implode(', ', $tmp).
    " WHERE ListID = '$request[ListID]'";
$mysqli = MySQLiC::getDBConnection('updater');
dump("query string: $query_string");
if(!$mysqli->query($query_string)){
    dump($mysqli->error);
    Utils::err(SQL_ERR, $mysqli->error);
}
else{
    dump("query executed successfully.");
	$response = ($mysqli->affected_rows != 0) ? true : false;
    Utils::logTransaction('list_descriptions', $request["ListID"], 'UPDATE', 'Fields changed: ' . implode(', ', $changed));
}
dump("Affected rows: ".$mysqli->affected_rows);
$result->close();
$mysqli->close();
$query_string = "SELECT * FROM ".DATABASE.".list_descriptions WHERE ListID = $request[ListID]";
Utils::runQueryAndWriteOutput($query_string);
?>
