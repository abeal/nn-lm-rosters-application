<?php
require_once("../utils.php");
require_once('MySQLi.php.inc');
$required_parameters = array(
    'PeopleID'=>"/NUMERIC/",    
	'query'=>"/NUMERIC/"
);
$request = Utils::getRequest();
Utils::sanity_check($request, $required_parameters);
if(isset($request['query'])){
   $request["PeopleID"] = $request['query'];
}
if(!isset($request['PeopleID'])){
    $query_string = Rosters_Stored_Procedures::getCurrentUserList();
}
else{
	$query_string = Rosters_Stored_Procedures::getPersonList($request['PeopleID']);
}
dump("Query string: $query_string");
Utils::runQueryAndWriteOutput($query_string);
?>
