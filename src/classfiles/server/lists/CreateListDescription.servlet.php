<?php
require_once("../utils.php");
require_once('MySQLi.php.inc');
$required_parameters = array(
    'ListTitle'=>"/COMPLEXTEXT/",
    'ListDescription'=>"/COMPLEXTEXT/",
    'ListVisibility'=>"/^(public|intranet)$/",
    'ListStartDate'=>"/DATE/",
    'ListEndDate'=>"/DATE/"
);
$request =  Utils::getRequest();
Utils::sanity_check($request, 'list_descriptions');
dump("Creation data:", $me);
dump(print_r($request, true), $me);
$keys = array();
$values = array();
foreach(Utils::getFields($request, 'list_descriptions') as $k=>$v){	
	$keys []= $k;
    $values []= ($v == 'NULL') ? $v : "'$v'";
}
$query_string = "
    INSERT INTO ".DATABASE.".list_descriptions
    (" . implode(', ', $keys) . ")
    VALUES (" . implode(', ', $values) . ")";
dump("query string: $query_string");
$mysqli = MySQLiC::getDBConnection('inserter');
if(!$mysqli->query($query_string)){
    dump($mysqli->error);
    Utils::err(SQL_ERR, $mysqli->error);
}
else{
    dump("query executed successfully.");
}
$response = ($mysqli->affected_rows != 0) ? true : false;
dump("Affected rows: ".$mysqli->affected_rows);
$lid = $mysqli->insert_id;
Utils::logTransaction('list_descriptions', $lid, 'CREATE', "new list title: \"$fields[ListTitle]\".");
$mysqli->close();
//add a record of this new list to the user's locker.
Rosters_Stored_Procedures::addListToLocker($lid);    
$query_string = "
    SELECT *
    FROM ".DATABASE.".list_descriptions
    WHERE ListID = '$lid'";
Utils::runQueryAndWriteOutput($query_string);
?>
