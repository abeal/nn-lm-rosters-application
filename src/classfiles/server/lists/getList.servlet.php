<?php
require_once("../utils.php");
require_once('MySQLi.php.inc');
require_once('scripts/rosters_stored_procedures.class.php');
$required_parameters = array(
    'ListID'=>"/NUMERIC/",
	'Visibility'=>"/(public|intranet|private)"
);


//good to continue from this point on.
/* check for required parameters.
Note that variables are standardized as lowercase.*/
dump("Incoming request data:");
dump($_REQUEST);

$request = array('ListID'=>intval($_REQUEST['ListID']));
sanity_check($request, $required_parameters);
if(!isset($request['Visibility'])){
	$request['Visibility'] = 'public';
}
$output = array();
dump("decoded id: $request[ListID]");
if($request['ListID'] === -1){
	$output = '[]';
	dump("Server response: $output");
	header("Content-type: application/json");
	print $output;
	exit(1);
}
$mysqli = MySQLiC::getDBConnection();
$query_string = Rosters_Stored_Procedures::getList($request['ListID']);
if(!$result = $mysqli->query($query_string)){
    dump("An critical error has occurred in the getList servlet.");
    Utils::err(SQL_ERR, $mysqli->error);
}
while(list(
    $ListEntryID,
    $ListID,
    $ListMemberTable,
    $ListMemberTableID,
	$ListEntryStartDate,
	$ListEntryEndDate,
    $Notes,
    $DescriptiveText,
	$PositionTitle,
	$PositionEmail,
	$PositionRegion,
	$Visibility,
	$PHONE_COUNTRY_CODE_INST,
	$PHONE_AREA_CODE_INST,
	$PHONE_INST,
	$PHONE_EXT_INST,
	$INST_INST,
	$DEPT_INST) = $result->fetch_row()){
	if($Visibility != $request['Visibility']){
		continue;
	}
    switch($ListMemberTable){
    case 'people':
        $output []= "{id: \"$ListMemberTableID\", text:\"$DescriptiveText\", leaf:true}";
        break;
    case 'list_descriptions':
        $output []= "{id:\"$ListMemberTableID\", text:\"$DescriptiveText\"}";
        break;
    default:
        Utils::err(BAD_REQ);
    }
}
$result->close();
$mysqli->close();
$output = '['.implode(',', $output).']';
dump("Server response: ".$output);
header("Content-type: application/json");
print $output;
/*
 Example output for reference.  Note; this servlet does not generate
 list children directly; the client will do so through repeat calls.
[{
    id: 1,
    text: 'A person',
    leaf: true
},{
    id: 2,
    text: 'A list',
    children: [{
        id: 3,
        text: 'A person within the contained list',
        leaf: true
    }]
},{
    id: 3 ,
    text: 'A childless list'
}]
 */
?>
