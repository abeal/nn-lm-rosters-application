<?php
/** Note: this servlet is required to make decisions about what
the 'proper' information is to be returned.  It will be an amalgam
of information from both the general and the docline databases.  The rule
is, the general database takes precedence unless no information is present for
a person, in which case the docline database information will be used as
a default.
*/

require_once("../utils.php");
require_once('scripts/class.rosters.php');
$required_parameters = array(
    'ListID'=>"/NUMERIC/",
    'nolabels'=>"/^(true|false)$/",
    'terse'=>"/^(true|false)$/",
    'short'=>"/^(true|false)$/"
);

dump("Incoming request data:");
dump($_REQUEST);
$request = array();
foreach($_REQUEST as $k=>$v){
    if(!array_key_exists($k, $required_parameters)){
        continue;
    }
    $request[$k] = $_REQUEST[$k];
}
/* check for required parameters.
Note that variables are standardized as lowercase.*/
if(!Utils::sanity_check($request, $required_parameters)){
    ?>
    <h1>Error</h1>
    <p>The id submitted to this page for information retrieval was not valid.
    Please check your entry and try again.</p>
    <?php
}      
if(!$request['ListID'] == 'undefined'){
    ?>            
    <p class="metadata">Awaiting input.</p>
    <?php
}
if(!isset($request['nolabels'])){
    $request['nolabels'] = false;
}
$options = array('ListID'=>$request['ListID'],'s'=>'l','v'=>'i','f'=>'l');
?>
<!DOCTYPE html 
     PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">  
  <head>
    <title>Virtual Library</title>
  </head>
  <body>
<?php
Rosters::printList($options);
?>
  </body>
</html>