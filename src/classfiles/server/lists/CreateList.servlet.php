<?php

//this servlet is called via LoadList.servlet.php
require_once("../utils.php");
require_once('MySQLi.php.inc');
$required_parameters = array(
	'ListEntryID'=>'/NUMERIC/',
    'ListID'=>"/NUMERIC/",
    'ListMemberTable'=>"/(people|list_descriptions|libraries)/",
	'ListMemberTableID'=>'/^([A-z]{6}|\d+)$/',
	'ListEntryStartDate'=>'/DATE/',
	'ListEntryEndDate'=>'/DATE/',
	'Notes'=>"/COMPLEXTEXT/"
);

function processList($l){
	global $required_parameters;
	Utils::sanity_check($l, $required_parameters);
	//prevent adding a list to itself
	if($l['ListID'] == $l['ListMemberTableID'] && $l['ListMemberTable'] == 'list_descriptions'){
	    Utils::err(ERR, 'Cannot add a list to itself.');
	}
	$fields = Utils::getFields($l, 'lists');
	//make sure list exists.
	$count = Utils::simpleQuery("SELECT COUNT(*) FROM ".DATABASE.".list_descriptions WHERE ListID = $fields[ListID]");
	if($count <= 0){
	    Utils::err(GEN_ERR, 'The specified list does not exist.');
	}
	//beyond this point, go ahead and add to the existing list
	$query_string = "
	    INSERT INTO ".DATABASE.".lists
	    (" . implode(', ', array_keys($fields)) . ")
	    VALUES ('" . implode("', '", $fields) . "')";
	$mysqli = MySQLiC::getDBConnection('updater');
	if($mysqli->query($query_string)){
	    Utils::logTransaction('lists', $fields['ListID'], 'CREATE', "list addition: $l[DescriptiveText]");
	}
	else{
	    dump("Warning: query failed: " . $mysqli->error);
	    Utils::err(BAD_REQ, $mysqli->error);
	}
	$affected = ($mysqli->affected_rows == 0) ? false : $mysqli->insert_id;
	dump("Affected rows: " . $mysqli->affected_rows . ", leid: $affected");
	$mysqli->close();
	return $affected;
}
/* check for required parameters.
Note that variables are standardized as lowercase.*/
$request = Utils::getRequest();
dump("Create request.  Data: ");
dump($request);
$leids = array();
if(Utils::is_assoc($request)){
	$leids []= processList($request);
}
else{
	for($i = 0; $i < count($request); $i++){
		$leids []= processList($request[$i]);
	}
}
$qsc = array();
for($i = 0; $i < count($leids); $i++){
	if($leids[$i] == false){
		continue;
	}
	$qsc []= 'ListEntryID = '.$leids[$i];
}
$query_string = "SELECT * FROM ".DATABASE.".descriptiveList WHERE " . implode(' OR ', $qsc);
Utils::runQueryAndWriteOutput($query_string);
?>
