<?php
/* LoadPosition.servlet.php
Retrieves a list of accounts using a People ID
*/
require_once("../utils.php");
require_once('MySQLi.php.inc');

/* begin processing */
$required_parameters = array(
    'ListEntryID'=>"/NUMERIC/",
    'query'=>"/NUMERIC/"
);
function processList($ListEntryID){
	global $required_parameters;
	$ListEntryID = intval($ListEntryID);
	dump("Processing id $ListEntryID");
	if($ListEntryID == 0){
		Utils::err(BAD_ARG, 'not a valid integer');
	}
	$count = Utils::simpleQuery("
		SELECT COUNT(*)
		FROM ".DATABASE.".lists l
		WHERE l.ListEntryID = $ListEntryID");
	if($count < 1){
		dump("ListEntryID $ListEntryID not found!");
		Utils::err(NO_RES);
	}
	list($ListID, $ListMemberTable, $ListMemberTableID) = Utils::singleRowQuery("
		SELECT l.ListID, l.ListMemberTable, l.ListMemberTableID
		FROM ".DATABASE.".lists l
		WHERE l.ListEntryID = '$ListEntryID'
	", false);
	dump("Retrieved info for ListID $ListID: Table - $ListMemberTable, LMTID - $ListMemberTableID");
	switch($ListMemberTable){
	case 'people':
		$Description = Utils::simpleQuery("
			SELECT CONCAT(p.FirstName, ' ', p.LastName)
			FROM ".DATABASE.".people p
			WHERE p.PeopleID = $ListMemberTableID");
		break;
	case 'list_descriptions':
		$Description = Utils::simpleQuery("
			SELECT ld.ListTitle
			FROM ".DATABASE.".list_descriptions ld
			WHERE ld.ListID = $ListMemberTableID");
		break;
	default: 
		Utils::err(BAD_REQ);
	}
	
	dump("Deleting ListEntryID $ListEntryID..");
	$query_string = "DELETE FROM ".DATABASE.".lists WHERE ListEntryID = $ListEntryID";
	$mysqli = MySQLiC::getDBConnection('master');
	dump("Query String: $query_string");
	$mysqli->query($query_string);
	$affected = $mysqli->affected_rows;
	dump("Affected rows: $affected");
	if($affected <= 0){
	    Utils::err(SQL_ERR, $mysqli->error);
	}
	$mysqli->close();
	Utils::logTransaction('lists', $ListID, 'DELETE', "Description of removed item: $Description");
}
/* begin processing */

dump("Incoming request data:");
dump($_REQUEST);

$request = Utils::getRequest();
dump("Request request: " . $request);   
if(ctype_digit($request)){
	$request = array('ListEntryID'=>$request);
}
if(Utils::is_assoc($request)){
	if(isset($request['query'])){
    	$request['ListEntryID'] = $request['query'];
	}
	processList($request['ListEntryID']);
	Utils::simple_response(true);
}
else{
	foreach($request as $ListEntryID){
		processList($ListEntryID);
	}
	Utils::simple_response(true);
}
Utils::err(BAD_REQ, "Unrecognized request value: ".print_r($request, true).".");
?>