<?php
require_once ("../utils.php"); require_once ('MySQLi.php.inc');

$required_parameters = array(
  'ListID' => "/NUMERIC/",
  'Visibility' => "/^(public|intranet|private)$/",
  'PeopleID' => "/NUMERIC/",
  'query' => "/COMPLEXTEXT/",
);
dump("Incoming request data:");
dump($_REQUEST);
$request = Utils::getRequest();
/* create requests should be handled by a dedicated servlet.  I think this forward is obsolete; check when I have more time.*/

if (isset($request['create'])) {
  unset($request['create']);
  dump('processing create request');
  require ('CreateList.servlet.php');
  die();
}
//standard load request beyond this point.
Utils::sanity_check($request, $required_parameters);
if (!isset($request['Visibility'])) {
  $request['Visibility'] = 'public';
}
elseif ($request['Visibility'] == 'intranet') {
  unset($request['Visibility']);
}

$output = array('success' => TRUE);
if (isset($request['ListID'])) {
  //note to self - add checks in here for private lists.
  $result = Rosters_Stored_Procedures::getList($request['ListID'], $request['Visibility']);
}
elseif (isset($request['PeopleID'])) {
  $result = Rosters_Stored_Procedures::getPersonList($request['PeopleID'], $request['Visibility']);
}
else {
  Utils::err(BAD_REQ, 'No result returned.');
}
dump("output: " . print_r($result, TRUE));
if ($result === FALSE) {
  dump("An critical error has occurred in the getList servlet.");
}
foreach ($result as $row) {
  $output[] = $row;
}
Utils::write_array($output);


