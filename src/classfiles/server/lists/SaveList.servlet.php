<?php
/** Note: this servlet is required to make decisions about what
the 'proper' information is to be returned.  It will be an amalgam
of information from both the general and the docline databases.  The rule
is, the general database takes precedence unless no information is present for
a person, in which case the docline database information will be used as
a default.
*/
require_once("../utils.php");
require_once('MySQLi.php.inc');
$required_parameters = array(
    'ListEntryID'=>"/NUMERIC/",
    'ListID'=>"/NUMERIC/",
    'ListMemberTable'=>"/(people|list_descriptions)/",
    'ListMemberTableID'=>"/NUMERIC/",
    'ListEntryStartDate'=>"/DATE/",
    'Notes'=>"/COMPLEXTEXT/",
);

function processList($entry){
	global $required_parameters;	
	Utils::sanity_check($entry, $required_parameters);
	dump("Processing list entry id: $entry[ListEntryID]");
	$count = Utils::simpleQuery("SELECT 
		COUNT(*) FROM ".DATABASE.".lists
		WHERE ListEntryID = $entry[ListEntryID]");
	if($count < 1){
		dump("ListEntryID $ListEntryID not found!");
		Utils::err(NO_RES);
	}	
	$ListID = Utils::simpleQuery("SELECT 
		ListID FROM ".DATABASE.".lists
		WHERE ListEntryID = $entry[ListEntryID]");
	dump("Entry: " . print_r($entry, true));
	foreach(Utils::getFields($entry, 'lists') as $k=>$v){
	    if($k == 'ListEntryID'){
	        continue; //won't need to update the primary key, obviously.
	    }
	    if($v == ''){
	        $tmp []= "$k=NULL";
	    }
	    else{
	        $tmp []= "$k='$v'";
	    }
	}
	//add a last updated date

	$query_string = "
	    UPDATE ".DATABASE.".lists
	    SET " . implode(', ', $tmp).
	    " WHERE ListEntryID = '$entry[ListEntryID]'";
	$mysqli = MySQLiC::getDBConnection('updater');
	dump("query string: $query_string");
	if(!$mysqli->query($query_string)){
	    dump($mysqli->error);
	    Utils::err(SQL_ERR, $mysqli->error);
	}
	else{
	    dump("query executed successfully.");
	    Utils::logTransaction('lists', $ListID, 'UPDATE');

	}
	$response = ($mysqli->affected_rows != 0) ? true : false;
	dump("Affected rows: ".$mysqli->affected_rows);
	$mysqli->close();
}

dump("Incoming request data:");
dump($_REQUEST);
$request = Utils::getRequest();
if(Utils::is_assoc($request)){
	processList($request);
	Utils::simple_response(true);
	exit;
}
else{
	dump("multiple results to process.");
	foreach($request as $r){
		processList($r);
	}
	Utils::simple_response(true);
	exit;
}
Utils::err(BAD_REQ, "Unrecognized request value: ".print_r($request, true).".");
exit;


/*

$request = Utils::getRequest();
dump("Decoded request data:");
dump($request);
sanity_check($request, 'lists');
if($request['ListEntryID'] == -1){
	require_once('CreateList.servlet.php');
	die();
}

$mysqli = MySQLiC::getDBConnection();
if(!$result = $mysqli->query($query_string)){
    dump($mysqli->error);
    Utils::err(SQL_ERR, $mysqli->error);
}
else{
    dump("query executed successfully.");
    list($ListEntryID, $ListID, $ListTitle) = $result->fetch_row();
	$ListTitle = str_replace("'", "&apos;", $ListTitle);
    $result->close();
}
$mysqli->close();
// test for duplicate before insertion? 
$tmp = array();
foreach(getFields($request, 'lists') as $k=>$v){
    if($k == 'ListEntryID'){
        continue; //won't need to update the primary key, obviously.
    }
    if($v == ''){
        $tmp []= "$k=NULL";
    }
    else{
        $tmp []= "$k='$v'";
    }
}
$query_string = "
    UPDATE ".DATABASE.".lists
    SET " . implode(', ', $tmp).
    " WHERE ListEntryID = '$request[ListEntryID]' ";
$mysqli = MySQLiC::getDBConnection('updater');
dump("query string: $query_string");
if(!$mysqli->query($query_string)){
    dump($mysqli->error);
    Utils::err(DUPLICATE, $mysqli->error);
}
else{
    dump("query executed successfully.");
}
$response = ($mysqli->affected_rows != 0) ? true : false;
dump("Affected rows: ".$mysqli->affected_rows);
$mysqli->close();
$query_string = "CALL ".DATABASE.".getListEntry($request[ListEntryID])";
dump("query string: $query_string");
$mysqli = MySQLiC::getDBConnection();
if(!$result = $mysqli->query($query_string)){
    dump($mysqli->error);
    Utils::err(SQL_ERR, $mysqli->error);
}
else{
    $row = $result->fetch_assoc();
    dump("query executed successfully.  row: " . print_r($row, true));
    $result->close();
}
$mysqli->close();
Utils::logTransaction('lists', $ListID, 'UPDATE', "entry '$row[DescriptiveText]' added to list '$ListTitle'");
runQueryAndWriteOutput($query_string);

*/


?>
