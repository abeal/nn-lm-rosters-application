<?php
/* GetDocline.servlet.php
Retrieves docline information for a given libid.
*/
require_once("utils.php");
require_once('MySQLi.php.inc');
$required_parameters = array(
    'PeopleID'=>"/NUMERIC/",
    'LIBID'=>'/TEXT/',
	'query'=>'/TEXT/'
);
dump("Incoming request data:");
dump($_REQUEST);

//good to continue from this point on.
$request = Utils::getRequest();
/* check for required parameters.
Note that variables are standardized as lowercase.*/
Utils::sanity_check($request, $required_parameters);

$query_string = "
SELECT
    d.LIBID,
    d.INST_INST,
    d.DEPT_INST,
    d.STREET_INST,
    d.STREET2_INST,
    d.CITY_INST,
    d.STATE_ETC_CODE_INST,
    d.ZIP_MAIL_CODE_INST,
    d.COUNTY_INST,
    d.PROVINCE_INST,
    d.PHONE_COUNTRY_CODE_INST,
    d.PHONE_AREA_CODE_INST,
    d.PHONE_INST,
    d.PHONE_EXT_INST
";
if(isset($request['PeopleID'])){
    $query_string .= " FROM rosters.positions po
    INNER JOIN docline.PUBLIC_LIBRARY_DOCUSERS d
    ON (po.LIBID = d.LIBID)
    WHERE po.PeopleID = '$request[PeopleID]'";
}
else if(isset($request['LIBID'])){
    $query_string .= "FROM docline.PUBLIC_LIBRARY_DOCUSERS d
    WHERE d.LIBID = '$request[LIBID]'";
}
else if(isset($request['query'])){
	if(preg_match("/^[A-Z]{6}$/", $request['query'])){
		$query_string .= "FROM docline.PUBLIC_LIBRARY_DOCUSERS d
	    WHERE d.LIBID = '$request[query]'";
	}
	else{
	    $query_string .= "FROM docline.PUBLIC_LIBRARY_DOCUSERS d
	    WHERE d.INST_INST LIKE '$request[query]%'
		OR d.DEPT_INST LIKE '$request[query]%'";
	}
}
else{
    Utils::err(BAD_REQ, 'No valid parameters were passed to the GetDocline Servlet');
}
dump("QUERY: $query_string");
Utils::runQueryAndWriteOutput($query_string);
?>
