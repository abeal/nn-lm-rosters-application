<?php
/** Note: this servlet is required to make decisions about what
the 'proper' information is to be returned.  It will be an amalgam
of information from both the general and the docline databases.  The rule
is, the general database takes precedence unless no information is present for
a person, in which case the docline database information will be used as
a default.
*/
require_once("../utils.php");
require_once('scripts/class.rosters.php');           
$required_parameters = array(
    'PeopleID'=>"/NUMERIC/"
);
$display_parameters = array(
    'display'=>"/^(terse|short|full)$/",
	'visibility'=>"/(public|intranet)/",
    'nolabels'=>"/^(true|false)$/"
);
$request = Utils::getRequest();
$params = Utils::getRequestParams();  
dump("checking peopleid $request[PeopleID]");
/* check for required parameters.
Note that variables are standardized as lowercase.*/ 
Utils::sanity_check($request, $required_parameters, true);
Utils::sanity_check($params, $display_parameters);

Rosters::printRecord(
array('PeopleID'=>$request['PeopleID'],
	'visibility'=>(isset($params['visibility'])) ? $params['visibility'] : 'public',
	'display'=>(isset($params['display'])) ? $params['display'] : 'full'
));
?>
