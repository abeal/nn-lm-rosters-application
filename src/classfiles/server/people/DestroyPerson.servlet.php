<?php
/* DestroyPerson.servlet.php
  removes a person's record using the supplied People ID
*/
require_once("../utils.php");
require_once('MySQLi.php.inc');
/* begin processing */
$required_parameters = array(
    'Prefix'=>"/TEXT/",
    'FirstName'=>"/TEXT/",
    'MiddleName'=>"/TEXT/",
    'LastName'=>"/TEXT/",
    'Suffix'=>"/TEXT/",
    'PeopleID'=>"/NUMERIC/",
    'query'=>"/^[A-z., ]+$/"
);
dump("Incoming request data:");
dump($_REQUEST);

/* begin processing */
$request =  Utils::getRequest();
Utils::sanity_check($request, $required_parameters);
$pids = array();
if(isset($request['PeopleID'])){
    $pids []= $request['PeopleID'];
}
else{
    //disambiguate, and find the PositionID to remove.
    if(isset($request['query'])){
        $request = Utils::nameCrunch($request['query']);
    }
    else{
        $request = Utils::getFields($request, 'people');
    }
    dump(print_r($request, true));
    $tmp = array();
    foreach($request as $k=>$v){
        $tmp []= "$k='$v'";
    }
    if(count($tmp) == 0){
        Utils::err("GEN_ERR", "Not enough information supplied.");
        die();
    }
    //lookup positionid by peopleid and libid.
    $query_string = "
        SELECT PeopleID
        FROM ".DATABASE.".people
        WHERE ".implode(" AND ", $tmp);
    $mysqli = MySQLiC::getDBConnection();
    if($result = $mysqli->query($query_string)){
        while (list($pid) = $result->fetch_row()) {
            dump("Found pid $pid to delete");
            $pids[]= $pid;
        }

        /* free result set */
        $result->close();
    }
    else{
        Utils::err(SQL_ERR, $mysqli->error);
    }
    $mysqli->close();
}
if(empty($pids)){
    Utils::err(NO_RES, 'Cannot delete: No positions were found for the passed query parameters');
}
//position delete request, delete by role type
$count = 0;
$mysqli = MySQLiC::getDBConnection('master');
foreach($pids as $pid){
    dump("Deleting Person $pid");
    $query_string = "DELETE FROM ".DATABASE.".people WHERE PeopleID = '$pid'";
    if(!$mysqli->query($query_string)){
        Utils::err(SQL_ERR, $mysqli->error);
    }
    $count += $mysqli->affected_rows;
}
dump("Records removed: ". $count);
Utils::simple_response($count);

?>
