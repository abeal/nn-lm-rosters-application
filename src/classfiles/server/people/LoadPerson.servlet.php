<?php
/** Note: this servlet is required to make decisions about what
the 'proper' information is to be returned.  It will be an amalgam
of information from both the general and the docline databases.  The rule
is, the general database takes precedence unless no information is present for
a person, in which case the docline database information will be used as
a default.
*/

require_once("../utils.php");
require_once('MySQLi.php.inc');

// Active assert and make it quiet
assert_options(ASSERT_ACTIVE, 1);
assert_options(ASSERT_WARNING, 0);
assert_options(ASSERT_QUIET_EVAL, 1);
function my_assert_handler($file, $line, $code)
{
    Utils::err(BAD_REQ);
}      
assert_options(ASSERT_CALLBACK, 'my_assert_handler');
$optional_parameters = array(
    'Prefix'=>"/TEXT/",
	'LIBID'=>"/TEXT/",
    'FirstName'=>"/TEXT/",
    'MiddleName'=>"/TEXT/",
    'LastName'=>"/TEXT/",
    'Suffix'=>"/TEXT/",
    'PeopleID'=>"/NUMERIC/",
    'query'=>"/COMPLEXTEXT/",
);
$optional_qualifiers = array(
	//'booleanMode'=>'/BOOLEAN/',
	'fulltext'=>'/BOOLEAN/',
    'searchby'=>"/^(name|role|peopleid|libid|region)$/"
);
$request = Utils::getRequest();
$params = Utils::getRequestParams();
dump("Parsed request data: ");
dump(print_r($request, true));
dump("Parsed params data: ");
dump(print_r($params, true));
//parse optional qualifiers
Utils::sanity_check($params, $optional_qualifiers);
dump("Parsed qualifiers: ".print_r($params, true));
/* check for required parameters.
Note that variables are standardized as lowercase.*/


$searchby = (!isset($params['searchby'])) ? 'name' : $params['searchby'];
$fulltext = (!isset($params['fulltext'])) ? false : $params['fulltext'];
//$booleanMode = (!isset($params['booleanMode'])) ? true : $params['booleanMode'];
/* do preparatory work to insure appropriate variables are present for each 
search type, prior to query assembly. */

dump('searchby: '.$searchby);
switch($searchby){
case 'role':	
	if(isset($request['query'])){
	    $request['RoleType'] = $request['query'];
		unset($request['query']);
	}
	assert(isset($request['RoleType']));
	break;
case 'peopleid':	
	if(isset($request['query'])){
	    $request['PeopleID'] = $request['query'];
		unset($request['query']);
	}
	assert(isset($request['PeopleID']));
	break;
case 'name':
	if(isset($request['query'])){
	    if(trim($request['query']) == ''){
	        //successful result, count = 0 for empty query.
	        Utils::simple_response(true);
	    }
	    else if(ctype_digit($request['query'])){
	        $request['PeopleID'] = $request['query'];
	        $searchby = 'peopleid';
	    }
	    else{
			$name = Utils::nameCrunch($request['query']);
			foreach($name as $k=>$v){
				$request[$k] = $v;
			}
	    }
		unset($request['query']);
	}
	else if(isset($request['PeopleID'])){
        $searchby = 'peopleid';
	}
	assert(isset($request['PeopleID']) || isset($request['FirstName']));
	break;
case 'libid':
	if(isset($request['query'])){
	    $request['LIBID'] = $request['query'];
		unset($request['query']);
	}
	assert(isset($request['LIBID']));
	break;
case 'region':
	if(isset($request['query'])){
	    $request['region'] = $request['query'];
		unset($request['query']);
	}
	assert(isset($request['region']));
	break;
default:
	Utils::err(BAD_REQ);
}
Utils::sanity_check($request, $optional_parameters);
//put in local scope for easier access
Utils::escape_array($request);
foreach($request as $k=>$v){
	${$k} = $v;
}
//Begin query assembly
$query_string = "SELECT p.*
                FROM ".DATABASE.".people p ";
$curdate = date("Y-m-d");
switch($searchby){
case 'peopleid':
	$query_string .= "WHERE p.PeopleID = $PeopleID";
	break;
case 'name':
	if(isset($FirstName) && isset($LastName)){
		if($fulltext === true){
			$query_string .= "
			WHERE MATCH(p.FirstName, p.LastName)
			AGAINST ('$FirstName* $LastName*' IN BOOLEAN MODE)
			OR  MATCH(p.FirstName, p.LastName)
			AGAINST ('*$FirstName *$LastName' IN BOOLEAN MODE)";
		}
		else{
			$query_string .= "WHERE (p.FirstName = '$FirstName' 
				OR p.FirstName LIKE '%$FirstName%')
			AND (p.LastName = '$LastName' 
				OR p.LastName LIKE '%$LastName%')";
		}
	}
	else{
		if(isset($FirstName)){
			$name = $FirstName;
		}
		else if (isset($LastName)){
			$name = $LastName;
		}
		else{
			$name = '';
		}
		if($fulltext === true){
			$query_string .= "
			WHERE MATCH(p.FirstName, p.LastName)
			AGAINST ('$name*' IN BOOLEAN MODE)
			OR  MATCH(p.FirstName, p.LastName)
			AGAINST ('*$name' IN BOOLEAN MODE)";
		}
		else{
			$query_string .= "WHERE (p.FirstName = '$name' 
			OR p.FirstName LIKE '%$name%')
			OR (p.LastName = '$name' 
			OR p.LastName LIKE '%$name%')";
		}
	} 
	break;
case 'libid':
	$query_string .= "INNER JOIN ".DATABASE.".positions po ON (p.PeopleID = po.PeopleID) 
	WHERE po.LIBID = '$LIBID'
	AND po.StartDate <= '$curdate'
	AND (po.EndDate IS NULL OR po.EndDate >= '$curdate')";
	break;
case 'region':                                             
	dump('requested region: '. $region);
	$query_string .= "INNER JOIN ".DATABASE.".positions po ON (p.PeopleID = po.PeopleID)
	WHERE po.LIBID = '$region'
	AND po.StartDate <= '$curdate'
	AND (po.EndDate IS NULL OR po.EndDate >= '$curdate')";
	break;
case 'role':
	$query_string .= "INNER JOIN ".DATABASE.".positions po ON (p.PeopleID = po.PeopleID) 
		INNER JOIN ".DATABASE.".roles r 
		ON (r.PositionID = po.PositionID)
			WHERE r.RoleType = '$RoleType'
			AND po.StartDate <= '$curdate'
			AND (po.EndDate IS NULL OR po.EndDate >= '$curdate')";
	break;
default:
	Utils::err(BAD_REQ);
}
$query_string .= " ORDER BY p.LastName, p.FirstName";
//clean extra whitespace for output.
$query_string = preg_replace("|[\n\r]*[\s]{2,}|", ' ', $query_string);
dump("query: $query_string");
Utils::runQueryAndWriteOutput($query_string);
?>
