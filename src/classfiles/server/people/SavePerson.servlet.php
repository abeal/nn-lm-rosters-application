<?php

require_once("../utils.php");
require_once('MySQLi.php.inc');

$required_parameters = array(
    'PeopleID'=>"/NUMERIC/",
	'Prefix'=>"/TEXT/",
	'FirstName'=>"/TEXT/",
	'MiddleName'=>"/TEXT/",
	'LastName'=>"/TEXT/",
	'FirstName'=>"/TEXT/",
	'Picture'=>"/URL/",
);

$request = Utils::getRequest();
dump("Decoded request data:");
dump($request);
Utils::sanity_check($request, 'people');
dump('post sanity check');
//Utils::sanity_check($request, $required_parameters, true);

/* check to insure the position exists */
$count = Utils::simpleQuery("
    SELECT COUNT(*)
    FROM ".DATABASE.".people
    WHERE PeopleID = '$request[PeopleID]'");
if($count == 0){
    Utils::err(INVALID_REQ, "The individual with PeopleID '$request[PositionID]' does not exist.");
}
else{
    dump("PeopleID $request[PeopleID] found.  Modifying...");
}
/* position does exist - save the changes to it. */
$tmp = array();
foreach(Utils::getFields($request, 'people') as $k=>$v){
    if($k == 'PeopleID'){
        continue; //won't need to update the primary key, obviously.
    }
    if($v == ''){
        $tmp []= "$k=NULL";
    }
    else{
        $tmp []= "$k='$v'";
    }
}
//add a last updated date
$tmp[] = "LastUpdt='".date("Y-m-d H:i:s")."'";
$query_string = "
    UPDATE ".DATABASE.".people
    SET " . implode(', ', $tmp).
    " WHERE PeopleID = '$request[PeopleID]'";
$mysqli = MySQLiC::getDBConnection('updater');
dump("query string: $query_string");
if(!$mysqli->query($query_string)){
    dump($mysqli->error);
    Utils::err(SQL_ERR, $mysqli->error);
}
else{
    dump("query executed successfully.");
    Utils::logTransaction('people', $request['PeopleID'], 'UPDATE', "updated fields");
}
$response = ($mysqli->affected_rows != 0) ? true : false;
dump("Affected rows: ".$mysqli->affected_rows);
$mysqli->close();
$query_string = "SELECT * FROM ".DATABASE.".people WHERE PeopleID = $request[PeopleID]";
Utils::runQueryAndWriteOutput($query_string);
?>
