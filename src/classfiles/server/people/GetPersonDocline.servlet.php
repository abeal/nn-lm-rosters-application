<?php
/** GetPersonDocline.servlet.php
    retrieves all docline information about a person when passed the Libid.
*/
require_once("../utils.php");
$required_parameters = array('People_ID');


/** perform a translation to People_ID if necessary */
perform_PID_conversion();
/* check for required parameters.
Note that variables are standardized as lowercase.*/
foreach($required_parameters as $param){
    if(@!isset($_REQUEST[$param])){
        Utils::err(BAD_REQ);
    }
    ${strtolower($param)} = $_REQUEST[$param];
}
$query_string = "CALL ".DATABASE.".getPersonDocline('$people_id')";
Utils::runQueryAndWriteOutput($query_string);
?>
