<?php
/** Note: this servlet is required to make decisions about what
the 'proper' information is to be returned.  It will be an amalgam
of information from both the general and the docline databases.  The rule
is, the general database takes precedence unless no information is present for
a person, in which case the docline database information will be used as
a default.
*/

require_once("../utils.php");
require_once('MySQLi.php.inc');

$required_parameters = array(
    'Prefix'=>"/TEXT/",
    'FirstName'=>"/TEXT/",
    'MiddleName'=>"/TEXT/",
    'LastName'=>"/TEXT/",
    'Suffix'=>"/TEXT/"
);
$result =  Utils::getRequest();
Utils::sanity_check($result, $required_parameters);
$fields = Utils::getFields($result, 'people');
if(count($fields) < 1){
	throw new UserDataException("No valid fields found.");
}
//first, check for this position's existence for this position.  Duplicates not allowed.
$query_string = "
    INSERT INTO ".DATABASE.".people
    (" . implode(', ', array_keys($fields)) . ")
    VALUES ('" . implode("', '", $fields) . "')";
$mysqli = MySQLiC::getDBConnection('inserter');
dump("query string: $query_string");
if(!$mysqli->query($query_string)){
	$err = $mysqli->error;
	$mysqli->close();
    dump($err);
    Utils::err(SQL_ERR, $err);
}
else{
    dump("query executed successfully.");
    dump("Affected rows: ".$mysqli->affected_rows);
    $pid = $mysqli->insert_id;
	$mysqli->close();
    Utils::logTransaction('people', $pid, 'CREATE');
}
//create a new list for this person.
$result['ListTitle'] = $result['FirstName'] . ' ' . $result['LastName'] . "\'s locker";
$result['ListVisibility'] = 'private';
$fields = Utils::getFields($result, 'list_descriptions');
dump("final fields: " . print_r($fields, true));
$query_string = "
    INSERT INTO ".DATABASE.".list_descriptions
    (" . implode(', ', array_keys($fields)) . ")
    VALUES ('" . implode("', '", $fields) . "')";
$mysqli = MySQLiC::getDBConnection('inserter');
dump("query string: $query_string");
if(!$mysqli->query($query_string)){
	$err = $mysqli->error;
	$mysqli->close();
    dump($err);
    Utils::err(SQL_ERR, $err);
}
else{
    dump("query executed successfully.");
    dump("Affected rows: ".$mysqli->affected_rows);
    $lid = $mysqli->insert_id;
	$mysqli->close();
    Utils::logTransaction('list_descriptions', $lid, 'CREATE');
}
$query_string = "UPDATE ".DATABASE.".people 
	SET MyListID = '$lid'
	WHERE PeopleID = $pid";
$mysqli = MySQLiC::getDBConnection('updater');
dump("query string: $query_string");
if(!$mysqli->query($query_string)){
	$err = $mysqli->error;
	$mysqli->close();
    dump($err);
    Utils::err(SQL_ERR, $err);
}
else{
    dump("query executed successfully.");
    dump("Affected rows: ".$mysqli->affected_rows);
	$mysqli->close();
}
$query_string = "
    SELECT
        p.*
    FROM ".DATABASE.".people p
    WHERE p.PeopleID = '$pid'";
Utils::runQueryAndWriteOutput($query_string);
?>
