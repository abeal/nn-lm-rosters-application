<?php
require_once("utils.php");
require_once('MySQLi.php.inc');            
$request =  Utils::getRequest();
dump("Incoming request data:");
dump($request);              
$offset = (isset($_REQUEST['start']) && is_numeric($_REQUEST['start'])) ? $_REQUEST['start'] : 0;  
$limit = (isset($_REQUEST['limit']) && is_numeric($_REQUEST['limit'])) ? $_REQUEST['limit'] : 50;
$total = Utils::simpleQuery("
    SELECT COUNT(*)
    FROM  ".DATABASE.".transaction_log");
$query_string =          
	"(SELECT 
	CONCAT(p1.FirstName, ' ', p1.LastName) as 'Modifier',
	t.ModifiedDate,
	CONCAT(p2.FirstName, ' ', p2.LastName) as 'Modified',  
	t.TableModified,
	t.ActionTaken,
	t.Comments
	FROM rosters.transaction_log t
	INNER JOIN rosters.people p1
	ON (p1.PeopleID = t.ModifierID)
	INNER JOIN rosters.people p2
	ON (p2.PeopleID = t.ModifiedID)
	WHERE t.TableModified = 'people'
	OR t.TableModified = 'positions'
	OR t.TableModified = 'roles'
	OR t.TableModified = 'accounts'
	ORDER BY ModifiedDate DESC
	) UNION (
	SELECT 
	CONCAT(p1.FirstName, ' ', p1.LastName) as 'Modifier',
	t.ModifiedDate,
	ld.ListTitle as 'Modified',      
	t.TableModified,
	t.ActionTaken,
	t.Comments
	FROM rosters.transaction_log t
	INNER JOIN rosters.people p1
	ON (p1.PeopleID = t.ModifierID)
	INNER JOIN rosters.list_descriptions ld
	ON (ld.ListID = t.ModifiedID)
	WHERE t.TableModified = 'lists' OR t.TableModified = 'list_descriptions'
	ORDER BY ModifiedDate DESC    
	) ORDER BY ModifiedDate DESC
	LIMIT $limit OFFSET $offset";
$output = array('success'=>true, 'count'=>$total, 'extid'=>Utils::getExtID(), 'results'=>array());
$mysqli = MySQLiC::getDBConnection();
dump("Query: $query_string");
if($result = $mysqli->query($query_string, MYSQLI_STORE_RESULT)){
    dump("Num rows: " . $result->num_rows);
    while ($row = $result->fetch_assoc()) {
        $output['results'][]= $row;
    }
}
else{
    dump("Warning: query failed: " . $mysqli->error);
    $mysqli->close();
    Utils::err(BAD_REQ);
}
$mysqli->close();
dump("Writing results: ".json_encode($output));
header("Content-type: application/json");
$output = json_encode($output);
print $output;
?>
