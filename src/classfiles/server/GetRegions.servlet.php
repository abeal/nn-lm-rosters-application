<?php
/** GetRegions.servlet.php
 * retrieves a list of all role types and descriptions currently in the roles
 * database table.
 */

require_once("utils.php");

$regions = Utils::fetchAllRegions();
$response = array();
foreach($regions as $code=>$name){
	$response []= array('Code'=>$code, 'Name'=>$name);
}
$output = array('success'=>true, 'results'=>$response, 'extid'=>Utils::getExtID(), 'count'=>count($response));
$output = json_encode($output);
header("Content-type: application/json");
dump("Server response: ".$output);
print $output
?>