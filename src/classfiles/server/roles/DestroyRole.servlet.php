<?php
/* LoadPosition.servlet.php
Retrieves a list of positions using a People ID
*/
require_once("../utils.php");
require_once('MySQLi.php.inc');
/* begin processing */

function processRole($RoleID){
	dump("Processing id $RoleID");
	if(!is_numeric($RoleID)){
		Utils::err(BAD_ARG);
	}
	$query_string = "
		SELECT 
			po.PeopleID,
			po.PositionID, 
			po.Title,
			r.RoleType 
		FROM ".DATABASE.".positions po
		INNER JOIN ".DATABASE.".roles r
		    ON (r.PositionID = po.PositionID)
		WHERE r.RoleID = '$RoleID' LIMIT 1";
	$mysqli = MySQLiC::getDBConnection('inserter');
	dump("Query String: $query_string");
	$mysqli->query($query_string);
	if(!$result = $mysqli->query($query_string)){
	    Utils::err(SQL_ERR, $mysqli->error);
	}
	if($result->num_rows < 1){
	    Utils::err(GEN_ERR, "Role $request[RoleID] not found.");
	}
	list($PeopleID, $PositionID, $Title, $RoleType) = $result->fetch_row();
	$result->close();
	$mysqli->close();
	dump("Deleting Roles ID $RoleID..");
	$query_string = "DELETE FROM ".DATABASE.".roles WHERE RoleID = '$RoleID'";
	$mysqli = MySQLiC::getDBConnection('master');
	dump("Query String: $query_string");
	$mysqli->query($query_string);
	$affected = $mysqli->affected_rows;
	dump("Affected rows: $affected");
	if($affected <= 0){
	    Utils::err(SQL_ERR, $mysqli->error);
	}
	$mysqli->close();
	Utils::logTransaction('roles', $PeopleID, 'DELETE', 'Role removed: '.$RoleType.".  Affected position: $Title ($PositionID)");
}
/* START */
$request =  Utils::getRequest();
dump("Incoming request data:");
dump($request);    
if(ctype_digit($request)){
	$request = array('RoleID'=>$request);
}
if(Utils::is_assoc($request)){
	if(isset($request['query'])){
    	$request['RoleID'] = $request['query'];
	}
	processRole($request['RoleID']);
}
else{
	foreach($request as $RoleID){
		processRole($RoleID);
	}
}
Utils::simple_response(true);
?>
