
<?php
/* GetRoleTypes.servlet.php
Retrieves a list of possible NLM contractual roles from the role_types table
*/
require_once("../utils.php");
$query_string = "
SELECT
RoleType,
IFNULL(RoleDescription, 'No description provided') as RoleDescription,
IF(LOCATE('obsolete', RoleDescription) <> 0, true, false) as 'obsolete'
FROM ".DATABASE.".role_types
WHERE RoleType NOT LIKE '{OBSOLETE}%' ORDER BY RoleType";
Utils::runQueryAndWriteOutput($query_string);
?>
