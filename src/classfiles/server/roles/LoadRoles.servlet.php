<?php
/* LoadPosition.servlet.php
Retrieves a list of positions using a People ID
*/
require_once("../utils.php");
require_once('MySQLi.php.inc');

$optional_parameters = array(
    'RoleID'=>"/NUMERIC/",
    'RoleType'=>"/TEXT/",
    'PositionID'=>"/NUMERIC/",
    'Scope'=>"/TEXT/",
    'PeopleID'=>"/NUMERIC/",
    'query'=>"/TEXT/",
    'create'=>true
);

$request = Utils::getRequest();
$params = Utils::getRequestParams();

/* check for required parameters.*/
/* Not a create request, process as a normal select request. */
if(isset($request['query'])){
    $request['RoleID'] = $request['query'];
    unset($request['query']);
}
Utils::sanity_check($request, $optional_parameters);
$query_string = '
    SELECT r.*
    FROM '.DATABASE.'.roles r
    INNER JOIN '.DATABASE.".role_types rt
        ON (r.RoleType = rt.RoleType) 
	INNER JOIN ".DATABASE.".positions po
        ON (po.PositionID = r.PositionID) 
	WHERE "; //default, will be overwritten.

$fields = Utils::getFields($request, 'roles');
//special exception to avoid a major rewrite here:
if(isset($request['PeopleID'])){
    $fields['PeopleID'] = $request['PeopleID'];
}
if(count($fields) == 0){
    Utils::err(BAD_REQ, 'Invalid parameters passed to the LoadRoles servlet.  Exiting.');
}
else{
    dump("processing...");
}
$first = true;
foreach($fields as $k=>$v){
	if(!$first){$query_string .= "AND ";}
    switch($k){
	case 'PeopleID':
        $query_string .= "po.PeopleID = '$request[$k]' ";
        break;
    case 'RoleID':
        $query_string .= "r.RoleID = '$request[$k]' ";
        break;
    case 'PositionID':
        $query_string .= "r.PositionID = '$v' ";
        break;
    case 'Scope':
    case 'RoleType':
		$query_string .= "r.$k = '$v' ";
        break;
    default:
		$where .= '0';
        //shouldn't ever reach, kill all results.
        Utils::err(BAD_REQ, "Unrecognized variable $k was passed.");
        die();
    }
	$first = false;
}
dump("Query string passed: $query_string");
Utils::runQueryAndWriteOutput($query_string);

/*
if(isset($request['create'])){
    $count = Utils::simpleQuery("
        SELECT COUNT(*)
        FROM ".DATABASE.".positions
        WHERE PositionID = '$request[PositionID]'");
    if($count == 0){
        Utils::err(INVALID_REQ, "RoleID '$request[RoleID]' does not exist.");
    }
    //PositionID is valid, make sure the role does not already exist.
    dump("Position id $request[PositionID] exists.  Checking for role id $request[RoleID]");
    $count = Utils::simpleQuery("
        SELECT COUNT(*)
        FROM ".DATABASE.".roles
        WHERE RoleType = '$request[RoleType]'
        AND PositionID = '$request[PositionID]'");
    if($count != 0){
        dump("Role type $request[RoleType] already assigned.  Returning...");
        Utils::simple_response(true);
        die();
    }
    dump("Updating record...");
    // RoleID exists, update with supplied fields. 
    $pid = $request['PositionID'];
    $fields = getFields($request, 'roles');

    $query_string = "
        INSERT INTO ".DATABASE.".roles (" .
            implode(', ', array_keys($fields)) . ')
        VALUES ("' . implode('", "', array_values($fields)). '")';

    $mysqli = MySQLiC::getDBConnection('inserter');
    dump("query string: $query_string");
    if(!$mysqli->query($query_string)){
        dump($mysqli->error);
        Utils::err(SQL_ERR, $mysqli->error);
    }
    else{
        dump("query executed successfully.");
    }
    $response = ($mysqli->affected_rows != 0) ? true : false;
    dump("Affected rows: ".$mysqli->affected_rows);
    $rid = $mysqli->insert_id;
    $mysqli->close();
    $PeopleID = Utils::simpleQuery("
        SELECT po.PeopleID
        FROM ".DATABASE.".positions po
        INNER JOIN ".DATABASE.".roles r
        ON (r.PositionID = po.PositionID)
        WHERE r.RoleID = $rid");
    Utils::logTransaction('roles', $PeopleID, 'CREATE', "Role Added: $request[RoleType]");
    $query_string = "
    SELECT * FROM ".DATABASE.".roles
    WHERE RoleID = '$rid'";
    Utils::runQueryAndWriteOutput($query_string);
    die();
}
*/
?>
