<?php
/** Note: this servlet is required to make decisions about what
the 'proper' information is to be returned.  It will be an amalgam
of information from both the general and the docline databases.  The rule
is, the general database takes precedence unless no information is present for
a person, in which case the docline database information will be used as
a default.
*/
require_once("../utils.php");
require_once('MySQLi.php.inc');
$allowed_parameters = array(
    'RoleID'=>"/NUMERIC/",
    'Scope'=>"/TEXT/"
);
$required_parameters = array(
	'PositionID'=>"/NUMERIC/",
	'RoleType'=>"/COMPLEXTEXT/"
);

function processRole($l){
	global $required_parameters, $allowed_parameters;
	Utils::sanity_check($l, $required_parameters, true);
	Utils::sanity_check($l, $allowed_parameters);
	dump("new role parameters are sane.");
	$fields = Utils::getFields($l, 'roles');
	//first, check for this position's existence for this position.  Duplicates not allowed.
	list($count) = Utils::simpleQuery("
	    SELECT COUNT(*)
	    FROM  ".DATABASE.".positions
	    WHERE PositionID = '$fields[PositionID]'");
	if($count == 0){
	    Utils::err(INVALID_REQ, "The position '$fields[PositionID]' does not exist.");
	}
	dump("Position exists for proposed role.");
	$query_string = "
	    INSERT INTO ".DATABASE.".roles
	    (" . implode(', ', array_keys($fields)) . ")
	    VALUES ('" . implode("', '", $fields) . "')";
	$mysqli = MySQLiC::getDBConnection('updater');
	if(!$mysqli->query($query_string)){
	    dump("Warning: query failed: " . $mysqli->error);
	    Utils::err(BAD_REQ, $mysqli->error);
	}
	$affected = ($mysqli->affected_rows == 0) ? false : $mysqli->insert_id;
	dump("Insertion successful.  Affected rows: ".$mysqli->affected_rows.", id: $affected");
	$mysqli->close();
	$pid = Utils::simpleQuery("
	    SELECT p.PeopleID
	    FROM ".DATABASE.".people p
		INNER JOIN ".DATABASE.".positions po
		ON(p.PeopleID = po.PeopleID)
	    WHERE po.PositionID = '$fields[PositionID]'");
	dump("Pid result: $pid");
    Utils::logTransaction('roles', $pid, 'CREATE', "role addition: $l[RoleType]");
	return $affected;
}

/* Begin processing */
$request = Utils::getRequest();
dump("Create request.  Data: ");
dump($request);
$ids = array();
if(Utils::is_assoc($request)){
	$ids []= processRole($request);
}
else{
	for($i = 0; $i < count($request); $i++){
		$ids []= processRole($request[$i]);
	}
}
$qsc = array();
for($i = 0; $i < count($ids); $i++){
	if($ids[$i] == false){
		continue;
	}
	$qsc []= 'RoleID = '.$ids[$i];
}
$query_string = "SELECT * FROM ".DATABASE.".roles WHERE " . implode(' OR ', $qsc);
Utils::runQueryAndWriteOutput($query_string);

?>
