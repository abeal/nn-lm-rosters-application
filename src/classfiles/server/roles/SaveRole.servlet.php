<?php
/** Note: this servlet is required to make decisions about what
the 'proper' information is to be returned.  It will be an amalgam
of information from both the general and the docline databases.  The rule
is, the general database takes precedence unless no information is present for
a person, in which case the docline database information will be used as
a default.
*/
require_once("../utils.php");
require_once('MySQLi.php.inc');
$required_parameters = array(
    'RoleID'=>'/NUMERIC/',
    'RoleType'=>'/TEXT/',
    'PositionID'=>'/NUMERIC/',
    'Scope'=>'/TEXT/'
);
dump("Incoming request data:");


$request = json_decode($_REQUEST['request'], true);dump($_REQUEST);
Utils::sanity_check($request, 'roles');//replace eventually with more strict checking.

/* check to insure the position exists */
$count = Utils::simpleQuery("
    SELECT COUNT(*)
    FROM ".DATABASE.".roles
    WHERE RoleID = '$request[RoleID]'");
if($count == 0){
    Utils::err(INVALID_REQ, "RoleID '$request[RoleID]' does not exist.");
}
else{
    dump("RoleID $request[RoleID] found.  Modifying role...");
}
/* position does exist - save the changes to it. */
foreach(Utils::getFields($request, 'roles') as $k=>$v){
    if($k == 'RoleID'){
        continue; //won't need to update the primary key, obviously.
    }
    $tmp []= "$k='$v'";
}
$query_string = "
    UPDATE ".DATABASE.".roles
    SET " . implode(', ', $tmp).
    " WHERE RoleID = '$request[RoleID]'";
$mysqli = MySQLiC::getDBConnection('updater');
dump("query string: $query_string");
if(!$mysqli->query($query_string)){
    dump($mysqli->error);
    Utils::err(SQL_ERR, $mysqli->error);
}
else{
    dump("query executed successfully.");
    //Utils::logTransaction('roles', $PeopleID, 'UPDATE', "ID: ($request[PositionID]), affected position title: \"$Title\".");

}
$response = ($mysqli->affected_rows != 0) ? true : false;
dump("Affected rows: ".$mysqli->affected_rows);
$success = ($mysqli->affected_rows > 0);
$result->close();
$mysqli->close();

/* Add a last updated date to the associated *position* for this role
if role updated successfully. */
if($success){
    $query_string = "
    SELECT PositionID
    FROM ".DATABASE.".roles
    WHERE RoleID = '$request[RoleID]'";

    $mysqli = MySQLiC::getDBConnection();
    if(!$result = $mysqli->query($query_string)){
        dump("No matching position found for role id $request[RoleID]!");
        Utils::err(SQL_ERR, "No matching position found for role id $request[RoleID]!".$mysqli->error);
    }
    list($pid) = $result->fetch_row();
    $result->close();
    $mysqli->close();
    if(!$pid){
        $success = false;
    }
    else{
        $pos = array("LastUpdt='".date("Y-m-d H:i:s")."'");
        $query_string = "
            UPDATE ".DATABASE.".positions
            SET " . implode(', ', $pos).
            " WHERE PositionID = '$pid'";
        $mysqli = MySQLiC::getDBConnection('updater');
        dump("query string: $query_string");
        if(!$mysqli->query($query_string)){
            dump($mysqli->error);
            Utils::err(SQL_ERR, $mysqli->error);
        }
        else{
            dump("query executed successfully.");
        }
        $response = $response && (($mysqli->affected_rows != 0) ? true : false);
        $result->close();
        $mysqli->close();
    }
}
Utils::simple_response($response);
?>
