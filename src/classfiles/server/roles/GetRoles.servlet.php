<?php
/** GetRoles.servlet.php
 * retrieves a list of all role types and descriptions currently in the roles
 * database table.
 */
require_once("../utils.php");
$query_string = "select distinct * from ".DATABASE.".roles";
Utils::runQueryAndWriteOutput($query_string);
?>
