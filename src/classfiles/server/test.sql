--SET @@max_sp_recursion_depth=5; /* allows for recursion; default is 0 */
USE rosters;
DROP PROCEDURE IF EXISTS getList;
DROP PROCEDURE IF EXISTS getPeople;
DROP PROCEDURE IF EXISTS getPersonDocline;
delimiter //
/*
	PROCEDURE getList:
	processes the self-referential rosters.lists table.  Takes
	a single argument, and creates a list of node ids that are 
	referenced by the id of the passed node.
*/
CREATE PROCEDURE getList (IN ListKey INT)
BEGIN
	DECLARE myKey INT;
	DECLARE loopControl INT;
	DECLARE tempCount INT;
	/* create tmp table structures for procedure execution */
	DROP TEMPORARY TABLE IF EXISTS _getList_listTemp;
	CREATE TEMPORARY TABLE _getList_listTemp (
		ListID INT UNIQUE NOT NULL,
		PRIMARY KEY (ListID)
	);
	

	DROP TEMPORARY TABLE IF EXISTS _getList_listResults;
	CREATE TEMPORARY TABLE _getList_listResults (
		ListID INT UNIQUE NOT NULL,
		PRIMARY KEY (ListID)
	);
	/* Seed our queue for the loop with the root node in the DAG */
	
	INSERT INTO _getList_listTemp (ListID)
	VALUES (ListKey);
	
	SET loopControl = 0; /* keep this loop from going crazy. */
	SET tempCount = (SELECT COUNT(*) FROM _getList_listTemp);
	
	
	
	WHILE tempCount > 0 && loopControl < 60 DO
		SET loopControl = loopControl + 1; 
		
		-- pop the next key off the queue 
		SET myKey = (SELECT ListID FROM _getList_listTemp LIMIT 1);
		
		-- add to the queue any keys not yet in the result
		
		INSERT IGNORE INTO _getList_listTemp (ListID)
		SELECT 
			l.ListMemberTableID
		FROM rosters.lists l
		LEFT JOIN _getList_listResults lr
		ON (lr.ListID = l.ListID)
		WHERE l.ListID = myKey
		AND l.ListMemberTable = 'list_descriptions';
		
		-- merge any new keys into the result.
		INSERT IGNORE INTO _getList_listResults
		SELECT ListID FROM _getList_listTemp;
		
		-- pop the processed key off the queue 
		DELETE FROM _getList_listTemp WHERE ListID = myKey;
		
		-- recount how many are left in our queue to be processed			
		SET tempCount = (SELECT COUNT(*) FROM _getList_listTemp);
		-- SHOW WARNINGS;
		
	END WHILE;	
	
	
	SELECT ListID as ListResult FROM _getList_listResults;
	-- SHOW WARNINGS;
	
	DROP TEMPORARY TABLE IF EXISTS _getList_listTemp;
	DROP TEMPORARY TABLE IF EXISTS _getList_listResults;
END;
//

CREATE PROCEDURE getPeople (IN ListIds TEXT)
BEGIN
	DECLARE delimiter CHAR(1);
	DECLARE myindex INT;
	DECLARE mynext INT;
	DECLARE mycount INT;
	SET delimiter = ",";
	SET myindex = 1;
	SET mycount = 50;
	SET mynext = (SELECT LOCATE(delimiter, ListIds, myindex));
	DROP TEMPORARY TABLE IF EXISTS _getPeople_idValues;
	CREATE TEMPORARY TABLE _getPeople_idValues (
		myID INT UNIQUE NOT NULL
	);
	/* parse string to create list of id values to match against. */
	WHILE mynext <> 0 DO
		INSERT INTO _getPeople_idValues (myID)
			SELECT TRIM(SUBSTR(ListIds FROM myindex FOR (mynext - myindex)));
		SET myindex = (mynext+1);
		SET mynext = IF(mycount < 0, 0, (SELECT LOCATE(delimiter, ListIds, myindex)));
		SET mycount = (mycount - 1);
		
	END WHILE;
	INSERT INTO _getPeople_idValues (myID)
		SELECT TRIM(SUBSTR(ListIds FROM myindex));
		
	DROP TEMPORARY TABLE IF EXISTS _getPeople_listResults;
	CREATE TEMPORARY TABLE _getPeople_listResults (
		ListID INT NOT NULL,
		PeopleID INT UNIQUE NOT NULL,
		Description TEXT,
		PRIMARY KEY (ListID, PeopleID)
	);
		/*
		OriginTable ENUM('account_types','list_descriptions','people','positions','roles') NOT NULL,
		*/
	-- get all people 
	INSERT IGNORE INTO _getPeople_listResults (ListID, PeopleID, Description)
	SELECT DISTINCT
		l.ListID,
		l.ListMemberTableID,
		CONCAT(p.LastName, ', ', p.FirstName) as Name
	FROM
		rosters.lists l
	INNER JOIN rosters.people p
		ON (p.PeopleID = l.ListMemberTableID)
	WHERE l.ListMemberTable = 'people'
		AND l.ListID IN (SELECT * FROM _getPeople_idValues);
	
	-- get all positions 
	INSERT IGNORE INTO _getPeople_listResults (ListID, PeopleID, Description)
	SELECT DISTINCT
		l.ListID,
		p.PeopleID,
		CONCAT(p.LastName, ', ', p.FirstName) as Name
	FROM
		rosters.lists l
	INNER JOIN rosters.positions po
		ON (po.PositionID = l.ListMemberTableID)
	INNER JOIN rosters.people p
		ON (po.PeopleID = p.PeopleID)
	WHERE l.ListMemberTable = 'positions'
		AND	l.ListID IN (SELECT * FROM _getPeople_idValues);
		
	-- get all roles 
	INSERT IGNORE INTO _getPeople_listResults (ListID, PeopleID, Description)
	SELECT DISTINCT
		l.ListID,
		p.PeopleID,
		CONCAT(p.LastName, ', ', p.FirstName) as Name
	FROM
		rosters.lists l
	INNER JOIN rosters.roles r
		ON (r.RoleID = l.ListMemberTableID)
	INNER JOIN rosters.positions po
		ON (r.PositionID = po.PositionID)
	INNER JOIN rosters.people p
		ON (po.PeopleID = p.PeopleID)
	WHERE l.ListMemberTable = 'roles'
		AND	l.ListID IN (SELECT * FROM _getPeople_idValues);
		
	-- get all accounts 
	INSERT IGNORE INTO _getPeople_listResults (ListID, PeopleID, Description)
	SELECT DISTINCT
		l.ListID,
		p.PeopleID,
		CONCAT(p.LastName, ', ', p.FirstName) as Name
	FROM
		rosters.lists l
	INNER JOIN rosters.account_types atype
		ON (atype.AccountTypeID = l.ListMemberTableID)
	INNER JOIN rosters.accounts a
		ON (a.AccountTypeID = atype.AccountTypeID)
	INNER JOIN rosters.people p
		ON (a.PeopleID = p.PeopleID)
	WHERE l.ListMemberTable = 'account_types'
		AND	l.ListID IN (SELECT * FROM _getPeople_idValues);

	SELECT * FROM _getPeople_listResults;	
	DROP TEMPORARY TABLE IF EXISTS _getPeople_listResults;
END;
//

CREATE PROCEDURE getPersonDocline (IN PeopleID INT)
BEGIN
	SELECT
	pld.PROVINCE_INST,
	pld.REGION_CODE,
	plc.TITLE,
	p.nnlm_user,
	p.People_ID,
	p.Personal_URL,
	p.Picture,
	pld.LIBID as Libid,
	IF(p.FirstName <> '', p.FirstName, plc.FIRST_NAME) as FirstName,
	IF(p.LastName <> '', p.LastName, plc.LAST_NAME) as LastName,
	IF(p.Institution <> '', p.Institution, pld.INST_INST) as Institution,
	IF(p.Department <> '', p.Department, pld.DEPT_INST) as Department,
	IF(p.Street <> '', p.Street, 
		Concat(pld.STREET_INST, IF(pld.STREET2_INST, 
			Concat(', ', pld.STREET2_INST), '')
		)
	) as Street,
	IF(p.City <> '', p.City, pld.CITY_INST) as City,
	IF(p.State <> '', p.State, pld.STATE_ETC_CODE_INST) as State,
	IF(p.Zip <> '', p.Zip, pld.ZIP_MAIL_CODE_INST) as Zip,
	IF(p.Email <> '', p.Email, plc.EMAIL) as Email,
	IF(p.Fax_Area_Code <> '', p.Fax_Area_Code, plc.FAX_AREA_CODE) as Fax_Area_Code,
	IF(p.Fax_Country_Code <> '', p.Fax_Country_Code, plc.FAX_COUNTRY_CODE) as Fax_Country_Code,
	IF(p.Fax <> '', p.Fax, plc.FAX) as Fax,
	p.Fax_ext,
	IF(p.Phone_Country_Code <> '', p.Phone_Country_Code, plc.PHONE_COUNTRY_CODE) as Phone_Country_Code,
	IF(p.Phone_Area_Code <> '', p.Phone_Area_Code, plc.PHONE_AREA_CODE) as Phone_Area_Code,
	IF(p.Phone <> '', p.Phone, plc.PHONE) as Phone,
	IF(p.Phone_Ext <> '', p.Phone_Ext, plc.PHONE_EXT) as Phone_Ext
	FROM general.people p
	INNER JOIN general.affiliations a
	ON (p.People_id = a.People_ID AND a.Main = 'Y')
	LEFT JOIN docline.PUBLIC_LIBRARY_DOCUSERS pld
	ON (a.ID = pld.LIBID)
	LEFT JOIN docline.PUBLIC_LIBRARY_CONTACTS plc
	on (plc.LIBID = pld.LIBID)
	WHERE p.People_ID = PeopleID
	LIMIT 1;
END;
//

DROP PROCEDURE IF EXISTS foo;
CREATE PROCEDURE foo ()
BEGIN
	SELECT 'bar' as foo;
END
//
delimiter ;
/* remove all prior nested lists for testing purposes. */
/*
DELETE FROM `rosters`.`lists` WHERE ListMemberTable = 'list_descriptions';
SHOW WARNINGS;

INSERT IGNORE INTO lists (ListID, ListMemberTable, ListMemberTableID)
VALUES 
	(1, 'list_descriptions', 2),
	(1, 'list_descriptions', 3),
	(2, 'list_descriptions', 4),
	(3, 'list_descriptions', 4),
	(4, 'list_descriptions', 5),
	(2, 'list_descriptions', 6)
;
SHOW WARNINGS;


CALL getList(3);
*/
/* _getList_listTemp should now contain 1,2,3,4,5 */




/* do cleanup */
/*
DELETE FROM `rosters`.`lists` WHERE ListMemberTable = 'list_descriptions';
SHOW WARNINGS;
*/
/*
SELECT COUNT(ListID) as Number_of_lists
FROM `rosters`.`lists`*/
/*CREATE TEMPORARY TABLE reachable(ListID INT UNIQUE NOT NULL);
	DROP TEMPORARY TABLE IF EXISTS reachable;
	*/
	
	/*DECLARE myListID INT;
	
	WHILE myListID > 0 DO
		SET myListID = myListID - 1
		SELECT myListID
	END WHILE*/