<?php
/*
* utils.php
* utility functions common to all servlets.
*/
ini_set('error_reporting', 0);
require_once("env.php");
require_once('MySQLi.php.inc');
require_once('scripts/rosters_stored_procedures.class.php');
require_once("scripts/EchoRequest.servlet.php");
function error_handler($errno, $errstr='n/a', $errfile='n/a', $errline='n/a'){
	dump("ERROR: $errstr");
    $bt = array_pop(debug_backtrace());
    $err = array(
        'success'=>false,
        'error'=>$errstr,
        'file'=>$bt['file'],
        'line'=>$bt['line']
    );
    write_array($err, true);
    die();
}
function exception_handler($e){
	dump("ERROR: ".$e->getMessage());
    $bt = array_pop(debug_backtrace());
    $err = array(
        'success'=>false,
        'error'=>$e->getMessage(),
        'file'=>$e->getFile(),
        'line'=>$e->getLine()
    );
    write_array($err, true);
    die();
}
set_exception_handler("exception_handler");
set_error_handler("error_handler", E_ERROR);
define("DEV", false);
define("NO_RES", 1);
define("SQL_ERR", 2);
define("NO_DATA_SENT", 3);
define("GEN_ERR", 4);
define("BAD_REQ", 5);
define("INVALID_REQ", 6);
define("ERR", 7);
define("ACCESS_RESTRICTED", 8);
define("OUTPUT_TYPE", "JSON");
define("DATABASE", "rosters");
//regular expressions for sanity checks
//EchoRequest::setClassesOfInterest(array("Load", "getList", "GetPer"));
function dump($msg, $src=false){
    $src = ($src == false) ? basename($_SERVER['SCRIPT_FILENAME']) : $src;
    $bt = array_pop(debug_backtrace());
    if(is_array($msg)){
        EchoRequest::writeArrayToTerminal($msg, $src);
    }
    else{
        EchoRequest::writeToTerminal('Line ' . $bt['line'] .': '.$msg, $src);
    }
}
/** begin general preprocessing **/
dump("RAW REQUEST DATA:");
dump($_REQUEST);
if(!isset($_REQUEST['request'])){
/*	if(isset($_REQUEST['query'])){
	    $_REQUEST['request'] = $_REQUEST['query'];
	}
	else{
		throw new Exception("Improper request format");
	}
*/	
//		throw new Exception("Improper request format");
} 
if($_REQUEST['request'] == ''){
	dump("Empty request.");
	//	simple_response(true);
}
$json_query =  json_decode($_REQUEST['request'], true);
$json_params =  (isset($_REQUEST['params']) ? json_decode($_REQUEST['params'], true) : array());
if(isset($_REQUEST['extid']) && is_numeric($_REQUEST['extid'])){
	$json_query['extid'] = $_REQUEST['extid'];
}
if(is_null($json_query) && is_string($_REQUEST['request'])){
    //dump("IS STRING", $me);
    $_REQUEST['request'] = str_replace('""', '"', '{"query":"'.$_REQUEST['request'].'"}');
    //TODO: add a sanitization for extid.
    $json_query = array('extid'=>$json_query['extid'], 'query'=>$json_query);
}
foreach($_REQUEST as $k=>$v){
	switch($k){
	case 'v': //visibility	
		unset($_REQUEST[$k]);
		switch($v){
		case 'p':
			$_REQUEST['visibility'] = 'public';
			break;
		case 'i':
			$_REQUEST['visibility'] = 'intranet';
			break;
		default:
			$_REQUEST['visibility'] = $v;
		}
		break;
	case 'd': //display	
		unset($_REQUEST[$k]);
		switch($v){
		case 't':
			$_REQUEST['display'] = 'terse';
			break;
		case 's':
			$_REQUEST['display'] = 'short';
			break;
		case 'f':
			$_REQUEST['display'] = 'full';
			break;
		default:
			$_REQUEST['display'] = $v;
		}
		break;
	case 's': //display	
		unset($_REQUEST[$k]);
		$_REQUEST['sort'] = $v;
		break;
	default:
	}
}
/** end general preprocessing **/
/** which_file
 * checks the system path, and determines which file would actually be included
 * by a require or include statement
 * @param relative_path {string} the relative pathname of the file in question.
 * This can simply be a filename.
 * @returns {mixed} The string corresponding to the absolute path to the file
 * that would be referenced, or false, if no viable candidate file was found
 * in the system include path.
 */
function which_file($relative_path){
    foreach(explode(':', ini_get('include_path')) as $prefix){
        $path = $prefix . '/' . $relative_path;
        if(file_exists($path)){
            return $path;
        }
    }
    return false;
}
/** is_assoc
*   returns whether or not the passed array is associative or normal.
*   @param $_array {array} the array to be examined.
*   @return {bool} true if the array is associative, false otherwise.
*/
function is_assoc($array) {
    return (is_array($array) && 0 !== count(array_diff_key($array, array_keys(array_keys($array)))));
}
/**
 * err
 * causes an error message to be returned from the server
 * @param $case {int} the type of error being returned.
 * Valid types are (constant values):
 * NO_RES: no results were found for the parameters sent.  Note that this is
 * a successful query.
 * SQL_ERR: A problem exists with the SQL query that was executed.
 * NO_DATA_SENT: No data was sent from the client.
 * BAD_REQ: An invalid parameter was supplied, or an invalid value for a correct
 * parameter.
 * GEN_ERR: Some other type of error not covered under these other categories
 * has occurred.
 * @param $msg {string} more information about the nature of the error, if any
 * exists.
 * @returns {special} Executing this function will halt processing. Does not
 * return.
 */
function err($case=4, $msg=""){
    global $query_string;
    $errno = 0;

    switch($case){
	case ACCESS_RESTRICTED:
		$err = 'You do not have sufficient permissions to view the requested record.';
		break;
	case ERR:
		$err = 'The following error has occurred:';
		break;
    case NO_RES: //create no leaf element
        $result = array(
            'success'=>true
        );
        write_array($result);
        exit;
        break;
    case SQL_ERR:
        $err = addslashes("A mysql error has occurred: ".$msg.". QS: ".preg_replace("/[\n ]+/", ' ', $query_string));
        break;
    case NO_DATA_SENT:
        $err = 'No data was sent.  Did you enter something in the search box?';
        break;
    case BAD_REQ:
        $err = 'Bad request. ';
        break;
	case DUPLICATE:
		$err = "A duplicate entry already exists: ";
		break;
    case GEN_ERR:
    default:
        $err = 'An undefined error has occurred.  This application cannot continue. ';

    }
    if($msg != ""){
        $err .="$msg";
    }
    error_handler(0, $err);
    exit(1);
}
function _rWriteXML($arr){
    global $oXMLout, $me;
    foreach($arr as $k=>$v){
        if(is_array($v)){
            $oXMLout->startElement($k);
            _rWriteXML($v);
            $oXMLout->endElement();
        }
        else{
            $oXMLout->writeElement($k, $v);
        }
    }
}
function getSuccess(&$arr){
    $success = (isset($arr['success'])) ? $arr['success'] : false;
    unset($arr['success']);
    return $success;
}
function getParam($param, &$nested_arr){
    $result = (isset($nested_arr[$param])) ? $nested_arr[$param] : (
    (isset($nested_arr[0]) && is_array($nested_arr[0])) ? getParam($param, $nested_arr[0]) : false);
    return $result;
}
function getCurrentUserID(){
	if(!isset($_SERVER['PHP_AUTH_USER'])){
		return -1;
	}
    $query_string = "
        SELECT
            p.PeopleID
        FROM ".DATABASE.".people p
        INNER JOIN http_auth.users u
        ON (u.People_ID = p.PeopleID)
        WHERE u.user  = '$_SERVER[PHP_AUTH_USER]'";
    $mysqli = MySQLiC::getDBConnection('nobody');
    $result = $mysqli->query($query_string);
    list($pid) = $result->fetch_row();
    $mysqli->close();
    return $pid;
}

function getCurrentUserLID(){
	if(!isset($_SERVER['PHP_AUTH_USER'])){
		return -1;
	}
    $query_string = "
        SELECT
            p.MyListID
        FROM  ".DATABASE.".people p
        INNER JOIN http_auth.users u
        ON (u.People_ID = p.PeopleID)
        WHERE u.user  = '$_SERVER[PHP_AUTH_USER]'";
    $mysqli = MySQLiC::getDBConnection('nobody');
    $result = $mysqli->query($query_string);
    list($pid) = $result->fetch_row();
    $mysqli->close();
    return $pid;
}
/** logTransaction
 * updates the transaction log with the passed parameters as entries.
 * @param $tk {VARCHAR(25)} the name of the table modified
 * @param $mkv {MEDIUMINT(8) unsigned} the primary key of the
 * record modified.  Note that this record may not exist if the action take
 * was a DELETE operaion.
 * @param ActionTaken {ENUM('CREATE','UPDATE','DELETE')} The action
 * performed on the specified record.
 * See also http://en.wikipedia.org/wiki/Create,_read,_update_and_delete
*/
function logTransaction($tm,$mkv,$at,$c=''){
    $pid = getCurrentUserID();
    $mysqli = MySQLiC::getDBConnection('updater');
	$c = addSlashes($c);
	
    $query_string = "INSERT INTO  ".DATABASE.".transaction_log
	(ModifierID,TableModified,ModifiedID,ActionTaken,Comments)
	VALUES ('$pid','$tm','$mkv','$at','$c')
    ON DUPLICATE KEY UPDATE
        ModifiedID='$mkv',
        ActionTaken='$at',
            Comments='$c'";

    dump("TL query: $query_string");
	$id = false;
    if($mysqli->query($query_string)){
        dump("Transaction log updated.");
        $id = $mysqli->insert_id;
    }
    else{
        dump("Warning: transaction log update failed: " . $mysqli->error);
    }
	$mysqli->close();
	return $id;
}
/** json_format
 * pretty prints json text for output to screen or echo listener.
 * @param json {string} the json text to format
 * @param htmlchars {boolean} whether or not to render with html for browser
 * output.  Defaults to false.
 * @returns {void}
 */
function json_format($json, $htmlchars=false){
    $new_json = "";
    $indent_level = 0;
    $in_string = false;
    $br = ($htmlchars) ? "<br />" : "\n";
    $sp = ($htmlchars) ? "&nbsp;" : " ";
    $tab = ($htmlchars) ? "$sp$sp$sp$sp" : "\t";
    $json_obj = json_decode($json);

    if($json_obj === false)
        return false;

    $json = json_encode($json_obj);
    $len = strlen($json);

    for($c = 0; $c < $len; $c++)
    {
        $char = $json[$c];
        switch($char)
        {
            case '{':
            case '[':
                if(!$in_string)
                {
                    $new_json .= $char . $br . str_repeat($tab, $indent_level+1);
                    $indent_level++;
                }
                else
                {
                    $new_json .= $char;
                }
                break;
            case '}':
            case ']':
                if(!$in_string)
                {
                    $indent_level--;
                    $new_json .= $br . str_repeat($tab, $indent_level) . $char;
                }
                else
                {
                    $new_json .= $char;
                }
                break;
            case ',':
                if(!$in_string)
                {
                    $new_json .= ",$br" . str_repeat($tab, $indent_level);
                }
                else
                {
                    $new_json .= $char;
                }
                break;
            case ':':
                if(!$in_string)
                {
                    $new_json .= ":$sp";
                }
                else
                {
                    $new_json .= $char;
                }
                break;
            case '"':
                if($c > 0 && $json[$c-1] != '\\')
                {
                    $in_string = !$in_string;
                }
            default:
                $new_json .= $char;
                break;
        }
    }

    return stripslashes($new_json);
}
/* write_array
  writes an array to the output socket (expecting standard XMLHttpRequest over
  port 80) in JSON format
  @param $arr {associative array} the array to be written
  @returns {void} (prints output to STDOUT)
*/
function write_array($arr, $nested=true){
    global $json_query;
    $results = array('success'=>getSuccess($arr));

    if($results['success']){
        if($nested){
            $results['count'] = count($arr);
            $results['results'] = array();
            foreach($arr as $k=>$v){
                $results['results'][$k] = $v;
            }
        }
        else{
            foreach($arr as $k=>$v){
                if(is_numeric($v)){
                    $v = integer($v);
                }
                $results[$k] = $v;
            }
        }
    }
    else{
        if(is_array($arr) && count($arr) != 0){
            $results['results'] = $arr;
        }
        $results['count'] = 0;
    }
    $results['extid'] = $json_query['extid'];
	dump("ExtID: " . $results['extid']);
	if(isset($arr['error'])){
		$results['message'] = $arr['error'];
	}
    if($results['extid'] !== 'test'){
        header("Content-type: application/json");
    }
    $results = json_encode($results);
    dump("Server response: ".json_encode($results));
    print $results;
}

/* simple response
    returns a canned response to a query, usually to a query that has
    modified the database in some fashion.
    @param $mixed {boolean|int} if boolean, returns a message to requester that
    the last request was successful or not.  If integer, returns that the
    last request was successful, with the integer supplying the count of
    records affected by the request.
*/
function simple_response($mixed){
    global $json_query;
    $results = array('extid'=>$json_query['extid'], 'success'=>true, 'count'=>0, 'results'=>array());
	if(isset($mixed['error'])){
		$results['message'] = $mixed['error'];
	}
    if($results['extid'] !== 'test'){
        header("Content-type: application/json");
    }
    $results = json_encode($results);
    dump("Server response: ".json_encode($results), $me);
    print $results;
	exit(0);
}

/** simpleQuery
 * Runs a simple query.  Returns a single result.
 **/
function simpleQuery($query_string){
	try{
		return MySQLiC::simpleQuery($query_string);
	} catch(Exception $e){
		err(SQL_ERR, $e->getMessage());
	}
}
/** simpleQuery
 * Runs a simple query.  Returns a single result.
 **/
function singleRowQuery($query_string, $assoc=false){
	try{
		return MySQLiC::singleRowQuery($query_string, $assoc);
	} catch(Exception $e){
		err(SQL_ERR, $e->getMessage());
	}
}
/*runQueryAndWriteOutput
    convenience function to execute a query string with 'nobody' level
    privileges, and write the output (in JSON format) to the requesting
    browser.
    @param $query_string {string} the query to be executed.
    @returns {special} halts execution upon completion.
*/
 function runQueryAndWriteOutput($query_string){
    dump("Executing query string: $query_string");
    $output = array('success'=>true);

    $mysqli = MySQLiC::getDBConnection();
    if($result = $mysqli->query($query_string, MYSQLI_STORE_RESULT)){
        dump("Num rows: " . $result->num_rows);
        while ($row = $result->fetch_assoc()) {
            $output[]= $row;
        }
    }
    else{
        dump("Warning: query failed: " . $mysqli->error);
        $mysqli->close();
        err(BAD_REQ);
    }
    $mysqli->close();
    write_array($output);
    exit;
}
/* fetchRegion
	gets the region, given a 6 character LIBID
*/
function fetchRegion($LIBID){
	/* Test the general.regions table for an exact match, indicating an RML or National Center.  If 
	not found, the libid can be assigned based on the first two character match from the docline
	database. */
	$exact = singleRowQuery("SELECT COUNT(*) FROM general.regions WHERE LIBID = '$LIBID'");
	if($exact > 0){
		return simpleQuery("SELECT Region_Code FROM general.regions WHERE LIBID = '$LIBID'");
	}
	$sec = substr($LIBID, 0, 2);
	return simpleQuery("SELECT REGION_CODE FROM docline.PUBLIC_STATE_ETC_CODES  WHERE STATE_ETC_CODE = '$sec'");
}
/*runProcedureAndWriteOutput
    convenience function to execute a stored procedure with 'nobody' level
    privileges, and write the output (in JSON format) to the requesting
    browser.
    @param $query_string {string} the query to be executed.
    @returns {special} halts execution upon completion.
*/
 function runProcedureAndWriteOutput($query_string){
    dump("Executing query string: $query_string");
    $output = array('success'=>true);

    $mysqli = MySQLiC::getDBConnection();
	if(!$mysqli->multi_query($query_string)){
	    dump("Warning: query failed: " . $mysqli->error);
        $mysqli->close();
        err(BAD_REQ);
	}
	while($mysqli->more_results()){
		$result = $mysqli->use_result();
		while($row = $result->fetch_assoc()){
			dump('row: ' . $row . ',:  ' . print_r($row, true));
			$output []= $row;
		}
	}
    $mysqli->close();
    write_array($output);
    exit;
}
/** sanity_check
 *  takes a data array, and purges it of key-value pairs that are not in the list of valid keys
 *  for the array.
 *  @param $request_data_array {array} the data array to be purged.
 *  @param $mixed {array|string} The valid keys.  If an array is passed, the keys
 *  will be valid value keys, and the values will be regular expressions to be used
 *  to validate the data present in the passed request_data_arr.
 *  If a string is passed instead, the string is assumed to be a tablename in
 *  the rosters database, and the valid keys are determined from fields in that
 *  table.
 *  @returns {special} return value is void if there is no issue with the data.
 *  Otherwise, the function will trigger an error message to standard out and
 *  halt execution.
 */
function sanity_check(&$request_data_arr, $mixed, $all_required=false){
    $valid_field_keys = false;
    if(!is_array($request_data_arr)){
        simple_response(true);
		exit;
    }
    if(is_array($mixed)){
        $valid_field_keys = $mixed;
    }
    else{ //is string, otherwise
        $valid_field_keys = getValidFieldNames($mixed);
    }
    $anySaneRequest = true;
    $valid_field_keys['extid'] = '/NUMERIC/'; //always an allowed input.
    $mysqli = MySQLiC::getDBConnection('nobody', DEV); //use for escaping parameters.
    foreach($request_data_arr as $k=>$v){
		$msg = "Checking key $k: ";
        //if the key exists in request_data_arr, it must match the pattern
        //in valid_field_keys
        if(array_key_exists($k, $valid_field_keys)){
            if($valid_field_keys[$k] == null){
                //no sanity expression defined.  Use a general mysqli escape.
                //dump("Escaping.  Prevalue: $request_data_arr[$k]");
                $request_data_arr[$k] = $mysqli->real_escape_string($request_data_arr[$k]);
                //dump("Escaping.  Postvalue: $request_data_arr[$k]");
				$msg .= "No sanity expression defined for key $k";
				dump($msg);
            }
            else if(substr($valid_field_keys[$k], 0, 1) == '/'){
                switch($valid_field_keys[$k]){
                case '/BOOLEAN/':
                    //special case: peopleid on client is not known.
					$msg .= " (Boolean):";
                    $anySaneRequest = $anySaneRequest && is_bool($request_data_arr[$k]);
                    if(!$anySaneRequest){
                        err(BAD_REQ, "The supplied value ".
                        "'$request_data_arr[$k]' for the key $k was not a valid boolean value.");
                    }
                    break;
                case '/NUMERIC/':	
					$msg .= " (Numeric):";
                    //special case: peopleid on client is not known.
                    $anySaneRequest = $anySaneRequest && is_numeric($request_data_arr[$k]);
                    if(!$anySaneRequest){
                        err(BAD_REQ, "The supplied value ".
                        "'$request_data_arr[$k]' for the key $k was not a valid numeric value.");
                    }
                    break;
                case '/DATE/':	
					$msg .= " (Date):";
					
					if($request_data_arr[$k] == ''){
						$msg .= " WARNING: empty string - unsetting value";
						$request_data_arr[$k] = 'NULL';
					}
					else{
						$request_data_arr[$k] = preg_replace("/T.*$/", "", $request_data_arr[$k]);
						$converted = date('Y-m-d H:i:s', strtotime($request_data_arr[$k]));
                    	$anySaneRequest = $anySaneRequest && ($converted !== false);
						$msg .= " NOTE: converted value $request_data_arr[$k] to date $converted";
				        $request_data_arr[$k] = $converted;
						
					}
					if(!$anySaneRequest){
                        err(BAD_REQ, "The supplied value ".
                        "'$request_data_arr[$k]' for the key $k was not a valid date value.");
                    }
                    break;
                case '/TEXT/':	
					$msg .= " (Text):";
                    $anySaneRequest = $anySaneRequest && preg_match("/^[A-z0-9., ]*$/", $request_data_arr[$k]);
                    if(!$anySaneRequest){
                        err(BAD_REQ, "The supplied value ".
                        "'$request_data_arr[$k]' for the key $k was not a ".
                        "valid text value.");
                    }
                    break;
                case '/COMPLEXTEXT/':	
					$msg .= " (Complex Text):";
                    $mysqli = MySQLiC::getDBConnection('nobody');
                    $request_data_arr[$k] = $mysqli->real_escape_string($request_data_arr[$k]);
                    $anySaneRequest = $anySaneRequest && true;
                    $mysqli->close();
                    break;
                default:	
					$msg .= " (Custom):";
                    //validation match is normal regular expression
                    if(!preg_match($valid_field_keys[$k], $request_data_arr[$k])){
                        err(BAD_REQ, "The value for the key '$k' is not valid.");
                        $anySaneRequest = false;
                    }
                    else{
                        $anySaneRequest = $anySaneRequest && true;
                    }
                }
				
				$msg .= " OK";
				dump($msg);

            }
            else{
                //value to be matched is a literal value.  Do direct comparison.

				$msg .= "Checking key $k (Literal)";
				dump($msg);
                if($valid_field_keys[$k] != $request_data_arr[$k]){
                    err(BAD_REQ, "The value for the key '$k' is not valid.");
                    $anySaneRequest = false;
                }
                else{
                    $anySaneRequest = $anySaneRequest && true;
                }
            }
        }
		else{
			dump("The key $k does not exist in the valid field keys array, skipping");
		}
    }
    $mysqli->close();
    unset($valid_field_keys['extid']); //not required.
    if($all_required){
        //The key in valid_field_keys *must* be present in the input array.
        foreach($valid_field_keys as $k=>$v){
            if(!array_key_exists($k, $request_data_arr)){
                $errors []= "The required key '$k' is missing from this request.";
                $anySaneRequest = false;
            }
        }
    }
    if(!$anySaneRequest){
        err(BAD_REQ," Sanity check fail: " .implode(" | ", $errors));
    }	
	dump("Sanity check passed.  Keys tested: " . implode(", ", array_keys($valid_field_keys)));
    return true;
}
/* getFields
  retrieves the field names for the named table in the rosters database, and
  provides the caller with an array consisting of only those entries that match
  table name entries.
  @param $request_data_arr {associative array} the array containing the data
  to be returned
  @param $target_table {string} the name of the table in the rosters database
  to query for field names
  @returns {associative array} the entries in $request_data_arr whose keys
  match the field names in the target table. Outputs an error and
  kills processing if bad data is passed.
*/
function getFields($request_data_arr, $target_table){
    $result = array();
	dump("target table: " . $target_table);
    $valid_field_keys = getValidFieldNames(trim($target_table));
    foreach($request_data_arr as $k=>$v){
        if(array_key_exists($k, $valid_field_keys)){
            $result[$k] = $v;
        }
    }
    if(count($result) == 0){
        dump('Caution: No valid field names were supplied (valid fields: ' .
            implode(',', array_keys($valid_field_keys)) .
            '), (fields given: ' . implode(',', array_keys($request_data_arr)).').'.
            ' This may be ok.');
    }
    //dump("final fields: " . print_r($result, true));
    return $result;
}
/* getValidFieldNames
    helper function for retrieving field names from the rosters database.
    @param tablename {string} the name of the table to retrieve field names from.
    @returns {assoc. array|false} an associative array with the keys being the field
    names,and the values being null. Triggers error message and kills processing
    in the event of an error.
*/
function getValidFieldNames($tablename){
    if(!preg_match("/^[a-z_]+$/", $tablename)){
        err(GEN_ERR, "getValidFieldNames: Illegal value supplied for table name: $tablename");
    }
    $mysqli = MySQLiC::getDBConnection('nobody');
    $output = array();
    $query_string = "
    SELECT COLUMN_NAME
    FROM information_schema.COLUMNS
    WHERE TABLE_SCHEMA = '".DATABASE."' AND TABLE_NAME = '$tablename';
    ";
    if(!$result = $mysqli->query($query_string)){
        dump("An error has occurred in the GetValidFieldNames function.");
        err(SQL_ERR, $mysqli->error);
    }
    while(list($colname) = $result->fetch_row()){
        $output[$colname]= null;
    }
    $result->close();
    $mysqli->close();
    return $output;
}
/* nameCrunch
  processes a textual string with the assumption that it is a user inputted
  person's name.  Attempts to extract name components based on general American
  English language assumptions.
  @param $query {string} the user provided string containing the person's name.
  @returns {assoc. array} an array with keys corresponding to field names in the
  people table, and values being the function's best guess as to what portion,
  if any, of the supplied string represented that particular value.
*/
function nameCrunch($query){
    $result = array();
    dump("crunching name " . $query);
	$query = trim($query);
    if(strpos($query, ',')){
        //1 comma: [honorific] lastname, firstname middle
		dump("comma split.");
		$parts = explode(',', $query);
		dump("Query parts:".print_r($parts, true));
		array_push($parts, array_shift($parts));
		dump("Query parts:".print_r($parts, true));
    }
	else{
		dump("nocomma");
    	$parts = explode(' ', $query);
	}
	foreach($parts as $k=>$v){
		$parts[$k] = trim($v);
	}
    //1: firstname only
    //2: first, then last name (commas already processed)
    //3: first, middle, last
    //4: prefix, first, middle, last (had to pick one; will mess up some cases)
    //5: prefix, first, middle, last, suffix
    //more: count only the first 5.
	
	dump("Query parts:".print_r($parts, true));
    switch(count($parts)){
    case 1:
        $result['FirstName'] = trim($parts[0]);
        break;
    case 2:
        $result['FirstName'] = trim($parts[0]);
        $result['LastName'] = trim($parts[1]);
        break;
    case 3:
        $result['FirstName'] = trim($parts[0]);
        $result['MiddleName'] = trim($parts[1]);
        $result['LastName'] = trim($parts[2]);
        break;
    case 4:
    case 5:
    default:
        $result['Prefix'] = trim($parts[0]);
        $result['FirstName'] = trim($parts[1]);
        $result['MiddleName'] = trim($parts[2]);
        $result['LastName'] = trim($parts[3]);
        if(isset($parts[4])){
            $result['Suffix'] = trim($parts[4]);
        }
    }
    dump("Name crunch results: (pref, first, mid, last, suf): \nPrefix: $result[Prefix],".
        "\nFirst:  $result[FirstName],\nMiddle: $result[MiddleName], \nLast:   $result[LastName], \nSuffix: $result[Suffix]");
    return $result;
}


/*
 function perform_PID_conversion(){
//OBSOLETE.
    global $link; //active mysql connection
    $optional_parameters = array('nnlm_user');
    $mysqli = MySQLiC::getConnection('nobody');
    $result = array();
    foreach($optional_parameters as $tmp){
        if(@isset($_REQUEST[$tmp])){
            $param = $tmp;//record index value of parameter passed.
            $stmt = $mysqli->prepare("
                SELECT p.People_ID
                FROM general.people p
                WHERE p.? = ?");
            $stmt->bind_param('ss', $param, $_REQUEST[$tmp]);
            DB::query($query_string) or err(SQL_ERR, true);
            while(list($people_id) = DB::getResultRow()){
                $_REQUEST['People_ID'] = $people_id;
            }


            unset($_REQUEST[$tmp]);
        }
    }
    $mysqli->close();
}
*/
?>
