<?php
/** Note: this servlet is required to make decisions about what
the 'proper' information is to be returned.  It will be an amalgam
of information from both the general and the docline databases.  The rule
is, the general database takes precedence unless no information is present for
a person, in which case the docline database information will be used as
a default.
*/
require_once("utils.php");
require_once('MySQLi.php.inc');
$required_parameters = array(
    'query'=>"/TEXT/"
);
dump("Incoming request data:");
dump($_REQUEST);

$request = Utils::getRequest();
/* check for required parameters.
Note that variables are standardized as lowercase.*/

Utils::sanity_check($request, $required_parameters);
$query_string = "
    SELECT DISTINCT
    pld.LIBID as LIBID,
    pld.INST_INST as INST_INST,
    pld.DEPT_INST as DEPT_INST
    FROM docline.PUBLIC_LIBRARY_DOCUSERS pld
    WHERE pld.NNLM_MEMBERSHIP != 'N'
	AND pld.LIB_CLOSED_YN <> 'Y'
	AND pld.DEMO_LIB_YN <> 'Y'
    AND ";
if(strlen($request['query']) == 6){
    $query_string .= " pld.LIBID = '?' ";
}
else{
    $query_string .= "
    pld.LIBID LIKE '%?%'
    OR pld.INST_INST LIKE '%?%'
    OR pld.DEPT_INST LIKE '%?%'";
}
$query_string = str_replace("?", $request['query'], $query_string);

Utils::runQueryAndWriteOutput($query_string);
?>
