<?php
require_once('MySQLi.php.inc'); //needed for mysqli escaping in sanity checks.  
require_once("scripts/EchoRequest.servlet.php");
/** @class Rosters_Stored_Procedures
 * This is a convenience class for php access to the stored procedures in the rosters database.  
 * It is provided to minimize the effect of signature change of the SQL methods, and to provide 
 * an easy way to do default arguments (a functionality that MYSQL does not offer)
 *
 * @package default
 * @author Aron Beal
 */
class Rosters_Stored_Procedures{
	public static function addListToLocker($ListToAdd){
		if(!isset($_SERVER['PHP_AUTH_USER'])){
			return false;
		}			
		$culid = self::getCurrentUserLID();
		if(!self::verifyLID($culid)){
			throw new RostersException("Current user has no default list.  This is unexpected.  Cannot continue.");
		}
		return self::addListToList($culid, $ListToAdd);
	}
	public static function addPersonToLocker($idToAdd){
		if(!isset($_SERVER['PHP_AUTH_USER'])){
			return false;
		}			
		$culid = self::getCurrentUserLID();
		if(!self::verifyLID($culid)){
			throw new RostersException("Current user has no default list.  This is unexpected.  Cannot continue.");
		}
		return self::addPersonToList($culid, $idToAdd);
	}
	public static function addListToList($ListToBeAddedTo, $ListToAdd){   
		$mysqli = MySQLiC::getDBConnection('inserter');
		if(!$mysqli->query("CALL rosters.addToList('list_descriptions', '$ListToBeAddedTo', '$ListToAdd')")){
		    self::dump($mysqli->error);
		    Utils::err(SQL_ERR, $mysqli->error);
		}
		else{
		    self::dump("query executed successfully.");
		}
		$response = ($mysqli->affected_rows != 0) ? true : false;
		self::dump("Affected rows: ".$mysqli->affected_rows);
		$mysqli->close();
		sleep(1);
		return $response;
	}
	public static function addPersonToList($ListToBeAddedTo, $idToAdd){    
		$mysqli = MySQLiC::getDBConnection('inserter');
		if(!$mysqli->query("CALL rosters.addToList('people', '$ListToBeAddedTo', '$idToAdd')")){
		    self::dump($mysqli->error);
		    Utils::err(SQL_ERR, $mysqli->error);
		}
		else{
		    self::dump("query executed successfully.");
		}
		$response = ($mysqli->affected_rows != 0) ? true : false;
		self::dump("Affected rows: ".$mysqli->affected_rows);
		$mysqli->close();
		sleep(1);
		return $response;   
	}
	public static function getUserLid($PeopleID){
		if(!is_numeric($PeopleID)){throw new Exception("Invalid ID");}
		$query_string = "
			SELECT MyListID 
			FROM rosters.people p 
			WHERE p.PeopleID = $PeopleID";
		$mysqli = MySQLiC::getDBConnection();
	    $result = $mysqli->query($query_string);
		if($result->num_rows == 0){
			throw new Exception("Invalid User.");
		}
	    list($lid) = $result->fetch_row();
	    $mysqli->close();
	    return $lid;
	}
	public static function getCurrentUserID(){
		if(!isset($_SERVER['PHP_AUTH_USER'])){
			return -1;
		}
	    $query_string = "
			SELECT PeopleID 
			FROM rosters.accounts a 
			INNER JOIN rosters.account_types at 
			ON (at.AccountTypeID = a.AccountTypeID) 
			WHERE AccountType = 'nnlm_user' 
			AND AccountValue = '$_SERVER[PHP_AUTH_USER]'";
	    $mysqli = MySQLiC::getDBConnection('inserter');
	    $result = $mysqli->query($query_string);
		if($result->num_rows == 0){
			return false;
		}
	    list($pid) = $result->fetch_row();
	    $mysqli->close();
	    return $pid;
	}
    private static function dump($msg, $src=false){
	    $src = ($src == false) ? basename($_SERVER['SCRIPT_FILENAME']) : $src;
		static $time = 0;
	    $bt = array_pop(debug_backtrace());
	    if(is_array($msg)){
	        EchoRequest::writeArrayToTerminal($msg, $src);
	    }
	    else{
	        EchoRequest::writeToTerminal('Line ' . $bt['line'] .': '.$msg, $src);
			//EchoRequest::writeToTerminal(($time - $microtime) . ' microseconds elapsed.', $src);
			//$time = microtime();
	    }
	}
	public static function getCurrentUserLID(){
		if(!isset($_SERVER['PHP_AUTH_USER'])){
			return -1;
		}
	    $query_string = "
			SELECT p.MyListID 
			FROM rosters.people p
			INNER JOIN rosters.accounts a
			ON (p.PeopleID = a.PeopleID) 
			INNER JOIN rosters.account_types at 
			ON (at.AccountTypeID = a.AccountTypeID) 
			WHERE AccountType = 'nnlm_user' 
			AND AccountValue = '$_SERVER[PHP_AUTH_USER]'";
	    $mysqli = MySQLiC::getDBConnection('inserter');
	    $result = $mysqli->query($query_string);		
		if($result->num_rows == 0){
			return false;
		}
	    list($lid) = $result->fetch_row();
	    $mysqli->close();
	    return $lid;
	}
	/** getList returns a combined result of lists, people, positions, 
	  and roles that belong to the list indentified by ListKey. Does not
	  recursively obtain sub-lists

	    @param $ListID {int} The ListID of the list to be retrieved.
		@param $sortkey1 a sorting variable for the call. One of the following:
			'r': sort by region.
			'a': sort alphabetically.  Alias for 'f'.
			'f': sort alphabetically, by first name
			'l': sort alphabetically, by last name
			'p': sort by position rank.  Lists are placed last in this sorting.
			'q': sort by position rank.  Lists are placed first in this sorting.
		@param $sortkey2 a sorting variable for the call. See sortkey1 for details:	
		@param $sortkey3 a sorting variable for the call. See sortkey1 for details:
		@returns {string} the SQL call for the stored procedure.
	**/
	public static function getList($ListID, $visibility='public'){
		if(!is_numeric($ListID)){return '';}                                     
		//TODO: add back in sorting by various parameters                   
		$query_string = "CALL rosters.getFullList($ListID, '$visibility', 1)";
		self::dump("Executing getList stored procedure.  Query: $query_string");  
		try{
	   		 $result = MySQLiC::query($query_string, true);     
		}catch(Exception $e){self::dump("Exception caught: " . $e->getMessage()); throw $e;}    
		return $result;
	}
	public static function getFullList($ListID, $visibility='public'){
		if(!is_numeric($ListID)){throw new Exception("Invalid ID: $ListID");}   		
		return MySQLiC::query("CALL rosters.getFullList($ListID,'$visibility', 0)", true); //TODO: add back in sorting by various parameters
	}
	
	public static function getFullPersonList($PeopleID, $visibility='public'){
		if(!is_numeric($PeopleID)){throw new Exception("Invalid ID");}
		$lid = self::getUserLid($PeopleID);
		return MySQLiC::query("CALL rosters.getFullList('$lid','$visibility', 0)", true); //TODO: add back in sorting by various parameters
	}
	/** getListAssignations
		@param PeopleID {int} the PeopleID of the person who is to be researched.
		@returns {string} a result set of list_descriptions that a member belongs to.
	*/
	public static function getListAssignations($PeopleID, $visibility='public'){
		if(!is_numeric($PeopleID)){throw new Exception("Invalid ID");}
		$visibility = ($visibility == 'public') ? ' = "public"' : ' <> "private"';
		return "SELECT DISTINCT 
			ld.ListID, 
			ld.ListTitle, 
			ld.ListDescription,
			ld.ListVisibility,
			ld.ListStartDate,
			ld.ListEndDate
		FROM rosters.lists l 
		INNER JOIN rosters.list_descriptions ld
		ON (l.ListID = ld.ListID)
		WHERE l.ListMemberTable = 'people' 
		AND l.ListMemberTableID = $PeopleID
		AND ld.ListVisibility $visibility
		AND (l.ListEntryStartDate IS NULL OR l.ListEntryStartDate <= CURDATE())
		AND (l.ListEntryEndDate IS NULL OR l.ListEntryEndDate > CURDATE());";
	}
	/** getPersonList
		Just retrieves a Locker ID for a person and calls that function with it. See also: 'getList'.  
	*/
	public static function getPersonList($PeopleID, $visibility='public'){
		if(!is_numeric($PeopleID)){throw new Exception("Invalid ID");}
		return Rosters_Stored_Procedures::getList(
			Rosters_Stored_Procedures::getCurrentUserLID(), $visibility);
	}
	/** getCurrentUserList
		Just retrieves a Locker ID for a person and calls that function with it. See also: 'getList'.  
	*/
	public static function getCurrentUserList(){
		return Rosters_Stored_Procedures::getList(
			Rosters_Stored_Procedures::getCurrentUserLID(), 'private');
	}
	/** removeListFromList is a convenience function that removes one list from another, after
	first checking that they both exist.
	@param $parent {int} The ListID of the parent list
	@param $target {int} The ListID of the list to be removed.
	@returns {string} the SQL call for the stored procedure.
	**/
	public static function removeListFromList($target, $parent){
		if(!is_numeric($target)){throw new Exception("Invalid List ID");}
		if(!is_numeric($parent)){throw new Exception("Invalid List ID");}
		return "CALL rosters.removeListFromList($parent, $target)";
	}
	/** verifyPID
     * insures the passed PeopleID is a valid one.
     * @param $PeopleID {mixed} an integer PeopleID from the rosters.people
     * database.
     * @returns {boolean} true if the id(s) are (all) valid, false otherwise.
     */
    public static function verifyPID($mixed){
		$pids = array();
		if(is_numeric($mixed)){
        	$pids []= $mixed;
		}
		else if(is_array($mixed)){
			$pids = $mixed;
		}
		else{
		   	throw new Exception("Invalid id passed!");
		}
        $strs = array();
        foreach($pids as $pid){
            if(!is_numeric($pid)){
                throw new Exception("Invalid entry passed to verifyPID function.  Integer expected, ".gettype($pid). ' found.');
            }
            $strs []= "PeopleID = $pid";
        }
        $expected = count($pids);
        $found = MySQLiC::simpleQuery("
	        SELECT COUNT(*)
	        FROM rosters.people
	        WHERE ".implode(' OR ', $strs));

        if($found === false || $expected != $found){
        	throw new Exception("Invalid id passed!");
        }
		return true;
    }
	
	/** verifyLID
     * insures the passed ListID is a valid one.
     * @param $ListID {mixed} an integer ListID from the rosters.list_descriptions
     * database.
     * @returns {boolean} true if the id is valid, false otherwise.
     */
	public static function verifyLID($mixed){
		$lids = array();
		if(is_numeric($mixed)){
        	$lids []= $mixed;
		}
		else if(is_array($mixed)){
			$lids = $mixed;
		}
		else{
		   	throw new Exception("Invalid id passed!");
		}
        $strs = array();
        foreach($lids as $lid){
            if(!is_numeric($lid)){
                throw new Exception("Invalid entry passed to verifyPID function.  Integer expected, ".gettype($lid). ' found.');
            }
            $strs []= "ListID = $lid";
        }
        $expected = count($lids);
		$query_string = "
        	SELECT COUNT(*)
	        FROM rosters.list_descriptions
	        WHERE ".implode(' OR ', $strs);
        $found = MySQLiC::simpleQuery($query_string);
		//print "QS: $query_string, expected: $expected, found: $found";
        if($found === false || $expected != $found){
        	throw new Exception("Invalid id passed!");
        }
		return true;
	}
}
?>