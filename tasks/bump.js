//see https://github.com/vojtajina/grunt-bump
module.exports = {
	options: {
		files: ['package.json'], //try to maintain all variable changes in package.json
		updateConfigs: [], //probably unnecessary - this task is run independently of others.
		commit: false, //true, once debugging is complete?
		commitMessage: 'Release v%VERSION%',
		commitFiles: ['package.json'],
		createTag: false, //true, once debugging is complete?
		tagName: 'v%VERSION%',
		tagMessage: 'Version %VERSION%',
		push: false, //true, once debugging is complete?
		pushTo: 'origin',
		gitDescribeOptions: '--tags --always --abbrev=1 --dirty=-d',
		globalReplace: false,
		prereleaseName: 'dev',
		regExp: false
	}
};