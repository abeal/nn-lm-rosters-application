module.exports = function(grunt) {
    grunt.registerTask("sandy-dev", [
        'build-module', 'rsync:dev'
    ]);
    var result = {
        options: {
            args: ["--verbose"],
            exclude: [".git*", "*.scss", "node_modules"],
            compareMode: 'checksum',
            recursive: true
        },
        dev: {
            options: {
                src: "build/module/",
                dest: "/var/www/ws-sandy-dev.hsl.washington.edu/sites/all/modules/custom/nnlm_rosters",
                host: 'abeal@november.hsl.washington.edu',
                dryRun: false, //disable when rsync is confirmed in terminal
                deleteAll: true // Careful this option could cause data loss, read the docs!
            }
        }
        //note: stage and prod are accomplished by git commits (CI server)
    };
    return result;
};