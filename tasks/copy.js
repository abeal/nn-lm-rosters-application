module.exports = function(grunt) {
	return {
		tmp: {
			//html and template files that need string translations
			files: [{
				cwd: 'src/',
				expand: true,
				src: ['index.html', 'views/help/**', 'classfiles/client/environment.js'],
				dest: 'tmp'
			}],
			options: {
				//insert timestamp 
				process: function(content, srcpath) {
					var replacement_patterns = {
						'javascript': grunt.option('javascript filename'),
						'stylesheet': grunt.option('stylesheet filename'),
						'version_stamp': grunt.option('version-stamp')
					};
					for (var pattern in replacement_patterns) {
						r_pattern = new RegExp('@@@' + pattern + '@@@', 'g');
						content = content.replace(r_pattern, replacement_patterns[pattern]);
					}
					return content;
				}
			}
		},
		module: {
			//html and template files that need string translations
			files: [{
				cwd: 'src/module',
				expand: true,
				src: [
					'nnlm_rosters.info',
					'nnlm_rosters.install',
					'nnlm_rosters.module',
					'nnlm_rosters.tokens.inc',
					'includes/**'
				],
				dest: 'build/module'
			},
			{
				cwd: 'src/templates',
				expand: true,
				src: [
					'**'
				],
				dest: 'build/module/templates'
			},
			{
				cwd: 'src/css',
				expand: true,
				src: [
					'rosters_output.screen.css'
				],
				dest: 'build/module/css/'
			}]
		},
		build: {
			files: [
				// concatenated scripts and styles
				{
					cwd: 'tmp',
					expand: true,
					src: ['rosters-*.css', 'rosters-*.js', 'rosters-*.map'],
					dest: 'build'
				},
				//prefs file
				{
					cwd: 'src/includes',
					expand: true,
					dot: true,
					src: ['**'],
					dest: 'build/includes'
				},
				// servlet files
				{
					cwd: 'src/classfiles/server',
					expand: true,
					src: [
						'utils.php',
						'GetDocline.servlet.php',
						'GetLibids.servlet.php',
						'GetRegions.servlet.php',
						'mailer.php',
						'transactionlog.servlet.php',
						'accounts/**',
						'people/**',
						'positions/**',
						'roles/**',
						'lists/**'
					],
					dest: 'build/servlet'
				},
				//output demo files
				{
					cwd: 'src/output_demo',
					expand: true,
					src: [
						'index.html',
						'docline.html',
						'list.html',
						'query.html',
						'includes/**'
					],
					dest: 'build/output_demo'
				},
				//screenshots
				{
					cwd: 'src/screenshots',
					expand: true,
					src: ['**'],
					dest: 'build/screenshots'
				},
				//translated files
				{
					cwd: 'tmp',
					expand: true,
					src: ['index.html', 'views/help/**'],
					dest: 'build'
				}
				//note, other files copied in concat task.
			]
		},
		localhost: {
			files: [
				// includes files within path and its sub-directories
      			{expand: true, src: ['build/**'], dest: '~/Sites/rosters/'}
			]
		}
	};
};