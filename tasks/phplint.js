module.exports = {
  'default': [
  	'src/classfiles/server/**/*.php',
  	'src/module/*.module',
  	'src/module/*.install',
  	'src/module/**/*.inc',
  	'src/module/**/*.php'
  ]
};