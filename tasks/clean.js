module.exports = {
	'default': ['build', 'tmp', 'src/css'],
	'tmp': ['tmp'],
	'build': ['build'], 
	'min': ['build/rosters-*.min.*'],
	'nomin': ['build/rosters-*', '!build/rosters-*.min.*']
};