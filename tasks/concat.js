module.exports = {
	js: {
		options: {
			separator: ";\n",
			sourceMap: true, //@see http://www.html5rocks.com/en/tutorials/developertools/sourcemaps/
			stripBanners: true //removes comments
		},
		files: {
			'tmp/<%= javascript_filename %>': [
			'deps/ext/ext-base.js',
			'deps/ext/ext-all.js',
			'deps/ext/CheckColumn.js',
			'deps/ext/TabCloseMenu.js',
			'tmp/classfiles/client/environment.js',
			'src/classfiles/cient/Records.js',
			'src/classfiles/cient/Components.js',
			'src/classfiles/cient/DataManager.js',
			'src/classfiles/cient/DisplayManager.js',
			'src/classfiles/cient/List.js',
			'src/classfiles/cient/Find.js',
			'src/classfiles/cient/Create.js',
			'src/classfiles/cient/Edit.js',
			'src/classfiles/client/init.js']
		},
		nonull: true
	},
	css: {
		options: {
			separator: "\n"
		},
		files: {
			'tmp/<%= stylesheet_filename %>': [
			'deps/ext/ext-all-norel.css',
			'deps/ext/xtheme-gray-norel.css',
			'tmp/css/rosters.css',
			'tmp/css/rosters.screen.css',
			'tmp/css/rosters.views.screen.css',
			'tmp/css/icons.css',
			'tmp/css/Find.css',
			'tmp/css/Edit.css',
			'tmp/css/List.css',
			'tmp/css/errata.css']
		},
		nonull: true
	}
};