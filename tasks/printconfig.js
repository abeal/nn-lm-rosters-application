module.exports = function(grunt) {
  grunt.registerTask('printConfig', 'Prints configuration file', function() {
    grunt.log.writeln(JSON.stringify(grunt.config()));
  });
};