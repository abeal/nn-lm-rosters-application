module.exports = {
	options: {            
		configFile: 'tasks/conf/eslint.json',
		quiet: true
	},
	target: [
		'src/classfiles/client/*.js'
	]
};