module.exports = {
	"module": {
		files: ['src/sass/**', 'src/templates/**', 'src/module/**'],
		tasks: ['sandy-dev'],
		options: {
			interrupt: false
		}
	}
};