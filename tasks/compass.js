module.exports = function(grunt) { // Task
	//@see https://github.com/gruntjs/grunt-contrib-compass
	var sharedoptions = {
		sourcemap: false,
		sassDir: 'src/sass',
		cssDir: 'src/css',
		httpPath: 'http://nnlm.gov',
		debugInfo: false,
		outputStyle: 'compact',
		noLineComments: true
	};
	var merge = function() {
		var o = {};
		for (var i = arguments.length - 1; i >= 0; i--) {
			var s = arguments[i];
			for (var k in s) o[k] = s[k];
		}
		return o;
	};
	return {
		'default': { // Another target
			options: sharedoptions
		}
	};
};