module.exports = function(grunt) {
	var result = {
		options: {
			mangle: false
		},
		build: {
			files: {}
		}
	};
	if(!grunt.option('minify')){
		return result;
	}
	result.build.files['build/'+grunt.option('javascript filename')] = ['tmp/'+grunt.option('javascript filename')];
	return result;
};