module.exports = function(grunt) {
	grunt.registerTask("build", [
		'lint', //syntax check rosters files
		'clean:default',
		'compass'
	]);
	grunt.registerTask("build-module", [
		'lint',//syntax check rosters files
		'clean:default',
		'compass',
		'copy:module'
	]);
};