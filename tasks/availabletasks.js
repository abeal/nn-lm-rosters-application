module.exports = {
	main: {
		options: {
			tasks: [
				'build', 
				'clean', 
				'default', 
				'help',
				'jshint',
				'phplint',
				'prompt',
				'sandy-dev'], 
			filter: 'include',
			descriptions: {
				'build': 'Builds a deployable version of the rosters app to the build/ directory in the root of this project',
				'default': 'Alias for help',
				'help': "Displays available tasks",
				'jshint': 'Performs javascript syntax checking on all js files in this project',
				'phplint': 'Performs php syntax checking on all php files in this project',
				'prompt': 'CLI wizard that guides the developer through a version bump, with optional commit and tagging',
				'sandy-dev': 'Deploys this code to the development instance of Drupal'
			},
			groups: {
				'Code validation Grunt tasks': ['jshint', 'phplint'],
				'Cleanup Grunt tasks': ['clean'],
				'Primary Grunt tasks': ['default', 'build', 'help', 'sandy-dev'],
				'Other Grunt tasks': ['availabletasks', 'prompt']
			}
		}
	}
};