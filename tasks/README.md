#Grunt build system

This project uses Grunt as its primary build system, which in turn relies on Node.  The primary task in this folder to be aware of is the 'default' task (the contents of 'default.js') which is invoked when 'grunt' is executed without arguments in the project root.  This is usually 'availabletask', which shows which tasks are considered primary targets, and therefore directly executable.

For a general overview of Grunt, see http://gruntjs.com/. For an overview of the advanced build technique used here, see https://www.npmjs.org/package/grunt-contrib-concat/ 

##Building: General Info 

There are a few steps that are necessary for building the project:

	1. Install ruby and ruby gems.   [More info](https://rubygems.org/pages/download).
	1. Install node and npm.  [More info]](http://nodejs.org/download/).
	1. 	Navigate to the project root at the command line. Execute `npm install`.  This will create the node_modules directory, and download all necessary node modules to build this project.
	1. Execute `grunt`, followed by the target you are interested in.  By default, grunt is configured to show all available primary targets.  `grunt build` is a target that is almost always present, and will provide a deployable version of the project.
