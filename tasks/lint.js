module.exports = function(grunt) {
	grunt.registerTask("lint", [
		'eslint', //syntax check js
		'phplint:default' //syntax check servlet files
	]);
};